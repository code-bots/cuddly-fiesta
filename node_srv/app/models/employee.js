var mongoose = require('mongoose');
module.exports = mongoose.model('employee', {
    name : String,
    phone_no : String,
    email_id : String,
    designation : String,
    manager_id : String

},'employee');