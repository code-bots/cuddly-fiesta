var employee = require('./models/employee');

module.exports = function(app) {

	// api ---------------------------------------------------------------------
	// get all todos
	app.get('/api/employees', function(req, res) {

		console.log("in method")
		console.log('DB ready state: ' + employee.db.readyState);
		// use mongoose to get all todos in the database
		employee.find(function(err, todos) {

			// if there is an error retrieving, send the error. nothing after res.send(err) will execute
			if (err)
				res.send(err)
			console.log(todos);
			res.json(todos); // return all todos in JSON format
		});
	});

	// create todo and send back all todos after creation
	app.post('/api/employees', function(req, res) {

		// create a todo, information comes from AJAX request from Angular
		console.log('body :: ' + req.body);
		employee.create({
			
			name : req.body.name,
			phone_no : req.body.phone_no,
			email_id : req.body.email_id,
			designation : req.body.designation,
			manager_id : req.body.designation

		}, function(err, todo) {
			if (err)
				res.send(err);

			// get and return all the todos after you create another
			employee.find(function(err, todos) {
				if (err)
					res.send(err)
				res.json(todos);
			});
		});

	});

	// delete a todo
	app.delete('/api/employee/:employee_id', function(req, res) {
		employee.remove({
			_id : req.params.employee_id
		}, function(err, todo) {
			if (err)
				res.send(err);

			// get and return all the todos after you create another
			employee.find(function(err, todos) {
				if (err)
					res.send(err)
				res.json(todos);
			});
		});
	});

	/* application -------------------------------------------------------------
	app.get('*', function(req, res) {
		res.sendfile('./public/index.html'); // load the single view file (angular will handle the page changes on the front-end)
    });
    */
};