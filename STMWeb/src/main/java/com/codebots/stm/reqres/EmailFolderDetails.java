package com.codebots.stm.reqres;

public class EmailFolderDetails {
	private String folderName;
	private int totalCount;
	private int unReadCount;
	public EmailFolderDetails(String folderName, int totalCount, int unReadCount) {
		super();
		this.folderName = folderName;
		this.totalCount = totalCount;
		this.unReadCount = unReadCount;
	}
	public EmailFolderDetails() {
		// TODO Auto-generated constructor stub
	}
	public String getFolderName() {
		return folderName;
	}
	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}
	public int getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}
	public int getUnReadCount() {
		return unReadCount;
	}
	public void setUnReadCount(int unReadCount) {
		this.unReadCount = unReadCount;
	}
}
