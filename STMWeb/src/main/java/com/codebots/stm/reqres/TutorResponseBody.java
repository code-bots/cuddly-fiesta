package com.codebots.stm.reqres;

import java.util.List;

import com.codebots.stm.spring.model.Tutor;
import com.codebots.stm.spring.model.TutorInstitute;

public class TutorResponseBody {

	private Tutor tutor;
	private Long star;
	private List<TutorInstitute> tutorInstitute;
	
	
	public List<TutorInstitute> getTutorInstitute() {
		return tutorInstitute;
	}
	public void setTutorInstitute(List<TutorInstitute> tutorInstitute) {
		this.tutorInstitute = tutorInstitute;
	}
	public Long getStar() {
		return star;
	}
	public void setStar(Long star) {
		this.star = star;
	}
	
	public Tutor getTutor() {
		return tutor;
	}
	public void setTutor(Tutor tutor) {
		this.tutor = tutor;
	}
	
	
}
