package com.codebots.stm.reqres;

import java.util.UUID;

import com.codebots.stm.spring.model.Assignments;

public class AssignmentRequestBody {
	private Assignments assignment;
	private UUID subjectId;

	public Assignments getAssignment() {
		return assignment;
	}
	public void setAssignment(Assignments assignment) {
		this.assignment = assignment;
	}
	public UUID getSubjectId() {
		return subjectId;
	}
	public void setSubjectId(UUID subjectId) {
		this.subjectId = subjectId;
	}

}
