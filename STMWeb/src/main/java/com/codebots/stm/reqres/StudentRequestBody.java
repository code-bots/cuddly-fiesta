package com.codebots.stm.reqres;

import com.codebots.stm.spring.model.Student;

public class StudentRequestBody {
	private Student registrationDetails;
	private String password;
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Student getRegistrationDetails() {
		return registrationDetails;
	}
	public void setRegistrationDetails(Student registrationDetails) {
		this.registrationDetails = registrationDetails;
	}
}
