package com.codebots.stm.reqres;

import java.util.List;

import com.codebots.stm.spring.model.Tutor;
import com.codebots.stm.spring.model.TutorInstitute;

public class TutorRequestBody {

	private Tutor registrationDetails;
	
	private String password;
	
	private List<TutorInstitute> instituteDetails;
	
	public List<TutorInstitute> getInstituteDetails() {
		return instituteDetails;
	}
	public void setInstituteDetails(List<TutorInstitute> instituteDetails) {
		this.instituteDetails = instituteDetails;
	}
	public Tutor getRegistrationDetails() {
		return registrationDetails;
	}
	public void setRegistrationDetails(Tutor registrationDetails) {
		this.registrationDetails = registrationDetails;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}	
}
