package com.codebots.stm.reqres;

import java.util.UUID;

public class PaymentInfo {
	UUID studentId;
	UUID assignmentId;
	public UUID getStudentId() {
		return studentId;
	}
	public void setStudentId(UUID studentId) {
		this.studentId = studentId;
	}
	public UUID getAssignmentId() {
		return assignmentId;
	}
	public void setAssignmentId(UUID assignmentId) {
		this.assignmentId = assignmentId;
	}
	@Override
	public String toString() {
		return "StudentId is - " + studentId.toString() + ": Assignment Id is - " + assignmentId.toString();
	}
}
