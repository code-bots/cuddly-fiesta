package com.codebots.stm.reqres;

public class ResponseObject {

	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
