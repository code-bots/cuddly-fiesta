package com.codebots.stm.communication;

import javax.management.MBeanServerConnection;
import javax.management.MBeanServerInvocationHandler;
import javax.management.ObjectName;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

import org.apache.james.domainlist.api.DomainListManagementMBean;
import org.apache.james.user.api.UsersRepositoryManagementMBean;

import com.codebots.stm.spring.exception.STMAPIException;

public class EmailUserGeneration {
	
	public String generateEmailUser(String domainName, String password) throws STMAPIException
	{
		String stmEmailId = System.currentTimeMillis() + "@" + domainName;
		try{
	        String serverUrl = "service:jmx:rmi:///jndi/rmi://localhost:9999/jmxrmi";
	        String beanNameUser = "org.apache.james:type=component,name=usersrepository";
	        String beanNameDomain = "org.apache.james:type=component,name=domainlist";

	        MBeanServerConnection server = JMXConnectorFactory.connect(new JMXServiceURL(serverUrl)).getMBeanServerConnection();

	        UsersRepositoryManagementMBean userBean =  MBeanServerInvocationHandler.newProxyInstance(server, new ObjectName(beanNameUser), UsersRepositoryManagementMBean.class, false);
	        DomainListManagementMBean domainBean =  MBeanServerInvocationHandler.newProxyInstance(server, new ObjectName(beanNameDomain), DomainListManagementMBean.class, false);

	        if(domainBean.containsDomain(stmEmailId.split("@")[1])
	                && !userBean.verifyExists(stmEmailId)){
	            userBean.addUser(stmEmailId,password);
	        }else{
	            throw new STMAPIException("0052", new String[] {domainName}, null);
	        }

	    }catch (Exception e){
	        throw new STMAPIException("0051", null, e);
	    }
		return stmEmailId;
	}
}
