package com.codebots.stm.communication;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.codebots.stm.spring.model.User;

public class PrintResponceBodyStudent {
	private String first_name;
	private String last_name;
	private String about_me;
	private String blood_group;
	private Date dob;
	private String mobile_number;
	private String food_preference;
	private String email;
	private String address;
	private String locality;
	private String city;
	private String state;
	private String pincode;
	private String emergency_contact;	
	private String father_name;
	private String father_qualification;
	private Date father_dob;
	private String father_profession;
	private String mother_name;
	private String mother_qualification;
	private Date mother_dob;
	private String mother_profession;
	public String getFirst_name() {
		return first_name;
	}
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}
	public String getLast_name() {
		return last_name;
	}
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}
	public String getAbout_me() {
		return about_me;
	}
	public void setAbout_me(String about_me) {
		this.about_me = about_me;
	}
	public String getBlood_group() {
		return blood_group;
	}
	public void setBlood_group(String blood_group) {
		this.blood_group = blood_group;
	}
	public Date getDob() {
		return dob;
	}
	public void setDob(Date dob) {
		this.dob = dob;
	}
	public String getMobile_number() {
		return mobile_number;
	}
	public void setMobile_number(String mobile_number) {
		this.mobile_number = mobile_number;
	}
	public String getFood_preference() {
		return food_preference;
	}
	public void setFood_preference(String food_preference) {
		this.food_preference = food_preference;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getLocality() {
		return locality;
	}
	public void setLocality(String locality) {
		this.locality = locality;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	public String getEmergency_contact() {
		return emergency_contact;
	}
	public void setEmergency_contact(String emergency_contact) {
		this.emergency_contact = emergency_contact;
	}
	public String getFather_name() {
		return father_name;
	}
	public void setFather_name(String father_name) {
		this.father_name = father_name;
	}
	public String getFather_qualification() {
		return father_qualification;
	}
	public void setFather_qualification(String father_qualification) {
		this.father_qualification = father_qualification;
	}
	public Date getFather_dob() {
		return father_dob;
	}
	public void setFather_dob(Date father_dob) {
		this.father_dob = father_dob;
	}
	public String getFather_profession() {
		return father_profession;
	}
	public void setFather_profession(String father_profession) {
		this.father_profession = father_profession;
	}
	public String getMother_name() {
		return mother_name;
	}
	public void setMother_name(String mother_name) {
		this.mother_name = mother_name;
	}
	public String getMother_qualification() {
		return mother_qualification;
	}
	public void setMother_qualification(String mother_qualification) {
		this.mother_qualification = mother_qualification;
	}
	public Date getMother_dob() {
		return mother_dob;
	}
	public void setMother_dob(Date mother_dob) {
		this.mother_dob = mother_dob;
	}
	public String getMother_profession() {
		return mother_profession;
	}
	public void setMother_profession(String mother_profession) {
		this.mother_profession = mother_profession;
	}
	
}
