package com.codebots.stm.communication;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.springframework.context.annotation.PropertySource;
@PropertySource("classpath:application.properties")
public class SendSMS {


	public static String sendSms(String msg,String mobileNumber) {
		try {
			// Construct data
			//x9CLs/agMO8-OHtabZDWtcCQRcvwpfG3cBSKPMdiSh -- venu
			//M7gcQWc7lG4-K0nByfB3xqI4gNRDk0bhCJmqmAJ4J1
			String apiKey = "apikey=" + "gCV/A0BUXBA-r80uz2LZCfJESNl60aTeTyACxDoaFn"; 
			String message = "&message=" + msg;
			String sender = "&sender=" + "TXTLCL";
			String numbers = "&numbers=" + mobileNumber;
			
			// Send data
			HttpURLConnection conn = (HttpURLConnection) new URL("https://api.textlocal.in/send/?").openConnection();
			String data = apiKey + numbers + message + sender;
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
			conn.getOutputStream().write(data.getBytes("UTF-8"));
			final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			final StringBuffer stringBuffer = new StringBuffer();
			String line;
			while ((line = rd.readLine()) != null) {
				stringBuffer.append(line);
			}
			rd.close();
			
			return stringBuffer.toString();
		} catch (Exception e) {
			System.out.println("Error SMS "+e);
			return "Error "+e;
		}
	}
	
}