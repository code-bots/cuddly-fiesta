package com.codebots.stm.spring.dao;


import java.util.List;
import java.util.UUID;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Repository;

import com.codebots.stm.spring.model.Course;


@Repository
@PropertySource("classpath:application.properties")
@Transactional
public class CourseDaoImple implements CourseDao{

	@Autowired
	private SessionFactory sessionfactory;
	
	
	@Override
	public Course save(Course course) {
		sessionfactory.getCurrentSession().save(course);
		return course;
	}
	
	
	@Override
	public List<Course> list() {
		Session session = sessionfactory.getCurrentSession();
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Course> cq = cb.createQuery(Course.class);
		Root<Course> root = cq.from(Course.class);
		cq.select(root);
		Query<Course> query = session.createQuery(cq);
		return query.getResultList();
		
	}

	
	@Override
	public void update(Course course, UUID course_id) {
		Session session = sessionfactory.getCurrentSession();
		Course cor = session.byId(Course.class).load(course_id);
		
		cor.setName(course.getName());
		cor.setDescription(course.getDescription());
		
		session.saveOrUpdate(cor);
		
	}

	 
}
