package com.codebots.stm.spring.dao;


import java.util.List;
import java.util.UUID;

import com.codebots.stm.spring.model.Course;

public interface CourseDao {

	Course save(Course course);
	List<Course> list();
	void update(Course course,UUID course_id);
}
