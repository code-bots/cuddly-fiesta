package com.codebots.stm.spring.dao;

import java.util.List;
import java.util.UUID;

import com.codebots.stm.spring.model.Comments;

public interface CommentsDao {

	Comments saveComments(Comments comment,UUID blog_id);
	Comments getCommentById(UUID commentId);
	List<Comments>	listByBlogs(UUID blog_id);
	List<Comments> listByParant(UUID parentcomment_id);
	Comments updateComment(Comments comment,UUID commentId);
	void deleteComment(UUID comment_id);
}
