package com.codebots.stm.spring.dao;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import com.codebots.stm.reqres.Pagination;
import com.codebots.stm.reqres.ResponseObject;
import com.codebots.stm.spring.model.AssignmentStatus;
import com.codebots.stm.spring.model.Assignments;

public interface AssignmentDao {

	Assignments save(Assignments assignment,UUID tutor_id, UUID subjectId,UUID institute_id);
	Assignments getById(UUID id);
	List<Assignments> list();
	List<Assignments> getComplated();
	List<Assignments> getFilterByDate(Date from_date,Date to_date);
	List<Assignments> filterByType(Integer type);
	List<Assignments> filterByStatus(Integer status);
	List<Assignments> getBytutorStatus(UUID tutor_id,Integer status,Date fromDate,Date toDate);
	Assignments refreshObject(Assignments assignment);
	List<AssignmentStatus> getStatusMetaData(int type);
	List<Assignments> getByTutor(UUID tutor_id,Date fromdate,Date toDate);
	List<Assignments>  getByStudentStatus(UUID student_id,Integer status,Date fromDate,Date toDate);
	
	
	Long countOfEnrolledStudent(UUID assignmentId);
	Long countOfStatusComplated(UUID assignmentId);
	
	void updateStatus();
	
	Assignments changeStatusOfAssignment(UUID assignmentid);
	List<Assignments> dueDatedAssignments() throws ParseException;
	
	Pagination getStuByTutAss(UUID tutor_id,int pageNumber,int pageSize,String type,String feild);
	
	Pagination getStuByTutAssSingleObject(UUID tutor_id,int pageNumber,int pageSize,String type,String feild);
}
