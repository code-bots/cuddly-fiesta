package com.codebots.stm.spring.dao;

import java.util.List;
import java.util.UUID;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Repository;

import com.codebots.stm.spring.model.QuestionSet;
import com.codebots.stm.spring.model.Subject;
import com.codebots.stm.spring.model.Tutor;
import com.codebots.stm.spring.model.TutorInstitute;


@Repository
@PropertySource("classpath:application.properties")
@PropertySource("classpath:mailbox.properties")
@Transactional
public class QuestionSetDaoImpl implements QuestionSetDao{

	@Autowired
	private SessionFactory sessionfactory;

	
	@Override
	public QuestionSet save(QuestionSet questionSet, UUID tutor_id,UUID subject_id) {
		Session session = sessionfactory.getCurrentSession();
		Tutor tutor = session.get(Tutor.class, tutor_id);
		Subject subject = session.get(Subject.class,subject_id);
		questionSet.setTutor_id(tutor);
		questionSet.setSubject_id(subject);
		sessionfactory.getCurrentSession().save(questionSet);
		return questionSet;
	}

	
	@Override
	public List<QuestionSet> list() {
		Session session = sessionfactory.getCurrentSession();
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<QuestionSet> cq = cb.createQuery(QuestionSet.class);
		Root<QuestionSet> root = cq.from(QuestionSet.class);
		cq.select(root);
		Query<QuestionSet> query = session.createQuery(cq);
		return query.getResultList(); 	
	}


	
	@Override
	public QuestionSet updateQset(QuestionSet queSet, UUID quSetId) {
		Session session = sessionfactory.getCurrentSession();
		QuestionSet qs = session.byId(QuestionSet.class).load(quSetId);
		
		
		qs.setNo_of_questions(queSet.getNo_of_questions());
		qs.setName(queSet.getName());
		qs.setIs_payment(queSet.getIs_payment());
		qs.setCreation_date(queSet.getCreation_date());
		
		session.saveOrUpdate(qs);
		return qs;
	}

	@PreAuthorize("hasAuthority('TUTOR') or hasAuthority('STUDENT')")
	@Override
	public List<QuestionSet> getById(UUID tutor_id) {
		Session session = sessionfactory.getCurrentSession();
		String hql = "from " + QuestionSet.class.getName() + " where tutor_id.tutor_id = :tutor";
		Query query = session.createQuery(hql);
		query.setParameter("tutor",tutor_id);
		List<QuestionSet> list = query.list();
		return list;
		
	}

	
	@Override
	public List<QuestionSet> getBySubject(UUID subject_id) {
		Session session = sessionfactory.getCurrentSession();
		String hql = "from " + QuestionSet.class.getName() + " where subject_id.id = :subject";
		Query query = session.createQuery(hql);
		query.setParameter("subject",subject_id);
		List<QuestionSet> list = query.list();
		return list;
	}

	
	@Override
	public QuestionSet getByQsetId(UUID questionSetId) {
		Session session = sessionfactory.getCurrentSession();
		QuestionSet qset = session.get(QuestionSet.class, questionSetId);
		return qset;
	}

	
	@Override
	public void deleteQuestionSet(UUID questionSetId) {
		Session session = sessionfactory.getCurrentSession();
		QuestionSet queSet = session.byId(QuestionSet.class).load(questionSetId);
		session.delete(queSet);
	}

	
	
}
