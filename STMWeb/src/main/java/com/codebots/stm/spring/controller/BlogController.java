package com.codebots.stm.spring.controller;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codebots.stm.reqres.ResponseObject;
import com.codebots.stm.spring.dao.BlogDao;
import com.codebots.stm.spring.dao.CommentsDao;
import com.codebots.stm.spring.exception.STMAPIException;
import com.codebots.stm.spring.model.Blog;
import com.codebots.stm.spring.model.Comments;

@RestController
@RequestMapping("/rest/api")
public class BlogController {

	@Autowired
	private BlogDao blogDao;
	
	@Autowired
	private CommentsDao comDao;
	
	@PreAuthorize("hasAuthority('TUTOR')")
	@PostMapping("/blog/{tutor_id}")
	public ResponseEntity<Blog> saveBlog(@RequestBody Blog blog,@PathVariable("tutor_id") UUID tutor_id) throws STMAPIException
	{
		try
		{
		Blog blg = blogDao.save(blog, tutor_id);
		return ResponseEntity.ok().body(blg);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0061", new String[] {e.getMessage()}, e);
		}
	}

	
	@PostMapping("comment/{blog_id}")
	public ResponseEntity<Comments> saveComment(@RequestBody Comments com,@PathVariable("blog_id") UUID blog_id) throws STMAPIException
	{
		try
		{
			Comments comment = comDao.saveComments(com, blog_id);
			return ResponseEntity.ok().body(comment);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0067", new String[] {e.getMessage()}, e);
		}
	}
	
	
	@GetMapping("tutor/blog/{tutor_id}")
	public ResponseEntity<List<Blog>> getByTutorBlog(@PathVariable("tutor_id") UUID tutor_id) throws STMAPIException
	{
		try
		{
			List<Blog> blogs = blogDao.getBlogByTutor(tutor_id);
			return ResponseEntity.ok().body(blogs);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0062", new String[] {e.getMessage()}, e);
		}
	}
	
	@GetMapping("blog/{id}")
	public ResponseEntity<Blog> getByBlogId(@PathVariable("id") UUID blog_id) throws STMAPIException
	{
		try
		{
			Blog blog = blogDao.getBogById(blog_id);
			return ResponseEntity.ok().body(blog);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0073", new String[] {e.getMessage()}, e);
		}
	} 
	
	@GetMapping("/bloglist")
	public ResponseEntity<List<Blog>> getAll() throws STMAPIException
	{
		try
		{
			List<Blog> blogs = blogDao.getAllBlogs();
			return ResponseEntity.ok().body(blogs);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0063", new String[] {e.getMessage()}, e);
		}
	}
	
	@GetMapping("/comments/{blog_id}")
	public ResponseEntity<List<Comments>> getAllCommentsByBlog(@PathVariable("blog_id") UUID blog_id) throws STMAPIException
	{
		try
		{
			List<Comments> coms = comDao.listByBlogs(blog_id);
			return ResponseEntity.ok().body(coms);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0068", new String[] {e.getMessage()}, e);
		}
	}
	
	@GetMapping("/comment/{commentId}")
	public ResponseEntity<Comments> getCommentById(@PathVariable("commentId") UUID commentId) throws STMAPIException
	{
		try
		{
			Comments com = comDao.getCommentById(commentId);
			return ResponseEntity.ok().body(com);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0068", new String[] {e.getMessage()}, e);
		}
	}
	
	
	
	@GetMapping("/commentchain/{parentCommentId}")
	public ResponseEntity<List<Comments>> getAllCommentsByParent(@PathVariable("parentCommentId") UUID parentCommentId) throws STMAPIException
	{
		try
		{
			List<Comments> coms = comDao.listByParant(parentCommentId);
			return ResponseEntity.ok().body(coms);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0069", new String[] {e.getMessage()}, e);
		}
	}
	
	
	@PutMapping("comment/{commentId}")
	public ResponseEntity<Comments> editComment(@RequestBody Comments comment,@PathVariable("commentId") UUID commentId) throws STMAPIException
	{
		try
		{
			Comments com = comDao.updateComment(comment, commentId);
			return ResponseEntity.ok().body(com);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0070", new String[] {e.getMessage()}, e);
		}
	}
	
	@PutMapping("blog/{blogId}")
	public ResponseEntity<Blog> editBlog(@RequestBody Blog blog,@PathVariable("blogId") UUID blogId) throws STMAPIException
	{
		try
		{
			Blog bl = blogDao.updateBlog(blog, blogId);
			return ResponseEntity.ok().body(bl);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0072", new String[] {e.getMessage()}, e);
		}
	}
	
	
	@DeleteMapping("comment/{commentId}")
	public ResponseEntity<ResponseObject> deleteComment(@PathVariable("commentId") UUID commentId) throws STMAPIException
	{
		try
		{
			comDao.deleteComment(commentId);
			ResponseObject obj = new ResponseObject();
			obj.setMessage("Comment deleted successfully");
			return ResponseEntity.ok().body(obj);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0071", new String[] {e.getMessage()}, e);
		}
	}
	
	@DeleteMapping("/blog/{blog_id}")
	public ResponseEntity<ResponseObject> deleteBlog(@PathVariable("blog_id") UUID blog_id) throws STMAPIException
	{
		try
		{
			blogDao.deleteBlog(blog_id);
			ResponseObject obj = new ResponseObject();
			obj.setMessage("Blog deleted successfully");
			return ResponseEntity.ok().body(obj);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0064", new String[] {e.getMessage()}, e);
		}
	}
	
	
	
	
}
