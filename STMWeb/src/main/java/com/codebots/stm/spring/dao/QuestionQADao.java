package com.codebots.stm.spring.dao;

import java.util.List;
import java.util.UUID;

import com.codebots.stm.spring.controller.StudentQueAnsController;
import com.codebots.stm.spring.model.StudentQuestionAnswer;

public interface QuestionQADao {

	StudentQuestionAnswer save(UUID queId,UUID stu_id,StudentQuestionAnswer stuqa);
	List<StudentQuestionAnswer> list(UUID student_id);
	List<StudentQuestionAnswer> listOfSelectedAnswer(UUID studentId,UUID questionSetId);
	void changeStatus(UUID questionSetId,UUID studentId);
}
