package com.codebots.stm.spring.controller;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codebots.stm.spring.dao.ResultDao;
import com.codebots.stm.spring.exception.STMAPIException;
import com.codebots.stm.spring.model.Results;
import com.codebots.stm.spring.model.Student;

@RestController
@RequestMapping("/rest/api")
public class ResultController {

	@Autowired
	private ResultDao rsDao;
	
	@PostMapping("/result/{student_id}/{queset_id}")
	public ResponseEntity<?> saveResult(@RequestBody Results res,@PathVariable("student_id")  UUID student_id,@PathVariable("queset_id") UUID queset_id) throws STMAPIException
	{    
		try
		{
			Results result = rsDao.save(res, student_id, queset_id);			
			return ResponseEntity.ok().body(result);
		}catch(Exception e)
		{
			throw new STMAPIException("0032", new String[] {e.getMessage()}, e);
		}
	}
	
	@GetMapping("/result/list")
	public ResponseEntity<List<Results>> listResult() throws STMAPIException
	{   try
		{
			List<Results> list = rsDao.list();
		    return ResponseEntity.ok().body(list);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0033", new String[] {e.getMessage()}, e);
		}
	}
	
	@GetMapping("/result/tutor/{tutorId}")
	public ResponseEntity<List<Results>> listByTutor(@PathVariable("tutorId") UUID tutorId) throws STMAPIException
	{
		try
		{
			List<Results> list = rsDao.listyTutor(tutorId);
			return ResponseEntity.ok().body(list);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0087", new String[] {e.getMessage()}, e);
		}
		
	}
	
	@GetMapping("/result/{student_id}")
	public ResponseEntity<List<Results>> listResultStudent(@PathVariable UUID student_id) throws STMAPIException
	{   try
		{
			List<Results> list = rsDao.listByStudent(student_id);
		    return ResponseEntity.ok().body(list);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0033", new String[] {e.getMessage()}, e);
		}
	}
	
}
