package com.codebots.stm.spring.crudrepo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.codebots.stm.spring.model.Book;

@Repository
public interface BookRepository extends CrudRepository<Book, Long>{

}
