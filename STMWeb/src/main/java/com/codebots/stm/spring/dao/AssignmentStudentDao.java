package com.codebots.stm.spring.dao;

import java.util.List;
import java.util.UUID;

import com.codebots.stm.spring.model.AssignmentStudent;

public interface AssignmentStudentDao {

	AssignmentStudent save(AssignmentStudent assignmentStu,UUID student_id,UUID assignmet_id);
	List<AssignmentStudent> list();
	List<AssignmentStudent> listbyStatus(String status);
	List<AssignmentStudent> listByStudent(UUID student_id);
	AssignmentStudent changeSatatus(UUID studentAssignmentId,Integer status);
	
	
	List<AssignmentStudent> getAllStudentWhoEnrollsByTutor(UUID tutorID);
}
