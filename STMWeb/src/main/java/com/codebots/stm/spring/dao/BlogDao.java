package com.codebots.stm.spring.dao;

import java.util.List;
import java.util.UUID;

import com.codebots.stm.spring.model.Blog;

public interface BlogDao {

	Blog save(Blog blog,UUID tutor_id);
	List<Blog> getBlogByTutor(UUID tutor_id);
	List<Blog> getAllBlogs();
	Blog getBogById(UUID id);
	Blog updateBlog(Blog blog,UUID blogId);
	void deleteBlog(UUID blog_id);
}
