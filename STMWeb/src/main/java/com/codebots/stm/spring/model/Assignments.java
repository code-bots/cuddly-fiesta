package com.codebots.stm.spring.model;

import java.util.Date;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity(name="assignments")
public class Assignments {

	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(name="UUID",strategy="org.hibernate.id.UUIDGenerator")
	private UUID id;
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long assNumber; 
	public Long getAssNumber() {
		return assNumber;
	}
	public void setAssNumber(Long assNumber) {
		this.assNumber = assNumber;
	}
	private String title;
	private String description;
	private Integer type;
	@OneToOne(targetEntity=Tutor.class,cascade=CascadeType.ALL)
	@JoinColumn(name="tutor_id",referencedColumnName="tutor_id")
	private Tutor tutor_id;
	@Temporal(TemporalType.DATE)
	private Date due_date;
	@OneToOne(targetEntity=Course.class,cascade=CascadeType.ALL)
	@JoinColumn(name="course_id",referencedColumnName="id")
	private Course coure_id;
	@OneToOne(targetEntity=Subject.class,cascade=CascadeType.ALL)
	@JoinColumn(name="subject_id",referencedColumnName="id")
	private Subject subject_id;
	@Temporal(TemporalType.DATE)
	private Date creation_date;
	private Integer payment;
	private Integer status;
	@OneToOne(targetEntity=TutorInstitute.class,cascade=CascadeType.ALL)
	@JoinColumn(name="tutorInstituteId",referencedColumnName="id")
	private TutorInstitute tutorInstituteId;
	public TutorInstitute getTutorInstituteId() {
		return tutorInstituteId;
	}
	public void setTutorInstituteId(TutorInstitute tutorInstituteId) {
		this.tutorInstituteId = tutorInstituteId;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public UUID getId() {
		return id;
	}
	public void setId(UUID id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public Tutor getTutor_id() {
		return tutor_id;
	}
	public void setTutor_id(Tutor tutor_id) {
		this.tutor_id = tutor_id;
	}
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd", timezone="UTC")
	public Date getDue_date() {
		return due_date;
	}
	public void setDue_date(Date due_date) {
		this.due_date = due_date;
	}
	public Course getCoure_id() {
		return coure_id;
	}
	public void setCoure_id(Course coure_id) {
		this.coure_id = coure_id;
	}
	public Subject getSubject_id() {
		return subject_id;
	}
	public void setSubject_id(Subject subject_id) {
		this.subject_id = subject_id;
	}
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd", timezone="UTC")
	public Date getCreation_date() {
		return creation_date;
	}
	public void setCreation_date(Date creation_date) {
		this.creation_date = creation_date;
	}
	public Integer getPayment() {
		return payment;
	}
	public void setPayment(Integer payment) {
		this.payment = payment;
	}
	
	
	
}
