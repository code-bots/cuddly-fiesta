package com.codebots.stm.spring.dao;

import java.util.List;
import java.util.UUID;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Repository;

import com.codebots.stm.spring.model.QuestionSet;
import com.codebots.stm.spring.model.Results;
import com.codebots.stm.spring.model.Student;
import com.codebots.stm.spring.model.StudentQuestionAnswer;
import com.codebots.stm.spring.model.Subject;

@Repository
@PropertySource("classpath:application.properties")
@Transactional
public class ResultDaoImple implements ResultDao {

	@Autowired
	private SessionFactory sessionfactory;
	
	
	@Override
	public Results save(Results res, UUID stu_id,UUID queset) {
		Session session = sessionfactory.getCurrentSession();
		Student stu = session.byId(Student.class).load(stu_id);
		QuestionSet ques = session.byId(QuestionSet.class).load(queset);
		Long noOfQue = ques.getNo_of_questions();
		res.setStudent_id(stu);
		res.setQuestion_set_id(ques);
				
		String hql = "select count(*) from "+StudentQuestionAnswer.class.getName() + "  where student_choice = correct_ans and student_id.student_id = :student";
		Query query = session.createQuery(hql);
		query.setParameter("student",stu_id);
		Long count = (Long) query.uniqueResult();
		Long percentage = (count*100L)/noOfQue;
		res.setResult(percentage.toString()+"%");
		sessionfactory.getCurrentSession().save(res);
		return res;
	}

	
	@Override
	public List<Results> list() {
		Session session = sessionfactory.getCurrentSession();
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Results> cq = cb.createQuery(Results.class);
		Root<Results> root = cq.from(Results.class);
		cq.select(root);
		Query<Results> query = session.createQuery(cq);
		return query.getResultList(); 	
	}

	
	@Override
	public List<Results> listByStudent(UUID student_id) {
		Session session = sessionfactory.getCurrentSession();
		String hql = "from  " +  Results.class.getName() + " where student_id.student_id = :student";
		Query query = session.createQuery(hql);
		query.setParameter("student", student_id);
		List<Results> list = query.list();
		return list;
	}


	
	@Override
	public List<Results> listyTutor(UUID tutorId) {
		Session session = sessionfactory.getCurrentSession();
		String hql = "from " + Results.class.getName() + " where question_set_id.tutor_id.tutor_id = :tutorId";
		Query query = session.createQuery(hql);
		query.setParameter("tutorId", tutorId);
		List<Results> list = query.list();
		return list;
	}

	
		
}
