package com.codebots.stm.spring.controller;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codebots.stm.reqres.AssignmentRequestBody;
import com.codebots.stm.reqres.Pagination;
import com.codebots.stm.reqres.ResponseObject;
import com.codebots.stm.spring.dao.AssignmentDao;
import com.codebots.stm.spring.dao.AssignmentStudentDao;
import com.codebots.stm.spring.exception.STMAPIException;
import com.codebots.stm.spring.model.AssignmentStatus;
import com.codebots.stm.spring.model.AssignmentStudent;
import com.codebots.stm.spring.model.Assignments;

@RestController
@RequestMapping("/rest/api")
public class AssignmentController {

	@Autowired
	private AssignmentDao assignmentsDao;
	
	@Autowired
	private AssignmentStudentDao assstuDao;
	
	
	@PostMapping("assignment/{tutor_id}/{institute_id}")
	public ResponseEntity<?> saveAss(@RequestBody AssignmentRequestBody assignmentReq,@PathVariable("tutor_id") UUID tutor_id,@PathVariable("institute_id") UUID institute_id) throws STMAPIException
	{
		try
		{
			Assignments ass = assignmentsDao.save(assignmentReq.getAssignment(), tutor_id, assignmentReq.getSubjectId(),institute_id);
			Assignments refreshedObject = assignmentsDao.refreshObject(ass);
			return ResponseEntity.ok().body(refreshedObject);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0016", new String[] {e.getMessage()}, e);
		}
	}
	
	
	
	@PutMapping("update/status/{studentAssignmentId}/{status}")
	public ResponseEntity<AssignmentStudent> updateStatus(@PathVariable("studentAssignmentId")UUID studentAssignmentId,
											@PathVariable("status")Integer status) throws STMAPIException	
	{
		try
		{
		AssignmentStudent assStud = assstuDao.changeSatatus(studentAssignmentId, status);
		return ResponseEntity.ok().body(assStud);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0081", new String[] {e.getMessage()}, e);
		}
		
	}
	
	
	@PutMapping("update/assignment/status/{assignmentId}")
	public ResponseEntity<Assignments> updateStatusOfAssignments(@PathVariable("assignmentId") UUID assignmentId) throws STMAPIException
	{
		try
		{
			Assignments ass = assignmentsDao.changeStatusOfAssignment(assignmentId);
			return ResponseEntity.ok().body(ass);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0084", new String[] {e.getMessage()}, e);
		}
	}
	
	@GetMapping("duedated/assignments")
	public ResponseEntity<List<Assignments>> listOfDueDatedAssignments() throws ParseException, STMAPIException
	{
		try
		{
			List<Assignments> list = assignmentsDao.dueDatedAssignments();
			return ResponseEntity.ok().body(list);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0085"
					+ "", new String[] {e.getMessage()}, e);
		}
		
	}
	
	@GetMapping("/assignments")
	public ResponseEntity<List<Assignments>> listAllAss() throws STMAPIException
	{
		try
		{
			List<Assignments> list = assignmentsDao.list();
			return ResponseEntity.ok().body(list);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0017", new String[] {e.getMessage()}, e);
		}
	}
	

	@GetMapping("/all/enrolled")
	public ResponseEntity<List<AssignmentStudent>> all() throws STMAPIException
	{
		try
		{
			List<AssignmentStudent> list = assstuDao.list();
			return ResponseEntity.ok().body(list);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0017", new String[] {e.getMessage()}, e);
		}
	}
	

	@GetMapping("/assignmentbyid/{id}")
	public ResponseEntity<Assignments> getById(@PathVariable("id")UUID id) throws STMAPIException
	{
		try
		{
			Assignments ass = assignmentsDao.getById(id);
			return ResponseEntity.ok().body(ass);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0075", new String[] {e.getMessage()}, e);
		}
	}
	
	
	@GetMapping("/assignmentsbytype/{type}")
	public ResponseEntity<List<Assignments>> getByType(@PathVariable("type") Integer type) throws STMAPIException
	{
		try
		{
			List<Assignments> list = assignmentsDao.filterByType(type);
			return ResponseEntity.ok().body(list);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0018", new String[] {e.getMessage()}, e);
		}
	}
	
	
	@GetMapping("/assignmentsbyStatus/{status}")
	public ResponseEntity<List<Assignments>> getByStatus(@PathVariable("status") Integer status) throws STMAPIException
	{
		try
		{
			List<Assignments> list = assignmentsDao.filterByStatus(status);
			return ResponseEntity.ok().body(list);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0018", new String[] {e.getMessage()}, e);
		}
	}
	
	
	@GetMapping("/assignment")
	public ResponseEntity<List<Assignments>> getbyDate(@RequestParam("from_date") @DateTimeFormat(pattern="yyyy-MM-dd") Date from_date,@RequestParam("to_date") @DateTimeFormat(pattern="yyyy-MM-dd") Date to_date) throws STMAPIException
	{
	
		try
		{
			List<Assignments> list = assignmentsDao.getFilterByDate(from_date,to_date);
			return ResponseEntity.ok().body(list);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0019", new String[] {e.getMessage()}, e);
		}
	}
	
	@GetMapping("assignments/{student_id}")
	public ResponseEntity<List<AssignmentStudent>> getByStudent(@PathVariable("student_id") UUID student_id) throws STMAPIException
	{
		try
		{
			List<AssignmentStudent> list = assstuDao.listByStudent(student_id);
			return ResponseEntity.ok().body(list);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0036", new String[] {e.getMessage()}, e);
		}
	}

	@GetMapping("assignment/statusmeta/{type}")
	public ResponseEntity<List<AssignmentStatus>> getAssignmentStatuses(@PathVariable("type") int type) throws STMAPIException
	{
		if(type!=1 && type!=2)
		{
			throw new STMAPIException("0072", new String[] {String.valueOf(type)}, null);
		}
		try
		{
			List<AssignmentStatus> lst = assignmentsDao.getStatusMetaData(type);
			return ResponseEntity.ok().body(lst);
		}catch(Exception e)
		{
			throw new STMAPIException("0071", new String[] {e.getMessage()}, e);
		}
	}
	
	@GetMapping("/studentlist/{tutorId}")
	public ResponseEntity<Pagination> getAllStudentByTutorSingleObject(@PathVariable("tutorId") UUID tutorId,
																		  @RequestParam(value = "page") Integer pageNumber,
																		  @RequestParam(value = "size") Integer pageSize,
																		  @RequestParam(value = "type",required = false) String type,
																		  @RequestParam(value = "feild",required = false) String feild) throws STMAPIException
	{
		try
		{
		Pagination page = assignmentsDao.getStuByTutAssSingleObject(tutorId, pageNumber, pageSize, type, feild);
		return  ResponseEntity.ok().body(page);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0075", new String[] {e.getMessage()}, e);
		}
	}
	
	@PutMapping("check/complated")
	public ResponseEntity<ResponseObject> countOfAssignment()
	{
		assignmentsDao.updateStatus();
		ResponseObject obj = new ResponseObject();
		obj.setMessage("status updated");
		return ResponseEntity.ok().body(obj);
	}
	
}
