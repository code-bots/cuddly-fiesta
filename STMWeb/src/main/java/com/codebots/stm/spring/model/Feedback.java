package com.codebots.stm.spring.model;

import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;
@Entity(name = "feedback")
public class Feedback 
{
	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(name="UUID",strategy="org.hibernate.id.UUIDGenerator")
	private UUID id;
	@OneToOne(targetEntity=AssignmentStudent.class,cascade=CascadeType.ALL)
	@JoinColumn(name="studentAssignmentId",referencedColumnName="id")
	private AssignmentStudent studentAssignmentId;
	
	public AssignmentStudent getStudentAssignmentId() {
		return studentAssignmentId;
	}
	public void setStudentAssignmentId(AssignmentStudent studentAssignmentId) {
		this.studentAssignmentId = studentAssignmentId;
	}
	private String content;
	private Long star;
	public UUID getId() {
		return id;
	}
	public void setId(UUID id) {
		this.id = id;
	}
	
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Long getStar() {
		return star;
	}
	public void setStar(Long start) {
		this.star = start;
	}
	
	
	
	
}
