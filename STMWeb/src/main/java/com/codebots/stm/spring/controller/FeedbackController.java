package com.codebots.stm.spring.controller;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codebots.stm.spring.dao.FeedbackDao;
import com.codebots.stm.spring.exception.STMAPIException;
import com.codebots.stm.spring.model.Feedback;

@RestController
@RequestMapping("/rest/api")
public class FeedbackController {

	@Autowired
	private FeedbackDao feedbackDao;
	
	@PostMapping("/feedback/{studentAssignmentId}")
	public ResponseEntity<Feedback> saveFeedbac(@RequestBody Feedback feedback,@PathVariable("studentAssignmentId") UUID studentAssignmentId) throws STMAPIException
	{
		try
		{

			Feedback obj = feedbackDao.saveFeedback(feedback, studentAssignmentId);
			return ResponseEntity.ok().body(obj);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0012", new String[] {e.getMessage()}, e);
		}
	}
	
	
	
	@GetMapping("/get/feedback")
	public ResponseEntity<List<Feedback>> getAll() throws STMAPIException
	{
		try
		{

			List<Feedback>  list = feedbackDao.getAll(); 
			return ResponseEntity.ok().body(list);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0012", new String[] {e.getMessage()}, e);
		}
	}
}
