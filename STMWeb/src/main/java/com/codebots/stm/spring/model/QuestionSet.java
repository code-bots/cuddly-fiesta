package com.codebots.stm.spring.model;

import java.util.Date;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity(name="question_set")
public class QuestionSet {
	
	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(name="UUID",strategy="org.hibernate.id.UUIDGenerator")
	private UUID id;
	private String name;
	
	@OneToOne(targetEntity=Subject.class,cascade=CascadeType.ALL)
	@JoinColumn(name="subject_id",referencedColumnName="id")
	private Subject subject_id;
	
	
	private Date creation_date;
	private Long no_of_questions;
	@OneToOne(targetEntity=Tutor.class,cascade=CascadeType.ALL)
	@JoinColumn(name="tutor_id",referencedColumnName="tutor_id")
	private Tutor tutor_id;
	private Boolean is_payment;
	

	public Subject getSubject_id() {
		return subject_id;
	}
	public void setSubject_id(Subject subject_id) {
		this.subject_id = subject_id;
	}
	
	public UUID getId() {
		return id;
	}
	public void setId(UUID id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd", timezone="UTC")
	public Date getCreation_date() {
		return creation_date;
	}
	public void setCreation_date(Date creation_date) {
		this.creation_date = creation_date;
	}
	public Long getNo_of_questions() {
		return no_of_questions;
	}
	public void setNo_of_questions(Long no_of_questions) {
		this.no_of_questions = no_of_questions;
	}
	public Tutor getTutor_id() {
		return tutor_id;
	}
	public void setTutor_id(Tutor tutor_id) {
		this.tutor_id = tutor_id;
	}
	public Boolean getIs_payment() {
		return is_payment;
	}
	public void setIs_payment(Boolean is_payment) {
		this.is_payment = is_payment;
	}
	
	
	
	
	
	
}
