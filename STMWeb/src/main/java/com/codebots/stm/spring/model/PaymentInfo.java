package com.codebots.stm.spring.model;

import java.util.Date;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity(name="payment_info")
public class PaymentInfo {

	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(name="UUID",strategy="org.hibernate.id.UUIDGenerator")
	private UUID id;
	
	@OneToOne(targetEntity=Assignments.class,cascade=CascadeType.ALL)
	@JoinColumn(name="assignment_id",referencedColumnName="id", updatable=false)
	private Assignments assignment_id;

	@OneToOne(targetEntity=Student.class,cascade=CascadeType.ALL)
	@JoinColumn(name="student_id",referencedColumnName="student_id", updatable=false)
	private Student student_id;
	
	private Date txn_time;
	private Long amount;
	private String txn_id;
	private String app_status;
	private String respcode;
	private String gateway_status;
	private String respmsg;
	public UUID getId() {
		return id;
	}
	public void setId(UUID id) {
		this.id = id;
	}
	public Student getStudent_id() {
		return student_id;
	}
	public void setStudent_id(Student student_id) {
		this.student_id = student_id;
	}
	public Assignments getAssignment_id() {
		return assignment_id;
	}
	public void setAssignment_id(Assignments assignment_id) {
		this.assignment_id = assignment_id;
	}
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss", timezone="IST")
	public Date getTxn_time() {
		return txn_time;
	}
	public void setTxn_time(Date txn_time) {
		this.txn_time = txn_time;
	}
	public Long getAmount() {
		return amount;
	}
	public void setAmount(Long amount) {
		this.amount = amount;
	}
	public String getTxn_id() {
		return txn_id;
	}
	public void setTxn_id(String txn_id) {
		this.txn_id = txn_id;
	}
	public String getApp_status() {
		return app_status;
	}
	public void setApp_status(String app_status) {
		this.app_status = app_status;
	}
	public String getRespcode() {
		return respcode;
	}
	public void setRespcode(String respcode) {
		this.respcode = respcode;
	}
	public String getGateway_status() {
		return gateway_status;
	}
	public void setGateway_status(String gateway_status) {
		this.gateway_status = gateway_status;
	}
	public String getRespmsg() {
		return respmsg;
	}
	public void setRespmsg(String respmsg) {
		this.respmsg = respmsg;
	}
	
	
}
