package com.codebots.stm.spring.dao;

import java.util.List;
import java.util.UUID;

import com.codebots.stm.reqres.ResponseObject;
import com.codebots.stm.spring.model.Questions;

public interface QuestionDao {

	Questions save(Questions que,UUID quesetid);
	List<Questions> list();
	Questions getById(UUID queid);
	Questions updateQue(Questions que,UUID queId);
	List<Questions> getByQSet(UUID queSetId);
	void deleteQuestion(UUID questionID);
	ResponseObject getAns(UUID queId);
	
}
