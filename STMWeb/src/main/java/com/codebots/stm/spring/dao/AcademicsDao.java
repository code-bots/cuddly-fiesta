package com.codebots.stm.spring.dao;

import java.util.List;
import java.util.UUID;

import com.codebots.stm.spring.model.Academics;

public interface AcademicsDao {

	Academics save(Academics academic,UUID student_id);
	List<Academics> list();
	List<Academics> getbyId(UUID student_id);
	Academics getSpecific(UUID student_id,UUID academics_id);
	int delete(UUID student_id,UUID academics_id);
	Academics update(UUID student_id,UUID academic_id,Academics academics);
}
