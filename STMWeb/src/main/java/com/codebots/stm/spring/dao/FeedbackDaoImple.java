package com.codebots.stm.spring.dao;

import java.util.List;
import java.util.UUID;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Repository;

import com.codebots.stm.spring.model.AssignmentStudent;
import com.codebots.stm.spring.model.Feedback;

@Repository
@PropertySource("classpath:application.properties")
@Transactional
public class FeedbackDaoImple implements FeedbackDao
{
	@Autowired
	private SessionFactory sessionFactory;

	@PreAuthorize("hasAuthority('STUDENT')")
	@Override
	public Feedback saveFeedback(Feedback feedback,UUID assignmentStudentId) {
		Session session = sessionFactory.getCurrentSession();
		
		AssignmentStudent stuAss = (AssignmentStudent) session.createQuery("from " + AssignmentStudent.class.getName() + " where id = :stuAssId")
									.setParameter("stuAssId", assignmentStudentId).uniqueResult();
		feedback.setStudentAssignmentId(stuAss);
		sessionFactory.getCurrentSession().save(feedback);
		return feedback;
	}

	@PreAuthorize("hasAuthority('TUTOR')")
	@Override
	public List<Feedback> getAll() {
		String hql = "from " + Feedback.class.getName();
		List<Feedback> list = sessionFactory.getCurrentSession().createQuery(hql).list();
		return list;
	}
	
	

}
