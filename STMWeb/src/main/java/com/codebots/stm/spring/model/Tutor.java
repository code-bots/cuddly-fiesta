package com.codebots.stm.spring.model;


import java.util.Date;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonFormat;



@Entity(name="tutor")
public class Tutor {
	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(name="UUID",strategy="org.hibernate.id.UUIDGenerator")
	private UUID tutor_id;
	private String first_name;
	private String last_name;
	private String about_me;
	
	private String email;
	private String mobile_number;
	@Temporal(TemporalType.DATE)
	private Date dob;
	
	private String address;
	private String locality;
	private String city;
	private String state;
	private String pincode;
	
	private String qualification;
	@OneToOne(targetEntity=User.class,cascade=CascadeType.ALL)
	@JoinColumn(name="user_id",referencedColumnName="id")
	private User user_id;
	private String profile_pic;
	
	private String stmEmailId;
	private Integer approvalFlag;
	
	public Integer getApprovalFlag() {
		return approvalFlag;
	}
	public void setApprovalFlag(Integer approvalFlag) {
		this.approvalFlag = approvalFlag;
	}
	public UUID getTutor_id() {
		return tutor_id;
	}
	public void setTutor_id(UUID tutor_id) {
		this.tutor_id = tutor_id;
	}
	public String getFirst_name() {
		return first_name;
	}
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}
	public String getLast_name() {
		return last_name;
	}
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}
	public String getAboutme() {
		return about_me;
	}
	public void setAboutme(String about_me) {
		this.about_me = about_me;
	}
	public String getEmail() {
		return email;
	}
	public String getAbout_me() {
		return about_me;
	}
	public void setAbout_me(String about_me) {
		this.about_me = about_me;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMobile_number() {
		return mobile_number;
	}
	public void setMobile_number(String mobile_number) {
		this.mobile_number = mobile_number;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd", timezone="UTC")
	public Date getDob() {
		return dob;
	}
	public void setDob(Date dob) {
		this.dob = dob;
	}
	public String getQualification() {
		return qualification;
	}
	public void setQualification(String qualification) {
		this.qualification = qualification;
	}
	public User getUser_id() {
		return user_id;
	}
	public void setUser_id(User user_id) {
		this.user_id = user_id;
	}
	public String getProfile_pic() {
		return profile_pic;
	}
	public void setProfile_pic(String profile_pic) {
		this.profile_pic = profile_pic;
	}
	public String getStmEmailId() {
		return stmEmailId;
	}
	public void setStmEmailId(String stmEmailId) {
		this.stmEmailId = stmEmailId;
	}
	
	public String getLocality() {
		return locality;
	}
	public void setLocality(String locality) {
		this.locality = locality;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	
	
}
