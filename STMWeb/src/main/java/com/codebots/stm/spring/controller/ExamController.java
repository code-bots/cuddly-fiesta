package com.codebots.stm.spring.controller;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codebots.stm.reqres.ResponseObject;
import com.codebots.stm.spring.dao.QuestionDao;
import com.codebots.stm.spring.dao.QuestionQADao;
import com.codebots.stm.spring.dao.QuestionSetDao;
import com.codebots.stm.spring.exception.STMAPIException;
import com.codebots.stm.spring.model.QuestionSet;
import com.codebots.stm.spring.model.Questions;
import com.codebots.stm.spring.model.StudentQuestionAnswer;

@RestController
@RequestMapping("/rest/api")
public class ExamController {

	@Autowired
	private QuestionSetDao queSetDao;
	
	@Autowired 
	private QuestionDao queDao;
	
	@Autowired
	private QuestionQADao qaDao;
	
	@PostMapping("exam/questionSet/{tutor_id}/{subject_id}")
	public ResponseEntity<QuestionSet> saveQueSet(@RequestBody QuestionSet queSet,@PathVariable("tutor_id") UUID tutor_id,@PathVariable("subject_id") UUID subject_id) throws STMAPIException
	{
		try
		{
			QuestionSet qs = queSetDao.save(queSet, tutor_id,subject_id);
			return ResponseEntity.ok().body(qs);
		}catch(Exception e)
		{
			throw new STMAPIException("0026", new String[] {e.getMessage()}, e);
		}
	}
	
	@PostMapping("/question/{questionSetId}")
	public ResponseEntity<Questions> saveQueSet(@RequestBody Questions que,@PathVariable("questionSetId") UUID qsId) throws STMAPIException
	{
		try
		{
			Questions q = queDao.save(que, qsId);
			
			return ResponseEntity.ok().body(q);
		}catch(Exception e)
		{
			throw new STMAPIException("0029", new String[] {e.getMessage()}, e);
		}
	}
	
	
	
	@GetMapping("/questionsetlist/{tutor_id}")
	public ResponseEntity<List<QuestionSet>> getQuestionSet(@PathVariable("tutor_id") UUID tutor_id)throws STMAPIException
	{
		try
		{
		List<QuestionSet> list = queSetDao.getById(tutor_id);
		return ResponseEntity.ok().body(list);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0027", new String[] {e.getMessage()}, e);
		}
	}
	

	@GetMapping("/questionsetlistbysubject/{subject_id}") 
	public ResponseEntity<List<QuestionSet>> getQuestionSetBySubject(@PathVariable("subject_id") UUID subject_id) throws STMAPIException
	{
		try
		{
		List<QuestionSet> list = queSetDao.getBySubject(subject_id);
		return ResponseEntity.ok().body(list);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0028", new String[] {e.getMessage()}, e);
		}
	}
	

	@GetMapping("/questionlist/{questionSetId}")
	public ResponseEntity<List<Questions>> getQuestions(@PathVariable("questionSetId") UUID questionSetId)throws STMAPIException
	{
		try
		{
		List<Questions> list = queDao.getByQSet(questionSetId);
		return ResponseEntity.ok().body(list);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0031", new String[] {e.getMessage()}, e);
		}
	}
	
	@GetMapping("/ans/{queId}")
	public ResponseEntity<ResponseObject> getAnsOfQue(@PathVariable("queId") UUID queId) throws STMAPIException
	{
		try
		{
			ResponseObject obj = queDao.getAns(queId);
			return ResponseEntity.ok().body(obj);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0080", new String[] {e.getMessage()}, e);
		}
		
	}
	
	
	
	@PutMapping("/questionSet/{questionSetId}")
	public ResponseEntity<QuestionSet> updateQueSet(@RequestBody QuestionSet queSet,@PathVariable("questionSetId") UUID qsId)throws STMAPIException
	{
		try
		{
		queSet.setId(qsId);
		QuestionSet qs = queSetDao.updateQset(queSet, qsId);
		
		return ResponseEntity.ok().body(qs);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0076", new String[] {e.getMessage()}, e);
		}
	}
	
	
	
	@PutMapping("/question/{questionId}")
	public ResponseEntity<Questions> updateQue(@RequestBody Questions que,@PathVariable("questionId") UUID qId)throws STMAPIException
	{
		try
		{
		que.setId(qId);
		Questions q = queDao.updateQue(que, qId);	
		return ResponseEntity.ok().body(q);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0077", new String[] {e.getMessage()}, e);
		}
	}
	
	@DeleteMapping("question/{questionId}")
	public ResponseEntity<ResponseObject> deleteQuestion(@PathVariable("questionId") UUID questionId) throws STMAPIException
	{
		try
		{
			queDao.deleteQuestion(questionId);
			ResponseObject obj = new ResponseObject();
			obj.setMessage("Deleted SuccessFully");
			return ResponseEntity.ok().body(obj);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0079", new String[] {e.getMessage()}, e);
		}
		
	}
	
	@DeleteMapping("questionset/{questionsetId}")
	public ResponseEntity<?> deleteQuestionSet(@PathVariable("questionsetId") UUID questionsetId) throws STMAPIException
	{
		try
		{
			queSetDao.deleteQuestionSet(questionsetId);
			ResponseObject obj = new ResponseObject();
			obj.setMessage("Deleted SuccessFully");
			return ResponseEntity.ok().body(obj);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0078", new String[] {e.getMessage()}, e);
		}
		
	}
	
	
	
	
}
