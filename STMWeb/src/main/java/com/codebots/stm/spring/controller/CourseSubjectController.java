package com.codebots.stm.spring.controller;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codebots.stm.reqres.ResponseObject;
import com.codebots.stm.spring.dao.CourseDao;
import com.codebots.stm.spring.dao.SubjectDao;
import com.codebots.stm.spring.exception.STMAPIException;
import com.codebots.stm.spring.model.Course;
import com.codebots.stm.spring.model.Subject;

@RestController
@RequestMapping("/rest/api")
public class CourseSubjectController {

	@Autowired
	private CourseDao coursedao;
	
	
	@Autowired 
	private SubjectDao subjectdao;
	
	@PostMapping("/course")
	public ResponseEntity<Course> saveCourse(@RequestBody Course course) throws STMAPIException
	{
		try
		{

			Course obj = coursedao.save(course);
			return ResponseEntity.ok().body(obj);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0012", new String[] {e.getMessage()}, e);
		}
	}
	
	@GetMapping("/course/list")
	public ResponseEntity<List<Course>> listCourse() throws STMAPIException
	{
		try
		{
		List<Course> list = coursedao.list();
		return ResponseEntity.ok().body(list);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0013", new String[] {e.getMessage()}, e);
		}
		
	}
	
	
	@PutMapping("/course/{course_id}")
	public ResponseEntity<ResponseObject> updateCourse(@RequestBody Course course,@PathVariable("course_id") UUID course_id) throws STMAPIException
	{
		
		try
		{
			course.setId(course_id);
			coursedao.update(course, course_id);
			ResponseObject obj = new ResponseObject();
			obj.setMessage("Course updated successfully");
			return ResponseEntity.ok().body(obj);
			
		}
		catch(Exception e)
		{
			throw new STMAPIException("0024", new String[] {e.getMessage()}, e);		
		}
		
	}
	
	@PostMapping("/course/{course_id}/subject")
	public ResponseEntity<?> saveSubject(@RequestBody Subject subject, @PathVariable("course_id") UUID course_id) throws STMAPIException
	{
		try
		{
			subjectdao.save(subject, course_id);
			return ResponseEntity.ok().body(subject);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0014", new String[] {e.getMessage()}, e);
		}
	}
	
	@GetMapping("/course/{course_id}/subject")
	public ResponseEntity<List<Subject>> listSubject(@PathVariable("course_id") UUID course_id) throws STMAPIException
	{
		try
		{	
			List<Subject> list = subjectdao.list(course_id);
			return ResponseEntity.ok().body(list);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0015", new String[] {e.getMessage()}, e);
		}
	}
	
	@PutMapping("/course/{course_id}/{subject_id}")
	public ResponseEntity<?> updatesUBJECT(@RequestBody Subject subject,@PathVariable("course_id") UUID course_id,@PathVariable("subject_id")UUID subject_id) throws STMAPIException
	{	
		try
		{
			subject.setId(subject_id);
			subject.setCourse_id(subject.getCourse_id());
			subjectdao.update(subject, course_id, subject_id);
			ResponseObject obj = new ResponseObject();
			obj.setMessage("subject updated successfully");
			return ResponseEntity.ok().body(obj);
			
		}
		catch(Exception e)
		{
			throw new STMAPIException("0025", new String[] {e.getMessage()}, e);		
		}
		
	}
	
	@GetMapping("/subjects")
	public ResponseEntity<List<Subject>> getAllSubject() throws STMAPIException
	{	
		try
		{
			List<Subject> list = subjectdao.listAll();
			return ResponseEntity.ok().body(list);
			
		}
		catch(Exception e)
		{
			throw new STMAPIException("0066", new String[] {e.getMessage()}, e);		
		}
		
	}
	
	

}
