package com.codebots.stm.spring.model;

import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;

@Entity(name="academics")
public class Academics {
	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(name="UUID",strategy="org.hibernate.id.UUIDGenerator")
	private UUID academic_id;
	@OneToOne(targetEntity=Student.class,cascade=CascadeType.ALL)
	@JoinColumn(name="student_id",referencedColumnName="student_id")
	private Student student_id;
	private String school;
	private String degree;
	private String grade;
	private int from_year;
	private int to_year;
	private String description;
	public UUID getAcademic_id() {
		return academic_id;
	}
	public void setAcademic_id(UUID academic_id) {
		this.academic_id = academic_id;
	}
	public Student getStudent_id() {
		return student_id;
	}
	public void setStudent_id(Student student_id) {
		this.student_id = student_id;
	}
	public String getSchool() {
		return school;
	}
	public void setSchool(String school) {
		this.school = school;
	}
	public String getDegree() {
		return degree;
	}
	public void setDegree(String degree) {
		this.degree = degree;
	}
	public String getGrade() {
		return grade;
	}
	public void setGrade(String grade) {
		this.grade = grade;
	}
	public int getFrom_year() {
		return from_year;
	}
	public void setFrom_year(int from_year) {
		this.from_year = from_year;
	}
	public int getTo_year() {
		return to_year;
	}
	public void setTo_year(int to_year) {
		this.to_year = to_year;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
