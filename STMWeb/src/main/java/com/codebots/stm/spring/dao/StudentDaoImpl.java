package com.codebots.stm.spring.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.stereotype.Repository;

import com.codebots.stm.common.Utility;
import com.codebots.stm.communication.EmailNotifier;
import com.codebots.stm.communication.EmailUserGeneration;
import com.codebots.stm.communication.PrintResponceBodyStudent;
import com.codebots.stm.communication.SendSMS;
import com.codebots.stm.reqres.Pagination;
import com.codebots.stm.reqres.PasswordChangeRequestBody;
import com.codebots.stm.reqres.StudentRequestBody;
import com.codebots.stm.spring.controller.RegistrationController;
import com.codebots.stm.spring.exception.STMAPIException;
import com.codebots.stm.spring.model.Role;
import com.codebots.stm.spring.model.Student;
import com.codebots.stm.spring.model.Tutor;
import com.codebots.stm.spring.model.User;

@Repository
@PropertySource("classpath:application.properties")
@PropertySource("classpath:mailbox.properties")
@Transactional
public class StudentDaoImpl implements StudentDao {

	@Autowired
	private SessionFactory sessionfactory;
	
	@Autowired
	private Environment env;
	
	
	@Override
	public Student save(StudentRequestBody student) throws STMAPIException {
		
		Session session = sessionfactory.getCurrentSession();
		String email = student.getRegistrationDetails().getEmail();
		String mob = student.getRegistrationDetails().getMobile_number();
		
		String hqlMobile = "select mobile_number from "+ Tutor.class.getName() +" where mobile_number = :mobile";
		Query queryMobile = session.createQuery(hqlMobile);
		queryMobile.setParameter("mobile", mob);
		List<String> listOfMobile = queryMobile.list();
		
		String hqlMobile1 = "select mobile_number from " +Student.class.getName()  +" where mobile_number = :mobile";
		Query queryMobile1 = session.createQuery(hqlMobile1);
		queryMobile1.setParameter("mobile", mob);
		List<String> listOfMobile1 = queryMobile1.list();
		
		String hqlUserName = "select username from " + User.class.getName() + " where username = :uname";
		Query queryUsrname = session.createQuery(hqlUserName);
		queryUsrname.setParameter("uname", student.getRegistrationDetails().getEmail());
		List<String> listUser = queryUsrname.list();
		
		
/*		String hql = "select email from student where email = :email";
		Query query = session.createQuery(hql);
		query.setParameter("email",email);
		List<String> listOfEmail = query.list();
*/		
		
		if (listUser.size() > 0) {
			throw new STMAPIException("0010", new String[] { email }, null);
		}
		else if(listOfMobile.size() > 0)
		{
			throw new STMAPIException("0086", new String[] { mob }, null);
		}
		else if(listOfMobile1.size() > 0)
		{
			throw new STMAPIException("0086", new String[] { mob }, null);
		}
		else
		{
			EmailUserGeneration userGeneration = new EmailUserGeneration();
		//	String emailId = userGeneration.generateEmailUser(env.getProperty("domain"), env.getProperty("userpassword"));
			String emailId = System.currentTimeMillis() + "@stm.com";
			// user object
			User user = new User();
			user.setUsername(student.getRegistrationDetails().getEmail());
			ShaPasswordEncoder encoder = new ShaPasswordEncoder(env.getProperty("security.encoding-strength", Integer.class));
			String encodedPassword = encoder.encodePassword(student.getPassword(), null);
			user.setPassword(encodedPassword);
			user.setEnabled(false);
			user.setEmail_OTP(Utility.getRandomOTP());
			user.setSms_OTP(Utility.getRandomOTP());
			//assigning student 
			List<Role> role = new ArrayList<>();
			Role studentRole = sessionfactory.getCurrentSession().get(Role.class, 2);
			role.add(studentRole);
			user.setRoles(role);
			user.setIsPresent(false);
			//save operations
			sessionfactory.getCurrentSession().save(user);
			Student studentDao = student.getRegistrationDetails();
			studentDao.setUser_id(user);
			studentDao.setStmEmailId(emailId);
			sessionfactory.getCurrentSession().save(studentDao);
			
			//otp and email send
			
			return student.getRegistrationDetails();
		}

	}

	
	@Override
	public void resetPassword(PasswordChangeRequestBody passchange,UUID student_id) throws STMAPIException {
		
		Session session = sessionfactory.getCurrentSession();
		Student student = session.get(Student.class, student_id);
		String original_pass = student.getUser_id().getPassword();
		//origional password
		String old_pass_by_user = passchange.getOld_pass();
		
		ShaPasswordEncoder encoder = new ShaPasswordEncoder(env.getProperty("security.encoding-strength", Integer.class));
		//entered old_pass (encoded)
		String old_Password_by_user = encoder.encodePassword(old_pass_by_user, null);
		
		
		if(old_Password_by_user.equals(original_pass))
		{
			ShaPasswordEncoder encoder2 = new ShaPasswordEncoder(env.getProperty("security.encoding-strength", Integer.class));
			String new_encodedPassword = encoder2.encodePassword(passchange.getNew_pass(), null);
			Query query = session.createQuery("update app_user set password = :newpass where id = :user_id");
			query.setParameter("newpass",  new_encodedPassword);
			query.setParameter("user_id",  student.getUser_id().getId());
			query.executeUpdate();
		}
		else
		{
			throw new STMAPIException("0010", new String[] {}, null);
		}
	}

	@PreAuthorize("hasAuthority('TUTOR') or hasAuthority('STUDENT')")
	@Override
	public Student get(UUID student_id) {
		return sessionfactory.getCurrentSession().get(Student.class, student_id);
	}

	@PreAuthorize("hasAuthority('TUTOR') or hasAuthority('STUDENT')")
	@Override
	public Student get(String userName)
	{
		return (Student) sessionfactory.getCurrentSession().createQuery(" from " + Student.class.getName() + " where email='" + userName + "'").list().get(0);
	}
	
	
	@Override
	public Pagination list(int pageNumber,int pageSize,String type,String feild) {
		
		Session session = sessionfactory.getCurrentSession();
		
		Query countQuery = session.createQuery("select count(*) from " + Student.class.getName());
		long result = (Long) countQuery.list().get(0);
		
		if(type == null && feild == null)
		{
			String hql = "from " + Student.class.getName();
			Query query = session.createQuery(hql);
			
			if(pageNumber == 1)
			{		
				query.setFirstResult(pageNumber-1);
				query.setMaxResults(pageSize);
			}
			else
			{
				int startRow = (pageNumber - 1) * pageSize;
				query.setFirstResult(startRow);
				query.setMaxResults(pageSize);
			}
			List<Object> list = query.list();		
			
			Pagination page = new Pagination();
			page.setListOfObjects(list);
			page.setCurrentPage(pageNumber);
			page.setPageSize(pageSize);
			page.setTotalRecord(result);
			return page;
		}
		else
		{
			String hql = "from " + Student.class.getName() + " order by " + feild + " " + type;
			Query query = session.createQuery(hql);
			
			if(pageNumber == 1)
			{		
				query.setFirstResult(pageNumber-1);
				query.setMaxResults(pageSize);
			}
			else
			{
				int startRow = (pageNumber - 1) * pageSize;
				query.setFirstResult(startRow);
				query.setMaxResults(pageSize);
			}
			List<Object> list = query.list();		
			
			Pagination page = new Pagination();
			page.setListOfObjects(list);
			page.setCurrentPage(pageNumber);
			page.setPageSize(pageSize);
			page.setTotalRecord(result);
			return page;
		}

	}

	@PreAuthorize("hasAuthority('STUDENT')")
	@Override
	public Student update(UUID student_id,Student student)
	{
		

		
		Session session = sessionfactory.getCurrentSession();
		Student stud = session.byId(Student.class).load(student_id);
		
		
		if(!stud.getMobile_number().equals(student.getMobile_number()) ||  !stud.getEmail().equals(student.getEmail()))
		{
			String emailOTP = Utility.getRandomOTP();
			String smsOTP = Utility.getRandomOTP();
			
			Integer uId = stud.getUser_id().getId();
			String email = student.getEmail();
			String hql = "UPDATE " + User.class.getName() + " us SET us.isEnabled = 0,us.email_OTP = " 
						+ emailOTP + ",us.sms_OTP = " + smsOTP + ",us.username = '" + email 
								+ "' where us.id = " + uId;
			
			Query query = session.createQuery(hql);
			query.executeUpdate();
			try
			{
				String emailContent = RegistrationController.EMAIL_CONTENT;
				String emailbody = emailContent.replace("@FirstName@", student.getFirst_name())
						.replace("@LastName@", student.getLast_name())
						.replace("@EMAIL@",email)
						.replace("@OTP@", emailOTP);

				EmailNotifier.sendEmailNotification(email, "Please verify your email", emailbody);
				
				String smsContent = RegistrationController.SMS_CONTENT;
				String smsBody = smsContent.replace("@FIRSTNAME@", student.getFirst_name())
						.replace("@OTP@",smsOTP);
				String mobile = student.getMobile_number();
				SendSMS.sendSms(smsBody, mobile);
				
				
			}
			catch(Exception e)
			{
				
			}
		}
		stud.setEmail(student.getEmail());
		stud.setFirst_name(student.getFirst_name());
		stud.setLast_name(student.getLast_name());
		stud.setAbout_me(student.getAbout_me());
		stud.setBlood_group(student.getBlood_group());
		stud.setDob(student.getDob());
		stud.setFood_preference(student.getFood_preference());
		
		stud.setAddress(student.getAddress());
		stud.setLocality(student.getLocality());
		stud.setCity(student.getCity());
		stud.setState(student.getState());
		stud.setPincode(student.getPincode());
		
		stud.setMobile_number(student.getMobile_number());
		stud.setEmergency_contact(student.getEmergency_contact());
		
		
		stud.setFather_name(student.getFather_name());
		stud.setFather_qualification(student.getFather_qualification());
		stud.setFather_dob(student.getDob());
		stud.setFather_profession(student.getFather_profession());
		stud.setMother_name(student.getMother_name());
		stud.setMother_qualification(student.getMother_qualification());
		stud.setMother_dob(student.getMother_dob());
		stud.setMother_profession(student.getMother_profession());
		stud.setRemind_pref(student.getRemind_pref());
		stud.setProfile_pic(student.getProfile_pic());
		
		session.saveOrUpdate(stud);
		return stud;
		
	}
	
	@PreAuthorize("hasAuthority('ADMIN')")
	@Override
	public void delete(UUID student_id) {
		Session session = sessionfactory.getCurrentSession();
		Student student = session.byId(Student.class).load(student_id);
		session.delete(student);
		
	}

	@PreAuthorize("hasAuthority('TUTOR') or hasAuthority('STUDENT')")
	@Override
	public String getUsername(UUID student_id) {
		Session session = sessionfactory.getCurrentSession();
		String hql = "select email from student where student_id = :id";
		Query query = session.createQuery(hql);
		query.setParameter("id", student_id);
		String username = (String) query.uniqueResult();
		return username;
	}


	@Override
	public List<PrintResponceBodyStudent> printStudentGrid(UUID tutorId) {
		Session session = sessionfactory.getCurrentSession();
		String hql = "select stu FROM student stu,assignment_student ass_stu,assignments ass,tutor tut WHERE ass_stu.assignment_id = ass.id AND tut.tutor_id = ass.tutor_id AND ass_stu.student_id = stu.student_id AND tut.tutor_id = :tutor and tut.approvalFlag = 1 group by stu.student_id";
		Query query = session.createQuery(hql);
		query.setParameter("tutor",tutorId);
		List<Student> students = query.list();
		List<PrintResponceBodyStudent> list = new ArrayList<>();
		
		for(Student student : students)
		{
			PrintResponceBodyStudent obj = new PrintResponceBodyStudent();
			obj.setFirst_name(student.getFirst_name());
			obj.setLast_name(student.getLast_name());
			obj.setAbout_me(student.getAbout_me());
			obj.setAddress(student.getAddress());
			obj.setBlood_group(student.getBlood_group());
			obj.setCity(student.getCity());
			obj.setMobile_number(student.getMobile_number());
			obj.setLocality(student.getLocality());
			obj.setPincode(student.getPincode());
			obj.setState(student.getState());
			obj.setFood_preference(student.getFood_preference());
			obj.setDob(student.getDob());
			obj.setEmail(student.getEmail());
			obj.setEmergency_contact(student.getEmergency_contact());
			obj.setFather_name(student.getFather_name());
			obj.setFather_profession(student.getFather_profession());
			obj.setFather_qualification(student.getFather_qualification());
			obj.setFather_dob(student.getFather_dob());
			obj.setMother_name(student.getMother_name());
			obj.setMother_profession(student.getMother_profession());
			obj.setMother_dob(student.getMother_dob());
			obj.setMother_qualification(student.getMother_qualification());

			list.add(obj);
		}

		return list;
	}
}
