package com.codebots.stm.spring.dao;

import com.codebots.stm.reqres.ContactResponseBody;

public interface EmailDao {

	ContactResponseBody getEmailContacts();

}
