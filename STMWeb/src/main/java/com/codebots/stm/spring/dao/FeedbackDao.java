package com.codebots.stm.spring.dao;

import java.util.List;
import java.util.UUID;

import com.codebots.stm.spring.model.Feedback;

public interface FeedbackDao {

	Feedback saveFeedback(Feedback feedback,UUID assignmentStudentId);
	List<Feedback> getAll();
}
