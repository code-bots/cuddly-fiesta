package com.codebots.stm.spring.dao;

import java.util.UUID;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.stereotype.Repository;

import com.codebots.stm.common.Utility;
import com.codebots.stm.communication.EmailNotifier;
import com.codebots.stm.communication.SendSMS;
import com.codebots.stm.reqres.ForgetPasswordEmailBody;
import com.codebots.stm.reqres.NewPasswordReqBody;
import com.codebots.stm.reqres.ResponseObject;
import com.codebots.stm.reqres.VerifyRequest;
import com.codebots.stm.spring.controller.RegistrationController;
import com.codebots.stm.spring.exception.STMAPIException;
import com.codebots.stm.spring.model.Student;
import com.codebots.stm.spring.model.Tutor;
import com.codebots.stm.spring.model.User;

@Repository
@Transactional
public class UserDaoImpl implements UserDao{
	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private Environment env;

	
	public static String EMAIL_CONTENT ="You have just requested to reset your password. Please Enter OTP for Password Reset,<BR><BR>"
			+ "OTP : @EMAIL@";

	
	@PreAuthorize("hasAuthority('TUTOR') or hasAuthority('STUDENT')")
	@Override
	public void resendVerificationDetails(String type, UUID userId) throws STMAPIException
	{
		String firstName = null;
		String emailId = null;
		String lastName = null;
		User user = null;
		String mobile = null;
		if(type.equals("student"))
		{
			Student student = sessionFactory.getCurrentSession().get(Student.class, userId);
			firstName = student.getFirst_name();
			emailId = student.getEmail();
			lastName = student.getLast_name();
			user = student.getUser_id();
			mobile = student.getMobile_number();
		}else if(type.equals("tutor"))
		{
			Tutor tutor = sessionFactory.getCurrentSession().get(Tutor.class, userId);
			firstName = tutor.getFirst_name();
			lastName = tutor.getLast_name();
			emailId = tutor.getEmail();
			user = tutor.getUser_id();
			mobile = tutor.getMobile_number();
		}else
		{
			throw new STMAPIException("0056", new String[]{type}, null);
		}
		String emailContent = RegistrationController.EMAIL_CONTENT;
		String emailbody = emailContent.replace("@FirstName@", firstName)
				.replace("@LastName@", lastName)
				.replace("@EMAIL@", emailId)
				.replace("@OTP@", user.getEmail_OTP());
		String smsContent = RegistrationController.SMS_CONTENT;
		String smsBody = smsContent.replace("@FIRSTNAME@", firstName)
				.replace("@OTP@", user.getSms_OTP());
		EmailNotifier.sendEmailNotification(emailId, "Please verify your email", emailbody);
		SendSMS.sendSms(smsBody, mobile);
	}

	@PreAuthorize("hasAuthority('TUTOR') or hasAuthority('STUDENT')")
	@Override
	public void setUnsetPresence(Integer id, boolean value) {
		User user = sessionFactory.getCurrentSession().get(User.class, id);
		user.setIsPresent(value);
	}

	@PreAuthorize("hasAuthority('TUTOR') or hasAuthority('STUDENT')")
	@Override
	public String verifydetailsMethod(VerifyRequest verifyDetails, Integer uid) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from " + User.class.getName() + " where id = :uid";
		Query query = session.createQuery(hql);
		query.setParameter("uid", uid);
		User u = (User) query.uniqueResult();
		String eOtp = u.getEmail_OTP();
		String sOtp = u.getSms_OTP();
		
		if(eOtp.equals(verifyDetails.getEmailOtp()) && sOtp.equals(verifyDetails.getSmsOtp()))
		{
			String hql1 = "update " + User.class.getName() + " SET isEnabled = '1' where id = :uid";
			Query query1 = session.createQuery(hql1);
			query1.setParameter("uid", uid);
			query1.executeUpdate();
			return "success";
		}
		else
		{
			return "Please try again,Entered values are incorrect";
		}
	}

	@Override
	public ResponseObject sendForgetPasswordDetails(ForgetPasswordEmailBody reqBody) throws STMAPIException {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from " + User.class.getName() + " where username = :userName";
		Query query = session.createQuery(hql);
		query.setParameter("userName",reqBody.getUserName());
		User user = (User) query.uniqueResult();
		if(user != null)
		{
			ResponseObject obj = new ResponseObject();
			obj.setMessage(Utility.getRandomOTP());
			String emailBody = EMAIL_CONTENT.replace("@EMAIL@", obj.getMessage());
			EmailNotifier.sendEmailNotification(user.getUsername(), "Forget Password OTP", emailBody);
			return obj;
		}
		else
		{
			throw new STMAPIException("0090", new String[] { reqBody.getUserName() }, null);
		}
		
		
	}

	@Override
	public void enterNewPassword(NewPasswordReqBody newPassBody) {
		Session session = sessionFactory.getCurrentSession();
		ShaPasswordEncoder encoder2 = new ShaPasswordEncoder(env.getProperty("security.encoding-strength", Integer.class));
		String new_encodedPassword = encoder2.encodePassword(newPassBody.getNewPassword(), null);
		String hql = "update " + User.class.getName() + " SET password = :password where username = :userName";
		Query query = session.createQuery(hql);
		query.setParameter("password",new_encodedPassword);
		query.setParameter("userName", newPassBody.getUserName());
		query.executeUpdate();
	}
}
