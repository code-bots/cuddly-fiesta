package com.codebots.stm.spring.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codebots.stm.reqres.ForgetPasswordEmailBody;
import com.codebots.stm.reqres.NewPasswordReqBody;
import com.codebots.stm.reqres.ResponseObject;
import com.codebots.stm.reqres.VerifyRequest;
import com.codebots.stm.spring.dao.StudentDao;
import com.codebots.stm.spring.dao.TutorDao;
import com.codebots.stm.spring.dao.UserDao;
import com.codebots.stm.spring.exception.STMAPIException;
import com.codebots.stm.spring.model.User;

@RestController
@RequestMapping("/rest/api")
public class UserController {

	@Autowired
	private StudentDao studentDao;
	
	@Autowired
	private TutorDao tutorDao;
	
	@Autowired
	private UserDao userDao;
	
	@GetMapping("/userInfo")
	public ResponseEntity<?> get() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String userName = authentication.getName();
		SimpleGrantedAuthority authority = (SimpleGrantedAuthority) new ArrayList(authentication.getAuthorities()).get(0);
		String type = authority.getAuthority();
		if(type.equals("TUTOR"))
		{
			return ResponseEntity.ok().body(tutorDao.get(userName));
		}else{
			return ResponseEntity.ok().body(studentDao.get(userName));
		}
	}
	
	@PutMapping("user/availability/{type}")
	public ResponseEntity setAvailability(@PathVariable("type") boolean value)
	{
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String userName = authentication.getName();
		SimpleGrantedAuthority authority = (SimpleGrantedAuthority) new ArrayList(authentication.getAuthorities()).get(0);
		String type = authority.getAuthority();
		Integer id;
		if(type.equals("TUTOR"))
		{
			id = tutorDao.get(userName).getUser_id().getId();
		}else{
			id = studentDao.get(userName).getUser_id().getId();
		}
		userDao.setUnsetPresence(id, value);
		return ResponseEntity.ok().build();
	}

	
	@PostMapping("/verifydetails/{userId}")
	public ResponseEntity<?> verify(@RequestBody VerifyRequest verifydetails,@PathVariable("userId") Integer uid) 
	{
		String status = userDao.verifydetailsMethod(verifydetails, uid);
	//	SendSMS.sendSms("Welcome! You’re successfully registered with AtoZ portal.");
		return ResponseEntity.ok().body(status);
	}
	
	
	
	@GetMapping("/resend/{userId}")
	public ResponseEntity<?> resend(@PathVariable("userId") Integer uid) 
	{
		//VerifyRequest vr = userDao.resendOTP(uid);
		ResponseObject obj = new ResponseObject();
		obj.setMessage("ok");
		return ResponseEntity.ok().body(obj);
	}
	
	
	
	
}