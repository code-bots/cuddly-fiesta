package com.codebots.stm.spring.dao;

import java.util.List;
import java.util.UUID;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.codebots.stm.spring.model.Assignments;
import com.codebots.stm.spring.model.PaymentInfo;
import com.codebots.stm.spring.model.Student;

@Repository
@Transactional
public class PaymentDaoImpl implements PaymentDao{

	
	@Autowired
	private SessionFactory sessionfactory;
	
	
	@Override
	public PaymentInfo save(PaymentInfo payment) {		
		Session session = sessionfactory.getCurrentSession();
		/*Assignments ass = session.get(Assignments.class,assignment_id);
		Student stu = session.get(Student.class,student_id);
		payment.setStudent_id(stu);
		payment.setAssignment_id(ass);*/
		sessionfactory.getCurrentSession().save(payment);
		return payment;
	}
	
	@Override
	public List<PaymentInfo> list() {
		Session session = sessionfactory.getCurrentSession();
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<PaymentInfo> cq = cb.createQuery(PaymentInfo.class);
		Root<PaymentInfo> root = cq.from(PaymentInfo.class);
		cq.select(root);
		Query<PaymentInfo> query = session.createQuery(cq);
		return query.getResultList(); 	
	}

	@Override
	public PaymentInfo updatePayment(PaymentInfo payInfo, UUID id) {
		Session session = sessionfactory.getCurrentSession();
		Query query = session.createQuery("from " + PaymentInfo.class.getName() + " where id = :id").setParameter("id",id);
		
		PaymentInfo pay = (PaymentInfo) query.uniqueResult();
		System.out.println("assignment_id:----------" + pay.getAssignment_id());
		System.out.println("student id:----------" + pay.getStudent_id());
		pay.setTxn_id(payInfo.getTxn_id());
		pay.setRespcode(payInfo.getRespcode());
		pay.setGateway_status(payInfo.getGateway_status());
		pay.setTxn_time(payInfo.getTxn_time());
		pay.setApp_status(payInfo.getApp_status());
		pay.setRespmsg(payInfo.getRespmsg());
		session.saveOrUpdate(pay);
		
		return pay;
	}
}
