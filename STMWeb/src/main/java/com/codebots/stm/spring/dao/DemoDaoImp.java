package com.codebots.stm.spring.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Repository;

import com.codebots.stm.spring.model.Demo;
@Repository
@PropertySource("classpath:application.properties")
@PropertySource("classpath:mailbox.properties")
@Transactional
public class DemoDaoImp implements DemoDao{

	@Autowired
	private SessionFactory seessionfactory;

	@Override
	public Demo save(Demo demo) {
		seessionfactory.getCurrentSession().save(demo);
		return demo;
	}

	@Override
	public List<Demo> list() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	
}
