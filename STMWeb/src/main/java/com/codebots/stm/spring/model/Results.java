package com.codebots.stm.spring.model;

import java.util.Date;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity(name="results")
public class Results {

	
	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(name="UUID",strategy="org.hibernate.id.UUIDGenerator")
	private UUID id;
	
	@OneToOne(targetEntity=Student.class,cascade=CascadeType.ALL)
	@JoinColumn(name="student_id",referencedColumnName="student_id")
	private Student student_id;
	
	@OneToOne(targetEntity=QuestionSet.class,cascade=CascadeType.ALL)
	@JoinColumn(name="question_set_id",referencedColumnName="id")
	private QuestionSet question_set_id;
	
	private Date result_date;
	private String result;
	public UUID getId() {
		return id;
	}
	public void setId(UUID id) {
		this.id = id;
	}
	public Student getStudent_id() {
		return student_id;
	}
	public void setStudent_id(Student student_id) {
		this.student_id = student_id;
	}
	public QuestionSet getQuestion_set_id() {
		return question_set_id;
	}
	public void setQuestion_set_id(QuestionSet question_set_id) {
		this.question_set_id = question_set_id;
	}
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd", timezone="UTC")
	public Date getResult_date() {
		return result_date;
	}
	public void setResult_date(Date result_date) {
		this.result_date = result_date;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	
	
	
}
