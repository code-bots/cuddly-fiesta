	package com.codebots.stm.spring.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.stereotype.Repository;

import com.codebots.stm.common.Utility;
import com.codebots.stm.communication.EmailNotifier;
import com.codebots.stm.communication.EmailUserGeneration;
import com.codebots.stm.communication.SendSMS;
import com.codebots.stm.reqres.Pagination;
import com.codebots.stm.reqres.PasswordChangeRequestBody;
import com.codebots.stm.reqres.PrintResponceBodyTutor;
import com.codebots.stm.reqres.TutorRequestBody;
import com.codebots.stm.reqres.TutorResponseBody;
import com.codebots.stm.spring.controller.RegistrationController;
import com.codebots.stm.spring.exception.STMAPIException;
import com.codebots.stm.spring.model.AssignmentStudent;
import com.codebots.stm.spring.model.Assignments;
import com.codebots.stm.spring.model.Feedback;
import com.codebots.stm.spring.model.Role;
import com.codebots.stm.spring.model.Student;
import com.codebots.stm.spring.model.Tutor;
import com.codebots.stm.spring.model.TutorInstitute;
import com.codebots.stm.spring.model.User;

import net.bytebuddy.implementation.bind.annotation.AllArguments.Assignment;
@Repository
@PropertySource("classpath:application.properties")
@PropertySource("classpath:mailbox.properties")
@Transactional
public class TutorDaoImpl implements TutorDao {

	@Autowired
	private SessionFactory sessionfactory;
	
	@Autowired
	private TutorInstituteDao tutInsDao;
	
	@Autowired
	private Environment env;

	
	@Override
	public Tutor save(TutorRequestBody tutor) throws STMAPIException {
	
		Session session = sessionfactory.getCurrentSession();
		String email = tutor.getRegistrationDetails().getEmail();
		String mob = tutor.getRegistrationDetails().getMobile_number();
/*		String hql = "select email from tutor where email = :email";
		Query query = session.createQuery(hql);
		query.setParameter("email",email);
		List<String> listOfEmail = query.list();
	*/	
		
		String hqlMobile = "select mobile_number from "+ Tutor.class.getName() +" where mobile_number = :mobile";
		Query queryMobile = session.createQuery(hqlMobile);
		queryMobile.setParameter("mobile", mob);
		List<String> listOfMobile = queryMobile.list();
		
		String hqlMobile1 = "select mobile_number from " +Student.class.getName()  +" where mobile_number = :mobile";
		Query queryMobile1 = session.createQuery(hqlMobile1);
		queryMobile1.setParameter("mobile", mob);
		List<String> listOfMobile1 = queryMobile1.list();
		
		String hqlUserName = "select username from " + User.class.getName() + " where username = :uname";
		Query queryUsrname = session.createQuery(hqlUserName);
		queryUsrname.setParameter("uname", tutor.getRegistrationDetails().getEmail());
		List<String> listUser = queryUsrname.list();
		
		
		
		if (listUser.size() > 0) {
			throw new STMAPIException("0010", new String[] { email }, null);
		}
		else if(listOfMobile.size() > 0)
		{
			throw new STMAPIException("0086", new String[] { mob }, null);
		}
		else if(listOfMobile1.size() > 0)
		{
			throw new STMAPIException("0086", new String[] { mob }, null);
		}
		else
		{	
			User user = new User();
			user.setUsername(tutor.getRegistrationDetails().getEmail());
			ShaPasswordEncoder encoder = new ShaPasswordEncoder(env.getProperty("security.encoding-strength", Integer.class));
			String encodedPassword = encoder.encodePassword(tutor.getPassword(), null);
			user.setPassword(encodedPassword);
		
			List<Role> role = new ArrayList<>();
			Role tutorrole = sessionfactory.getCurrentSession().get(Role.class, 1);
			role.add(tutorrole);
			user.setRoles(role);
			user.setEnabled(false);
			user.setEmail_OTP(Utility.getRandomOTP());
			user.setSms_OTP(Utility.getRandomOTP());
			user.setIsPresent(false);
			sessionfactory.getCurrentSession().save(user);
			
			//mailbox registration
			EmailUserGeneration userGeneration = new EmailUserGeneration();
			//String emailId = userGeneration.generateEmailUser(env.getProperty("domain"), env.getProperty("userpassword"));
			String emailId = System.currentTimeMillis() + "@stm.com";
			Tutor tutorDao = tutor.getRegistrationDetails();
			tutorDao.setUser_id(user);
			tutorDao.setStmEmailId(emailId);
			tutorDao.setApprovalFlag(0);
			sessionfactory.getCurrentSession().save(tutorDao);
			return tutor.getRegistrationDetails();
		}
	}

	@Override
	public Tutor get(String userName)
	{
		return (Tutor) sessionfactory.getCurrentSession().createQuery(" from " + Tutor.class.getName() + " where email='" + userName + "'").list().get(0);
	}
	

	@Override
	public Pagination list(Integer status,int pageNumber,int pageSize,String type,String feild) {
		
	
		Session session = sessionfactory.getCurrentSession();
		
		
		if(type == null && feild == null || status == null)
		{
			String hql = "from " + Tutor.class.getName();
			Query query = session.createQuery(hql);
			
			if(pageNumber == 1)
			{		
				query.setFirstResult(pageNumber-1);
				query.setMaxResults(pageSize);
			}
			else
			{
				int startRow = (pageNumber - 1) * pageSize;
				query.setFirstResult(startRow);
				query.setMaxResults(pageSize);
			}
			List<Object> list = query.list();		
			List<Object> listOfTutor = new ArrayList<Object>();
			//List<TutorInstitute> listInstitute = new ArrayList<TutorInstitute>();
			for(Object obj : list)
			{
				TutorResponseBody tut = new TutorResponseBody();
				tut.setTutor((Tutor)obj);
				
				List<TutorInstitute> listOfInstitute = tutInsDao.getByTutor(((Tutor)obj).getTutor_id());
				tut.setTutorInstitute(listOfInstitute);
				
				String hql1 = "select avg(fb.star) from " + Tutor.class.getName() + " tut,"
						  + AssignmentStudent.class.getName() + " assStu," 
						  + Assignments.class.getName() + " ass,"
						  + Feedback.class.getName() + " fb where "
						  + " fb.studentAssignmentId = assStu.id AND "
						  + " assStu.assignment_id = ass.id AND "
						  + " ass.tutor_id = tut.tutor_id AND "
						  + " tut.tutor_id = :tutorId"
						  + " group by tut.tutor_id";
				Query starQuery = session.createQuery(hql1);
				starQuery.setParameter("tutorId",((Tutor)obj).getTutor_id());
				Double star = (Double) starQuery.uniqueResult();
				if(star == null)
				{
					tut.setStar((long) 0);
				}
				else
				{
					tut.setStar(star.longValue());
				}
				listOfTutor.add(tut);
			}
			
			Pagination page = new Pagination();
			page.setListOfObjects(listOfTutor);
			page.setCurrentPage(pageNumber);
			page.setPageSize(pageSize);
			page.setTotalRecord((long)list.size());
			return page;
		}
		else
		{
			String hql = "from " + Tutor.class.getName() + " where approvalFlag = :status "+ " order by " + feild + " " + type;
			Query query = session.createQuery(hql);
			query.setParameter("status", status);
			if(pageNumber == 1)
			{		
				query.setFirstResult(pageNumber-1);
				query.setMaxResults(pageSize);
			}
			else
			{
				int startRow = (pageNumber - 1) * pageSize;
				query.setFirstResult(startRow);
				query.setMaxResults(pageSize);
			}
			List<Object> list = query.list();		
			List<Object> listOfTutor = new ArrayList<Object>();
			
			for(Object obj : list)
			{
				TutorResponseBody tut = new TutorResponseBody();
				tut.setTutor((Tutor)obj);

				List<TutorInstitute> listOfInstitute = tutInsDao.getByTutor(((Tutor)obj).getTutor_id());
				tut.setTutorInstitute(listOfInstitute);
				
				/*String hql1 = "select avg(fb.star) from " 
								+ Feedback.class.getName() + " fb," 
								+ AssignmentStudent.class.getName() + "  as," 
								+ Assignments.class.getName() + " a," 
								+ Tutor.class.getName() + " tut where "
						  		+ "fb.assignmentStudentId = as.id and as.assignment_id = a.id "
						  		+ "and a.tutor_id = tut.tutor_id and tut.tutor_id = :tutorId group by tut.tutor_id";*/
				
				String hql1 = "select avg(fb.star) from " + Tutor.class.getName() + " tut,"
							  + AssignmentStudent.class.getName() + " assStu," 
							  + Assignments.class.getName() + " ass,"
							  + Feedback.class.getName() + " fb where "
							  + " fb.studentAssignmentId = assStu.id AND "
							  + " assStu.assignment_id = ass.id AND "
							  + " ass.tutor_id = tut.tutor_id AND "
							  + " tut.tutor_id = :tutorId"
							  + " group by tut.tutor_id";
				System.out.println("----------------------------" + hql1 + "---------------------------------");
				Query starQuery = session.createQuery(hql1);
				UUID id = ((Tutor)obj).getTutor_id();
				System.out.println("----------------------------" + hql1 + "---------------------------------");
				starQuery.setParameter("tutorId", id);
				Double star = (Double) starQuery.uniqueResult();
				if(star == null)
				{
					tut.setStar((long) 0);
				}
				else
				{
					tut.setStar(star.longValue());
				}
				listOfTutor.add(tut);
			}
			Pagination page = new Pagination();
			page.setListOfObjects(listOfTutor);
			page.setCurrentPage(pageNumber);
			page.setPageSize(pageSize);
			page.setTotalRecord((long)list.size());
			return page;
		}
	}

	
	@Override
	public Tutor update(Tutor tutor, UUID tutor_id) {
		Session session = sessionfactory.getCurrentSession();
		Tutor tut = session.byId(Tutor.class).load(tutor_id);
		
		
		if(!tut.getMobile_number().equals(tutor.getMobile_number()) ||  !tut.getEmail().equals(tutor.getEmail()))
		{
			String emailOTP = Utility.getRandomOTP();
			String smsOTP = Utility.getRandomOTP();
			
			Integer uId = tut.getUser_id().getId();
			String email = tutor.getEmail();
			String hql = "UPDATE " + User.class.getName() + " us SET us.isEnabled = 0,us.email_OTP = " 
						+ emailOTP + ",us.sms_OTP = " + smsOTP + ",us.username = '" + email 
								+ "' where us.id = " + uId;
			
			Query query = session.createQuery(hql);
			query.executeUpdate();
			try
			{
				String emailContent = RegistrationController.EMAIL_CONTENT;
				String emailbody = emailContent.replace("@FirstName@", tutor.getFirst_name())
						.replace("@LastName@", tutor.getLast_name())
						.replace("@EMAIL@",email)
						.replace("@OTP@", emailOTP);

				EmailNotifier.sendEmailNotification(email, "Please verify your email", emailbody);
				
				String smsContent = RegistrationController.SMS_CONTENT;
				String smsBody = smsContent.replace("@FIRSTNAME@", tutor.getFirst_name())
						.replace("@OTP@",smsOTP);
				String mobile = tutor.getMobile_number();
				SendSMS.sendSms(smsBody, mobile);
				
				
			}
			catch(Exception e)
			{
				
			}
		}
		
		tut.setEmail(tutor.getEmail());
		tut.setFirst_name(tutor.getFirst_name());
		tut.setLast_name(tutor.getLast_name());
		tut.setAbout_me(tutor.getAbout_me());
		
		
		tut.setAddress(tutor.getAddress());
		tut.setLocality(tutor.getLocality());
		tut.setCity(tutor.getLocality());
		tut.setState(tutor.getState());
		tut.setPincode(tutor.getPincode());
		
		tut.setMobile_number(tutor.getMobile_number());

		tut.setDob(tutor.getDob());
		tut.setQualification(tutor.getQualification());	
		tut.setProfile_pic(tutor.getProfile_pic());
		tut.setApprovalFlag(1);
		session.saveOrUpdate(tut);
		return tut;
	}

	@Override
	public void resetPassword(PasswordChangeRequestBody passchange, UUID tutor_id) throws STMAPIException {
		Session session = sessionfactory.getCurrentSession();
		Tutor tut = session.get(Tutor.class, tutor_id);
		String original_pass = tut.getUser_id().getPassword();
		//origional password
		String old_pass_by_user = passchange.getOld_pass();
		
		ShaPasswordEncoder encoder = new ShaPasswordEncoder(env.getProperty("security.encoding-strength", Integer.class));
		//entered old_pass (encoded)
		String old_Password_by_user = encoder.encodePassword(old_pass_by_user, null);
		
		
		if(old_Password_by_user.equals(original_pass))
		{
			ShaPasswordEncoder encoder2 = new ShaPasswordEncoder(env.getProperty("security.encoding-strength", Integer.class));
			String new_encodedPassword = encoder2.encodePassword(passchange.getNew_pass(), null);
			Query query = session.createQuery("update app_user set password = :newpass where id = :user_id");
			query.setParameter("newpass",  new_encodedPassword);
			query.setParameter("user_id",  tut.getUser_id().getId());
			query.executeUpdate();
		}
		else
		{
			throw new STMAPIException("0010", new String[] {}, null);
		}
		
	}

//	@PreAuthorize("hasAuthority('ADMIN')")
	@Override
	public Tutor updateStatus(UUID tutorId,int status) {
		Session session =sessionfactory.getCurrentSession();
		Query query = session.createQuery("update " + Tutor.class.getName() + " set approvalFlag = :status where tutor_id = :tutor");
		query.setParameter("tutor", tutorId);
		query.setParameter("status", status);
		query.executeUpdate();
		Tutor tutor = session.get(Tutor.class, tutorId);
		return tutor;
	}

	@Override
	public List<PrintResponceBodyTutor> printTutorGrid(Integer status) {
		Session session = sessionfactory.getCurrentSession();
		String hql;
		if(status == null)
		{
			hql = "from " + Tutor.class.getName();
		}
		else
		{
			hql = "from " + Tutor.class.getName() + " where approvalFlag = :status";
		}
		Query query = session.createQuery(hql);
		query.setParameter("status", status);
		List<Tutor> tutors = query.list();
		List<PrintResponceBodyTutor> list = new ArrayList<PrintResponceBodyTutor>();
		for(Tutor tutor : tutors)
		{
			PrintResponceBodyTutor obj = new PrintResponceBodyTutor();
//			List<String> listOfNames = new ArrayList<String>();
//			List<TutorInstitute> listOfInstitutes= tutInsDao.getByTutor(tutor.getTutor_id());
//			for(TutorInstitute tutInst : listOfInstitutes)
//			{
//				listOfNames.add(tutInst.getName());
//			}
//			obj.setInstitutes(listOfNames);
			obj.setFirst_name(tutor.getFirst_name());
			obj.setLast_name(tutor.getLast_name());
			obj.setAbout_me(tutor.getAbout_me());
			obj.setAddress(tutor.getAddress());
			obj.setCity(tutor.getCity());
			obj.setDob(tutor.getDob());
			obj.setEmail(tutor.getEmail());
			obj.setLocality(tutor.getLocality());
			obj.setMobile_number(tutor.getMobile_number());
			obj.setPincode(tutor.getPincode());
			obj.setQualification(tutor.getQualification());
			obj.setState(tutor.getState());
			
			list.add(obj);
		}
		return list;
	}
	
	
	

	
}
