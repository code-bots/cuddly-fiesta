package com.codebots.stm.spring.dao;

import java.util.List;
import java.util.UUID;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Repository;

import com.codebots.stm.spring.model.Course;
import com.codebots.stm.spring.model.Subject;
import com.codebots.stm.spring.model.Tutor;



@Repository
@PropertySource("classpath:application.properties")
@Transactional
public class SubjectDaoImpl implements SubjectDao{

	@Autowired
	private SessionFactory sessionfactory;
	
	
	@Override
	public Subject save(Subject subject, UUID course_id) {
		Session session = sessionfactory.getCurrentSession();
		Course course = session.get(Course.class, course_id);
		subject.setCourse_id(course);
		sessionfactory.getCurrentSession().save(subject);
		return subject;
	}

	
	@Override
	public List<Subject> list(UUID course_id) {
		Session session = sessionfactory.getCurrentSession();
		Query<Subject> query = session.createQuery(" from " + Subject.class.getName() + " where course_id.id=:courseId");
		query.setParameter("courseId", course_id);
		return query.getResultList(); 	
	}


	@Override
	public void update(Subject subject, UUID course_id, UUID subject_id) {
		Session session = sessionfactory.getCurrentSession();
		Subject sub = session.byId(Subject.class).load(subject_id);
		Course c = session.get(Course.class, course_id);
		sub.setName(subject.getName());
		sub.setCourse_id(c);
		sub.setDescription(subject.getDescription());
		
		session.saveOrUpdate(sub);
		
		
	}

	@PreAuthorize("hasAuthority('TUTOR') or hasAuthority('STUDENT')")
	@Override
	public List<Subject> listAll() {
		Session session = sessionfactory.getCurrentSession();
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Subject> cq = cb.createQuery(Subject.class);
		Root<Subject> root = cq.from(Subject.class);
		cq.select(root);
		Query<Subject> query = session.createQuery(cq);		
		return query.getResultList();
	}

	
	
	
}
