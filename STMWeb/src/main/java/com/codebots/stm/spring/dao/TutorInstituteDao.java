package com.codebots.stm.spring.dao;

import java.util.List;
import java.util.UUID;

import com.codebots.stm.spring.model.TutorInstitute;

public interface TutorInstituteDao {

	TutorInstitute save(TutorInstitute tutInstitute,UUID tutor_id);
	TutorInstitute update(TutorInstitute tutorInstitute,UUID tutInstituteId);
	List<TutorInstitute> getByTutor(UUID tutor_id);
	
}
