package com.codebots.stm.spring.dao;

import java.util.List;
import java.util.UUID;

import com.codebots.stm.spring.model.PaymentInfo;

public interface PaymentDao {
	PaymentInfo save(PaymentInfo payment);
	List<PaymentInfo> list();
	PaymentInfo updatePayment(PaymentInfo payInfo,UUID id); 
}
