package com.codebots.stm.spring.model;

import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;

@Entity(name="student_que_ans")
public class StudentQuestionAnswer {

	
	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(name="UUID",strategy="org.hibernate.id.UUIDGenerator")
	private UUID id;
	@OneToOne(targetEntity=Questions.class,cascade=CascadeType.ALL)
	@JoinColumn(name="question_id",referencedColumnName="id")
	private Questions question_id;
	@OneToOne(targetEntity=Student.class,cascade=CascadeType.ALL)
	@JoinColumn(name="student_id",referencedColumnName="student_id")
	private Student student_id;
	private String student_choice;
	private String correct_ans;
	private Integer status;
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public UUID getId() {
		return id;
	}
	public void setId(UUID id) {
		this.id = id;
	}
	public Questions getQuestion_id() {
		return question_id;
	}
	public void setQuestion_id(Questions question_id) {
		this.question_id = question_id;
	}
	public Student getStudent_id() {
		return student_id;
	}
	public void setStudent_id(Student student_id) {
		this.student_id = student_id;
	}
	public String getStudent_choice() {
		return student_choice;
	}
	public void setStudent_choice(String student_choice) {
		this.student_choice = student_choice;
	}
	public String getCorrect_ans() {
		return correct_ans;
	}
	public void setCorrect_ans(String correct_ans) {
		this.correct_ans = correct_ans;
	}
	
	
	
	
}
