package com.codebots.stm.spring.controller;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codebots.stm.reqres.ResponseObject;
import com.codebots.stm.reqres.StudentRequestBody;
import com.codebots.stm.spring.dao.ScheduleDao;
import com.codebots.stm.spring.exception.STMAPIException;
import com.codebots.stm.spring.model.Results;
import com.codebots.stm.spring.model.Schedule;

@RestController
@RequestMapping("/rest/api")
public class ScheduleController {

	@Autowired
	private ScheduleDao schDao;
	
	

	@PostMapping("/schedule/{assignment_id}")
	public ResponseEntity<?> saveSchedule(@RequestBody Schedule sch,@PathVariable("assignment_id")  UUID ass_id) throws STMAPIException
	{    
		try
		{
			Schedule sc = schDao.save(sch, ass_id);		
			return ResponseEntity.ok().body(sc);
		}catch(Exception e)
		{
			throw new STMAPIException("0045", new String[] {e.getMessage()}, e);
		}
	}
	
	@GetMapping("/schedule/{assignment_id}")
	public ResponseEntity<List<Schedule>> listByAssignment(@PathVariable("assignment_id")UUID assignment_id) throws STMAPIException
	{   try
		{
			List<Schedule> list = schDao.listByAssignment(assignment_id);
		    return ResponseEntity.ok().body(list);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0046", new String[] {e.getMessage()}, e);
		}
	}
	
	
	@GetMapping("/schedule")
	public ResponseEntity<List<Schedule>> listSchedule() throws STMAPIException
	{   try
		{
			List<Schedule> list = schDao.list();
		    return ResponseEntity.ok().body(list);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0046", new String[] {e.getMessage()}, e);
		}
	}
	
	
	@GetMapping("/list/schedule")
	public ResponseEntity<List<Schedule>> listOfSchedules(@RequestParam(value = "type") Integer type) throws STMAPIException
	{   try
		{
			List<Schedule> list = schDao.listOfSchedules(type);
		    return ResponseEntity.ok().body(list);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0046", new String[] {e.getMessage()}, e);
		}
	}
	
	@GetMapping("/schedule/tutor/{tutor_id}")
	public ResponseEntity<List<Schedule>> listByTutor(@PathVariable("tutor_id") UUID tutor_id) throws STMAPIException
	{
		try
		{
			List<Schedule> list = schDao.listByTutor(tutor_id);
		    return ResponseEntity.ok().body(list);	
		}catch(Exception e)
		{
			throw new STMAPIException("0058", new String[] {e.getMessage()}, e);
		}
		
	}
	
	@GetMapping("/schedule/student/{student_id}")
	public ResponseEntity<List<Schedule>> listByStudent(@PathVariable("student_id") UUID student_id) throws STMAPIException
	{
		try
		{
			List<Schedule> list = schDao.listByStudent(student_id);
			return ResponseEntity.ok().body(list);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0059", new String[] {e.getMessage()}, e);
		}
		
		
	}
	
	
	@DeleteMapping("/schedule/{id}")
	public ResponseEntity<ResponseObject> deleteSchedule(@PathVariable("id") UUID id) throws STMAPIException
	{
		try
		{
			schDao.delete(id);
			ResponseObject obj = new ResponseObject();
			obj.setMessage("Deleted SuccessFully");
			return ResponseEntity.ok().body(obj);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0047", new String[] {e.getMessage()}, e);
		}
	}
	
	@PutMapping("/schedule/{id}")
	public ResponseEntity<Schedule> updateSchedule(@RequestBody Schedule sch,@PathVariable("id") UUID id) throws STMAPIException
	{
		try
		{
			sch.setId(id);
			Schedule schedule = schDao.update(id, sch);
			return ResponseEntity.ok().body(schedule);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0057", new String[] {e.getMessage()}, e);
		}
	}
	
	
}
