package com.codebots.stm.spring.dao;

import java.util.List;
import java.util.UUID;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Repository;

import com.codebots.stm.spring.model.Blog;
import com.codebots.stm.spring.model.Comments;
import com.codebots.stm.spring.model.Student;
import com.codebots.stm.spring.model.Tutor;


@Repository
@PropertySource("classpath:application.properties")
@PropertySource("classpath:mailbox.properties")
@Transactional
public class BlogDaoImple implements BlogDao{

	@Autowired
	private SessionFactory sessionfactory;
	
	
	@Override
	public Blog save(Blog blog, UUID tutor_id) {
		Session session = sessionfactory.getCurrentSession();
		Tutor tutor = session.get(Tutor.class , tutor_id);
		blog.setTutor_id(tutor);
		sessionfactory.getCurrentSession().save(blog);
		
		return blog;
	}

	
	@Override
	public List<Blog> getBlogByTutor(UUID tutor_id) {
		Session session = sessionfactory.getCurrentSession();
		String hql = "from " + Blog.class.getName() + " where tutor_id.tutor_id= :tutor";
		Query query = session.createQuery(hql);
		query.setParameter("tutor",tutor_id);
		List<Blog> list = query.list();
		return list;
	}

	
	@Override
	public List<Blog> getAllBlogs() {
		Session session = sessionfactory.getCurrentSession();
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Blog> cq = cb.createQuery(Blog.class);
		Root<Blog> root = cq.from(Blog.class);
		cq.select(root);
	    Query<Blog> query = session.createQuery(cq);
	    return query.getResultList();
	}

	@PreAuthorize("hasAuthority('TUTOR')")
	@Override
	public void deleteBlog(UUID blog_id) {
		Session session = sessionfactory.getCurrentSession();
		String hql = "delete from " + Blog.class.getName() + " where id = :blogId";
		Query query = session.createQuery(hql).setParameter("blogId", blog_id);
		query.executeUpdate();		
	}

	
	@Override
	public Blog updateBlog(Blog blog, UUID blogId) {
		Session session = sessionfactory.getCurrentSession();
		
		Blog bl = session.get(Blog.class, blogId);
		
		bl.setContent(blog.getContent());
		bl.setTitle(blog.getTitle());
		bl.setTags(blog.getTags());
		
		session.saveOrUpdate(bl);		
		return bl;
	}

	
	@Override
	public Blog getBogById(UUID id) {
		Session session = sessionfactory.getCurrentSession();
		Blog blog = session.byId(Blog.class).load(id);
		return blog;
	}

	
}
