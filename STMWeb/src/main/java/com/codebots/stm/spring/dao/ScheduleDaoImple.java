package com.codebots.stm.spring.dao;

import java.util.List;
import java.util.UUID;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Repository;

import com.codebots.stm.spring.model.AssignmentStudent;
import com.codebots.stm.spring.model.Assignments;
import com.codebots.stm.spring.model.Schedule;

@Repository
@PropertySource("classpath:application.properties")
@PropertySource("classpath:mailbox.properties")
@Transactional
public class ScheduleDaoImple implements ScheduleDao{

	@Autowired
	private SessionFactory sessionfactory;
	
	
	@Override
	public Schedule save(Schedule sch, UUID ass_id) {
		Session session = sessionfactory.getCurrentSession();
		Assignments ass = session.get(Assignments.class,ass_id);		
		sch.setAssignment_id(ass);
		sessionfactory.getCurrentSession().save(sch);
		return sch;
	}

	
	@Override
	public List<Schedule> list() {
		Session session = sessionfactory.getCurrentSession();
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Schedule> cq = cb.createQuery(Schedule.class);
		Root<Schedule> root = cq.from(Schedule.class);
		cq.select(root);
	    Query<Schedule> query = session.createQuery(cq);
	    return query.getResultList();
	}

	
	@Override
	public Schedule update(UUID sch_id, Schedule sch) {
		Session session = sessionfactory.getCurrentSession();
		Schedule sc = session.byId(Schedule.class).load(sch_id);
		
		sc.setEnd_date(sch.getEnd_date());
		sc.setStart_date(sch.getStart_date());
		sc.setTitle(sch.getTitle());
		
		session.saveOrUpdate(sc);
		return sc;
	}

	
	@Override
	public void delete(UUID sch_id) {
		Session session = sessionfactory.getCurrentSession();
		Schedule sch = session.byId(Schedule.class).load(sch_id);
		session.delete(sch);
		
	}
	
	
	@Override
	public List<Schedule> listByTutor(UUID tutor_id) {
		Session session = sessionfactory.getCurrentSession();
		String hql = "from schedule sch,assignments ass,tutor tut WHERE ass.id = sch.assignment_id  and tut.tutor_id = ass.tutor_id and tut.tutor_id = :tutor";
		Query query = session.createQuery(hql);
		query.setParameter("tutor", tutor_id);
		List<Schedule> list = query.list();
		return list;
	}

	
	@Override
	public List<Schedule> listByStudent(UUID student_id) {
		Session session = sessionfactory.getCurrentSession();
		String hql = "from schedule sch,"+ AssignmentStudent.class.getName() + " ass_stu,student stu where sch.assignment_id = ass_stu.assignment_id and ass_stu.student_id = stu.student_id and stu.student_id = :student";
		Query query = session.createQuery(hql);
		query.setParameter("student", student_id);
		List<Schedule> list = query.list();
		return list;
	}

	
	@Override
	public List<Schedule> listByAssignment(UUID assignment_id) {
		Session session = sessionfactory.getCurrentSession();
		String hql = "from  "+ Schedule.class.getName()  +" where assignment_id.id = :assignment";
		Query query = session.createQuery(hql);
		query.setParameter("assignment",assignment_id);
		List<Schedule> list = query.list();
		return list;
	}

	
	@Override
	public List<Schedule> listOfSchedules(Integer type) {
		Session session = sessionfactory.getCurrentSession();
		String hql = "from " + Schedule.class.getName() + " where asignment_type = :type";
		Query query = session.createQuery(hql);
		query.setParameter("type", type);
		List<Schedule> list = query.list();
		return list;
	}

}
