package com.codebots.stm.spring.dao;

import java.util.List;
import java.util.UUID;


import javax.transaction.Transactional;
import org.hibernate.query.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Repository;

import com.codebots.stm.spring.model.Tutor;
import com.codebots.stm.spring.model.TutorInstitute;
@Repository
@PropertySource("classpath:application.properties")
@PropertySource("classpath:mailbox.properties")
@Transactional
public class TutorInstituteDaoImpl implements TutorInstituteDao {

	@Autowired
	private SessionFactory sessionfactory;
	

	@Override
	public TutorInstitute save(TutorInstitute tutInstitute, UUID tutor_id) {
		Session session = sessionfactory.getCurrentSession();
		Tutor tutor = session.byId(Tutor.class).load(tutor_id);
		tutInstitute.setTutor_id(tutor);
		sessionfactory.getCurrentSession().save(tutInstitute);
		return tutInstitute;
	}

	
	@Override
	public TutorInstitute update(TutorInstitute tutorInstitute, UUID tutInstituteId) {
		Session session = sessionfactory.getCurrentSession();
		TutorInstitute tutIn = session.byId(TutorInstitute.class).load(tutInstituteId);
		
		
		tutIn.setWeekdays(tutorInstitute.getWeekdays());
		tutIn.setName(tutorInstitute.getName());
		tutIn.setLongitude(tutorInstitute.getLongitude());
		tutIn.setLatitude(tutorInstitute.getLatitude());
		tutIn.setInst_startTime(tutorInstitute.getInst_startTime());
		tutIn.setInst_endTime(tutorInstitute.getInst_endTime());
		tutIn.setFees(tutorInstitute.getFees());
		
		session.saveOrUpdate(tutIn);
		
		return tutIn;
	}

	
	@Override
	public List<TutorInstitute> getByTutor(UUID tutor_id) {
		Session session = sessionfactory.getCurrentSession();
		String hql = "from " + TutorInstitute.class.getName() + " where tutor_id.tutor_id = :tutor";
		Query query = session.createQuery(hql);
		query.setParameter("tutor",tutor_id);
		List<TutorInstitute> list = query.list();
		return list;
	}

	
	
}
