package com.codebots.stm.spring.crudrepo;

import java.util.UUID;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.codebots.stm.spring.model.Role;
@Repository
public interface RoleRepository extends CrudRepository<Role, Integer> {
}