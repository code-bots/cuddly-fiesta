package com.codebots.stm.spring.dao;

import java.util.List;
import java.util.UUID;

import com.codebots.stm.reqres.StudentRequestBody;
import com.codebots.stm.spring.model.Schedule;

public interface ScheduleDao {

	Schedule save(Schedule sch,UUID ass_id);
	List<Schedule> list();
	Schedule update(UUID sch_id,Schedule sch);
	void delete(UUID sch_id);
	List<Schedule> listByTutor(UUID tutor_id);
	List<Schedule> listByStudent(UUID student_id);
	List<Schedule> listByAssignment(UUID assignment_id);
	List<Schedule> listOfSchedules(Integer type);
	
}
