package com.codebots.stm.spring.controller;


import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codebots.stm.communication.PrintResponceBodyStudent;
import com.codebots.stm.reqres.Pagination;
import com.codebots.stm.reqres.PasswordChangeRequestBody;
import com.codebots.stm.reqres.ResponseObject;
import com.codebots.stm.spring.dao.AcademicsDao;
import com.codebots.stm.spring.dao.AssignmentDao;
import com.codebots.stm.spring.dao.AssignmentStudentDao;
import com.codebots.stm.spring.dao.QuestionQADao;
import com.codebots.stm.spring.dao.StudentDao;
import com.codebots.stm.spring.exception.STMAPIException;
import com.codebots.stm.spring.model.Academics;
import com.codebots.stm.spring.model.AssignmentStudent;
import com.codebots.stm.spring.model.Assignments;
import com.codebots.stm.spring.model.Student;
import com.codebots.stm.spring.model.StudentQuestionAnswer;
@RestController
@RequestMapping("/rest/api")
public class StudentController {

	@Autowired
	private StudentDao studentDao;
		
	@Autowired 
	private AcademicsDao academicdao;
	
	@Autowired
	private AssignmentStudentDao assignmentStudentDao;

	@Autowired
	private AssignmentDao assDao;
	
	
	@Autowired
	private QuestionQADao qaDao;
	
	@GetMapping("/studentgrid")
	public ResponseEntity<Pagination> listStudentGrid(@RequestParam(value = "page") Integer pageNumber,
														 @RequestParam(value = "size") Integer pageSize,
														 @RequestParam(value = "type",required = false) String type,
														 @RequestParam(value = "feild",required = false) String feild) throws STMAPIException
	{   try
		{
				Pagination page = studentDao.list(pageNumber,pageSize,type,feild);
			    return ResponseEntity.ok().body(page);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0005", new String[] {e.getMessage()}, e);
		}
	}
	
	@PutMapping("student/changepassword/{student_id}")
	public ResponseEntity<ResponseObject> resetPassword(@PathVariable UUID student_id,@RequestBody PasswordChangeRequestBody passchange) throws STMAPIException
	{		
		try
		{
		studentDao.resetPassword(passchange, student_id);
		ResponseObject obj = new ResponseObject();
		obj.setMessage("Password Updated SuccessFully");
		return ResponseEntity.ok().body(obj);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0011", new String[] {e.getMessage()}, e);
		}
	}


	
	@PostMapping("student/{student_id}/{question_id}")
	public ResponseEntity<StudentQuestionAnswer> saveAns(@RequestBody StudentQuestionAnswer stuqa,@PathVariable("student_id") UUID studentId,@PathVariable("question_id") UUID questionId) throws STMAPIException
	{
		try
		{
			StudentQuestionAnswer stuans = qaDao.save(questionId, studentId, stuqa);
			return ResponseEntity.ok().body(stuans);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0034", new String[] {e.getMessage()}, e);
		}
	}
	
	
	@GetMapping("/student/{student_id}/assignmentlist/{status}")
	public ResponseEntity<List<Assignments>> assignmentByStudentStatus(@PathVariable("student_id") UUID student_id,
																	   @PathVariable("status")Integer status,
																	   @RequestParam(value = "from",required = false)@DateTimeFormat(pattern="yyyy-MM-dd") Date fromDate,
																		 @RequestParam(value = "to",required = false) @DateTimeFormat(pattern="yyyy-MM-dd") Date toDate) throws STMAPIException {
		try {
			List<Assignments> list = assDao.getByStudentStatus(student_id, status,fromDate,toDate);
			return ResponseEntity.ok().body(list);
		} catch (Exception e) {	
			throw new STMAPIException("0009", new String[] { e.getMessage() }, e);
		}
	}
	
	
	//update student
	@PutMapping("/student/{student_id}")
	public ResponseEntity<Student> updateStudent(@PathVariable("student_id") UUID id,@RequestBody Student student) throws STMAPIException
	{
		try
		{
		student.setStudent_id(id);
		Student stud = studentDao.update(id, student);
		return ResponseEntity.ok().body(stud);
		}catch(Exception e)
		{
			throw new STMAPIException("0023", new String[] {e.getMessage()},e);
		}
	}
	
	
	@GetMapping("/academicsdetails")
	public ResponseEntity<List<Academics>> listAcademics() throws STMAPIException
	{
		try
		{
			List<Academics> academics = academicdao.list();
		    return ResponseEntity.ok().body(academics);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0006", new String[] {e.getMessage()}, e);
		}
	}
	
	
	@PostMapping("/student/{student_id}/academics")
	public ResponseEntity<?> saveAcademics(@RequestBody Academics academic,@PathVariable UUID student_id) throws STMAPIException
	{
		try
		{
			Academics acd = academicdao.save(academic,student_id);
			return ResponseEntity.ok().body(acd);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0007", new String[] {e.getMessage()}, e);
		}
	}
	
	//get all academics
	@GetMapping("/student/{student_id}/academics")
	public ResponseEntity<List<Academics>> getByIdAcademics(@PathVariable("student_id")  UUID student_id)throws STMAPIException
	{
		try
		{
			List<Academics> academics = academicdao.getbyId(student_id);
		    return ResponseEntity.ok().body(academics);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0006", new String[] {e.getMessage()}, e);
		}
	
	}
	
	//get student wise academics
	@GetMapping("/student/{student_id}/academics/{academics_id}")
	public ResponseEntity<Academics> getSpecificAcademics(@PathVariable("student_id") UUID student_id,@PathVariable("academics_id")UUID academics_id ) throws STMAPIException
	{
		try
		{
			Academics academics = academicdao.getSpecific(student_id, academics_id);
			return ResponseEntity.ok().body(academics);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0006", new String[] {e.getMessage()}, e);
		}
	}
	
	//delete academics student and academics wise
	@DeleteMapping("/student/{student_id}/academics/{academics_id}")
	public ResponseEntity<?> deleteAcademics(@PathVariable("student_id") UUID student_id,@PathVariable("academics_id")UUID academics_id ) throws STMAPIException
	{
		try
		{
			int result = academicdao.delete(student_id, academics_id);
			ResponseObject obj = new ResponseObject();
			obj.setMessage("Deleted SuccessFully");
			return ResponseEntity.ok().body(obj);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0008", new String[] {e.getMessage()}, e);
		}
	}
	
	
	//update academics
	@PutMapping("/student/{student_id}/academics/{academics_id}")	
	public ResponseEntity<Academics> updateAcademics(@PathVariable("student_id") UUID student_id,@PathVariable("academics_id")UUID academics_id,@RequestBody Academics academics) throws STMAPIException
	{
		try
		{
		academics.setAcademic_id(academics_id);
		Academics ad = academicdao.update(student_id,academics_id,academics);
		return ResponseEntity.ok().body(ad);
		}catch(Exception e)
		{
			throw new STMAPIException("0003", new String[] {e.getMessage()},e);
		}
	}

	@PostMapping("/assignmentslist/{assignment_id}/{student_id}")
	public ResponseEntity<?> saveAssignement(@RequestBody AssignmentStudent assStudent,@PathVariable("assignment_id") UUID assignment_id,@PathVariable("student_id") UUID student_id) throws STMAPIException
	{
		try
		{
			
			AssignmentStudent assstu = assignmentStudentDao.save(assStudent, student_id, assignment_id);
			return ResponseEntity.ok().body(assstu);	
		}
		catch(Exception e)
		{
			throw new STMAPIException("0020", new String[] {e.getMessage()},e);
		}
	}
	
	
	@GetMapping("/assignment/{status}")
	public ResponseEntity<List<AssignmentStudent>> getByStatus(@PathVariable String status) throws STMAPIException
	{
		
		try
		{
		List<AssignmentStudent> list = assignmentStudentDao.listbyStatus(status);
		return  ResponseEntity.ok().body(list);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0021", new String[] {e.getMessage()},e);
		}
	}
	
	@GetMapping("/print/student/{tutorId}")
	public ResponseEntity<List<PrintResponceBodyStudent>> getPrintBody(@PathVariable("tutorId") UUID tutorId) throws STMAPIException
	{
		
		try
		{
			List<PrintResponceBodyStudent> list = studentDao.printStudentGrid(tutorId);
			return  ResponseEntity.ok().body(list);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0093", new String[] {e.getMessage()},e);
		}
	}
	
}
