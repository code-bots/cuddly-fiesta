package com.codebots.stm.spring.controller;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codebots.stm.reqres.ResponseObject;
import com.codebots.stm.spring.dao.QuestionQADao;
import com.codebots.stm.spring.exception.STMAPIException;
import com.codebots.stm.spring.model.Results;
import com.codebots.stm.spring.model.StudentQuestionAnswer;
import com.sun.mail.iap.Response;

@RestController
@RequestMapping("/rest/api")
public class StudentQueAnsController {

	@Autowired
	private QuestionQADao qadao;
	
	@PostMapping("/{student_id}/{questionset_id}/answersbyStudent")
	public ResponseEntity<?> saveQA(@RequestBody StudentQuestionAnswer sqa,@PathVariable("student_id") UUID student_id,@PathVariable ("questionset_id")UUID queset_id) throws STMAPIException
	{    
		try
		{
			StudentQuestionAnswer stqa = qadao.save(queset_id, student_id, sqa);
			return ResponseEntity.ok().body(stqa);
		}catch(Exception e)
		{
			throw new STMAPIException("0034", new String[] {e.getMessage()}, e);
		}
	}
	
	@GetMapping("/studentanswers/{student_id}")
	public ResponseEntity<List<StudentQuestionAnswer>> listStudentAnswers(@PathVariable UUID student_id) throws STMAPIException
	{   try
		{
			List<StudentQuestionAnswer> list = qadao.list(student_id);
		    return ResponseEntity.ok().body(list);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0035", new String[] {e.getMessage()}, e);
		}
	}
	
	
	@GetMapping("/selected/ans/{questionset}/{student}")
	public ResponseEntity<List<StudentQuestionAnswer>> getSelectedAnswers(@PathVariable("questionset") UUID questionSetId,
																	@PathVariable("student") UUID studentId) throws STMAPIException
	{
		try
		{
			List<StudentQuestionAnswer> list = qadao.listOfSelectedAnswer(studentId, questionSetId);
			return ResponseEntity.ok().body(list);	
		}
		catch(Exception e)
		{
			throw new STMAPIException("0082", new String[] {e.getMessage()}, e);
		}
	}
	
	@PutMapping("status/change/{questionset}/{student}")
	public ResponseEntity<ResponseObject> updateStatus(@PathVariable("questionset") UUID questionSetId,
																		  @PathVariable("student") UUID studentId) throws STMAPIException
	{
		try
		{
			qadao.changeStatus(questionSetId, studentId);
			ResponseObject obj = new ResponseObject();
			obj.setMessage("status updated");
			return ResponseEntity.ok().body(obj);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0083", new String[] {e.getMessage()}, e);
		}
	}
	
}
