package com.codebots.stm.spring.dao;

import java.util.List;
import java.util.UUID;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.aspectj.weaver.patterns.TypePatternQuestions.Question;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Repository;

import com.codebots.stm.reqres.ResponseObject;
import com.codebots.stm.spring.model.QuestionSet;
import com.codebots.stm.spring.model.Questions;


@Repository
@PropertySource("classpath:application.properties")
@PropertySource("classpath:mailbox.properties")
@Transactional
public class QuestionDaoImple implements QuestionDao{

	@Autowired
	private SessionFactory sessionfactory;
	
	
	@Override
	public Questions save(Questions que,UUID quesetid) {
		
		Session session = sessionfactory.getCurrentSession();
		QuestionSet queset = session.get(QuestionSet.class, quesetid);
		que.setQuestion_set_id(queset);
		sessionfactory.getCurrentSession().save(que);
		return que;
	}

	
	@Override
	public List<Questions> list() {
		Session session = sessionfactory.getCurrentSession();
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Questions> cq = cb.createQuery(Questions.class);
		Root<Questions> root = cq.from(Questions.class);
		cq.select(root);
		Query<Questions> query = session.createQuery(cq);
		return query.getResultList(); 
	}

	
	@Override
	public Questions getById(UUID queid) {
		Session session = sessionfactory.getCurrentSession();
		Questions que = session.get(Questions.class, queid);
		return que;
	}


	@Override
	public Questions updateQue(Questions que, UUID queId) {
		Session session = sessionfactory.getCurrentSession();
		Questions q = session.byId(Questions.class).load(queId);
		
		q.setQuestion(que.getQuestion());
		q.setOption_D(que.getOption_D());
		q.setOption_C(que.getOption_C());
		q.setOption_B(que.getOption_B());
		q.setOption_A(que.getOption_A());
		q.setCorrect_ans(que.getCorrect_ans());
		
		session.saveOrUpdate(q);
		
		return q;
	}

	
	@Override
	public List<Questions> getByQSet(UUID queSetId) {
		Session session = sessionfactory.getCurrentSession();
		String hql = "from " + Questions.class.getName() + " where question_set_id.id = :qset";
		Query query = session.createQuery(hql);
		query.setParameter("qset",queSetId);
		List<Questions> list = query.list();
		return list;
	}

	
	@Override
	public void deleteQuestion(UUID questionID) {
		Session session = sessionfactory.getCurrentSession();
		Questions que = session.byId(Questions.class).load(questionID);
		session.delete(que);
		
	}

	
	@Override
	public ResponseObject getAns(UUID queId) {
		Session session = sessionfactory.getCurrentSession();
		String hql = "select correct_ans from " + Questions.class.getName() + " where id = :queId";
		Query query = session.createQuery(hql);
		query.setParameter("queId", queId);
		String ans = (String) query.uniqueResult();
		ResponseObject obj = new ResponseObject();
		obj.setMessage(ans);
		return obj;
	}
	
	
	
}
