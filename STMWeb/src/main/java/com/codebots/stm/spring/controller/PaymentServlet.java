package com.codebots.stm.spring.controller;

import java.util.Enumeration;
import java.util.Map;
import java.util.TreeMap;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.View;

import com.codebots.stm.common.PaymentConstants;
import com.codebots.stm.reqres.PaymentInfo;
import com.codebots.stm.spring.dao.AssignmentDao;
import com.codebots.stm.spring.dao.AssignmentStudentDao;
import com.codebots.stm.spring.dao.PaymentDao;
import com.codebots.stm.spring.dao.StudentDao;
import com.codebots.stm.spring.exception.STMAPIException;
import com.codebots.stm.spring.model.AssignmentStudent;
import com.codebots.stm.spring.model.Assignments;
import com.codebots.stm.spring.model.Student;
import com.paytm.pg.merchant.CheckSumServiceHelper;

@PropertySource("classpath:payment.properties")
@Controller
public class PaymentServlet {
	
	@Autowired
	private Environment env;
	
	@Autowired
	private StudentDao studentDao;
	
	@Autowired
	private AssignmentDao assignmentDao;

	@Autowired 
	private AssignmentStudentDao assignmentStudentDao;
	
	@Autowired
	private PaymentDao paymentDao;
	
	@RequestMapping(value = "/paymentResult", method = RequestMethod.POST, produces="text/html")
	public void paymentResult(HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		Enumeration<String> paramNames = request.getParameterNames();

		Map<String, String[]> mapData = request.getParameterMap();
		TreeMap<String,String> parameters = new TreeMap<String,String>();
		String paytmChecksum =  "";
		while(paramNames.hasMoreElements()) {
			String paramName = (String)paramNames.nextElement();
			if(paramName.equals("CHECKSUMHASH")){
				paytmChecksum = mapData.get(paramName)[0];
			}else{
				parameters.put(paramName,mapData.get(paramName)[0]);
			}
		}
		boolean isValideChecksum = false;
		com.codebots.stm.spring.model.PaymentInfo paymentInfo = new com.codebots.stm.spring.model.PaymentInfo();
		paymentInfo.setGateway_status(parameters.get("STATUS"));
		paymentInfo.setRespcode(parameters.get("RESPCODE"));
		paymentInfo.setRespmsg(parameters.get("RESPMSG"));
		paymentInfo.setTxn_id(parameters.get("TXNID"));
		paymentInfo.setApp_status("Completed");
		com.codebots.stm.spring.model.PaymentInfo updatedPaymentInfo = paymentDao.updatePayment(paymentInfo, UUID.fromString(parameters.get("ORDERID")));
		
		AssignmentStudent assignmentStudent = new AssignmentStudent();
		assignmentStudent.setStatus(1);
		assignmentStudentDao.save(assignmentStudent, updatedPaymentInfo.getStudent_id().getStudent_id(), updatedPaymentInfo.getAssignment_id().getId());
		
//		TXNDATE
//		paymentInfo.setTxn_time(null);
		isValideChecksum = CheckSumServiceHelper.getCheckSumServiceHelper().verifycheckSum(env.getProperty(PaymentConstants.MERCHANT_KEY),parameters,paytmChecksum);
		if(!isValideChecksum){
			throw new STMAPIException("0076", new String[] {paymentInfo.getTxn_id()}, null);
		}
		response.setContentType("application/json;charset=UTF-8");
		ObjectMapper mapper = new ObjectMapper();
// 		response.getWriter().write(mapper.writeValueAsString(updatedPaymentInfo));
 		response.sendRedirect(env.getProperty("APP_REDIRECT_AFTER_PAYMENT") + updatedPaymentInfo.getAssignment_id().getId().toString() + "/" + parameters.get("STATUS"));
	}
	
	@RequestMapping(value = "/payment", method = RequestMethod.POST, produces="text/html")
    public void proccedPayment(HttpServletRequest request, @Validated PaymentInfo paymentDetails,
                                   HttpServletResponse response) throws Exception {
		//load student details 
		//load assignmnet details
		//load price associated with assignment
		Student student = studentDao.get(paymentDetails.getStudentId());
		Assignments assignment = assignmentDao.getById(paymentDetails.getAssignmentId());
		String custId = paymentDetails.getStudentId().toString();
		String amount = String.valueOf(assignment.getTutorInstituteId().getFees());
		String emailId = student.getEmail();
		String mobileNumber = student.getMobile_number();
		
		
		com.codebots.stm.spring.model.PaymentInfo paymentInfo = new com.codebots.stm.spring.model.PaymentInfo();
		paymentInfo.setAssignment_id(assignment);
		paymentInfo.setStudent_id(student);
		paymentInfo.setAmount(assignment.getTutorInstituteId().getFees());
		paymentInfo.setApp_status("Initialized");
		com.codebots.stm.spring.model.PaymentInfo savedPaymentInfo = paymentDao.save(paymentInfo);
		
		String orderId = String.valueOf(savedPaymentInfo.getId().toString());
		
		request.setAttribute(View.RESPONSE_STATUS_ATTRIBUTE, HttpStatus.TEMPORARY_REDIRECT);
		TreeMap<String,String> parameters = new TreeMap<String,String>();
		parameters.put(PaymentConstants.MID,env.getProperty(PaymentConstants.MID));
		parameters.put(PaymentConstants.CHANNEL_ID,env.getProperty(PaymentConstants.CHANNEL_ID));
		parameters.put(PaymentConstants.INDUSTRY_TYPE_ID,env.getProperty(PaymentConstants.INDUSTRY_TYPE_ID));
		parameters.put(PaymentConstants.WEBSITE,env.getProperty(PaymentConstants.WEBSITE));
		parameters.put(PaymentConstants.MOBILE_NO,mobileNumber);
		parameters.put(PaymentConstants.EMAIL,emailId);
		parameters.put(PaymentConstants.CALLBACK_URL, env.getProperty(PaymentConstants.CALLBACK_URL));
		parameters.put(PaymentConstants.ORDER_ID, orderId);
		parameters.put(PaymentConstants.CUST_ID, custId);
		parameters.put(PaymentConstants.TXN_AMOUNT, amount);
		


		String checkSum =  CheckSumServiceHelper.getCheckSumServiceHelper().genrateCheckSum(env.getProperty(PaymentConstants.MERCHANT_KEY), parameters);


		StringBuilder outputHtml = new StringBuilder();
		outputHtml.append("<!DOCTYPE html PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>");
		outputHtml.append("<html>");
		outputHtml.append("<head>");
		outputHtml.append("<title>Merchant Check Out Page</title>");
		outputHtml.append("</head>");
		outputHtml.append("<body>");
		outputHtml.append("<center><h1>Please do not refresh this page...</h1></center>");
		outputHtml.append("<form method='post' action='"+ env.getProperty(PaymentConstants.PAYTM_URL) +"' name='f1'>");
		outputHtml.append("<table border='1'>");
		outputHtml.append("<tbody>");

		for(Map.Entry<String,String> entry : parameters.entrySet()) {
			String key = entry.getKey();
			String value = entry.getValue();
			System.out.println(key + ":" + value);
			outputHtml.append("<input type='hidden' name='"+key+"' value='" +value+"'>");	
		}	  
			  


		outputHtml.append("<input type='hidden' name='CHECKSUMHASH' value='"+checkSum+"'>");
		outputHtml.append("</tbody>");
		outputHtml.append("</table>");
		outputHtml.append("<script type='text/javascript'>");
		outputHtml.append("document.f1.submit();");
		outputHtml.append("</script>");
		outputHtml.append("</form>");
		outputHtml.append("</body>");
		outputHtml.append("</html>");
		response.setContentType("text/html;charset=UTF-8");
		response.getWriter().write(outputHtml.toString());
    }
	
}
