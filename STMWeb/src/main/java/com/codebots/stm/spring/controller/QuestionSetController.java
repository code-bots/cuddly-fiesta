package com.codebots.stm.spring.controller;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codebots.stm.spring.dao.QuestionSetDao;
import com.codebots.stm.spring.exception.STMAPIException;
import com.codebots.stm.spring.model.Course;
import com.codebots.stm.spring.model.QuestionSet;

@RestController
@RequestMapping("/rest/api")
public class QuestionSetController {
	
	@Autowired
	private QuestionSetDao questionsetDao;
	
	/*@PostMapping("questionset/{tutorId}/{subjectId}")
	public ResponseEntity<?> saveQueSet(@PathVariable("tutor_id") UUID tutor_id,@PathVariable("subjectId") UUID subjectId,@RequestBody QuestionSet queSet) throws STMAPIException
	{
		try
		{	
			QuestionSet ques = questionsetDao.save(queSet, tutor_id,subjectId);
			return ResponseEntity.ok().body(ques);
		}catch(Exception e)
		{
			throw new STMAPIException("0026", new String[] {e.getMessage()}, e);
		}
	}
	*/
	@GetMapping("/questionSet/list")
	public ResponseEntity<List<QuestionSet>> listQuestionSet() throws STMAPIException
	{
		try
		{
		List<QuestionSet> list = questionsetDao.list();
		return ResponseEntity.ok().body(list);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0027", new String[] {e.getMessage()}, e);
		}
		
	}
	
	@GetMapping("/questionSet/{id}")
	public ResponseEntity<?> getOne(@PathVariable("id") UUID id) throws STMAPIException
	{
		try
		{
			QuestionSet qset = questionsetDao.getByQsetId(id);
			return ResponseEntity.ok().body(qset);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0028", new String[] {e.getMessage()}, e);
		}
	}
	
	


}
