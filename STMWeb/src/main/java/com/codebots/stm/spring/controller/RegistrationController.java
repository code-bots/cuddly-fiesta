package com.codebots.stm.spring.controller;


import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codebots.stm.communication.EmailNotifier;
import com.codebots.stm.communication.SendSMS;
import com.codebots.stm.reqres.ForgetPasswordEmailBody;
import com.codebots.stm.reqres.NewPasswordReqBody;
import com.codebots.stm.reqres.ResponseObject;
import com.codebots.stm.reqres.StudentRequestBody;
import com.codebots.stm.reqres.TutorRequestBody;
import com.codebots.stm.spring.dao.StudentDao;
import com.codebots.stm.spring.dao.TutorDao;
import com.codebots.stm.spring.dao.UserDao;
import com.codebots.stm.spring.exception.STMAPIException;
import com.codebots.stm.spring.model.Student;
import com.codebots.stm.spring.model.Tutor;
import com.codebots.stm.spring.model.User;

@RestController
@RequestMapping("/rest/registration")
public class RegistrationController {

	@Autowired
	private StudentDao studentDao;
	
	@Autowired
	private TutorDao tutordao;
	
	@Autowired
	private UserDao userDao;
	
	public static String EMAIL_CONTENT = "Dear @FirstName@ @LastName@,	<BR><BR>"
			+ "Welcome! You've entered @EMAIL@ as the contact email address for your profile."
			+ " In order to verify your email address, all you have to do is simply provide OTP after login:<BR>"
			+ "<b>Your OTP is :</b> @OTP@<BR>"
			+ "Please <a href=\"http://35.185.51.218:8080/dist/\">click here</a> to login and verify verify your email address<BR><BR>"
			+ "Please note, this OTP will expire in 24 hours. After this, you will need to resubmit the request to add/change your email address.<BR>"
			+ "<b>Why did I get this email?</b><BR>"
			+ "You receive this email whenever you provide a new email address or change your email address in your account.<BR>"
			+ "If you did not initiate this request, please contact the customer care center at 123 456 789.<BR>" 
			+ "This is an automated email for providing important information. Therefore, please do not reply to this email.<BR>"
			+ "In case you want to get in touch with us, you can email us at customercare@sss.in or call us directly at 123 456 789.<BR><BR>"
			+ "Thanks,<BR>"
			+ "Team";
	
	public static String SMS_CONTENT ="Dear @FIRSTNAME@, Please verify your mobile with AtoZ portal." + 
			  "Your Mobile verification code is @OTP@. Valid for 30 minutes.";

	
	@PostMapping("/sms")
	public ResponseEntity<?> sendSSS() 
	{
		SendSMS obj = new SendSMS();
		String test =  obj.sendSms("hello","7405450312");
		return ResponseEntity.ok().body(test);
	
	}
	
	@PostMapping("/resend/{type}/{userId}")
	public ResponseEntity<?> resendVerificationDetails(@PathVariable("type") String type, @PathVariable("userId") UUID id) throws STMAPIException
	{
		
			userDao.resendVerificationDetails(type, id);
			return  ResponseEntity.ok().build();
		
	}
	
	@PostMapping("/student")
	public ResponseEntity<?> saveStudent(@RequestBody StudentRequestBody student) throws STMAPIException
	{    
		Student studentObject = null;
		try
		{
			studentObject = studentDao.save(student);
		}catch(Exception e)
		{
			throw new STMAPIException("0001", new String[] {e.getMessage()}, e);
		}
		try{
			String emailbody = EMAIL_CONTENT.replace("@FirstName@", studentObject.getFirst_name())
					.replace("@LastName@", studentObject.getLast_name())
					.replace("@EMAIL@",studentObject.getEmail())
					.replace("@OTP@", studentObject.getUser_id().getEmail_OTP());
			EmailNotifier.sendEmailNotification(studentObject.getEmail(), "Please verify your email", emailbody);
			
			String smsBody = SMS_CONTENT.replace("@FIRSTNAME@", studentObject.getFirst_name())
					.replace("@OTP@", studentObject.getUser_id().getSms_OTP());
			SendSMS.sendSms(smsBody, studentObject.getMobile_number());
			
		}catch(Exception e)
		{
			//eat exception
		}
		return ResponseEntity.ok().body(studentObject);
	}
	
	@PostMapping("/tutor")
	public ResponseEntity<Tutor> saveTutor(@RequestBody TutorRequestBody tutor) throws STMAPIException
	{
		Tutor tut = null;
		try
		{
			tut = tutordao.save(tutor);
			
		}catch(Exception e)
		{
			throw new STMAPIException("0002", new String[] {e.getMessage()},e);
		}
		
		try{
			String emailbody = EMAIL_CONTENT.replace("@FirstName@", tut.getFirst_name())
					.replace("@LastName@", tut.getLast_name())
					.replace("@EMAIL@",tut.getEmail())
					.replace("@OTP@", tut.getUser_id().getEmail_OTP());
			EmailNotifier.sendEmailNotification(tut.getEmail(), "Please verify your email", emailbody);
			String smsBody = SMS_CONTENT.replace("@FIRSTNAME@", tut.getFirst_name())
					.replace("@OTP@", tut.getUser_id().getSms_OTP());
			SendSMS.sendSms(smsBody, tut.getMobile_number());
		}catch(Exception e)
		{
			//eat exception;
		}
		return ResponseEntity.ok().body(tut);
	}
	

	@PostMapping("/forget")
	public ResponseEntity<?> forggateEmailOTP(@RequestBody ForgetPasswordEmailBody reqBody) throws STMAPIException 
	{
		try
		{
			ResponseObject obj  = userDao.sendForgetPasswordDetails(reqBody);
			return ResponseEntity.ok().body(obj);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0091", new String[] { e.getMessage() }, e);
		}
		
	}
	
	@PutMapping("/change/password")
	public ResponseEntity<?> changePassword(@RequestBody NewPasswordReqBody reqBody) throws STMAPIException
	{
		try
		{
			userDao.enterNewPassword(reqBody);
			ResponseObject obj = new ResponseObject();
			obj.setMessage("Successfully Changed Password");
			return ResponseEntity.ok().body(obj);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0092", new String[] { e.getMessage() }, e);
		}
		
	}
	
}
