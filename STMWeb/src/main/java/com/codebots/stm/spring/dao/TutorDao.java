package com.codebots.stm.spring.dao;

import java.util.List;
import java.util.UUID;

import com.codebots.stm.reqres.Pagination;
import com.codebots.stm.reqres.PasswordChangeRequestBody;
import com.codebots.stm.reqres.PrintResponceBodyTutor;
import com.codebots.stm.reqres.TutorRequestBody;
import com.codebots.stm.spring.exception.STMAPIException;
import com.codebots.stm.spring.model.Tutor;

public interface TutorDao {

	Tutor save(TutorRequestBody tutor) throws STMAPIException;
	Pagination list(Integer status,int pageNumber,int pageSize,String type,String feild);
	Tutor update(Tutor tutor,UUID tutor_id);
	Tutor get(String userName);
	void resetPassword(PasswordChangeRequestBody passchange,UUID tutor_id)throws STMAPIException;
	Tutor updateStatus(UUID tutorId,int status);
	List<PrintResponceBodyTutor> printTutorGrid(Integer status);
}
