package com.codebots.stm.spring.dao;


import java.util.List;
import java.util.UUID;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Repository;

import com.codebots.stm.spring.model.Academics;
import com.codebots.stm.spring.model.Student;


@Repository
@PropertySource("classpath:application.properties")
@Transactional
public class AcademicsDaoImpl implements AcademicsDao {

	@Autowired
	private SessionFactory sessionfactory;

	
	
	@Override
	public Academics save(Academics academic, UUID student_id) {
		Student student = sessionfactory.getCurrentSession().get(Student.class, student_id);
		academic.setStudent_id(student);
		sessionfactory.getCurrentSession().save(academic);
		return academic;
	}

	@Override
	public List<Academics> list() {
		Session session = sessionfactory.getCurrentSession();
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Academics> cq = cb.createQuery(Academics.class);
		Root<Academics> root = cq.from(Academics.class);
		cq.select(root);
		Query<Academics> query = session.createQuery(cq);
		return query.getResultList();
	}
	
	
	@Override
	public List<Academics> getbyId(UUID student_id) {
		Session session = sessionfactory.getCurrentSession();
		Query query = sessionfactory.getCurrentSession().createQuery(" from " + Academics.class.getName() + " where student_id.student_id = :student_id order by from_year desc");
		query.setParameter("student_id", student_id);
		List<Academics> list = query.list();
		return list;
	}

	
	@Override
	public Academics getSpecific(UUID student_id, UUID academics_id) {
		Session session = sessionfactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Academics.class);
		criteria.add(Restrictions.eq("student_id", student_id));
		criteria.add(Restrictions.eq("academic_id", academics_id));
		Academics academics = (Academics) criteria.uniqueResult();
		return academics;
	}

	
	@Override
	public int delete(UUID student_id, UUID academics_id) {
		Session session = sessionfactory.getCurrentSession();
		Query query = session.createQuery("delete from academics where academic_id = :academic and student_id = :student");
		query.setParameter("academic", academics_id);
		query.setParameter("student", student_id);
		int result = query.executeUpdate();
		return result;
	}


	
	@Override
	public Academics update(UUID student_id,UUID academic_id, Academics academics) {
		Session session = sessionfactory.getCurrentSession();
		String hql = "from academics where academic_id = :academic and student_id.student_id = :student ";
		Query query = session.createQuery(hql);
		query.setParameter("academic", academic_id);
		query.setParameter("student", student_id);
		Academics ad = (Academics) query.uniqueResult();
		ad.setDegree(academics.getDegree());
		ad.setSchool(academics.getSchool());
		ad.setGrade(academics.getGrade());
		ad.setFrom_year(academics.getFrom_year());
		ad.setTo_year(academics.getTo_year());
		ad.setDescription(academics.getDescription());
		session.saveOrUpdate(ad);
		return ad;
	}

}
