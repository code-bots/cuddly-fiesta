package com.codebots.stm.spring.dao;

import java.util.List;
import java.util.UUID;

import javax.transaction.Transactional;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Repository;

import com.codebots.stm.spring.model.QuestionSet;
import com.codebots.stm.spring.model.Questions;
import com.codebots.stm.spring.model.Student;
import com.codebots.stm.spring.model.StudentQuestionAnswer;



@Repository
@PropertySource("classpath:application.properties")
@Transactional
public class QuestionQADaoImp  implements QuestionQADao{

	
	@Autowired
	private SessionFactory sessionfactory;
	
	
	
	@Override
	public StudentQuestionAnswer save(UUID queId, UUID stu_id, StudentQuestionAnswer stuqa) {
		Session session = sessionfactory.getCurrentSession();
		Student stu = session.byId(Student.class).load(stu_id);
		Questions que = session.byId(Questions.class).load(queId);
		stuqa.setQuestion_id(que);
		stuqa.setStudent_id(stu);
		stuqa.setStatus(0);
		sessionfactory.getCurrentSession().save(stuqa);
		return stuqa;
	}

	
	@Override
	public List<StudentQuestionAnswer> list(UUID student_id) {
		Session session = sessionfactory.getCurrentSession();
		String hql = "from " + StudentQuestionAnswer.class.getName() +  " where student_id.student_id = :st";
		Query query = session.createQuery(hql);
		query.setParameter("st", student_id);
		List<StudentQuestionAnswer> list = query.list();
		return list;
	}

	
	@Override
	public List<StudentQuestionAnswer> listOfSelectedAnswer(UUID studentId, UUID questionSetId) {
		Session session = sessionfactory.getCurrentSession();
		String hql = "select qa from " + StudentQuestionAnswer.class.getName() + " qa where qa.question_id.question_set_id.id = :queSetId and qa.student_id.student_id = :studentId and qa.status = 0";
		Query query = session.createQuery(hql);
		query.setParameter("queSetId", questionSetId);
		query.setParameter("studentId", studentId);
		List<StudentQuestionAnswer> list = query.list();
		return list;
	}

	
	@Override
	public void changeStatus(UUID questionSetId, UUID studentId) {
		Session session = sessionfactory.getCurrentSession();
		//String hql = "update " + StudentQuestionAnswer.class.getName() + " qa SET qa.status  = 1 where qa.question_id.question_set_id.id = :queSetId and qa.student_id.student_id = :studentId ";
		
		//String hql = "update " + StudentQuestionAnswer.class.getName() + " qa SET qa.status = 1 where qa.question_id.question_set_id.id = :setId and student_id.student_id = :stuId";
//		String hql = "update " + StudentQuestionAnswer.class.getName() + " qa set qa.status = 1,"  
//							   + Questions.class.getName() + " q,"
//							   + QuestionSet.class.getName() + " qs,"
//							   + Student.class.getName() + " stu "
//							   + " where qa.question_id = q.id and q.question_set_id = qs.id and qa.student_id = stu.student_id "
//							   + " and stu.student_id = :student and qs.id = :questionSet";
		
		
		
//		String sql = "UPDATE student_que_ans qa  INNER JOIN questions q ON qa.question_id  = q.id INNER JOIN  question_set qs ON  q.question_set_id = qs.id  INNER JOIN student stu SET qa.status = 1  WHERE  q.question_set_id = qs.id AND qa.student_id = stu.student_id AND qs.id = :questionSet AND stu.student_id = :student";
//		
//		session.createStoredProcedureQuery(sql).setParameter("questionSet", questionSetId).setParameter("student", studentId).executeUpdate();
		List<StudentQuestionAnswer> list = listOfSelectedAnswer(studentId,questionSetId);
		for(StudentQuestionAnswer ans : list)
		{
			//Transaction tra = session.beginTransaction();
			ans.setStatus(1);
			session.saveOrUpdate(ans);
			//tra.commit();
		}
		
	}
	
	
	
	
}
