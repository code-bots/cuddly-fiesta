package com.codebots.stm.spring.controller;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codebots.stm.spring.dao.QuestionDao;
import com.codebots.stm.spring.exception.STMAPIException;
import com.codebots.stm.spring.model.QuestionSet;
import com.codebots.stm.spring.model.Questions;

@RestController
@RequestMapping("/rest/api")
public class QuestionControler {

	@Autowired
	private QuestionDao quedao;
	
	
	/*@PostMapping("/{questionSet_id}/question")
	public ResponseEntity<?> saveQueSet(@PathVariable("questionSet_id") UUID quesetId,@RequestBody Questions que) throws STMAPIException
	{
		try
		{	
			Questions questions = quedao.save(que, quesetId);
			return ResponseEntity.ok().body(questions);
		}catch(Exception e)
		{
			throw new STMAPIException("0029", new String[] {e.getMessage()}, e);
		}
	}*/
	
	

	@GetMapping("/question/list")
	public ResponseEntity<List<Questions>> listCourse() throws STMAPIException
	{
		try
		{
		List<Questions> list = quedao.list();
		return ResponseEntity.ok().body(list);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0030", new String[] {e.getMessage()}, e);
		}
		
	}
	
	
	@GetMapping("/question/{id}")
	public ResponseEntity<?> getOne(@PathVariable("id") UUID id) throws STMAPIException
	{
		try
		{
			Questions que = quedao.getById(id);
			return ResponseEntity.ok().body(que);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0031", new String[] {e.getMessage()}, e);
		}
	}
}
