package com.codebots.stm.spring.dao;

import java.util.List;

import com.codebots.stm.spring.model.Demo;

public interface DemoDao {

	Demo save(Demo demo);
	List<Demo> list();
	
}
