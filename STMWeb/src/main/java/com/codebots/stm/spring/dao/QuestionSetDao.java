package com.codebots.stm.spring.dao;

import java.util.List;
import java.util.UUID;

import com.codebots.stm.spring.model.QuestionSet;

public interface QuestionSetDao {

	QuestionSet save(QuestionSet questionSet,UUID tutor_id,UUID subject_id);
	List<QuestionSet> list();
	List<QuestionSet> getById(UUID tutor_id);
	List<QuestionSet> getBySubject(UUID subject_id);
	QuestionSet updateQset(QuestionSet queSet,UUID quSetId);
	QuestionSet getByQsetId(UUID questionSetId);
	void deleteQuestionSet(UUID questionSetId);
}
