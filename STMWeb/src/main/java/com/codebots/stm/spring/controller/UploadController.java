package com.codebots.stm.spring.controller;
import java.io.IOException;
import java.util.Base64;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.codebots.stm.spring.exception.STMAPIException;

@PropertySource("classpath:application.properties")
@Controller
public class UploadController {

	@Autowired org.springframework.core.env.Environment env;
	

    @GetMapping("/")
    public String index() {
        return "This is API for STM project";
    }

    @RequestMapping(value = "/tobase64", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<String> singleFileUpload(@RequestParam("file") MultipartFile file,
                                   RedirectAttributes redirectAttributes) throws STMAPIException {

        if (file.isEmpty()) {
            throw new STMAPIException("0037", new String[] {"File is empty"}, null);
        }

        try {
            // Get the file and save it somewhere
            byte[] bytes = file.getBytes();
            return new ResponseEntity<String>(new String(Base64.getEncoder().encode(bytes)), HttpStatus.OK);
        } catch (IOException e) {
            throw new STMAPIException("0037", new String[] {e.getMessage()}, e);
        }

    }
}