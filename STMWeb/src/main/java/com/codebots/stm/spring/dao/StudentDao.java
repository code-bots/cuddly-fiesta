package com.codebots.stm.spring.dao;


import java.util.List;
import java.util.UUID;

import com.codebots.stm.communication.PrintResponceBodyStudent;
import com.codebots.stm.reqres.Pagination;
import com.codebots.stm.reqres.PasswordChangeRequestBody;
import com.codebots.stm.reqres.StudentRequestBody;
import com.codebots.stm.spring.exception.STMAPIException;
import com.codebots.stm.spring.model.Student;

public interface StudentDao {

	Student get(UUID id);
	Pagination list(int pageNumber,int pageSize,String type,String feild);
	Student update(UUID id,Student student);
	void delete(UUID id);
	void resetPassword(PasswordChangeRequestBody passchange,UUID student_id)throws STMAPIException;
	String getUsername(UUID student_id);
	Student save(StudentRequestBody student)
			throws STMAPIException;
	Student get(String userName);
	List<PrintResponceBodyStudent> printStudentGrid(UUID tutorId);
	
}
