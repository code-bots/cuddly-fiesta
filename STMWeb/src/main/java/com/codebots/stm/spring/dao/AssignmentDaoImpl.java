package com.codebots.stm.spring.dao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Repository;

import com.codebots.stm.reqres.Pagination;
import com.codebots.stm.reqres.ResponseObject;
import com.codebots.stm.spring.model.AssignmentStatus;
import com.codebots.stm.spring.model.AssignmentStudent;
import com.codebots.stm.spring.model.Assignments;
import com.codebots.stm.spring.model.Subject;
import com.codebots.stm.spring.model.Tutor;
import com.codebots.stm.spring.model.TutorInstitute;


@Repository
@PropertySource("classpath:application.properties")
@PropertySource("classpath:mailbox.properties")
@Transactional
public class AssignmentDaoImpl implements AssignmentDao {

	@Autowired
	private SessionFactory sessionfactory;
	
	@PreAuthorize("hasAuthority('TUTOR') or hasAuthority('STUDENT')")
	@Override
	public List<AssignmentStatus> getStatusMetaData(int type)
	{
		Query query = sessionfactory.getCurrentSession().createQuery(" from " + AssignmentStatus.class.getName() + " where type=:type");
		query.setParameter("type", type);
		return query.list();
	}
	
	
	@Override
	public Assignments save(Assignments assignment,UUID tutor_id, UUID subject_id,UUID institute_id) {
		
		Session session = sessionfactory.getCurrentSession();
		Tutor tutor = (Tutor) session.get(Tutor.class,tutor_id);
		if(subject_id!=null){
			Subject subject = (Subject)session.get(Subject.class,subject_id);
			assignment.setCoure_id(subject.getCourse_id());
			assignment.setSubject_id(subject);	
		}
		assignment.setTutor_id(tutor);
		TutorInstitute tutIn = session.get(TutorInstitute.class, institute_id);
		assignment.setTutorInstituteId(tutIn);
		
		sessionfactory.getCurrentSession().save(assignment);
		return assignment;
	}

	@Override
	public Assignments refreshObject(Assignments assignment)
	{
		sessionfactory.getCurrentSession().refresh(assignment);
		return assignment;
	}
	
	
	@Override
	public List<Assignments> list() {
		Session session = sessionfactory.getCurrentSession();		
		String hql = "select ass from " + Assignments.class.getName() + " ass," 
							 		   + Tutor.class.getName() +" tut where "
							 		   		+ " ass.tutor_id = tut.tutor_id and tut.approvalFlag = 1";
		Query query = session.createQuery(hql);
		List<Assignments> list = query.list();
		return list;	
	}

	@Override
	public List<Assignments> getComplated() {
		// TODO Auto-generated method stub
		return null;
	}

	
	@Override
	public List<Assignments> getFilterByDate(Date from_date, Date to_date) {
		Session session = sessionfactory.getCurrentSession();
		String hql = "from " +  Assignments.class.getName() + " where creation_date between :from_date and :to_date";
		Query query = session.createQuery(hql);
		query.setParameter("from_date",from_date);
		query.setParameter("to_date", to_date);
		List<Assignments> list = query.list();
		return list;
	}

	
	@Override
	public List<Assignments> filterByType(Integer type) {
		Session session = sessionfactory.getCurrentSession();
		String hql = "from assignments where type = :types";
		Query query = session.createQuery(hql);
		query.setParameter("types", type);
		List<Assignments> list  = query.list();
		return list;
	}

	
	@Override
	public List<Assignments> getByTutor(UUID tutor_id,Date fromdate,Date toDate) {
		
		Session session = sessionfactory.getCurrentSession();
		if(fromdate == null && toDate == null)
		{
			String  hql = "from " + Assignments.class.getName() + 
					" where tutor_id.tutor_id = :tutor and tutor_id.approvalFlag = 1";
			Query query = session.createQuery(hql);
			query.setParameter("tutor", tutor_id);
		
			List<Assignments> list  = query.list();
			return list;
		}
		else
		{
			String  hql = "from " + Assignments.class.getName() + 
					" where tutor_id.tutor_id = :tutor and tutor_id.approvalFlag = 1 and creation_date >= :fromdate and creation_date <= :toDate";
			Query query = session.createQuery(hql);
			query.setParameter("tutor", tutor_id);
			query.setParameter("fromdate", fromdate);
			query.setParameter("toDate", toDate);
			List<Assignments> list  = query.list();
			return list;
		}
		
		
	}

	
	@Override
	public List<Assignments> filterByStatus(Integer status) {
		Session session = sessionfactory.getCurrentSession();
		String  hql = "from " + Assignments.class.getName() + " where status = :status";
		Query query = session.createQuery(hql);
		query.setParameter("status", status);
		List<Assignments> list  = query.list();
		return list;
	}

	
	@Override
	public List<Assignments> getBytutorStatus(UUID tutor_id, Integer status,Date fromDate,Date toDate) {
		if(fromDate == null && toDate == null)
		{
			Session session = sessionfactory.getCurrentSession();
			String  hql = "from " + Assignments.class.getName() + " where status = :status and tutor_id.tutor_id = :tutor and tutor_id.approvalFlag = 1";
			Query query = session.createQuery(hql);
			query.setParameter("status", status);
			query.setParameter("tutor", tutor_id);
			List<Assignments> list  = query.list();
			return list;
		}
		else
		{
			Session session = sessionfactory.getCurrentSession();
			String  hql = "from " + Assignments.class.getName() + " where status = :status and tutor_id.tutor_id = :tutor and tutor_id.approvalFlag = 1  and creation_date >= :fromDate and creation_date <= :toDate";
			Query query = session.createQuery(hql);
			query.setParameter("status", status);
			query.setParameter("tutor", tutor_id);
			query.setParameter("fromDate", fromDate);
			query.setParameter("toDate", toDate);
			List<Assignments> list  = query.list();
			return list;
		}
	
	}

	@PreAuthorize("hasAuthority('TUTOR') or hasAuthority('STUDENT')")
	@Override
	public List<Assignments> getByStudentStatus(UUID student_id, Integer status,Date fromDate,Date toDate) {
		if(fromDate == null && toDate == null)
		{
			Session session = sessionfactory.getCurrentSession();
			String  hql = "from " + AssignmentStudent.class.getName() + " where status = :status and student_id.student_id = :student";
			Query query = session.createQuery(hql);
			query.setParameter("status", status);
			query.setParameter("student", student_id);
			List<Assignments> list  = query.list();
			return list;	
		}
		else
		{
			Session session = sessionfactory.getCurrentSession();
			String  hql = "from " + AssignmentStudent.class.getName() + " where status = :status and student_id.student_id = :student and completion_date >= :fromDate and completion_date <= :toDate";
			Query query = session.createQuery(hql);
			query.setParameter("status", status);
			query.setParameter("student", student_id);
			query.setParameter("fromDate", fromDate);
			query.setParameter("toDate", toDate);
			List<Assignments> list  = query.list();
			return list;
		}
		
	}

	
	@Override
	public Assignments getById(UUID id) {
		Session session = sessionfactory.getCurrentSession();
		Assignments ass = session.byId(Assignments.class).load(id); 
		return ass;
	}

	
	@Override
	public Pagination getStuByTutAss(UUID tutor_id,int pageNumber,int pageSize,String type,String feild) {
		
		Pagination page = new Pagination();
		
		
		Session session = sessionfactory.getCurrentSession();
		String hql = "select stu FROM student stu,assignment_student ass_stu,assignments ass,tutor tut WHERE ass_stu.assignment_id = ass.id AND tut.tutor_id = ass.tutor_id AND ass_stu.student_id = stu.student_id AND tut.tutor_id = :tutor and tut.approvalFlag = 1";
		Query query = session.createQuery(hql);
		query.setParameter("tutor",tutor_id);
		if(pageNumber == 1)
		{		
			query.setFirstResult(pageNumber-1);
			query.setMaxResults(pageSize);
		}
		else
		{
			int startRow = (pageNumber - 1) * pageSize;
			query.setFirstResult(startRow);
			query.setMaxResults(pageSize);
		}
		List<Object> list = query.list();		
		page.setListOfObjects(list);
		page.setCurrentPage(pageNumber);
		page.setPageSize(pageSize);
		
		
		/*String countSQL = "select COUNT(stu) FROM student stu,assignment_student ass_stu,assignments ass,tutor tut WHERE ass_stu.assignment_id = ass.id AND tut.tutor_id = ass.tutor_id AND ass_stu.student_id = stu.student_id AND tut.tutor_id = :tutor group by stu";
		Query countQuery = session.createQuery(countSQL);
		countQuery.setParameter("tutor",tutor_id);
		long result = (Long) countQuery.list().get(0);*/
		
		page.setTotalRecord((long)list.size());
		return page;
	}

	
	@Override
	public Pagination getStuByTutAssSingleObject(UUID tutor_id, int pageNumber, int pageSize, String type,
			String feild) {
			Pagination page = new Pagination();
		
		
		Session session = sessionfactory.getCurrentSession();
		String hql = "select stu FROM student stu,assignment_student ass_stu,assignments ass,tutor tut WHERE ass_stu.assignment_id = ass.id AND tut.tutor_id = ass.tutor_id AND ass_stu.student_id = stu.student_id AND tut.tutor_id = :tutor and tut.approvalFlag = 1 group by stu.student_id";
		Query query = session.createQuery(hql);
		query.setParameter("tutor",tutor_id);
		if(pageNumber == 1)
		{		
			query.setFirstResult(pageNumber-1);
			query.setMaxResults(pageSize);
		}
		else
		{
			int startRow = (pageNumber - 1) * pageSize;
			query.setFirstResult(startRow);
			query.setMaxResults(pageSize);
		}
		List<Object> list = query.list();		
		page.setListOfObjects(list);
		page.setCurrentPage(pageNumber);
		page.setPageSize(pageSize);
		
		
		/*String countSQL = "select COUNT(stu) FROM student stu,assignment_student ass_stu,assignments ass,tutor tut WHERE ass_stu.assignment_id = ass.id AND tut.tutor_id = ass.tutor_id AND ass_stu.student_id = stu.student_id AND tut.tutor_id = :tutor group by stu";
		Query countQuery = session.createQuery(countSQL);
		countQuery.setParameter("tutor",tutor_id);
		long result = (Long) countQuery.list().get(0);*/
		
		page.setTotalRecord((long)list.size());
		return page;
	}

	
	@Override
	public Assignments changeStatusOfAssignment(UUID assignmentid) {
		Session session = sessionfactory.getCurrentSession();
		String hql = "update " + Assignments.class.getName() + " set status = 5 where id = :assId";
		Query query = session.createQuery(hql);
		query.setParameter("assId", assignmentid);
		query.executeUpdate();
		return session.get(Assignments.class, assignmentid);
	}

	
	@Override
	public List<Assignments> dueDatedAssignments() throws ParseException {
		Session session = sessionfactory.getCurrentSession();
		Date currentDate = new Date();
		SimpleDateFormat dateFormate = new SimpleDateFormat("yyyy-MM-dd");
		String cDate = dateFormate.format(currentDate);
		Date date1 = dateFormate.parse(cDate);
				
		String hql = "from " + Assignments.class.getName() + " where due_date < sysdate()";
		Query query = session.createQuery(hql);
		List<Assignments> list = query.list();
		
		return list;
	}

	
	@Override
	public Long countOfEnrolledStudent(UUID assignmentId) {
		
		Session session = sessionfactory.getCurrentSession();
		String hqlForCount = "select count(stuass.student_id) from " + AssignmentStudent.class.getName() + " stuass "
				+ "	where stuass.assignment_id.id = :assignmentId group by stuass.assignment_id.id";
		
		Query query = session.createQuery(hqlForCount);
		query.setParameter("assignmentId", assignmentId);
		Long count = (Long) query.uniqueResult();
			
		return count;	
		}

	
	@Override
	public Long countOfStatusComplated(UUID assignmentId) {
		Session session = sessionfactory.getCurrentSession();
		String hqlForStatusCount = "select count(stuass.status) from " + AssignmentStudent.class.getName() + " stuass "
				+ " where stuass.assignment_id.id = :assignmentId and stuass.status = 3 group by stuass.assignment_id.id";
		
		Query countQuery = session.createQuery(hqlForStatusCount);
		countQuery.setParameter("assignmentId", assignmentId);
		Long statusCount = (Long) countQuery.uniqueResult();
		
		return statusCount;
	}

	
	@Override
	public void updateStatus() {
		
		List<Assignments> listOfAssignments = list();
		
		for(Assignments ass : listOfAssignments)
		{
			Long countOfEnrolled = countOfEnrolledStudent(ass.getId());
			Long countOfComplated = countOfStatusComplated(ass.getId());
			
			if(countOfEnrolled == null || countOfComplated == null)
			{
				
			}
			else if(countOfEnrolled == countOfComplated)
			{
				Session session = sessionfactory.getCurrentSession();
				Query query = session.createQuery("update " + Assignments.class.getName() + " set status = 6 where id = :assignmentId");
				query.setParameter("assignmentId", ass.getId());
				query.executeUpdate();
			}
		}
		
	}

	
	
}
