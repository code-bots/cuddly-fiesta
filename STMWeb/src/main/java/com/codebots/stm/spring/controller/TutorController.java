package com.codebots.stm.spring.controller;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codebots.stm.communication.PrintResponceBodyStudent;
import com.codebots.stm.reqres.Pagination;
import com.codebots.stm.reqres.PasswordChangeRequestBody;
import com.codebots.stm.reqres.PrintResponceBodyTutor;
import com.codebots.stm.reqres.ResponseObject;
import com.codebots.stm.spring.dao.AssignmentDao;
import com.codebots.stm.spring.dao.AssignmentStudentDao;
import com.codebots.stm.spring.dao.QuestionSetDao;
import com.codebots.stm.spring.dao.TutorDao;
import com.codebots.stm.spring.dao.TutorInstituteDao;
import com.codebots.stm.spring.exception.STMAPIException;
import com.codebots.stm.spring.model.AssignmentStudent;
import com.codebots.stm.spring.model.Assignments;
import com.codebots.stm.spring.model.Tutor;
import com.codebots.stm.spring.model.TutorInstitute;

@RestController
@RequestMapping("/rest/api")
public class TutorController {

	@Autowired
	private TutorDao tutordao;

	@Autowired
	private TutorInstituteDao tutInstDao;
	
	@Autowired
	private QuestionSetDao queSetDao;
	
	
	@Autowired
	private AssignmentDao assDao;
	
	@Autowired
	private AssignmentStudentDao assStuDao;
	
	@GetMapping("/tutorgrid")
	public ResponseEntity<Pagination> listTutor( @RequestParam(value = "status",required = false) Integer status,
												 @RequestParam(value = "page") Integer pageNumber,
			 									 @RequestParam(value = "size") Integer pageSize,
			 									 @RequestParam(value = "type",required = false) String type,
			 									 @RequestParam(value = "feild",required = false) String feild) throws STMAPIException {
		try {
			Pagination page = tutordao.list(status,pageNumber,pageSize,type,feild);
			return ResponseEntity.ok().body(page);
		} catch (Exception e) {
			throw new STMAPIException("0009", new String[] { e.getMessage() }, e);
		}
	}
	
	@GetMapping("/tutor/{tutor_id}/assignments")
	public ResponseEntity<List<Assignments>> listTutorssignmentByTutor(@PathVariable("tutor_id") UUID tutor_id,
																	   @RequestParam(value = "from",required = false) @DateTimeFormat(pattern="yyyy-MM-dd") Date from,
																	   @RequestParam(value = "to",required = false) @DateTimeFormat(pattern="yyyy-MM-dd") Date to) throws STMAPIException {
		try {
			List<Assignments> list = assDao.getByTutor(tutor_id,from,to); 
			return ResponseEntity.ok().body(list);
		} catch (Exception e) {
			throw new STMAPIException("0009", new String[] { e.getMessage() }, e);
		}
	}
	
	
	@GetMapping("/tutor/{tutor_id}/assignments/{status}")
	public ResponseEntity<List<Assignments>> assignmentByTutorStatus(@PathVariable("tutor_id") UUID tutor_id,@PathVariable("status")Integer status,@RequestParam(value = "from",required = false) @DateTimeFormat(pattern="yyyy-MM-dd") Date from,
			   @RequestParam(value = "to",required = false) @DateTimeFormat(pattern="yyyy-MM-dd") Date to) throws STMAPIException {
		try {
			List<Assignments> list = assDao.getBytutorStatus(tutor_id, status,from,to);
			return ResponseEntity.ok().body(list);
		} catch (Exception e) {	
			throw new STMAPIException("0009", new String[] { e.getMessage() }, e);
		}
	}
	
	
	@GetMapping("student/assignmentlist/{tutor_id}")
	public ResponseEntity<List<AssignmentStudent>> getStudentList(@PathVariable("tutor_id") UUID tutor_id) throws STMAPIException {
		try {
			List<AssignmentStudent> list = assStuDao.getAllStudentWhoEnrollsByTutor(tutor_id);
			return ResponseEntity.ok().body(list);
		} catch (Exception e) {	
			throw new STMAPIException("0074", new String[] { e.getMessage() }, e);
		}
	}
	
	/*@GetMapping("/questionSet/{tutor_id}")
	public ResponseEntity<List<QuestionSet>> getQuestionSet(@PathVariable("tutor_id") UUID tutor_id)
	{
		List<QuestionSet> list = queSetDao.getById(tutor_id);
		return ResponseEntity.ok().body(list);
	}
	*/

	@PutMapping("/approv/tutor/{tutorId}/{flag}")
	public ResponseEntity<Tutor> changeStatus(@PathVariable("tutorId") UUID tutorId,@PathVariable("flag") Integer flag)
	{
		Tutor tutor = tutordao.updateStatus(tutorId,flag);
		return ResponseEntity.ok().body(tutor);
	}
	
	@PutMapping("/tutor/{tutor_id}")
	public ResponseEntity<Tutor> updateTutor(@PathVariable("tutor_id") UUID id, @RequestBody Tutor tutor)
			throws STMAPIException {
		try {
			tutor.setTutor_id(id);
			tutordao.update(tutor, id);
			return new ResponseEntity<Tutor>(tutor, HttpStatus.OK);
		} catch (Exception e) {
			throw new STMAPIException("0004", new String[] { e.getMessage() }, e);
		}

	}
	
	@PutMapping("tutor/changepassword/{tutor_id}")
	public ResponseEntity<ResponseObject> resetPassword(@PathVariable("tutor_id") UUID tutor_id,@RequestBody PasswordChangeRequestBody passchange) throws STMAPIException
	{		
		try
		{
		tutordao.resetPassword(passchange, tutor_id);
		ResponseObject obj = new ResponseObject();
		obj.setMessage("Update Password SuccessFully");
		return ResponseEntity.ok().body(obj);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0011", new String[] {e.getMessage()}, e);
		}
	}

	
	@PostMapping("institute/{tutor_id}")
	public ResponseEntity<?> saveInstitute(@RequestBody TutorInstitute tutInstitute,@PathVariable("tutor_id") UUID tutor_id) throws STMAPIException
	{
		try
		{
		TutorInstitute tut = tutInstDao.save(tutInstitute, tutor_id);
		return ResponseEntity.ok().body(tut);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0048", new String[] {e.getMessage()}, e);
		}
	}
	
	@PutMapping("institute/{instituteId}")
	public ResponseEntity<?> updateInstitute(@RequestBody TutorInstitute tutInstitute,@PathVariable("instituteId") UUID instituteId) throws STMAPIException
	{
		try
		{
		tutInstitute.setId(instituteId);
		TutorInstitute tutIN = tutInstDao.update(tutInstitute, instituteId);
		return ResponseEntity.ok().body(tutIN);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0049", new String[] {e.getMessage()}, e);
		}
	}
	
	@GetMapping("institute/{tutor_id}")
	public ResponseEntity<List<TutorInstitute>> getByTutorInstitute(@PathVariable("tutor_id") UUID tutor_id) throws STMAPIException
	{
		try
		{
			List<TutorInstitute> list = tutInstDao.getByTutor(tutor_id);
			return ResponseEntity.ok().body(list);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0050", new String[] {e.getMessage()}, e);
		}

	}
	
	@GetMapping("tutor/assignments/student/{tutor_id}")
	public ResponseEntity<Pagination> getStudentAssignmentByTutor(@PathVariable("tutor_id") UUID tutor_id,
																  @RequestParam(value = "page") Integer pageNumber,
																  @RequestParam(value = "size") Integer pageSize,
																  @RequestParam(value = "type",required = false) String type,
																  @RequestParam(value = "feild",required = false) String feild) throws STMAPIException
	{
		try
		{
			Pagination page = assDao.getStuByTutAss(tutor_id,pageNumber,pageSize,type,feild);
			return ResponseEntity.ok().body(page);
		}catch(Exception e)
		{
			throw new STMAPIException("0065", new String[] {e.getMessage()}, e);
		}
		
	}
	@GetMapping("/print/tutor")
	public ResponseEntity<List<PrintResponceBodyTutor>> getPrintBodyTutor(@RequestParam(value = "status",required = false) Integer status) throws STMAPIException
	{
		
		try
		{
			List<PrintResponceBodyTutor> list = tutordao.printTutorGrid(status);
			return  ResponseEntity.ok().body(list);
		}
		catch(Exception e)
		{
			throw new STMAPIException("0094", new String[] {e.getMessage()},e);
		}
	}
	
}
