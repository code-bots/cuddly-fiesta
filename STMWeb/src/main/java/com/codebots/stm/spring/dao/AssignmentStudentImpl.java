package com.codebots.stm.spring.dao;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.UUID;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Repository;

import com.codebots.stm.spring.model.AssignmentStudent;
import com.codebots.stm.spring.model.Assignments;
import com.codebots.stm.spring.model.Student;



@Repository
@PropertySource("classpath:application.properties")
@PropertySource("classpath:mailbox.properties")
@Transactional
public class AssignmentStudentImpl implements AssignmentStudentDao{

	@Autowired
	private SessionFactory sessionfactory;
	
	/*
	@Override
	public AssignmentStudent save(AssignmentStudent assignmentStu,UUID assignmet_id,UUID student_id,UUID instituteID) {
		
	}*/

	
	@Override
	public List<AssignmentStudent> list() {
		Session session = sessionfactory.getCurrentSession();
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<AssignmentStudent> cq = cb.createQuery(AssignmentStudent.class);
		Root<AssignmentStudent> root = cq.from(AssignmentStudent.class);
		cq.select(root);
	    Query<AssignmentStudent> query = session.createQuery(cq);
	    return query.getResultList();
	}

	@PreAuthorize("hasAuthority('TUTOR') or hasAuthority('STUDENT')")
	@Override
	public List<AssignmentStudent> listbyStatus(String status) {
		Session session = sessionfactory.getCurrentSession();
		String hql = "from assignment_student where status = :st";
		Query query = session.createQuery(hql);
		query.setParameter("st", status);
		List<AssignmentStudent> list  = query.list();
		return list;
	}

	
	@Override
	public List<AssignmentStudent> listByStudent(UUID student_id) {
		Session session = sessionfactory.getCurrentSession();
		String hql = "from assignment_student where student_id.student_id = :student";
		Query query = session.createQuery(hql);
		query.setParameter("student", student_id);
		List<AssignmentStudent> list = query.list();
		return list;
	}

	
	@Override
	public AssignmentStudent save(AssignmentStudent assignmentStu, UUID student_id, UUID assignmet_id) {
		Session session = sessionfactory.getCurrentSession();
		Assignments ass = session.get(Assignments.class,assignmet_id);
		
		Student stu = session.get(Student.class,student_id);
		assignmentStu.setAssignment_id(ass);
		assignmentStu.setStudent_id(stu);
		
		sessionfactory.getCurrentSession().save(assignmentStu);
		return assignmentStu;
	}

	
	@Override
	public AssignmentStudent changeSatatus(UUID studentAssignmentId,Integer status) {
		/*String hql = "from " + AssignmentStudent.class.getName() + 
				" where student_id.student_id = :student and assignment_id.id = :assignment";
		Session session = sessionfactory.getCurrentSession();
		Query query = session.createQuery(hql);
		
		query.setParameter("student",studentId);
		query.setParameter("assignment", assignmentId);
		
		AssignmentStudent assStud = (AssignmentStudent) query.uniqueResult();
		
		assStud.setStatus(status);
		
		session.saveOrUpdate(assStud);
		return assStud;*/
		long time = System.currentTimeMillis();
		java.sql.Date date = new java.sql.Date(time);
		Session session = sessionfactory.getCurrentSession();

		
		String hql = "update " + AssignmentStudent.class.getName() + " SET completion_date =  :dd  where id = :id ";
		Query query = session.createQuery(hql);
//		query.setParameter("status", status);
		query.setParameter("dd", date);
		query.setParameter("id", studentAssignmentId);
		query.executeUpdate();
		
		
		
		String hqlStatus = "update " + AssignmentStudent.class.getName() + " set status = :status where id = :id ";
		Query queryStatus = session.createQuery(hqlStatus);
		queryStatus.setParameter("status", status);
		
		queryStatus.setParameter("id", studentAssignmentId);
		queryStatus.executeUpdate();
		AssignmentStudent assStu =session.byId(AssignmentStudent.class).load(studentAssignmentId);
		return assStu;
	}

	@Override
	public List<AssignmentStudent> getAllStudentWhoEnrollsByTutor(UUID tutorID) {
		Session session = sessionfactory.getCurrentSession();
		String hql = "select ass_stu FROM student stu,assignment_student ass_stu,assignments ass,tutor tut WHERE ass_stu.assignment_id = ass.id AND tut.tutor_id = ass.tutor_id AND ass_stu.student_id = stu.student_id AND tut.tutor_id = :tutor";
		Query query = session.createQuery(hql);
		query.setParameter("tutor", tutorID);
		List<AssignmentStudent> list = query.list();
		
		return list;
	}

	
	
}
