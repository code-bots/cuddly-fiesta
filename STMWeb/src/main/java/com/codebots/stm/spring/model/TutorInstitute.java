package com.codebots.stm.spring.model;

import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;

@Entity(name="tutor_institute")
public class TutorInstitute {

	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(name="UUID",strategy="org.hibernate.id.UUIDGenerator")
	private UUID id;
	@OneToOne(targetEntity=Tutor.class,cascade=CascadeType.ALL)
	@JoinColumn(name="tutor_id",referencedColumnName="tutor_id")
	private Tutor tutor_id;
	private String latitude;
	private String longitude;
	private String inst_startTime;
	private String inst_endTime;
	private String weekdays;
	private String name;
	private long fees;
	
	
	public UUID getId() {
		return id;
	}
	public void setId(UUID id) {
		this.id = id;
	}
	
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getInst_startTime() {
		return inst_startTime;
	}
	public void setInst_startTime(String inst_startTime) {
		this.inst_startTime = inst_startTime;
	}
	public String getInst_endTime() {
		return inst_endTime;
	}
	public void setInst_endTime(String inst_endTime) {
		this.inst_endTime = inst_endTime;
	}
	public Tutor getTutor_id() {
		return tutor_id;
	}
	public void setTutor_id(Tutor tutor_id) {
		this.tutor_id = tutor_id;
	}
	public String getWeekdays() {
		return weekdays;
	}
	public void setWeekdays(String weekdays) {
		this.weekdays = weekdays;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getFees() {
		return fees;
	}
	public void setFees(long fees) {
		this.fees = fees;
	}

	
}
