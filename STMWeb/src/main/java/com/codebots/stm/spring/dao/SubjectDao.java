package com.codebots.stm.spring.dao;


import java.util.List;
import java.util.UUID;

import com.codebots.stm.spring.model.Subject;

public interface SubjectDao {

	Subject save(Subject subject, UUID course_id);
	void update(Subject subject,UUID course_id,UUID subject_id);
	List<Subject> list(UUID course_id);
	List<Subject> listAll();
}
