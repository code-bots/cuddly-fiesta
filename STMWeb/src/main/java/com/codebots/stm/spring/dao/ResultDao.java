package com.codebots.stm.spring.dao;

import java.util.List;
import java.util.UUID;

import com.codebots.stm.spring.model.Results;


public interface ResultDao {

	Results save(Results res,UUID stu_id,UUID queset);
	List<Results> list();
	List<Results> listByStudent(UUID student_id);
	List<Results> listyTutor(UUID tutorId);
}
