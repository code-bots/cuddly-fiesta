package com.codebots.stm.spring.model;

import java.util.Date;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity(name="assignment_student")
public class AssignmentStudent {

	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(name="UUID",strategy="org.hibernate.id.UUIDGenerator")
	private UUID id;
	@OneToOne(targetEntity=Assignments.class,cascade=CascadeType.ALL)
	@JoinColumn(name="assignment_id",referencedColumnName="id")
	private Assignments assignment_id;
	@OneToOne(targetEntity=Student.class,cascade=CascadeType.ALL)
	@JoinColumn(name="student_id",referencedColumnName="student_id")
	private Student student_id;	
	private Date completion_date;
	private Integer status;
	private Integer tutor_ratings;
	

	public UUID getId() {
		return id;
	}
	public void setId(UUID id) {
		this.id = id;
	}
	public Assignments getAssignment_id() {
		return assignment_id;
	}
	public void setAssignment_id(Assignments assignment_id) {
		this.assignment_id = assignment_id;
	}
	public Student getStudent_id() {
		return student_id;
	}
	public void setStudent_id(Student student_id) {
		this.student_id = student_id;
	}
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd", timezone="UTC")
	public Date getCompletion_date() {
		return completion_date;
	}
	public void setCompletion_date(Date completion_date) {
		this.completion_date = completion_date;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer getTutor_ratings() {
		return tutor_ratings;
	}
	public void setTutor_ratings(Integer tutor_ratings) {
		this.tutor_ratings = tutor_ratings;
	}
	
}
