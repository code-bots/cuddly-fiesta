package com.codebots.stm.spring.dao;

import java.util.UUID;

import com.codebots.stm.reqres.ForgetPasswordEmailBody;
import com.codebots.stm.reqres.NewPasswordReqBody;
import com.codebots.stm.reqres.ResponseObject;
import com.codebots.stm.reqres.VerifyRequest;
import com.codebots.stm.spring.exception.STMAPIException;

public interface UserDao {

	void resendVerificationDetails(String type, UUID id) throws STMAPIException;
	void setUnsetPresence(Integer id, boolean value);
	String verifydetailsMethod(VerifyRequest verifyDetails,Integer uid);
	
	ResponseObject sendForgetPasswordDetails(ForgetPasswordEmailBody reqBody) throws STMAPIException;
	void enterNewPassword(NewPasswordReqBody newPassBody);
}
