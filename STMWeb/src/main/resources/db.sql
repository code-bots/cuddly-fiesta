/*
SQLyog Community v13.0.1 (64 bit)
MySQL - 5.5.59 : Database - stmdb
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`stmdb` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `stmdb`;

/*Table structure for table `academics` */

DROP TABLE IF EXISTS `academics`;

CREATE TABLE `academics` (
  `academic_id` varchar(36) NOT NULL,
  `student_id` varchar(36) NOT NULL,
  `school` varchar(200) NOT NULL,
  `degree` varchar(100) DEFAULT NULL,
  `grade` varchar(25) DEFAULT NULL,
  `from_year` int(11) DEFAULT NULL,
  `to_year` int(11) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`academic_id`),
  KEY `fk_student_table` (`student_id`),
  CONSTRAINT `fk_student_table` FOREIGN KEY (`student_id`) REFERENCES `student` (`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `app_role` */

DROP TABLE IF EXISTS `app_role`;

CREATE TABLE `app_role` (
  `id` int(11) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `role_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `app_user` */

DROP TABLE IF EXISTS `app_user`;

CREATE TABLE `app_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `isEnabled` tinyint(1) NOT NULL DEFAULT '0',
  `email_OTP` varchar(6) NOT NULL,
  `sms_OTP` varchar(6) NOT NULL,
  `isPresent` BIT DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unq_email` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Table structure for table `assignment_student` */

DROP TABLE IF EXISTS `assignment_student`;

CREATE TABLE `assignment_student` (
  `id` varchar(36) NOT NULL,
  `assignment_id` varchar(36) NOT NULL,
  `student_id` varchar(36) NOT NULL,
  `completion_date` date DEFAULT NULL,
  `status` tinyint(4)	 NOT NULL,
  `tutor_ratings` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_as_assignment` (`assignment_id`),
  KEY `fk_student_assigment` (`student_id`),
  CONSTRAINT `fk_as_assignment` FOREIGN KEY (`assignment_id`) REFERENCES `assignments` (`id`),
  CONSTRAINT `fk_student_assigment` FOREIGN KEY (`student_id`) REFERENCES `student` (`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `assignments` */

DROP TABLE IF EXISTS `assignments`;

CREATE TABLE `assignments` (
  `id` varchar(36) NOT NULL,
  `assNumber` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `description` varchar(500) NOT NULL,
  `type` tinyint(4) NOT NULL,
  `tutor_id` varchar(36) NOT NULL,
  `due_date` date NOT NULL,
  `course_id` varchar(36) DEFAULT NULL,
  `subject_id` varchar(36) DEFAULT NULL,
  `creation_date` date NOT NULL,
  `payment` bigint(20) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `tutorInstituteId` varchar(36) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `assNumber` (`assNumber`),
  KEY `fk_assignment_tutor` (`tutor_id`),
  KEY `fk_assignment_course` (`course_id`),
  KEY `fk_assignment_subject` (`subject_id`),
  KEY `fk_assignment_institute` (`tutorInstituteId`),
  CONSTRAINT `fk_assignment_course` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`),
  CONSTRAINT `fk_assignment_institute` FOREIGN KEY (`tutorInstituteId`) REFERENCES `tutor_institute` (`id`),
  CONSTRAINT `fk_assignment_subject` FOREIGN KEY (`subject_id`) REFERENCES `subject` (`id`),
  CONSTRAINT `fk_assignment_tutor` FOREIGN KEY (`tutor_id`) REFERENCES `tutor` (`tutor_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Table structure for table `book` */

DROP TABLE IF EXISTS `book`;

CREATE TABLE `book` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `author` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `course` */

DROP TABLE IF EXISTS `course`;

CREATE TABLE `course` (
  `id` varchar(36) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `demo` */

DROP TABLE IF EXISTS `demo`;

CREATE TABLE `demo` (
  `id` binary(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `payment_info` */

DROP TABLE IF EXISTS `payment_info`;

CREATE TABLE `payment_info` (
  `id` varchar(36) NOT NULL,
  `student_id` varchar(36) NOT NULL,
  `assignment_id` varchar(36) NOT NULL,
  `txn_time` datetime DEFAULT NULL,
  `amount` bigint(20) NOT NULL,
  `txn_id` varchar(255) NULL,
  `app_status` varchar(20) DEFAULT NULL,
  `respcode` varchar(5) DEFAULT NULL,
  `gateway_status` varchar(50) DEFAULT NULL,
  `respmsg` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_payment_student` (`student_id`),
  KEY `fk_payment_assignment` (`assignment_id`),
  CONSTRAINT `fk_payment_assignment` FOREIGN KEY (`assignment_id`) REFERENCES `assignments` (`id`),
  CONSTRAINT `fk_payment_student` FOREIGN KEY (`student_id`) REFERENCES `student` (`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `question_set` */

DROP TABLE IF EXISTS `question_set`;

CREATE TABLE `question_set` (
  `id` varchar(36) NOT NULL,
  `name` varchar(100) NOT NULL,
  `subject_id` varchar(36) NOT NULL,
  `creation_date` date DEFAULT NULL,
  `no_of_questions` bigint(20) NOT NULL,
  `tutor_id` varchar(36) NOT NULL,
  `is_payment` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_questionSet_tutor` (`tutor_id`),
  KEY `fk_questionSet_subject` (`subject_id`),
  CONSTRAINT `fk_questionSet_tutor` FOREIGN KEY (`tutor_id`) REFERENCES `tutor` (`tutor_id`),
  CONSTRAINT `fk_questionSet_subject` FOREIGN KEY (`subject_id`) REFERENCES `subject` (`id`)
) ENGINE=InnoDB;

/*Table structure for table `questions` */

DROP TABLE IF EXISTS `questions`;

CREATE TABLE `questions` (
  `id` varchar(36) NOT NULL,
  `question_set_id` varchar(36) NOT NULL,
  `question` varchar(1000) NOT NULL,
  `option_A` varchar(1000) NOT NULL,
  `option_B` varchar(1000) NOT NULL,
  `option_C` varchar(1000) NOT NULL,
  `option_D` varchar(1000) NOT NULL,
  `correct_ans` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_questions_quesionSet` (`question_set_id`),
  CONSTRAINT `fk_questions_quesionSet` FOREIGN KEY (`question_set_id`) REFERENCES `question_set` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `results` */

DROP TABLE IF EXISTS `results`;

CREATE TABLE `results` (
  `id` varchar(36) NOT NULL,
  `student_id` varchar(36) NOT NULL,
  `question_set_id` varchar(36) NOT NULL,
  `result_date` date NOT NULL,
  `result` varchar(15) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_result_student` (`student_id`),
  KEY `fk_result_questionSet` (`question_set_id`),
  CONSTRAINT `fk_result_student` FOREIGN KEY (`student_id`) REFERENCES `student` (`student_id`),
  CONSTRAINT `fk_result_questionSet` FOREIGN KEY (`question_set_id`) REFERENCES `question_set` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `schedule` */

DROP TABLE IF EXISTS `schedule`;

CREATE TABLE `schedule` (
  `id` varchar(36) NOT NULL,
  `assignment_id` varchar(36) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `title` varchar(500) NOT NULL,
  `asignment_type` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_schedule_assignment1` (`assignment_id`),
  CONSTRAINT `fk_schedule_assignment1` FOREIGN KEY (`assignment_id`) REFERENCES `assignments` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `student` */

DROP TABLE IF EXISTS `student`;

CREATE TABLE `student` (
  `student_id` varchar(36) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `about_me` varchar(1500) DEFAULT NULL,
  `blood_group` varchar(255) NOT NULL,
  `dob` date NOT NULL,
  `mobile_number` varchar(20) NOT NULL,
  `email` varchar(255) NOT NULL,
  `food_preference` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `locality` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `pincode` varchar(8) NOT NULL,
  `emergency_contact` varchar(20) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `father_name` varchar(50) DEFAULT NULL,
  `father_qualification` varchar(50) DEFAULT NULL,
  `father_dob` date DEFAULT NULL,
  `father_profession` varchar(50) DEFAULT NULL,
  `mother_name` varchar(50) DEFAULT NULL,
  `mother_qualification` varchar(50) DEFAULT NULL,
  `mother_dob` date DEFAULT NULL,
  `mother_profession` varchar(50) DEFAULT NULL,
  `remind_pref` int(11) NOT NULL,
  `profile_pic` blob,
  `stmEmailId` varchar(100) NOT NULL,
  PRIMARY KEY (`student_id`),
  UNIQUE KEY `unq_email` (`email`),
  KEY `fk_user_table` (`user_id`),
  CONSTRAINT `fk_user_table_student` FOREIGN KEY (`user_id`) REFERENCES `app_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `student_que_ans` */

DROP TABLE IF EXISTS `student_que_ans`;

	CREATE TABLE `student_que_ans` (
  `id` varchar(36) NOT NULL,
  `question_id` varchar(36) NOT NULL,
  `student_id` varchar(36) NOT NULL,
  `student_choice` varchar(20) NOT NULL,
  `correct_ans` varchar(20) NOT NULL,
  `status` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_queans_student` (`student_id`),
  KEY `fk_queans_question` (`question_id`),
  CONSTRAINT `fk_queans_question` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`),
  CONSTRAINT `fk_queans_student` FOREIGN KEY (`student_id`) REFERENCES `student` (`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `subject` */

DROP TABLE IF EXISTS `subject`;

CREATE TABLE `subject` (
  `id` varchar(36) NOT NULL,
  `course_id` varchar(36) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_course_subject` (`course_id`),
  CONSTRAINT `fk_course_subject` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `tutor` */

DROP TABLE IF EXISTS `tutor`;

CREATE TABLE `tutor` (
  `tutor_id` varchar(36) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `dob` date NOT NULL,
  `about_me` varchar(1500) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `mobile_number` varchar(20) NOT NULL,
  `address` varchar(255) NOT NULL,
  `locality` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `pincode` varchar(8) NOT NULL,
  `qualification` varchar(255) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `profile_pic` blob,
  `stmEmailId` varchar(100) NOT NULL,
  `approvalFlag` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`tutor_id`),
  UNIQUE KEY `unq_email` (`email`),
  KEY `fk_user_table_tutor` (`user_id`),
  CONSTRAINT `fk_user_table` FOREIGN KEY (`user_id`) REFERENCES `app_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `tutor_institute` */

DROP TABLE IF EXISTS `tutor_institute`;

CREATE TABLE `tutor_institute` (
  `id` varchar(36) NOT NULL,
  `name` varchar(100) NOT NULL,
  `tutor_id` varchar(36) NOT NULL,
  `latitude` varchar(50) NOT NULL,
  `longitude` varchar(50) NOT NULL,
  `inst_startTime` varchar(20) NOT NULL,
  `inst_endTime` varchar(20) NOT NULL,
  `weekdays` varchar(20) NOT NULL,
  `fees` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_institute_tutor` (`tutor_id`),
  CONSTRAINT `fk_institute_tutor` FOREIGN KEY (`tutor_id`) REFERENCES `tutor` (`tutor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `user_role` */

DROP TABLE IF EXISTS `user_role`;

CREATE TABLE `user_role` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  KEY `fk_app_user1` (`user_id`),
  KEY `fk_app_rile1` (`role_id`),
  CONSTRAINT `fk_app_user1` FOREIGN KEY (`user_id`) REFERENCES `app_user` (`id`),
  CONSTRAINT `fk_app_rile1` FOREIGN KEY (`role_id`) REFERENCES `app_role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



/*Data for the table `app_role` */

insert  into `app_role`(`id`,`description`,`role_name`) values 
(1,'Tutor User, for specific permissions on resources','TUTOR'),
(2,'Student User, for student related resource access','STUDENT'),
(3,'Admin User, for admin related resource access','ADMIN');


INSERT INTO `app_user` (`id`,`password`,`username`,`isEnabled`,`email_OTP`,`sms_OTP`)
VALUES (10,'88d4266fd4e6338d13b845fcf289579d209c897823b9217da3e161936f031589','admin@stmweb.com',1,00,00);

INSERT INTO `user_role` values (10,3);
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

/*Table structure for table `assignment_status` */

DROP TABLE IF EXISTS `assignment_status`;

CREATE TABLE `assignment_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `type` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `assignment_status` */

insert  into `assignment_status`(`id`,`title`,`type`) values 
(1,'Open',1),
(2,'In-Progress',1),
(3,'Completed',1),
(4,'Open',2),
(5,'In-Progress',2),
(6,'Completed',2);



CREATE TABLE `blog` (
  `id` varchar(36) NOT NULL,
  `title` varchar(500) NOT NULL,
  `tutor_id` varchar(36) NOT NULL,
  `content` text NOT NULL,
  `tags` varchar(500) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_blog_tutorId` (`tutor_id`),
  CONSTRAINT `fk_blog_tutorId` FOREIGN KEY (`tutor_id`) REFERENCES `tutor` (`tutor_id`)
) ENGINE=InnoDB;


CREATE TABLE `comments` (
  `id` VARCHAR(36) NOT NULL,
  `blog_id` VARCHAR(36) NOT NULL,
  `description` TEXT NOT NULL,
  `auther_id` VARCHAR(36) NOT NULL,
  `comment_date` DATETIME NOT NULL,
  `parentcomment_id` VARCHAR(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_comments_blog` (`blog_id`),
  CONSTRAINT `fk_comments_blog` FOREIGN KEY (`blog_id`) REFERENCES `blog` (`id`)
) ENGINE=INNODB;


CREATE TABLE `chatChannel` (
  `id` varchar(40) NOT NULL,
  `userIdOne` int(11) NOT NULL,
  `userIdTwo` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userIdOne` (`userIdOne`),
  KEY `userIdTwo` (`userIdTwo`),
  CONSTRAINT `chatChannel_ibfk_1` FOREIGN KEY (`userIdOne`) REFERENCES `app_user` (`id`),
  CONSTRAINT `chatChannel_ibfk_2` FOREIGN KEY (`userIdTwo`) REFERENCES `app_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `chatMessage` (
  `id` int(40) NOT NULL AUTO_INCREMENT,
  `authorUserId` int(11) NOT NULL,
  `recipientUserId` int(11) NOT NULL,
  `contents` text NOT NULL,
  `timeSent` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `authorUserId` (`authorUserId`),
  KEY `recipientUserId` (`recipientUserId`),
  CONSTRAINT `chatMessage_ibfk_1` FOREIGN KEY (`authorUserId`) REFERENCES `app_user` (`id`),
  CONSTRAINT `chatMessage_ibfk_2` FOREIGN KEY (`recipientUserId`) REFERENCES `app_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;


CREATE TABLE `feedback` (
  `id` varchar(36) NOT NULL,
  `studentAssignmentId` varchar(36) NOT NULL,
  `content` varchar(500) DEFAULT NULL,
  `star` bigint(20) DEFAULT '0',
  `assignmentStudentId` binary(255) DEFAULT NULL,
  PRIMARY KEY (`id`,`studentAssignmentId`),
  KEY `fk_feedback_stuAssId` (`studentAssignmentId`),
  CONSTRAINT `fk_feedback_stuAssId` FOREIGN KEY (`studentAssignmentId`) REFERENCES `assignment_student` (`id`)
) ENGINE=InnoDB;
