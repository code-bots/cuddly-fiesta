import { Component, OnInit, TemplateRef } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '@app/core';
import { DomSanitizer } from '@angular/platform-browser';
import { FormBuilder } from '@angular/forms';
import { keyBy, filter, clone, remove } from 'lodash';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { PatientsService } from '@app/patients/patients.service';
import { Input } from '@angular/core';
import { UserInfo } from '@app/shared/dataModels/userInfo';
import { FormGroup } from '@angular/forms';
import { Validators } from '@angular/forms';
import { ChangeDetectorRef } from '@angular/core';
import { LabTest } from '@app/shared/dataModels/constData';
import { DatePipe } from '@angular/common';
import { NotificationService } from '@app/core/notification.service';
import { Output } from '@angular/core';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-prescription',
  templateUrl: './prescription.component.html',
  styleUrls: ['./patients.component.scss']
})
export class PrescriptionComponent implements OnInit {

  @Input()
  appointment: any;
  @Output() refreshAndShowBoard: EventEmitter<boolean> = new EventEmitter();

  addUpForm: FormGroup;
  addTestForm: FormGroup;
  presType = 'medicine';
  prescToSave = [];
  show = this.prescToSave.length ? true : false;
  labTests = LabTest;
  finalRemark = '';
  prescHistory = [];
  loading = false;

  constructor(private router: Router,
              private authenticationService: AuthenticationService,
              private patService: PatientsService,
              private domSanitizer: DomSanitizer,
              private notificationService: NotificationService,
              private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private datePipe: DatePipe) {
    // this.dragulaService.setOptions(
    //   'opened', { revertOnSpill: true });
    // this.dragulaService.setOptions(
    //   'completed', { revertOnSpill: true });
    this.createForm();
  }

  getPrescriptionPatientWise() {
    this.loading = false;
    this.patService.getPrescriptionPatientWise(this.appointment.patientId.id).subscribe(
      data => {
        // this.targetHospital = this.hospitals.filter(hospital => hospital.id === this.selectedHospital)[0];
        data.forEach(pData => {
           this.prescHistory.push({
              id: pData.prescriptionId.id,
              prescriptions: pData.prescriptionDetails,
              date: pData.prescriptionId.scheduleId.date,
              remarks: pData.prescriptionId.remarks,
              prescribedBy: {
                profilePic: pData.prescriptionId.scheduleId.doctorHospitalId.doctorId.profilePic,
                firstName: pData.prescriptionId.scheduleId.doctorHospitalId.doctorId.firstName,
                lastName: pData.prescriptionId.scheduleId.doctorHospitalId.doctorId.lastName,
                id: pData.prescriptionId.scheduleId.doctorHospitalId.doctorId.id
              }
            });
        });
        this.loading = true;
      }
    );
  }

  createForm() {
      this.addUpForm = this.formBuilder.group({
        name: ['', Validators.required],
        additionalInfo1: ['', Validators.required],
        additionalInfo2: ['', Validators.required],
        additionalInfo3: ['', Validators.required],
        sideEffect: '',
        remarks: ''
        });

        this.addTestForm = this.formBuilder.group({
          name: ['', Validators.required],
          remarks: ''
          });
  }

  addPrec(type: string) {
    this.presType = type;
    let formToSave = {} as any;
    if (this.presType === 'test') {
      formToSave.name = this.addTestForm.value.name;
      formToSave.additionalInfo1 = '';
      formToSave.additionalInfo2 = '';
      formToSave.additionalInfo3 = '';
      formToSave.remarks = this.addTestForm.value.remarks ? this.addTestForm.value.remarks : '';
    } else {
      formToSave = clone(this.addUpForm.value);
      formToSave.remarks = this.addUpForm.value.remarks ? this.addUpForm.value.remarks : '';
    }
    formToSave.type = this.presType;
    this.prescToSave.push(formToSave);
    this.show = (!this.show);
    this.addUpForm.reset();
    this.addTestForm.reset();
  }

  ngOnInit() {
    this.getPrescriptionPatientWise();
  }

  getAdditionalInfo1(info1: string) {
    if (this.presType === 'medicine') {
      const prefix = '<label>Times: </label>';
      switch (info1) {
        case '24' : return prefix + 'Once in a day';
        case '12' : return prefix + 'Two times in a day';
        case '8' : return prefix + 'Three times in a day';
        case '6' : return prefix + 'Four times in a day';
      }
    }
    return info1;
  }

  getAdditionalInfo1Text(info1: string) {
      switch (info1) {
        case '24' : return 'Once in a day';
        case '12' : return 'Two times in a day';
        case '8' : return 'Three times in a day';
        case '6' : return 'Four times in a day';
      }
    return info1;
  }

  getAdditionalInfo2(info1: string) {
    if (this.presType === 'medicine') {
      const prefix = '<label>When: </label>';
      switch (info1) {
        case 'BeforeFood' : return prefix + 'Before food';
        case 'AfterFood' : return prefix + 'After food';
      }
    }
    return info1;
  }

  getAdditionalInfo2Text(info1: string) {
      switch (info1) {
        case 'BeforeFood' : return 'Before food';
        case 'AfterFood' : return 'After food';
      }
    return info1;
  }

  getAdditionalInfo3(info1: string) {
    if (this.presType === 'medicine' && info1 !== null) {
      const prefix = '<label>Duration: </label>' + info1 + ' days';
      return prefix;
    }
    return '';
  }

  removePrescription(prec: any) {
    remove(this.prescToSave, function(precItem: any) {
      return precItem.name === prec.name;
    });
    this.show = (!this.show);
  }

  prescribe() {
    const saveContext = {
      prescription : {
        date: this.datePipe.transform(new Date(), 'yyyy-MM-dd'),
        remarks: this.finalRemark ?  this.finalRemark : ''
      },
      prescripionDetails : this.prescToSave,
      remarks: this.finalRemark ? this.finalRemark : ''
    };
    this.patService.postPrescription(this.appointment.id, saveContext).subscribe(data => {
      this.notificationService.showNotification('Success', 'Prescription posted successfully', 'success');
      this.sendEmail();
      // http://localhost:8080/DocWeb/rest/api/update/status/{scheduleId}/{status}
      this.patService.updateAppointmentStatus(this.appointment.id, this.getStatus() ).subscribe(
        updatedData => {
          this.refreshAndShowBoard.emit(true);
        }
      );
      // TODO save schedule with status
    }, err => {
      this.notificationService.showNotification('Error', 'Failure in prescription post', 'error');
    });
  }

  getStatus() {
    let foundTest = false;
    this.prescToSave.forEach(element => {
      if (element.type === 'test') {
        foundTest = true;
      }
    });
    return foundTest ? 2 : 3;
  }

  sendEmail() {
    //     Post::http://localhost:8080/STMWeb/rest/api/email/send
    const requestObj: any = {};
    // stmEmailId or email?
    requestObj.fromEmail = this.appointment.doctorHospitalId.doctorId.dpmEmailId;
    requestObj.subject = 'Prescription on ' + this.appointment.date + ' ' + this.appointment.startTime;
    requestObj.body = this.getPrintContents();
    requestObj.toEmails = [this.appointment.patientId.dpmEmailId];
    requestObj.ccEmails = [];
    this.patService.sendEmail(requestObj).subscribe(
        data => {
          this.notificationService.showNotification('Success', 'Prescription Mail sent successfully.', 'warning');
        }, err => {
            console.log(err);
        }
    );
}

  print() {
    let printContents, popupWin;
  //  printContents = document.getElementById('print-section').innerHTML;
    printContents = this.getPrintContents();
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>Prescription Summary </title>` +
          `<style>
          //........Customized style.......
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }

  getPrintContents() {
    let contents = '';
    contents = contents + '<h4>Prescription on ' + this.appointment.date + ' at ' +
    this.appointment.startTime + '</h4>';
    contents = contents + '<br><br><b>Patient Name: </b>' + this.appointment.patientId.firstName + ' ' +
    this.appointment.patientId.lastName + '<br><br>';
    contents = contents + '<h4>Prescription Details</h4><hr>';
    const medicines = [];
    const tests = [];
    this.prescToSave.forEach(element => {
      if (element.type === 'medicine') {
        const htmlMedicine = '<b>Medicine: </b>' + element.name + '<br>' +
        '<b>Time period: </b>' + element.additionalInfo3 + ' days<br>' +
        '<b>Time to take: </b>' + this.getAdditionalInfo1Text(element.additionalInfo1) +
        '<b> When to take: </b>' + this.getAdditionalInfo2Text(element.additionalInfo2) + '<br>' +
        '<b>Side Effect of medicine: </b>' + (element.sideEffect ? element.sideEffect : 'Not any') + '<br>' +
        '<b>Any remark: </b>' + (element.remarks ? element.remarks : 'Not any') + '<hr>';
        medicines.push(htmlMedicine);
      } else {
        const testMedicine = '<b>Lab Test: </b>' + element.name + '<br>' +
        '<b>Any remark: </b>' + (element.remarks ? element.remarks : 'Not any') + '<hr>';
        tests.push(testMedicine);
      }
    });
    medicines.forEach(med => {
      contents = contents + med;
    });
    tests.forEach(test => {
      contents = contents + test;
    });
    contents = contents + '<hr><b>Note: </b>' + this.finalRemark + '<hr><br><br>';
    contents = contents + '<b>By: </b>Dr. ' + this.appointment.doctorHospitalId.doctorId.firstName + ' ' +
    this.appointment.doctorHospitalId.doctorId.lastName;
    return contents;
  }
}
