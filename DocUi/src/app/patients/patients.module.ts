import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { DragulaModule } from 'ng2-dragula';
import { AlertModule, BsDatepickerModule, ButtonsModule, TimepickerModule, ModalModule,
  PopoverModule, RatingModule, PaginationModule, TabsModule } from 'ngx-bootstrap';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';

import { CoreModule } from '@app/core';
import { SharedModule } from '@app/shared';
import { ToastyModule } from 'ng2-toasty';
import { NgPipesModule } from 'ngx-pipes';
import { PatientsService } from '@app/patients/patients.service';
import { PatientsRoutingModule } from '@app/patients/patients-routing.module';
import { PatientsComponent } from '@app/patients/patients.component';
import { PrescriptionComponent } from '@app/patients/prescription.component';
import { DashboardComponent } from '@app/patients/dashboard.component';
import { CalendarModule } from 'ap-angular2-fullcalendar';
import { ScheduleService } from '@app/schedule/schedule.service';
import { FileUploadModule } from 'ng2-file-upload';
import { PatientListComponent } from '@app/patients/patients-list.component';


@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    CoreModule,
    SharedModule,
    DragulaModule,
    ReactiveFormsModule,
    FormsModule,
    PatientsRoutingModule,
    NgPipesModule,
    AlertModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ButtonsModule.forRoot(),
    TimepickerModule.forRoot(),
    ModalModule.forRoot(),
    ToastyModule.forRoot(),
    CalendarModule.forRoot(),
    PopoverModule.forRoot(),
    RatingModule.forRoot(),
    PaginationModule.forRoot(),
    TabsModule.forRoot(),
    FileUploadModule
  ],
  declarations: [
    PatientsComponent,
    PrescriptionComponent,
    DashboardComponent,
    PatientListComponent
  ],
  providers: [PatientsService, ScheduleService]
})
export class PatientsModule { }
