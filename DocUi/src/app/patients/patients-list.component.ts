import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { forEach, clone } from 'lodash';
import { AuthenticationService } from '@app/core/authentication/authentication.service';
import { UserInfo } from '@app/shared/dataModels/userInfo';
import { EmailFolderDetails } from '@app/email/dataModels/emailFolderDetails';
import { EmailInfo } from '@app/email/dataModels/emailInfo';
import { thisExpression } from 'babel-types';
import { DoctorsService } from '@app/doctors/doctors.service';
import { DomSanitizer } from '@angular/platform-browser';
import { ADMIN, PAGESIZE } from '@app/shared/dataModels/constData';
import { NotificationService } from '@app/core/notification.service';
import { PatientsService } from '@app/patients/patients.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-patients-list',
  templateUrl: './patients-list.component.html',
  styleUrls: ['./patients.component.scss']
})
export class PatientListComponent implements OnInit {

  userData: UserInfo;
  patients = [];
  searchTerm = '';
  scheduleTriggered = false;
  listView = false;
  isAdminUser = false;
  page = 1;
  pageSize = PAGESIZE;
  totalRecord = 0;

  constructor(private router: Router, private patService: PatientsService,
    private authenticationService: AuthenticationService,
    private domSanitizer: DomSanitizer, private notificationService: NotificationService) {
  }

  ngOnInit() {
    const savedCredentials = sessionStorage.getItem('credentials') || localStorage.getItem('credentials');
    if (savedCredentials) {
        this.authenticationService.getUserDetails(this.authenticationService.credentials.token)
        .subscribe(
          data => {
            this.userData = data.doctor ? data.doctor : data;
            this.loadPatientsForDoctor();
          }
        );
      }
  }

  loadPatientsForDoctor() {
    this.patService.getPatientsListForDoctor(this.page, this.pageSize, 'asc', 'firstName', this.userData.id).subscribe(
      response => {
        this.patients = response.listOfObjects;
        this.totalRecord = response.totalRecord;
      }, error => {
        // error handling
      }
    );
  }

  pageChanged($event: any) {
    this.page = $event.page;
    this.loadPatientsForDoctor();
  }

  gotToPatientTimeLine(patient: any) {
    this.patService.setPatientForTimeLine(patient);
    this.router.navigate(['/patient', patient.id], { replaceUrl: false });
  }
}
