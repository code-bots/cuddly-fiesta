import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Route, extract } from '@app/core';
import { PatientsComponent } from '@app/patients/patients.component';
import { DashboardComponent } from '@app/patients/dashboard.component';
import { PatientListComponent } from '@app/patients/patients-list.component';


const routes: Routes = [
  Route.withShell([
    { path: 'patients', component: PatientsComponent, data: { title: extract('Patients') } },
    { path: 'patients-list', component: PatientListComponent, data: { title: extract('Patients') } },
    { path: 'dashboard', component: DashboardComponent, data: { title: extract('Profile') } },
    { path: 'patient/:id', component: DashboardComponent, data: { title: extract('Patient Timeline') } },
  ])
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class PatientsRoutingModule { }
