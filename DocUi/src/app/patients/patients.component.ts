import { Component, OnInit, TemplateRef } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { DragulaService } from 'ng2-dragula';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '@app/core';
import { DomSanitizer } from '@angular/platform-browser';
import { FormBuilder } from '@angular/forms';
import { keyBy, filter } from 'lodash';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { PatientsService } from '@app/patients/patients.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-patients',
  templateUrl: './patients.html',
  styleUrls: ['./patients.component.scss']
})
export class PatientsComponent implements OnInit {

  boardData = [];
  boards = [
    {id: '1', title: 'New'},
    {id: '2', title: 'Waiting on result'},
    {id: '3', title: 'Completed'}
  ];
  quote: string;
  isLoading: boolean;
  dateVisit = [new Date(), new Date()];
  userData;
  appointmentsData = {};
  paymentData;
  modalRef: BsModalRef;
  assignmentToPay;
  paymentRef: TemplateRef<any>;
  status: string;
  appointmentForPrescription;
  startDateFilter: string;
  endDateFilter: string;


  constructor(private dragulaService: DragulaService,
              private router: Router,
              private authenticationService: AuthenticationService,
              private patService: PatientsService,
              private domSanitizer: DomSanitizer,
              private modalService: BsModalService,
              private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private datePipe: DatePipe) {
    // this.dragulaService.setOptions(
    //   'opened', { revertOnSpill: true });
    // this.dragulaService.setOptions(
    //   'completed', { revertOnSpill: true });
  }

  ngOnInit() {
    this.authenticationService.getUserDetails(this.authenticationService.credentials.token)
    .subscribe(
      data => {
        this.userData = data.doctor ? data.doctor : data;
        this.isLoading = true;
      //  this.loadAppointments();
      }
    );
  }

  loadAppointments() {
    this.patService.getPatientAppointmentForDoctor(this.userData.id,
      this.endDateFilter, this.startDateFilter).subscribe(
      data => {
        this.boardData = data;
        // const _this_ref = this;
        // this.boards.forEach(board => {
        //   if (board.id !== 0) {
        //     this.loadBoardDataForPatients(board, data);
        //   }
        //  });
      }, err => {
        console.log('Error in loading boards');
      }
    );
  }

  loadBoardDataForPatients(board: any) {
    // TODO Filter logic
    const data = filter(this.boardData, function(boardData: any) {
      return boardData.schStatus === board.id || (board.id === '1' && boardData.schStatus === null);
    });
    this.appointmentsData['board-' + board.id] = data;
    if (data && data.length) {
      return data;
    } else {
      return [];
    }
  }

  prescribe(appointment: any) {
    // TODO
    // prescription logic
    this.appointmentForPrescription = appointment;
  }

  refreshAndShowBoard() {
    this.loadAppointments();
    this.appointmentForPrescription = undefined;
  }


   enroll(assignment: any, template: TemplateRef<any>) {
    window.location.href =
                'http://35.185.51.218:8080/dist/payment.html?assignmentId=' + assignment.id +
                '&studentId=' + this.userData.student_id;
                this.assignmentToPay = assignment;
                this.paymentRef = template;
    console.log('enrol after payment');
  }

  onValueChange($event: any) {
    console.log($event);
    if ($event === null) {
      return;
    }
    if ($event) {
      this.startDateFilter = this.datePipe.transform($event[0], 'yyyy-MM-dd'),
      console.log(this.startDateFilter);
      this.endDateFilter = this.datePipe.transform($event[1], 'yyyy-MM-dd'),
      console.log(this.endDateFilter);
      this.loadAppointments();
    }
  }

  loadReportsBySchedule(entity: any) {
    entity.loadReports = true;
    this.patService.getPrescriptionOnSchedule(entity.id).subscribe(
      data => {
        entity.testReports = [];
        for (const key in data.listOfFiles) {
          // check also if property is not inherited from prototype
          if (data.listOfFiles.hasOwnProperty(key)) {
            entity.testReports.push({
              key: key,
              files: data.listOfFiles[key]
            });
          }
        }
        entity.loadReports = false;
      }
    );
  }

  markItDone(schedule: any) {
    this.patService.updateAppointmentStatus(schedule.id, 3).subscribe(
      data => {
        this.loadAppointments();
        this.appointmentForPrescription = undefined;
      }
    );
  }

}
