import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { Http, Headers, Response } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';


export interface Credentials {
  // Customize received credentials here
  username: string;
  token: string;
}

const credentialsKey = 'credentials';

@Injectable()
export class PatientsService {

  private _credentials: Credentials | null;
  private patient;

  constructor(private http: HttpClient) {
    const savedCredentials = sessionStorage.getItem(credentialsKey) || localStorage.getItem(credentialsKey);
    if (savedCredentials) {
      this._credentials = JSON.parse(savedCredentials);
    }
  }

  setPatientForTimeLine(pat: any): any {
    this.patient = pat;
  }

  getPatientFOrTimeLine() {
    return this.patient;
  }

  // http://localhost:8080/DocWeb/rest/api/schedule/doctor/{doctorId}
  getPatientAppointmentForDoctor(doctorId: string, toDate: string, fromDate: string) {
    return this.http.get<any[]>('/DocWeb/rest/api/schedule/doctor/' + doctorId +
    '?fromdate=' + (fromDate ?  fromDate : '') +
    '&todate=' + (toDate ?  toDate : ''),
    this.getHeaderForRequest());
  }

  // http://localhost:8080/DocWeb/rest/api/prescription/{scheduleId}
  postPrescription(scheduleId: string, context: any) {
    return this.http.post('/DocWeb/rest/api/prescription/' + scheduleId
      , context, this.getHeaderForRequest());
  }

  // http://localhost:8080/DocWeb/rest/api/update/status/{scheduleId}/{status}
  updateAppointmentStatus(scheduleId: string, status: any) {
    return this.http.put('/DocWeb/rest/api/update/status/' + scheduleId + '/' + status
      , {}, this.getHeaderForRequest());
  }

  // http://localhost:8080/DocWeb/rest/api/prescription/{patientId}
  getPrescriptionPatientWise(patientId: string) {
    return this.http.get<any[]>('/DocWeb/rest/api/prescription/patient/' + patientId,
    this.getHeaderForRequest());
  }

  getPrescriptionOnSchedule(scheduleId: string) {
    return this.http.get<any>('/DocWeb/rest/api/prescription/schedule/' + scheduleId,
    this.getHeaderForRequest());
  }

  getPatientsListForDoctor(page: number, pageSize: number, type: string, field: string, doctorId: string) {
    return this.http.get<any>('/DocWeb/rest/api/doctor/patientlist/' + doctorId + '?page=' + page + '&size='
    + pageSize + '&type=' + type + '&feild=' + field, this.getHeaderForRequest());
  }

  postFeedback(scheduleId: string, context: any) {
    return this.http.post('/DocWeb/rest/api/feedback/' + scheduleId
      , context, this.getHeaderForRequest());
  }

  sendEmail(context: any) {
    // http://localhost:8080/STMWeb/rest/api/email/send
    return this.http.post('/DocWeb/rest/api/email/send', context,
    this.getHeaderForRequest());
  }

  getHeaderForRequest() {
    const httpOptions = {
      headers: new HttpHeaders({ 'Authorization': 'bearer ' + this._credentials.token
    })};
    return httpOptions;
  }
}
