import { Component, OnInit } from '@angular/core';
import { BlogService } from '@app/blog/blog.service';
import { Router, ActivatedRoute } from '@angular/router';
import { UserInfo } from '@app/shared/dataModels/userInfo';
import { AuthenticationService } from '@app/core/authentication/authentication.service';

@Component({
  selector: 'app-bloglist',
  templateUrl: './bloglist.component.html',
  //   styleUrls: ['./bloglist.component.scss']
})
export class BloglistComponent implements OnInit {


  userData: UserInfo;
  blogList: Array<any> = [];
  idDoc: string;
  constructor(private blogSevice: BlogService,
    private router: Router, private route: ActivatedRoute, private authenticationService: AuthenticationService) { }

  ngOnInit() {
    // this.userData = this.authenticationService.getUserInfo();
    this.authenticationService.getUserDetails(this.authenticationService.credentials.token)
      .subscribe(
        data => {
          this.userData = data.doctor ? data.doctor : data;
          this.loadBlogs();
        }
      );

  }

  loadBlogs() {
    this.blogSevice.getBlogs().subscribe(
      response => {
        this.blogList = response;
      }, error => {
        // error handling
      }
    );
    // throw new Error("Method not implemented.");
  }

  gotoView(blogId: string) {
    this.router.navigate(['/viewblog', blogId], { replaceUrl: true });
    // this.router.navigate(['/blog'], { replaceUrl: false });
  }

  gotoEdit(blogId: string) {
    this.router.navigate(['/blogedit', blogId], { replaceUrl: true });
    // this.router.navigate(['/blog'], { replaceUrl: false });
  }

  deleteBlog(blogId: string) {
    this.blogSevice.deleteBlog(blogId).subscribe(
      response => {
        this.loadBlogs();
      }, error => {
        // error handling
      }
    );
  }


  isDoctor() {
    return this.userData.userId.roles[0].roleName === 'DOCTOR';
  }


  createNewBlog() {
    this.router.navigate(['/blog'], { replaceUrl: false });
  }
}
