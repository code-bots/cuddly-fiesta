import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { finalize } from 'rxjs/operators';

import { environment } from '@env/environment';
import { Logger, I18nService, AuthenticationService } from '@app/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { ViewChild } from '@angular/core';
import { TemplateRef } from '@angular/core';
import { UserInfo } from '@app/shared/dataModels/userInfo';
import { RegisterService } from '@app/login/register.service';

const log = new Logger('Login');

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  version: string = environment.version;
  error: string;
  loginForm: FormGroup;
  verifyForm: FormGroup;
  isLoading = false;
  loginTypeModel = '1';
  modalRef: BsModalRef;
  userData: UserInfo;
  @ViewChild('verifyOtp') modalContent: TemplateRef<any>;


  constructor(private router: Router,
              private modalService: BsModalService,
              private formBuilder: FormBuilder,
              private i18nService: I18nService,
              private authenticationService: AuthenticationService,
              private registerService: RegisterService) {
    this.createForm();
  }

  ngOnInit() { }

  login() {
    this.isLoading = true;
    const data = {
      username: this.loginForm.value.username,
      token: ''
    };
    this.authenticationService.login(this.loginForm.value)
      .pipe(finalize(() => {
        this.loginForm.markAsPristine();
        this.isLoading = false;
      }))
      .subscribe(credentials => {
        log.debug(`${credentials.username} successfully logged in`);
        const result = credentials as any;
        data.token = result.access_token;
        this.authenticationService.setCredentials(data, true);
        if (data.username !== 'admin@docweb.com') {
          this.authenticationService.setAvailibility(true).subscribe();
          this.authenticationService.getUserDetails(result.access_token).subscribe(
            response => {
              this.userData = response.doctor ? response.doctor : response;
              const isEnabled = this.userData.userId.enabled;
              // const isEnabled = true;
              if (isEnabled) {
                this.routeAppropriate();
              } else {
                // otp
                this.modalRef = this.modalService.show(this.modalContent, {class: 'modal-sm'});
              }
            }, error => {

            }
          );
        } else {
          this.router.navigate(['doctors'], { replaceUrl: true });
        }
      }, error => {
        log.debug(`Login error: ${error}`);
        this.error = error;
      });
  }

  routeAppropriate() {
    if (this.userData && this.userData.userId.roles[0].roleName === 'DOCTOR') {
      this.router.navigate(['doc-dashboard'], { replaceUrl: true });
    } else {
      this.router.navigate(['dashboard'], { replaceUrl: true });
    }
  }

  setLanguage(language: string) {
    this.i18nService.language = language;
  }

  get currentLanguage(): string {
    return this.i18nService.language;
  }

  get languages(): string[] {
    return this.i18nService.supportedLanguages;
  }

  private createForm() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
      remember: true
    });
    this.verifyForm = this.formBuilder.group({
      emailOtp: ['', Validators.required],
      smsOtp: ['', Validators.required]
    });
  }

  goToRegister() {
    this.router.navigate(['/register'], { replaceUrl: true });
  }

  goToChangePassword() {
    this.router.navigate(['/change_password',
    this.userData && this.userData.userId.roles[0].roleName === 'DOCTOR' ? 'doctor' : 'patient',
      this.userData.id], { replaceUrl: false });
  }

  confirm(): void {
    this.registerService.verifyOTP(this.verifyForm.value,
      this.userData.userId.id).subscribe(
        data => {
          if (data['_body'] === 'Please try again,Entered values are incorrect') {
            $('#regenerate').html('<b>' + data['_body'] + '</b>');
          } else {
            $('#regenerate').html('');
            this.modalRef.hide();
            this.routeAppropriate();
          }
        },
        err => {
          console.log('Error in verification');
        }
      );
  }

  decline(): void {
    this.modalRef.hide();
  }

  regenerateOtp() {
    console.log('Regenerate OTP');
    this.registerService.resendOTP(this.userData &&
      this.userData.userId.roles[0].roleName === 'DOCTOR' ? 'doctor' : 'patient',
      this.userData.id).subscribe(
      data => {
        console.log('Re-sent the OTP' + data);
      },
      err => {
        console.log('Error in re generating the OTP, Please contact your admin.');
      }
    );
  }

}
