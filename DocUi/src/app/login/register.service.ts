import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { Http, Response, Headers, RequestOptions } from '@angular/http';


const credentialsKey = 'credentials';
export interface Credentials {
  // Customize received credentials here
  username: string;
  token: string;
}

@Injectable()
export class RegisterService {

  private _credentials: Credentials | null;

  constructor(private http: Http) {
      const savedCredentials = sessionStorage.getItem(credentialsKey) || localStorage.getItem(credentialsKey);
      if (savedCredentials) {
        this._credentials = JSON.parse(savedCredentials);
      }
    }

  // DocWeb/rest/registration/doctor
  // http://localhost:8080/DocWeb/rest/registration/patient
  register(context: any, type: string) {
    return this.http.post('/DocWeb/rest/registration/' + type, context)
                .map((res: Response) => res.json());
  }

  // DocWeb/rest/api/verifydetails/{userID}
  verifyOTP(context: any, userId: number) {
    const savedCredentials = sessionStorage.getItem(credentialsKey) || localStorage.getItem(credentialsKey);
      if (savedCredentials) {
        this._credentials = JSON.parse(savedCredentials);
      }
      const headers =  new Headers();
        headers.append('Authorization', 'bearer ' + this._credentials.token);
    return this.http.post('/DocWeb/rest/api/verifydetails/' + userId, context, { headers: headers });
  }

  // DocWeb/rest/api/resend/{userId}
  resendOTP(type: string, userId: string) {
    const savedCredentials = sessionStorage.getItem(credentialsKey) || localStorage.getItem(credentialsKey);
      if (savedCredentials) {
        this._credentials = JSON.parse(savedCredentials);
      }
    const headers =  new Headers();
    headers.append('Authorization', 'bearer ' + this._credentials.token);
    return this.http.post('/DocWeb/rest/registration/resend/' + type + '/' + userId, {headers})
                .map((res: Response) => res.json());
  }

  changePassword(data: any, type: string, id: string) {
    return this.http.put('/DocWeb/rest/api/' + type + '/changepassword/' + id,
      data, this.getRequestHeaderWithOptions(null));
  }

  private  getRequestHeaderWithOptions(headersInput: Headers) {
    let options: {};
    if (headersInput) {
      options = { headers: headersInput };
    } else {
      const headers = new Headers({
        'Content-Type': 'application/json',
        'Cache-control': 'no-cache',
        'Expires': '0',
        'Pragma': 'no-cache',
        'Authorization': 'bearer ' + this._credentials.token
    });
    options =  { headers: headers };
  }
  return options;
}

}
