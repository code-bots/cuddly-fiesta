import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ButtonsModule, BsDatepickerModule } from 'ngx-bootstrap';
import { FormsModule } from '@angular/forms';
import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
import { RegisterComponent } from '@app/login/register.component';
import { SharedModule } from '@app/shared';
import { RegisterService } from '@app/login/register.service';
import { ChangePasswordComponent } from '@app/login/change-password.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    ReactiveFormsModule,
    TranslateModule,
    NgbModule,
    LoginRoutingModule,
    ButtonsModule.forRoot(),
    BsDatepickerModule.forRoot()
  ],
  declarations: [
    LoginComponent,
    RegisterComponent,
    ChangePasswordComponent
  ],
  providers: [
    RegisterService
  ],
})
export class LoginModule { }
