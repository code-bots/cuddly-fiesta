import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { finalize } from 'rxjs/operators';

import { environment } from '@env/environment';
import { NotificationService } from '@app/core/notification.service';
import { Logger, I18nService, AuthenticationService } from '@app/core';
import { Student, RegistrationDetails } from '@app/login/data.modal';
import { RegisterService } from '@app/login/register.service';
import { TYPES_OF_SPECIALIZATION } from '@app/shared/dataModels/constData';

const log = new Logger('Login');

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./login.component.scss']
})
export class RegisterComponent implements OnInit {

    version: string = environment.version;
    error: string;
    loginForm: FormGroup;
    patientRegistrationForm: FormGroup;
    doctorRegistrationForm: FormGroup;
    isLoading = false;
    registrationMode = 'patient';
    student: Student;
    acceptTerm = false;
    typesOfDoc = TYPES_OF_SPECIALIZATION;
    public editEnabled = true;
    public profile_pic: string;

    constructor(private router: Router,
        private formBuilder: FormBuilder,
        private i18nService: I18nService,
        private authenticationService: AuthenticationService,
        private registerService: RegisterService,
        private notificationService: NotificationService) {
        this.createForm();
    }

    ngOnInit() { }

    private createForm() {
        this.doctorRegistrationForm = this.formBuilder.group({
            first_name: ['', Validators.required],
            last_name: ['', Validators.required],
            gender: ['', Validators.required],
            qualification: ['', Validators.required],
            title: ['General Physician', Validators.required],
            mobileNumber: ['', Validators.required],
            email: ['', Validators.required],
            dob: ['', Validators.required],
            medicalCouncil: '',
            licNo: ['', Validators.required],
            addressLine: ['', Validators.required],
            locality: ['', Validators.required],
            city: ['', Validators.required],
            pincode: ['', Validators.required],
            state: ['', Validators.required],
            awards: '',
            password: ['', Validators.required],
            re_password: ['', Validators.required]
        });
        this.patientRegistrationForm = this.formBuilder.group({
            first_name: ['', Validators.required],
            last_name: ['', Validators.required],
            dob: ['', Validators.required],
            gender: ['', Validators.required],
            height: ['', [Validators.required,  Validators.maxLength(3)]],
            weight: ['', Validators.required],
            address: ['', Validators.required],
            locality: ['', Validators.required],
            city: ['', Validators.required],
            pincode: ['', Validators.required],
            state: ['', Validators.required],
            email: ['', Validators.required],
            mobileNumber: ['', [Validators.required, Validators.pattern('^((\\+91-?)|0)?[0-9]{10}$')]],
            emergency_contact: '',
            existingDesease: '',
            familyHistory: '',
            allergic: '',
            remindPref: '',
            password: ['', Validators.required],
            re_password: ['', Validators.required]
        });
    }

    private gotoRegister() {
        this.router.navigate(['/register'], { replaceUrl: true });
    }

    gotToLogin(withRegister: boolean) {
        if (!withRegister) {
            this.router.navigate(['/login'], { replaceUrl: true });
            return;
        }
        const patientToBeRegister = {
            registrationDetails: {
                firstName: this.patientRegistrationForm.value.first_name,
                lastName: this.patientRegistrationForm.value.last_name,
                dob: this.patientRegistrationForm.value.dob,
                height: this.patientRegistrationForm.value.height,
                weight: this.patientRegistrationForm.value.weight,
                address: this.patientRegistrationForm.value.address,
                mobileNumber: this.patientRegistrationForm.value.mobileNumber,
                email: this.patientRegistrationForm.value.email,
                emergencyContact: this.patientRegistrationForm.value.emergency_contact,
                existingDesease: this.patientRegistrationForm.value.existingDesease,
                familyHistory: this.patientRegistrationForm.value.familyHistory,
                allergic: this.patientRegistrationForm.value.allergic,
                remindPref: this.patientRegistrationForm.value.remindPref,
                profilePic: this.profile_pic,
                gender: this.patientRegistrationForm.value.gender,
                city: this.patientRegistrationForm.value.city,
                state: this.patientRegistrationForm.value.state,
                locality: this.patientRegistrationForm.value.locality,
                pincode: this.patientRegistrationForm.value.pincode
            },
            password: this.patientRegistrationForm.value.password
        };

        const doctorToBeRegister = {
            registrationDetails: {
                firstName: this.doctorRegistrationForm.value.first_name,
                lastName: this.doctorRegistrationForm.value.last_name,
                qualification: this.doctorRegistrationForm.value.qualification,
                title: this.doctorRegistrationForm.value.title,
                email: this.doctorRegistrationForm.value.email,
                mobileNumber: this.doctorRegistrationForm.value.mobileNumber,
                licNo: this.doctorRegistrationForm.value.licNo,
                medicalCouncil: this.doctorRegistrationForm.value.medicalCouncil,
                awards: this.doctorRegistrationForm.value.awards,
                dob: this.doctorRegistrationForm.value.dob,
                profilePic: this.profile_pic,
                gender: this.doctorRegistrationForm.value.gender,
                city: this.doctorRegistrationForm.value.city,
                state: this.doctorRegistrationForm.value.state,
                locality: this.doctorRegistrationForm.value.locality,
                pincode: this.doctorRegistrationForm.value.pincode,
                addressLine: this.doctorRegistrationForm.value.addressLine
            },
            password: this.doctorRegistrationForm.value.password
        };
        this.registerService.register(this.registrationMode === 'patient' ? patientToBeRegister : doctorToBeRegister,
            this.registrationMode).subscribe(
                data => {
                    const dataCreated = this.registrationMode === 'patient' ? patientToBeRegister : doctorToBeRegister;
                    this.notificationService.showNotification('Registered Successfully',
                    this.registrationMode === 'patient' ? 'Patient ' : 'Doctor '
                    + dataCreated.registrationDetails.firstName + ' ' + dataCreated.registrationDetails.lastName +
                    ' registered to app.', 'success');
                    this.router.navigate(['/login'], { replaceUrl: true });
                }, err => {
                    const errorObj = err._body ? JSON.parse(err._body) : '';
                    if (errorObj && errorObj.errorCode && errorObj.errorCode === '0002') {
                    this.notificationService.showNotification('Registration failed',
                    this.registrationMode === 'patient' ? 'Patient ' : 'Doctor '
                    + ' registration failed to app. Reason of failure is ' + errorObj.errorMessage, 'error');
                    } else {
                        this.notificationService.showNotification('Registration failed',
                        this.registrationMode === 'patient' ? 'Patient ' : 'Doctor '
                        + ' registration failed to app.', 'error');
                    }
                }
            );
    }

    setProfilePicData($event: any) {
        this.profile_pic = $event;
    }

}
