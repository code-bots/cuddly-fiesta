import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { finalize } from 'rxjs/operators';

import { environment } from '@env/environment';
import { Logger, I18nService, AuthenticationService } from '@app/core';

const log = new Logger('Login');

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./login.component.scss']
})
export class ChangePasswordComponent implements OnInit {

  version: string = environment.version;
  error: string;
  passwordForm: FormGroup;
  isLoading = false;
  type: string;
  id: string;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private formBuilder: FormBuilder,
              private i18nService: I18nService,
              private authenticationService: AuthenticationService) {
                this.route.params.subscribe( params => {
                  console.log(params);
                  this.type = params.type;
                  this.id = params.id;
                });
    this.createForm();
  }

  ngOnInit() { }

  changePassword() {
    this.isLoading = true;
    const data = {
      old_pass: this.passwordForm.value.old_pass,
      new_pass: this.passwordForm.value.new_pass
    };
    this.authenticationService.changePassword(data, this.type, this.id)
      .pipe(finalize(() => {
        this.passwordForm.markAsPristine();
        this.isLoading = false;
      }))
      .subscribe(credentials => {
        this.router.navigate(['login'], { replaceUrl: true });
      }, error => {
        this.error = error;
      });
  }

  private createForm() {
    this.passwordForm = this.formBuilder.group({
      old_pass: ['', Validators.required],
      new_pass: ['', Validators.required],
      re_pass: ['', Validators.required]
    });
  }
}
