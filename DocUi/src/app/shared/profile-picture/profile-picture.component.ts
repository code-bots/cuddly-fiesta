import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { FileUploader } from 'ng2-file-upload';

@Component({
  selector: 'app-profile-picture',
  templateUrl: './profile-picture.component.html',
  styleUrls: ['./profile-picture.component.css']
})
export class ProfilePictureComponent implements OnInit {
  public uploader: FileUploader;
  private hasDragOver = false;

  @Input()
  public editmode = false;
  @Output()
  public base64DataForProfile = new EventEmitter<string>();
  @Input()
  base64Image: string;
  @Input()
  fromProf = false;

  constructor(private domSanitizer: DomSanitizer) {
    this.uploader = new FileUploader({
      url: '/DocWeb/tobase64/',
      disableMultipart: false,
      autoUpload: true,
      allowedMimeType: ['image/jpeg', 'image/png' ]
    });

    this.uploader.response.subscribe(res => {
      // Upload returns a JSON with the image ID
      this.base64Image = 'data:image/jpg;base64,' + res;
      this.base64DataForProfile.emit(this.base64Image);
    });
  }

  public fileOver(e: any): void {
    this.hasDragOver = e;
  }

  ngOnInit() {
  }

}
