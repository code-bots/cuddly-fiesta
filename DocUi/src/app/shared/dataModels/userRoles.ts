export class UserRoles {
    id: number;
    roleName: string;
    description: string;
}
