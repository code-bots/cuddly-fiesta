export const ADMIN = 'admin@docweb.com';
export const PAGESIZE = 12;
export const TYPES_OF_SPECIALIZATION = [{
    name: 'General Physician',
    description: 'Diagnoses and treats the general disease.'
},
{
    name: 'Allergist/Immunologist',
    description: 'Conducts the diagnosis and treatment of allergic conditions.'
},
{
    name: 'Anesthesiologist',
    description: 'Treats chronic pain syndromes; administers anesthesia and monitors the patient during surgery.'
},
{
    name: 'Cardiologist',
    description: 'Treats heart disease.'
},
{
    name: 'Dermatologist',
    description: 'Treats skin diseases, including some skin cancers.'
},
{
    name: 'Gastroenterologist',
    description: 'Treats stomach disorders.'
},
{
    name: 'Hematologist/Oncologist',
    description: 'Treats diseases of the blood and blood-forming tissues (oncology including cancer and other tumors).'
},
{
    name: 'Internal Medicine Physician',
    description: 'Treats diseases and disorders of internal structures of the body.'
},
{
    name: 'Nephrologist',
    description: 'Treats kidney diseases.'
},
{
    name: 'Neurologist',
    description: 'Treats diseases and disorders of the nervous system.'
},
{
    name: 'Neurosurgeon',
    description: 'Conducts surgery of the nervous system.'
},
{
    name: 'Obstetrician',
    description: 'Treats women during pregnancy and childbirth.'
},
{
    name: 'Gynecologist',
    description: 'Treats diseases of the female reproductive system and genital tract.'
},
{
    name: 'Nurse-Midwifery',
    description: 'Manages a woman\'s health care, especially during pregnancy,' +
        ' delivery, and the postpartum period.'
},
{
    name: 'Occupational Medicine Physician',
    description: 'Diagnoses and treats work-related disease or injury.'
},
{
    name: 'Ophthalmologist',
    description: 'Treats eye defects, injuries, and diseases.'
},
{
    name: 'Oral and Maxillofacial Surgeon',
    description: 'Surgically treats diseases, injuries, and defects of the hard' +
        ' and soft tissues of the face, mouth, and jaws.'
},
{
    name: 'Orthopaedic Surgeon',
    description: 'Preserves and restores the function of the musculoskeletal system.'
},
{
    name: 'Otolaryngologist',
    description: 'Treats diseases of the ear, nose, and throat,and' +
        ' some diseases of the head and neck, including facial plastic surgery.'
},
{
    name: 'Pathologist',
    description: 'Diagnoses and treats the study of the changes in body' +
        ' tissues and organs which cause or are caused by disease.'
},
{
    name: 'Pediatrician',
    description: 'Treats infants, toddlers, children and teenagers.'
},
{
    name: 'Plastic Surgeon',
    description: 'Restores, reconstructs, corrects or improves in the shape' +
        ' and appearance of damaged body structures, especially the face.'
},
{
    name: 'Podiatrist',
    description: 'Provides medical and surgical treatment of the foot.'
},
{
    name: 'Psychiatrist',
    description: 'Treats patients with mental and emotional disorders.'
},
{
    name: 'Pulmonary Medicine Physician',
    description: 'Diagnoses and treats lung disorders..'
},
{
    name: 'Radiation Onconlogist',
    description: 'Diagnoses and treats disorders with the use of diagnostic imaging,' +
        ' including X-rays, sound waves, radioactive substances, and magnetic fields.'
},
{
    name: 'Diagnostic Radiologist',
    description: 'Diagnoses and medically treats diseases and disorders of internal structures of the body.'
},
{
    name: 'Rheumatologist',
    description: 'Treats rheumatic diseases, or conditions characterized by inflammation,' +
        'soreness and stiffness of muscles, and pain in joints and associated structures.'
},
{
    name: 'Urologist',
    description: 'Diagnoses and treats the male and female urinary tract and the male reproductive system.'
}];

export const slots = [{
    key: '15 mins',
    value: 15
},
{
    key: '30 mins',
    value: 30
},
{
    key: '45 mins',
    value: 45
},
{
    key: '1 hour',
    value: 60
}];

export const WeekDays = [
    {id: 0, name: 'Sun'},
    {id: 1, name: 'Mon'},
    {id: 2, name: 'Tue'},
    {id: 3, name: 'Wed'},
    {id: 4, name: 'Thu'},
    {id: 5, name: 'Fri'},
    {id: 6, name: 'Sat'}
];

export const LabTest = [
'Absolute Eosinophil count (Hematology)',
'Absolute Neutrophil count (Hematology)',
'Blood Grouping & Rh Typing (Hematology)',
'Bone Marrow Aspiration (Hematology)',
'BT/CT (Hematology)',
'CBP (Hematology)',
'Coomb\'s Direct & Indirect (Hematology)',
'ESR (Hematology)',
'Haemoglobin (Hematology) ',
'Haemogram (Hematology)',
'PCV (Hematology)',
'Parepheral Smear (MP/PF) (Hematology)',
'Platelet Count (Hematology)',
'PT.APTT (Hematology)',
'Reticulocyte Count (Hematology)',
'Sickling Test (Hematology)',
'Total Leucocyte Count (Hematology)',
'Body Fluids (Clinical Pathology)',
'Complete Urine Examination (Clinical Pathology)',
'Seman Analysis (Clinical Pathology)',
'Stool Occult Blood (Clinical Pathology)',
'Stool Routine Examination (Clinical Pathology)',
'Urine for Pregnancy Test(hCG) (Clinical Pathology)',
'Biopsy (Histopathology)',
'Cytopathology-Gynaac(PAP smear)(Histopathology)',
'Cytopathology-Non-Gynaac(Body Fluids)(Histopathology)',
'IHC (Histopathology)',
'Acid Phosphatase (Biochemestry)',
'Alkalin Phosphatase (Biochemestry)',
'Amylase (Biochemestry)',
'Bilirubin(Direct & Indirect) (Biochemestry)',
'Calcium (Biochemestry)',
'Cholesterol (Biochemestry)',
'CPK,CPK-MB (Biochemestry)',
'Electrolytes (Biochemestry)',
'FBS/PLBS/RBS (Biochemestry)',
'Glycated Haemoglobin(HbA1c) (Biochemestry)',
'GTT (Biochemestry)',
'GCT (Biochemestry)',
'GDM (Biochemestry)',
'HDL Cholesterol (Biochemestry)',
'Iron / TIBC / UIBC (Biochemestry)',
'LDH (Biochemestry)',
'LDL Cholesterol (Biochemestry)',
'LFT A (Biochemestry)',
'Lipid Profile (Biochemestry)',
'Phosphorous (Biochemestry)',
'Proteins / Albumin (Biochemestry)',
'Protine Electrophoresis (Biochemestry)',
'Triglycerides (Biochemestry)',
'Uric Asid (Biochemestry)',
'Urin for Microalbumin (Biochemestry)',
'Beta hCG (Hormones & Immunoassays)',
'CEA (Hormones & Immunoassays)',
'Cortisol (Hormones & Immunoassays)',
'E2-Estradiol (Hormones & Immunoassays)',
'Ferritin (Hormones & Immunoassays)',
'Folic Asid / Folate (Hormones & Immunoassays)',
'FSH,LH,Prolactin (Hormones & Immunoassays)',
'New Born Screening (Hormones & Immunoassays)',
'PTH (Hormones & Immunoassays)',
'T3,T4,TSH (Hormones & Immunoassays)',
'Testosterone (Hormones & Immunoassays)',
'Total Vitamin D (Hormones & Immunoassays)',
'Triple Marker / Double Marker (Hormones & Immunoassays)',
'Vitamin B12 (Hormones & Immunoassays)',
'AFP (Tumor Markers)',
'CA 125 (Tumor Markers)',
'CA 19-9 (Tumor Markers)',
'CA 15-3 (Tumor Markers)',
'PSA(Male) (Tumor Markers)',
'Others (Tumor Markers)',
'Blood(Bact Alert / Routine / Bactec) (Vitek/Routine)',
'Calheter tips(Central Line,ET tubes,etc) (Vitek/Routine)',
'Fluids (Vitek/Routine)',
'Pus (Vitek/Routine)',
'Semen (Vitek/Routine)',
'Sputum (Vitek/Routine)',
'Stool (Vitek/Routine)',
'Swabs(Throat,PUS,OT,swabs,any other) (Vitek/Routine)',
'Tissues (Vitek/Routine)',
'Urine (Vitek/Routine)',
'TB Culture(MGIT) (Vitek/Routine)',
'Anaemia Culture(Bactec / Routine)',
'Fungal Culture(Bactec / Routine)',
'AFB Stain (Stains)',
'Albert\s stain (Stains)',
'Gram\'s Stain (Stains)',
'KOH mounts (Stains)',
'India ink preparation for cryptococcus (Stains)',
'Modified ZN stain for cryptosporidium / cyclospora / isospora (Stains)',
'Scraping for Fungal Elements (Stains)',
'Slit Skin for AFB (Stains)',
'Stool Hanging drop (Stains)',
'ASO (Serology)',
'Anti HBs AG Titres (Serology)',
'Anti HBe (Serology)',
'CRP (Serology)',
'Dengue lgG,lgM & NS1 Antigem (Serology)',
'HAV lgM (Serology)',
'HBsAg (Serology)',
'HBeAg (Serology)',
'HCV (Serology)',
'HEV IgM (Serology)',
'HIV | & || (Serology)',
'Mantoux test (Serology)',
'Paul Bunnel Test (Serology)',
'Quntiferon Gold TB (Serology)',
'RA (Serology)',
'RF lgM (Serology)',
'RPR (VDRL) (Serology)',
'TORCH Panel-4 (Serology)',
'TORCH Panel-8 (Serology)',
'TORCH Panel-10 (Serology)',
'TPHA (Serology)',
'VDRL (Serology)',
'Widal (Serology)',
'Well / Felix Test (Serology)',
'BCR ABL (Molecular Biology)',
'MTB DR Plus(Drug Sensitivity of MTB) (Molecular Biology)',
'HBV TNA PCR - Qulitative (Molecular Biology)',
'HBV TNA PCR - Quantitative (Molecular Biology)',
'HPA HYBRID Capture Assay (Molecular Biology)',
'HIV RNA PCR (Molecular Biology)',
'ANA Profile (Molecular Biology)',
'Liver Profile (Molecular Biology)',
'Gastric Profile (Molecular Biology)',
'Vasculitis Profile (Molecular Biology)',
'U/S Adbomen Pelvis (Ultrasound)',
'U/S Breast (Ultrasound)',
'U/S Chest (Ultrasound)',
'U/S Neck (Ultrasound)',
'U/S TRUS (Ultrasound)',
'TVS (Ultrasound)',
'HRUS (Ultrasound)',
'X-Ray (Rediological Investigation)',
'Chest (Rediological Investigation)',
'Elbow (Rediological Investigation)',
'Hands (Rediological Investigation)',
'Shoulder (Rediological Investigation)',
'Wrist (Rediological Investigation)',
'Arterial Doppler of lumbs (Doppier Study)',
'Cardio Doppler (Doppier Study)',
'Portal Doppler (Doppier Study)',
'Renal Doppler (Doppier Study)',
'Venous Doppler of lumbs (Doppier Study)',
'ECG (Cardiology)',
'2D-ECHO (Cardiology)',
'TMT (Cardiology)',
'EEG (Neurology)',
'ENMG (Neurology)',
'Bone Densitometry (Miscellaneous)',
'CT (64 Slice)(Miscellaneous)',
'MRI (1.5 Tesla)(Miscellaneous)',
'OPG Digital(Miscellaneous)',
'PFT(Miscellaneous)',
'Others(Miscellaneous)'
];
