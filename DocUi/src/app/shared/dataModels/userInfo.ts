import {UserIdInfo} from './userIdInfo';

export class UserInfo {
    /** Common */
    id: string;
    address: string;
    dob: string;
    email: string;
    firstName: string;
    lastName: string;
    mobileNumber: string;
    profilePic: string;
    dpmEmailId: string;
    userId: UserIdInfo;
    title: string;
    pincode: string;
    addressLine: string;
    city: string;
    state: string;
    locality: string;
    awards: string;
    gender: string;
    medicalCouncil: string;
    licNo: string;
    remindPref: string;
    allergic: string;
    familyHistory: string;
    existingDesease: string;
    emergencyContact;
    height;
    weight;
    approvalFlag;

    /** Used for tutor */
    tutor_id: string;
    fees: number;
    inst_endTime: string;
    inst_startTime: string;
    institute: string;
    qualification: string;
}
