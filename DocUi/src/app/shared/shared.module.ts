import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoaderComponent } from './loader/loader.component';
import { ProfilePictureComponent } from '@app/shared/profile-picture/profile-picture.component';
import { FileUploadModule } from 'ng2-file-upload';

@NgModule({
  imports: [
    CommonModule,
    FileUploadModule
  ],
  declarations: [
    LoaderComponent,
    ProfilePictureComponent
  ],
  exports: [
    LoaderComponent,
    ProfilePictureComponent
  ]
})
export class SharedModule { }
