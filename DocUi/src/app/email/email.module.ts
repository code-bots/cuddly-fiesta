import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { PaginationModule } from 'ngx-bootstrap';


import { CoreModule } from '@app/core';
import { SharedModule } from '@app/shared';
import { EmailRoutingModule } from '@app/email/email-routing.module';
import { EmailComponent } from '@app/email/email.component';
import { ComposeEmailComponent } from './composeEmail/composeEmailComponent';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EmailService } from './email.service';
import { SelectModule } from 'ng2-select';
import { NgxEditorModule } from 'ngx-editor';


@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    CoreModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    EmailRoutingModule,
    SelectModule,
    NgxEditorModule,
    PaginationModule.forRoot()
  ],
  declarations: [
    EmailComponent,
    ComposeEmailComponent
  ],
  providers: [
    EmailService
  ]
})
export class EmailModule { }
