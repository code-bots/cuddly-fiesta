import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, Headers, Response } from '@angular/http';

const credentialsKey = 'credentials';
export interface Credentials {
    // Customize received credentials here
    username: string;
    token: string;
  }

@Injectable()
export class EmailService {
    private _credentials: Credentials | null;

    constructor(private http: Http) {
        const savedCredentials = sessionStorage.getItem(credentialsKey) || localStorage.getItem(credentialsKey);
        if (savedCredentials) {
          this._credentials = JSON.parse(savedCredentials);
        }
      }

      getContacts() {
        // http://localhost:8080/STMWeb/rest/api/email/contacts
        return this.http.get('/DocWeb/rest/api/email/contacts',
        { headers: this.getHeaderForRequest() }).map((res: Response) => res.json());
      }

      getEmailFolderDetails(stmEmailId: string) {
        // http://localhost:8080/STMWeb/rest/api/email/folder-details/{stmEmailId}/list
        return this.http.get('/DocWeb/rest/api/email/folder-details/' + stmEmailId + '/list',
        { headers: this.getHeaderForRequest() }).map((res: Response) => res.json());
      }

      getEmailItemsFromFolder(stmEmailId: string, folder: string, startIndex: number, endIndex: number) {
        // http://localhost:8080/STMWeb/rest/api/email/emailItems/{stmEmailId}/folder/INBOX?startIndex=1&endIndex=10
        return this.http.get('/DocWeb/rest/api/email/emailItems/' + stmEmailId +
        '/folder/' + folder + '?startIndex=' + startIndex + '&endIndex=' + endIndex,
        { headers: this.getHeaderForRequest() }).map((res: Response) => res.json());
      }

      sendEmail(context: any) {
        // http://localhost:8080/STMWeb/rest/api/email/send
        return this.http.post('/DocWeb/rest/api/email/send', context,
        { headers: this.getHeaderForRequest() }).map((res: Response) => res.json());
      }

      getHeaderForRequest() {
        const headers = new Headers();
        headers.append('Authorization', 'bearer ' + this._credentials.token);
        return headers;
      }
}
