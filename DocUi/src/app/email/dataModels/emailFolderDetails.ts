export class EmailFolderDetails {
    folderName: string;
    totalCount: number;
    unReadCount: number;
}
