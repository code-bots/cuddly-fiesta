import { ContactInfo } from './contactInfo';
export class Contacts {
    patients: ContactInfo[];
    doctors: ContactInfo[];
}
