import { EmailReceivers } from '@app/email/dataModels/emailReceivers';

export class EmailInfo {
    subject: string;
    body: string;
    from: string;
    receivers: EmailReceivers[];
    emailTime: number;
    fromContact: string;
}
