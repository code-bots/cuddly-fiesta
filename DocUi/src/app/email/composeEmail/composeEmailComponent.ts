import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { EmailService } from '../email.service';
import { forEach } from 'lodash';
import { UserInfo } from '@app/shared/dataModels/userInfo';
import { AuthenticationService } from '@app/core/authentication/authentication.service';

@Component({
    selector: 'app-compose-email',
    templateUrl: './composeEmail.component.html',
    styleUrls: ['../email.component.scss']
})
export class ComposeEmailComponent implements OnInit {
    @Input() toEmail: Array<any> = [];
    @Input() ccEmail: Array<any> = [];
    @Input() subjectEmail: string;
    @Input() bodyEmail: string;
    @Input() emailContactsList: Array<any> = [];
    @Input() userData: UserInfo;
    @Output() closeComposeEmailSection: EventEmitter<boolean> = new EventEmitter();
    composeEmailForm: FormGroup;

    constructor(private formBuilder: FormBuilder, private emailService: EmailService,
        private authenticationService: AuthenticationService) {
        this.refreshToEmailValue(this.toEmail);
        this.refreshCcEmailValue(this.ccEmail);
        this.createForm();
    }

    ngOnInit() {
        // code
    }

    goBackToMailList(isReloadNeeded: boolean) {
        this.closeComposeEmailSection.emit(isReloadNeeded);
    }

    sendEmail() {
        //     Post::http://localhost:8080/STMWeb/rest/api/email/send
        const requestObj: any = {};
        // stmEmailId or email?
        requestObj.fromEmail = this.userData.dpmEmailId;
        requestObj.subject = this.subjectEmail;
        requestObj.body = this.bodyEmail;
        requestObj.toEmails = this.getEmailIds(this.toEmail);
        requestObj.ccEmails = this.getEmailIds(this.ccEmail);
        this.emailService.sendEmail(requestObj).subscribe(
            data => {
                console.log(data);
            }, err => {
                console.log(err);
            }
        );
        this.goBackToMailList(true);
    }

    getEmailIds(emailCategory: any[]) {
        const emailIds: string[] = [];
        forEach(emailCategory, function(email: any) {
            emailIds.push(email.id);
        });
        return emailIds;
    }

    refreshToEmailValue(value: any): void {
        this.toEmail = value;
    }

    refreshCcEmailValue(value: any): void {
        this.ccEmail = value;
    }

    private createForm() {
        this.composeEmailForm = this.formBuilder.group({
            emailTo: ['', Validators.required],
            emailCc: '',
            emailSubject: ['', Validators.required],
            emailBody: ['', Validators.required]
        });
    }
}
