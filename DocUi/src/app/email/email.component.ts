import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { EmailService } from './email.service';
import { Contacts } from './dataModels/contacts';
import { forEach } from 'lodash';
import { ContactInfo } from './dataModels/contactInfo';
import { AuthenticationService } from '@app/core/authentication/authentication.service';
import { UserInfo } from '@app/shared/dataModels/userInfo';
import { EmailFolderDetails } from '@app/email/dataModels/emailFolderDetails';
import { EmailInfo } from '@app/email/dataModels/emailInfo';
import { thisExpression } from 'babel-types';

@Component({
  selector: 'app-email',
  templateUrl: './email.component.html',
  styleUrls: ['./email.component.scss']
})
export class EmailComponent implements OnInit {


  quote: string;
  isLoading: boolean;
  dateVisit = new Date();
  mode = '';
  userData: UserInfo;

  emailFolderDetails: EmailFolderDetails;
  currentPage: number;
  startPageIndex: number;
  endPageIndex: number;
  recordsPerPage = 10;
  emailList: EmailInfo[];

  /** Used for compose email */
  isComposeEmailView = false;
  subjectEmail: string;
  bodyEmail: string;
  ccEmail: Array<any> = [];
  toEmail: Array<any> = [];
  emailContactsList: Array<any> = [];

  emails = [];

  constructor(private route: ActivatedRoute, private emailService: EmailService,
    private authenticationService: AuthenticationService) {
    this.route.params.subscribe(params => {
      this.mode = params.folder;
      this.emailList = [];
      this.isComposeEmailView = false;
      console.log('In email construct');
      this.authenticationService.getUserDetails(this.authenticationService.credentials.token)
      .subscribe(
        data => {
          this.userData = data.doctor ? data.doctor : data;
          this.getEmailFolderDetails(this.mode === 'inbox' ? 'INBOX' : 'Sent');
        }
      );
    });
  }

  ngOnInit() {
    this.isLoading = true;
    this.isComposeEmailView = false;
    console.log('In email init');
    this.getContactDetails();
  }

  pageChanged($event: any) {
    this.startPageIndex = ($event.page * $event.itemsPerPage) - ($event.itemsPerPage - 1);
    this.getEmailItemsFromFolder();
}

  getContactDetails() {
    this.emailService.getContacts().subscribe(
      response => {
        this.emailContactsList = [];
        this.emailContactsList = this.setContactsEmailList(response.students,
          this.emailContactsList);
        this.emailContactsList = this.setContactsEmailList(response.tutors,
          this.emailContactsList);
      }, error => {
        // error handling
      }
    );
  }

  getEmailFolderDetails(emailFolder: string) {
    this.emailList = [];
    this.currentPage = 1;
    this.startPageIndex = 1;
    this.emailService.getEmailFolderDetails(this.userData.dpmEmailId).subscribe(
      response => {
        const foldersArray = response as EmailFolderDetails[];
        foldersArray.forEach(folder => {
          if (folder.folderName === emailFolder) {
            this.emailFolderDetails = folder;
          }
        });
        this.getEmailItemsFromFolder();
      }, error => {
        // error handling
      }
    );
  }

  getEmailItemsFromFolder() {
    this.endPageIndex = this.startPageIndex + (this.recordsPerPage - 1);
    this.emailService.getEmailItemsFromFolder(this.userData.dpmEmailId, this.emailFolderDetails.folderName,
      this.startPageIndex, this.endPageIndex).subscribe(
      response => {
        this.getUpdatedEmailIds(response);
      }, error => {
        // error handling
      }
    );
  }

  getUpdatedEmailIds(emailListResponse: EmailInfo[]) {
    emailListResponse.forEach(emailInfo => {
      this.emailContactsList.forEach(contact => {
        if (emailInfo.from === contact.id) {
          emailInfo.fromContact = contact.text;
        }
      });
    });
    this.emailList = emailListResponse;
  }

  replyAllInComposeMail(mail: EmailInfo) {
    const toList = [];
    const ccList = [];
    mail.receivers.forEach(receiver => {
      if (receiver.type === 'TO') {
        toList.push(receiver.emailId);
      } else if (receiver.type === 'CC') {
        ccList.push(receiver.emailId);
      }
    });
    this.goToComposeEmail(toList, ccList, 'Re: ' + mail.subject, mail.body);
  }

  viewMail(mail: EmailInfo) {
    const toList = [];
    const ccList = [];
    mail.receivers.forEach(receiver => {
      if (receiver.type === 'TO') {
        toList.push(receiver.emailId);
      } else if (receiver.type === 'CC') {
        ccList.push(receiver.emailId);
      }
    });
    this.goToComposeEmail(toList, ccList, mail.subject, mail.body);
  }

  composeNewEmail(toEmail: string, ccEmail: string, subjectEmail: string, bodyEmail: string) {
    this.goToComposeEmail([toEmail], [ccEmail], subjectEmail, bodyEmail);
  }

  goToComposeEmail(toEmail: string[], ccEmail: string[], subjectEmail: string, bodyEmail: string) {
    this.toEmail = toEmail ? this.findEmailInfoFromList(toEmail) : null;
    this.ccEmail = ccEmail ? this.findEmailInfoFromList(ccEmail) : null;
    this.subjectEmail = subjectEmail;
    this.bodyEmail = bodyEmail;
    this.isComposeEmailView = true;
  }

  closeComposeEmailSection(isReloadNeeded: boolean) {
    if (isReloadNeeded) {
      this.getEmailFolderDetails(this.mode === 'inbox' ? 'INBOX' : 'Sent');
    }
    this.isComposeEmailView = false;
    this.toEmail = null;
    this.ccEmail = null;
    this.subjectEmail = null;
  }

  private findEmailInfoFromList(emailIdList: string[]) {
    const emailInfo = [];
    emailIdList.forEach(emailId => {
      this.emailContactsList.forEach(email => {
        if (emailId === email.id) {
          emailInfo.push(email);
        }
      });
    });
    return emailInfo;
  }

  private setContactsEmailList(responseList: ContactInfo[], items: Array<any>) {
    forEach(responseList, function (contact: ContactInfo) {
      items.push({
        id: contact.stmEmailId,
        text: contact.firstName + ' ' + contact.lastName
      });
    });
    return items;
  }
}
