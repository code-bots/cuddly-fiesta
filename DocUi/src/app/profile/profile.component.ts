import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { AuthenticationService } from '../core/authentication/authentication.service';
import { UserInfo } from '../shared/dataModels/userInfo';
import { DomSanitizer } from '@angular/platform-browser';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ProfileService } from '@app/profile/profile.service';
import { TYPES_OF_SPECIALIZATION } from '@app/shared/dataModels/constData';
import { NotificationService } from '@app/core/notification.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  quote: string;
  isLoading: boolean;
  dateVisit = new Date();
  userData: UserInfo;
  editSummaryDetails = false;
  tempUserSummary: string;
  addUpForm: FormGroup;
  fromYear = [] as number[];
  toYear = [] as number[];
  showForm = false;
  lat = 28.532616848211614 as number;
  lng = 77.25288623303231 as number;
  startTime: Date = new Date();
  endTime: Date = new Date();
  weekModal = {
    sun: false,
    mon: true,
    tue: true,
    wed: true,
    thu: true,
    fri: true,
    sat: false
  };
  surgeries = [] as any[];
  loadingA = false;
  hospitals = [] as any[];
  hospitalToEdit;
  surgeryToEdit;
  starRating;
  patientRegistrationForm: FormGroup;
  doctorRegistrationForm: FormGroup;
  typesOfDoc = TYPES_OF_SPECIALIZATION;

  constructor(private authenticationService: AuthenticationService, private domSanitizer: DomSanitizer,
    private formBuilder: FormBuilder, private profileService: ProfileService,
    private notificationService: NotificationService, private router: Router) {
  }

  ngOnInit() {
    this.authenticationService.getUserDetails(this.authenticationService.credentials.token)
      .subscribe(
        data => {
          this.userData = data.doctor ? data.doctor : data;
          if (data.doctor && data.doctor.approvalFlag !== 1) {
            this.notificationService.showNotification('Warning',
            'You have updated the crucial contact detail, please revalidate with new OTP.', 'warning');
            sessionStorage.removeItem('credentials');
            localStorage.removeItem('credentials');
            this.router.navigate(['/login'], { replaceUrl: false });
          }
          this.starRating = data.star;
          this.isLoading = true;
          let startYear = new Date().getFullYear() - 30;
          const endYear = new Date().getFullYear();
          for (startYear; startYear <= endYear; startYear++) {
            this.fromYear.push(+startYear);
            this.toYear.push(+startYear);
          }
          console.log(this.fromYear);
          this.loadingA = true;
          if (!this.isDoctor) {
            this.profileService.getSurgeryForPatient(this.userData.id).subscribe(
              data1 => {
                this.surgeries = data1;
                this.loadingA = false;
                console.log(this.surgeries);
              }, err => {
                console.log('error in loading academics');
              }
            );
          } else {
            this.profileService.getHospitalForDoctor(this.userData.id).subscribe(
              data1 => {
                this.hospitals = data1;
                this.loadingA = false;
                console.log(this.hospitals);
              }, err => {
                console.log('error in loading institute');
              }
            );
          }
          this.createForm();
          this.createEditForm();
        }
      );
  }

  onLocationChoose($event: any) {
    this.lat = $event.coords.lat;
    this.lng = $event.coords.lng;
    console.log(this.lat + '::' + this.lng);
  }

  createForm() {
    if (this.isDoctor) {
      this.addUpForm = this.formBuilder.group({
        hospStartTime: '',
        hospEndTime: '',
        fees: ['', Validators.required],
        sun: ['false', Validators.required],
        mon: ['false', Validators.required],
        tue: ['false', Validators.required],
        wed: ['false', Validators.required],
        thu: ['false', Validators.required],
        fri: ['false', Validators.required],
        sat: ['false', Validators.required],
        name: ['', Validators.required],
        timeslot: ['15', Validators.required],
        paymentMode: ['', Validators.required]
      });
    } else {
      this.addUpForm = this.formBuilder.group({
        surgeryName: ['', Validators.required],
        hospitalName: ['', Validators.required],
        datePerformed: ['', Validators.required]
      });
    }
  }

  resetForm() {
    if (this.isDoctor) {
      this.addUpForm.controls.name.value('');
      this.addUpForm.controls.timeslot.value('');
      this.addUpForm.controls.paymentMode.value('');
    } else {
      this.addUpForm.controls.surgeryName.value('');
      this.addUpForm.controls.hospitalName.value('');
      this.addUpForm.controls.datePerformed.value('');
    }
  }

  createEditForm() {
    if (this.isDoctor) {
      this.doctorRegistrationForm = this.formBuilder.group({
        first_name: [this.userData.firstName, Validators.required],
        last_name: [this.userData.lastName, Validators.required],
        gender: [this.userData.gender, Validators.required],
        qualification: [this.userData.qualification, Validators.required],
        title: [this.userData.title, Validators.required],
        mobileNumber: [this.userData.mobileNumber, Validators.required],
        email: [this.userData.email, Validators.required],
        dob: [this.userData.dob, Validators.required],
        medicalCouncil: this.userData.medicalCouncil,
        licNo: [this.userData.licNo, Validators.required],
        addressLine: [this.userData.addressLine, Validators.required],
        locality: [this.userData.locality, Validators.required],
        city: [this.userData.city, Validators.required],
        pincode: [this.userData.pincode, Validators.required],
        state: [this.userData.state, Validators.required],
        awards: this.userData.awards
      });
    } else {
      this.patientRegistrationForm = this.formBuilder.group({
        first_name: [this.userData.firstName, Validators.required],
        last_name: [this.userData.lastName, Validators.required],
        dob: [this.userData.dob, Validators.required],
        gender: [this.userData.gender, Validators.required],
        height: [this.userData.height, [Validators.required, Validators.maxLength(3)]],
        weight: [this.userData.weight, Validators.required],
        address: [this.userData.address, Validators.required],
        locality: [this.userData.locality, Validators.required],
        city: [this.userData.city, Validators.required],
        pincode: [this.userData.pincode, Validators.required],
        state: [this.userData.state, Validators.required],
        email: [this.userData.email, Validators.required],
        mobileNumber: [this.userData.mobileNumber,
          [Validators.required, Validators.pattern('^((\\+91-?)|0)?[0-9]{10}$')]],
        emergency_contact: this.userData.emergencyContact,
        existingDesease: this.userData.existingDesease,
        familyHistory: this.userData.familyHistory,
        allergic: this.userData.allergic,
        remindPref: this.userData.remindPref
      });
    }
  }

  save() {
    const saveContext = this.addUpForm.value;
    console.log(saveContext);
    if (this.isDoctor) {
      saveContext.latitude = this.lat;
      saveContext.longitude = this.lng;
      saveContext.weekdays = this.getWeekdays();
      saveContext.hospStartTime = this.startTime.getTime();
      saveContext.hospEndTime = this.endTime.getTime();
      saveContext.paymentMode = +saveContext.paymentMode;
      delete saveContext.sun;
      delete saveContext.mon;
      delete saveContext.tue;
      delete saveContext.wed;
      delete saveContext.thu;
      delete saveContext.fri;
      delete saveContext.sat;
      this.profileService.saveHospital(this.hospitalToEdit ? this.hospitalToEdit.id : '-1',
        saveContext, this.userData.id).subscribe(
          result => {
            this.showForm = false;
            this.addUpForm.reset();
            this.loadingA = true;
            this.hospitalToEdit = undefined;
            this.profileService.getHospitalForDoctor(this.userData.id).subscribe(
              data => {
                this.hospitals = data;
                this.loadingA = false;
              }
            );
          }, err => {
            console.log('error in adding hospital');
          }
        );
    } else {
      this.profileService.saveSurgery(this.surgeryToEdit ? this.surgeryToEdit.id : '-1',
        saveContext, this.userData.id).subscribe(
          result => {
            this.showForm = false;
            this.addUpForm.reset();
            this.loadingA = true;
            this.surgeryToEdit = undefined;
            this.profileService.getSurgeryForPatient(this.userData.id).subscribe(
              data => {
                this.surgeries = data;
                this.loadingA = false;
              }
            );
          },
          err => {
            console.log('error in adding surgery');
          }
        );
    }
  }

  /** Called when user clicks on edit icon for summary */
  editSummary() {
    if (this.editSummaryDetails) {
      this.editSummaryDetails = false;
      return;
    }
    this.editSummaryDetails = true;
  }

  /** Save the user summary */
  public updateSummary() {
    // call api to save the user data and on success perform the following steps
    if (this.isDoctor) {
      const docToSave = {
        firstName: this.doctorRegistrationForm.value.first_name,
        lastName: this.doctorRegistrationForm.value.last_name,
        qualification: this.doctorRegistrationForm.value.qualification,
        title: this.doctorRegistrationForm.value.title,
        email: this.doctorRegistrationForm.value.email,
        mobileNumber: this.doctorRegistrationForm.value.mobileNumber,
        licNo: this.doctorRegistrationForm.value.licNo,
        medicalCouncil: this.doctorRegistrationForm.value.medicalCouncil,
        awards: this.doctorRegistrationForm.value.awards,
        dob: this.doctorRegistrationForm.value.dob,
        profilePic: this.userData.profilePic,
        gender: this.doctorRegistrationForm.value.gender,
        city: this.doctorRegistrationForm.value.city,
        state: this.doctorRegistrationForm.value.state,
        locality: this.doctorRegistrationForm.value.locality,
        pincode: this.doctorRegistrationForm.value.pincode,
        addressLine: this.doctorRegistrationForm.value.addressLine,
        approvalFlag: this.userData.approvalFlag
      };
      const needRedirectionToLogin = this.userData.mobileNumber !== docToSave.mobileNumber
      || this.userData.email !== docToSave.email ;
      this.profileService.saveDoctorDetails(this.userData.id, docToSave).subscribe(
        data => {
          this.notificationService.showNotification('Success', 'Dr.' + docToSave.firstName + ' ' + docToSave.lastName +
            ' details successfully updated.', 'success');
          if (needRedirectionToLogin) {
            sessionStorage.removeItem('credentials');
            localStorage.removeItem('credentials');
            this.router.navigateByUrl(`/login`);
          } else {
            location.reload();
          }
          this.editSummaryDetails = false;
        }, err => {
          this.notificationService.showNotification('Error', 'Dr.' + docToSave.firstName + ' ' + docToSave.lastName +
            ' details not updated.', 'error');
        }
      );
    } else {
      const patientToBeSaved = {
        firstName: this.patientRegistrationForm.value.first_name,
        lastName: this.patientRegistrationForm.value.last_name,
        dob: this.patientRegistrationForm.value.dob,
        height: this.patientRegistrationForm.value.height,
        weight: this.patientRegistrationForm.value.weight,
        address: this.patientRegistrationForm.value.address,
        mobileNumber: this.patientRegistrationForm.value.mobileNumber,
        email: this.patientRegistrationForm.value.email,
        emergencyContact: this.patientRegistrationForm.value.emergency_contact,
        existingDesease: this.patientRegistrationForm.value.existingDesease,
        familyHistory: this.patientRegistrationForm.value.familyHistory,
        allergic: this.patientRegistrationForm.value.allergic,
        remindPref: this.patientRegistrationForm.value.remindPref,
        profilePic: this.userData.profilePic,
        gender: this.patientRegistrationForm.value.gender,
        city: this.patientRegistrationForm.value.city,
        state: this.patientRegistrationForm.value.state,
        locality: this.patientRegistrationForm.value.locality,
        pincode: this.patientRegistrationForm.value.pincode
      };
      const needRedirectionToLogin = this.userData.mobileNumber !== patientToBeSaved.mobileNumber
      || this.userData.email !== patientToBeSaved.email ;
      this.profileService.savePatientDetails(this.userData.id, patientToBeSaved).subscribe(
        data => {
          this.notificationService.showNotification('Success', 'Mr.' + patientToBeSaved.firstName + ' ' +
            patientToBeSaved.lastName + ' details successfully updated.', 'success');
            if (needRedirectionToLogin) {
              sessionStorage.removeItem('credentials');
              localStorage.removeItem('credentials');
              this.router.navigateByUrl(`/login`);
            } else {
              location.reload();
            }
          this.editSummaryDetails = false;
        }, err => {
          this.notificationService.showNotification('Error', 'Mr.' + patientToBeSaved.firstName + ' ' +
            patientToBeSaved.lastName + ' details not updated.', 'error');
        }
      );
    }
  }

  private getWeekdays(): string {
    const days = [];
    if (this.addUpForm.value.sun === true) {
      days.push(0);
    }
    if (this.addUpForm.value.mon === true) {
      days.push(1);
    }
    if (this.addUpForm.value.tue === true) {
      days.push(2);
    }
    if (this.addUpForm.value.wed === true) {
      days.push(3);
    }
    if (this.addUpForm.value.thu === true) {
      days.push(4);
    }
    if (this.addUpForm.value.fri === true) {
      days.push(5);
    }
    if (this.addUpForm.value.sat === true) {
      days.push(6);
    }
    return days.toString();
  }

  private getUIFriendlyWeeks(weekNos: string) {
    const weekNosArr = weekNos.split(',');
    const returnWeekNames = [];
    weekNosArr.forEach(weekNo => {
      returnWeekNames.push(this.getWeekName(weekNo));
    });
    return returnWeekNames.toString();
  }

  private getWeekName(weekNo: string) {
    switch (weekNo) {
      case '0': return 'Sun';
      case '1': return 'Mon';
      case '2': return 'Tue';
      case '3': return 'Wed';
      case '4': return 'Thu';
      case '5': return 'Fri';
      case '6': return 'Sat';
    }
  }

  editHospital(hospital: any) {
    this.hospitalToEdit = hospital;
    if (this.hospitalToEdit) {
      this.startTime = this.getTime(this.hospitalToEdit.hospStartTime);
      this.endTime = this.getTime(this.hospitalToEdit.hospEndTime);
      this.addUpForm.controls['fees'].setValue(this.hospitalToEdit.fees);
      this.lat = +this.hospitalToEdit.latitude;
      this.lng = +this.hospitalToEdit.longitude;
      this.addUpForm.controls['name'].setValue(this.hospitalToEdit.name);
      this.addUpForm.controls['timeslot'].setValue(this.hospitalToEdit.timeslot);
      this.addUpForm.controls['paymentMode'].setValue(this.hospitalToEdit.paymentMode);
      this.addUpForm.controls['sun'].setValue(this.hospitalToEdit.weekdays.indexOf('0') !== -1);
      this.addUpForm.controls['mon'].setValue(this.hospitalToEdit.weekdays.indexOf('1') !== -1);
      this.addUpForm.controls['tue'].setValue(this.hospitalToEdit.weekdays.indexOf('2') !== -1);
      this.addUpForm.controls['wed'].setValue(this.hospitalToEdit.weekdays.indexOf('3') !== -1);
      this.addUpForm.controls['thu'].setValue(this.hospitalToEdit.weekdays.indexOf('4') !== -1);
      this.addUpForm.controls['fri'].setValue(this.hospitalToEdit.weekdays.indexOf('5') !== -1);
      this.addUpForm.controls['sat'].setValue(this.hospitalToEdit.weekdays.indexOf('6') !== -1);
      this.showForm = true;
    }
  }

  editSurgery(surgery: any) {
    this.surgeryToEdit = surgery;
    this.addUpForm.controls['surgeryName'].setValue(this.surgeryToEdit.surgeryName);
    this.addUpForm.controls['hospitalName'].setValue(this.surgeryToEdit.hospitalName);
    this.addUpForm.controls['datePerformed'].setValue(this.surgeryToEdit.datePerformed);
    this.showForm = true;
  }

  getTime(time: any) {
    const readDate = new Date(+time);
    const currentDate = new Date();
    currentDate.setHours(readDate.getHours());
    currentDate.setMinutes(readDate.getMinutes());
    return currentDate;
  }

  get isDoctor() {
    return this.userData.userId.roles[0].roleName === 'DOCTOR';
  }

  setProfilePicData($event: any) {
    this.userData.profilePic = $event;
  }
}
