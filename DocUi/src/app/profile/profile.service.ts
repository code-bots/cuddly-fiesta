import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

export interface Credentials {
  // Customize received credentials here
  username: string;
  token: string;
}

const credentialsKey = 'credentials';

@Injectable()
export class ProfileService {

  private _credentials: Credentials | null;

  constructor(private http: Http) {
    const savedCredentials = sessionStorage.getItem(credentialsKey) || localStorage.getItem(credentialsKey);
    if (savedCredentials) {
      this._credentials = JSON.parse(savedCredentials);
    }
  }

  saveSurgery(surgeryId: string, context: any, id: string) {
    const headers =  new Headers();
    headers.append('Authorization', 'bearer ' + this._credentials.token);
    if (surgeryId !== '-1') {
      return this.http.put('/DocWeb/rest/api/patient/' + id + '/surgery/' + surgeryId
      , context, { headers: headers });
    } else {
      return this.http.post('/DocWeb/rest/api/patient/' + id + '/surgery'
      , context, { headers: headers }).map((res: Response) => res.json());
    }
  }

  getSurgeryForPatient(id: string) {
    const headers =  new Headers();
    headers.append('Authorization', 'bearer ' + this._credentials.token);
    return this.http.get('/DocWeb/rest/api/patient/' + id + '/surgeries',
    {headers}).map((res: Response) => res.json());
  }

  saveHospital(hospitalId: string, context: any, docId: string) {
    const headers =  new Headers();
    headers.append('Authorization', 'bearer ' + this._credentials.token);
    if (hospitalId !== '-1') {
      return this.http.put('/DocWeb/rest/api/hospital/' + hospitalId
      , context, { headers: headers }).map((res: Response) => res.json());
    } else {
      return this.http.post('/DocWeb/rest/api/hospital/' + docId
      , context, { headers: headers }).map((res: Response) => res.json());
    }
  }

  saveDoctorDetails(doctorId: string, context: any) {
    const headers =  new Headers();
    headers.append('Authorization', 'bearer ' + this._credentials.token);
    return this.http.put('/DocWeb/rest/api/doctor/' + doctorId
    , context, { headers: headers }).map((res: Response) => res.json());
  }

  savePatientDetails(patientId: string, context: any) {
    const headers =  new Headers();
    headers.append('Authorization', 'bearer ' + this._credentials.token);
    return this.http.put('/DocWeb/rest/api/patient/' + patientId
    , context, { headers: headers }).map((res: Response) => res.json());
  }

  getHospitalForDoctor(docId: string) {
    const headers =  new Headers();
    headers.append('Authorization', 'bearer ' + this._credentials.token);
    return this.http.get('/DocWeb/rest/api/hospital/' + docId,
    {headers}).map((res: Response) => res.json());
  }
}
