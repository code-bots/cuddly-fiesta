import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '@app/core';
import { ADMIN } from '@app/shared/dataModels/constData';

@Component({
  selector: 'app-shell',
  templateUrl: './shell.component.html',
  styleUrls: ['./shell.component.scss']
})
export class ShellComponent implements OnInit {

  userData: any;
  constructor(private authenticationService: AuthenticationService) {
    const savedCredentials = sessionStorage.getItem('credentials') || localStorage.getItem('credentials');
    if (savedCredentials) {
      const credentials = JSON.parse(savedCredentials);
      if (credentials.username === ADMIN) {
        this.userData = {
          firstName: 'Admin',
          lastName: '',
          email: ADMIN
        };
      } else {
        this.authenticationService.getUserDetails(this.authenticationService.credentials.token)
        .subscribe(
          data => {
            this.userData = data.doctor ? data.doctor : data;
          }
        );
      }
    }
  }

  ngOnInit() { }

}
