import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthenticationService } from '../../authentication/authentication.service';
import { I18nService } from '../../i18n.service';
import { DomSanitizer } from '@angular/platform-browser';
import { UserInfo } from '@app/shared/dataModels/userInfo';
import * as $ from 'jquery';
import { timeout } from 'rxjs/operators';
import { ADMIN } from '@app/shared/dataModels/constData';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  userData: any;
  menuHidden = true;

  constructor(private router: Router,
    private authenticationService: AuthenticationService,
    private i18nService: I18nService,
    private domSanitizer: DomSanitizer) { }

  ngOnInit() {
    const savedCredentials = sessionStorage.getItem('credentials') || localStorage.getItem('credentials');
    if (savedCredentials) {
      const credentials = JSON.parse(savedCredentials);
      if (credentials.username === ADMIN) {
        this.userData = {
          firstName: 'Admin',
          lastName: '',
          email: ADMIN
        };
      } else {
        this.authenticationService.getUserDetails(this.authenticationService.credentials.token)
        .subscribe(
          data => {
            this.userData = data.doctor ? data.doctor : data;
          });
      }
    }
    setTimeout(function () {
      const _nav = $('.vertical-nav');
      $('.collapse-menu').click(function () {
        _nav.toggleClass('vertical-nav-sm');
        $('.dashboard-wrapper').toggleClass('dashboard-wrapper-lg');
        $('footer').toggleClass('footer-sm');
        $('i', this).toggleClass('fa-list');
        $('i', this).toggleClass('fa-th');
      });

      $('.toggle-menu').click(function () {
        _nav.toggleClass('vertical-nav-opened');
      });
    }, 1000);
  }

  toggleMenu() {
    $('.vertical-nav').toggleClass('vertical-nav-opened');
  }

  setLanguage(language: string) {
    this.i18nService.language = language;
  }

  logout() {
    const savedCredentials = sessionStorage.getItem('credentials') || localStorage.getItem('credentials');
    if (savedCredentials) {
      const credentials = JSON.parse(savedCredentials);
      if (credentials.username === ADMIN) {
      } else {
        this.authenticationService.setAvailibility(false).subscribe();
      }
      const removed = sessionStorage.removeItem('credentials') || localStorage.removeItem('credentials');
    }
    this.router.navigate(['/login'], { replaceUrl: false });
  }

  goTo(page: string, param: string) {
    console.log(page + '::' + param);
    this.toggleMenu();
    if (param) {
      this.router.navigate(['/' + page, param], { replaceUrl: false });
    } else {
      this.router.navigate(['/' + page], { replaceUrl: false });
    }
  }

  goToChangePassword() {
    this.router.navigate(['/change_password', this.isDoctor ? 'doctor' : 'patient',
      this.userData.id], { replaceUrl: false });
  }

  get currentLanguage(): string {
    return this.i18nService.language;
  }

  get languages(): string[] {
    return this.i18nService.supportedLanguages;
  }

  get isDoctor() {
    return this.userData && this.userData.userId && this.userData.userId.roles[0].roleName === 'DOCTOR';
  }

  get isAdmin() {
    return this.userData && this.userData.email === ADMIN;
  }
}
