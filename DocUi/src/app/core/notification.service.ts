import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { remove, indexOf } from 'lodash';
import { ToastyService, ToastyConfig, ToastOptions, ToastData } from 'ng2-toasty';
@Injectable()
export class NotificationService {
    public static readonly WARNING = 'warning';
    public static readonly ERROR = 'error';
    public static readonly SUCCESS = 'success';
    public id: string;
    public titles: string;
    public content: string;
    public notification: Notification ;

    constructor(private toastyService: ToastyService,
    private toastyConfig: ToastyConfig) {
        // Assign the selected theme name to the `theme` property of the instance of ToastyConfig.  
        // Possible values: default, bootstrap, material
        this.toastyConfig.theme = 'material';
        this.toastyConfig.position = 'center-center';
    }

    clearAll() {
        this.toastyService.clearAll();
    }

    showNotification(titles: string, content: string, notificationType: string) {
        const toastOptions = this.getToastOptionsFromFactory(titles, content, 10000);
        this.showNotificationBase(titles, content, notificationType, toastOptions);
    }

    /**
     * produces the toastOptions object
     * @param titles
     * @param content
     * @param timeoutValue
     */
    private getToastOptionsFromFactory(titles: string, content: string, timeoutValue: number): ToastOptions {
        const toastOptions: ToastOptions = {
            title: titles ,
            msg: content ,
            showClose: true ,
            timeout: timeoutValue,
            theme: 'default' ,
            onAdd: (toast: ToastData) => {
                this.id = '' + toast.id;
            },
        };
        return toastOptions;
    }

    private showNotificationBase(titles: string, content: string, notificationType: string,
        toastOptions: ToastOptions, details?: string) {
        switch (notificationType) {
            case 'default': this.toastyService.default(toastOptions); break;
            case 'info': this.toastyService.info(toastOptions); break;
            case 'success': this.toastyService.success(toastOptions); break;
            case 'wait': this.toastyService.wait(toastOptions); break;
            case 'error': this.toastyService.error(toastOptions); break;
            case 'warning': this.toastyService.warning(toastOptions); break;
        }
    }
}
