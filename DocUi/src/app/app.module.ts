import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ServiceWorkerModule } from '@angular/service-worker';
import { TranslateModule } from '@ngx-translate/core';
import { NgPipesModule } from 'ngx-pipes';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastyModule } from 'ng2-toasty';
import { environment } from '@env/environment';
import { CoreModule } from '@app/core';
import { SharedModule } from '@app/shared';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { HomeModule } from './home/home.module';
import { AboutModule } from './about/about.module';
import { LoginModule } from './login/login.module';
import { ScheduleModule } from './schedule/schedule.module';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ChatModule } from '@app/chat/chat.module';
import { EmailModule } from '@app/email/email.module';
import { ProfileModule } from '@app/profile/profile.module';
import { AuthInterceptor } from '@app/core/http/auth.interceptor';
import { Router } from '@angular/router';
import { DoctorsModule } from '@app/doctors/doctors.module';
import { BlogComponent } from './blog/blog.component';
import { BlogModule } from '@app/blog/blog.module';
import { PatientsModule } from '@app/patients/patients.module';
import { NotificationService } from '@app/core/notification.service';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ToastyModule.forRoot(),
    ServiceWorkerModule.register('./ngsw-worker.js', { enabled: environment.production }),
    FormsModule,
    HttpClientModule,
    TranslateModule.forRoot(),
    NgbModule.forRoot(),
    NgPipesModule,
    CoreModule,
    SharedModule,
    HomeModule,
    AboutModule,
    LoginModule,
    ScheduleModule,
    ChatModule,
    EmailModule,
    BlogModule,
    ProfileModule,
    DoctorsModule,
    PatientsModule,
    AppRoutingModule
  ],
  declarations: [AppComponent],
  providers: [
    AuthInterceptor,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
      deps: [Router, NotificationService]
  }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
