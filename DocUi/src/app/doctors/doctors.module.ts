import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { PaginationModule, ButtonsModule, ModalModule } from 'ngx-bootstrap';


import { CoreModule } from '@app/core';
import { SharedModule } from '@app/shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SelectModule } from 'ng2-select';
import { NgxEditorModule } from 'ngx-editor';
import { DoctorsService } from '@app/doctors/doctors.service';
import { DoctorsComponent } from '@app/doctors/doctors.component';
import { DoctorsRoutingModule } from '@app/doctors/doctors-routing.module';
import { NgPipesModule } from 'ngx-pipes';
import { DoctorScheduleComponent } from '@app/doctors/doctor_schedule.component';
import { RatingModule } from 'ngx-bootstrap';
import { DatePipe } from '@angular/common';
import { ToastyModule } from 'ng2-toasty';
import { DocDashboardComponent } from '@app/doctors/doc-dashboard.component';
import { CalendarModule } from 'ap-angular2-fullcalendar';
import { NgxChartsModule } from '@swimlane/ngx-charts';



@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    NgPipesModule,
    CoreModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    DoctorsRoutingModule,
    SelectModule,
    ModalModule.forRoot(),
    ButtonsModule.forRoot(),
    PaginationModule.forRoot(),
    RatingModule.forRoot(),
    ToastyModule.forRoot(),
    CalendarModule.forRoot(),
    NgxChartsModule
  ],
  declarations: [
    DoctorsComponent,
    DoctorScheduleComponent,
    DocDashboardComponent
  ],
  providers: [
    DoctorsService,
    DatePipe
  ]
})
export class DoctorsModule { }
