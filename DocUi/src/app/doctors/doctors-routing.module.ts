import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Route, extract } from '@app/core';
import { DoctorsComponent } from '@app/doctors/doctors.component';
import { DocDashboardComponent } from '@app/doctors/doc-dashboard.component';

const routes: Routes = [
  Route.withShell([
    { path: 'doctors', component: DoctorsComponent, data: { title: extract('Doctors') } },
    { path: 'doc-dashboard', component: DocDashboardComponent, data: { title: extract('Dashboard') } },
    { path: 'doctors/:scheduleId/:status',
      component: DoctorsComponent, data: { title: extract('Doctors') } }
  ])
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class DoctorsRoutingModule { }
