import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { AuthenticationService } from '../core/authentication/authentication.service';
import { UserInfo } from '../shared/dataModels/userInfo';
import { DomSanitizer } from '@angular/platform-browser';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { keyBy, filter, clone, remove } from 'lodash';
import { ProfileService } from '@app/profile/profile.service';
import { PatientsService } from '@app/patients/patients.service';
import { ScheduleService } from '@app/schedule/schedule.service';
import { NotificationService } from '@app/core/notification.service';
import { DoctorsService } from '@app/doctors/doctors.service';
import { DatePipe } from '@angular/common';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { TemplateRef } from '@angular/core';
import { ViewChild } from '@angular/core';

@Component({
  selector: 'app-doc-dashboard',
  templateUrl: './doc-dashboard.component.html',
  styleUrls: ['./doctors.component.scss']
})
export class DocDashboardComponent implements OnInit {
  isLoading: boolean;
  userData: UserInfo;
  loading = false;
  prescHistory = [];
  events;
  calendarOptions;
  currentMedicine = [];
  blogs = [];
  appointmentTrend = [];

  // options
  view;

  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = false;
  showXAxisLabel = true;
  xAxisLabel = 'Date';
  showYAxisLabel = true;
  yAxisLabel = 'No of appointment';
  modalRef: BsModalRef;
  @ViewChild('eventDetail') eventPopup: TemplateRef<any>;
  @ViewChild('eventDetail2') calEvent: TemplateRef<any>;

  colorScheme = {
    domain: ['#0080FF', '#73C2FB', '#89CFF0', '#7EF9FF', '#B0DFE5', '#3FE0D0', '#6593F5']
  };

  dateForTrend;
  mode = 'month';


  constructor(private authenticationService: AuthenticationService, private domSanitizer: DomSanitizer,
    private formBuilder: FormBuilder, private doctorService: DoctorsService, private scheduleService: ScheduleService,
    private notificationService: NotificationService, private datePipe: DatePipe,
    private modalService: BsModalService) {
  }

  ngOnInit() {
    this.authenticationService.getUserDetails(this.authenticationService.credentials.token)
      .subscribe(
        data => {
          this.userData = data.doctor ? data.doctor : data;
          this.isLoading = true;
          this.getPrescriptionDoctorWise();
          this.loadCalendarEvent();
          this.loadBlogForDoctor();
          const date = new Date();
          date.setDate(date.getDate() - 30);
          this.dateForTrend = this.datePipe.transform(date, 'yyyy-MM-dd');
          this.loadTrendData();
        }
      );
  }

  loadTrendData() {
    this.doctorService.getVisitTrendForDoctor(this.userData.id, this.dateForTrend).subscribe(
      data => {
        data.forEach(element => {
          this.appointmentTrend.push({
            name: element[0],
            value: element[1]
          });
        });
      }
    );
  }

  getPrescriptionDoctorWise() {
    this.loading = false;
    this.doctorService.getPrescriptionGivenByDoctor(this.userData.id).subscribe(
      data => {
        // this.targetHospital = this.hospitals.filter(hospital => hospital.id === this.selectedHospital)[0];
        data.forEach(pData => {
            this.prescHistory.push({
              id: pData.prescriptionId.id,
              for: pData.prescriptionId.scheduleId.remarks,
              status: pData.prescriptionId.scheduleId.schStatus,
              prescriptions: pData.prescriptionDetails,
              testFiles: pData.listOfFiles,
              date: pData.prescriptionId.scheduleId.date,
              startTime: pData.prescriptionId.scheduleId.startTime,
              remarks: pData.prescriptionId.remarks,
              prescribedTo: {
                profilePic: pData.prescriptionId.scheduleId.patientId.profilePic,
                firstName: pData.prescriptionId.scheduleId.patientId.firstName,
                lastName: pData.prescriptionId.scheduleId.patientId.lastName,
                id: pData.prescriptionId.scheduleId.patientId.id
              }
            });
        });
        this.prescHistory.forEach(prec => {
          const dateOfPrec = prec.date;
          prec.prescriptions.forEach(details => {
            if (details.type === 'medicine' && this.qualifiedForCurrentDate(dateOfPrec, details)) {
              this.currentMedicine.push(details);
            }
          });
        });
        this.loading = true;
      }
    );
  }

  loadBlogForDoctor() {
    this.doctorService.getBlogByDoctor(this.userData.id).subscribe(
      data => {
        this.blogs = data;
      }
    );
  }

  qualifiedForCurrentDate(dateOfPrec: any, dayToContinue: any) {
    const currentDate = new Date();
    const dateOfCourse = new Date(dateOfPrec);
    dateOfCourse.setDate(dateOfCourse.getDate() + (+dayToContinue.additionalInfo3));
    dayToContinue.targetDate = dateOfCourse;
    return dateOfCourse.getTime() >= currentDate.getTime();
  }

  loadCalendarEvent() {
    this.scheduleService.getCalenderEventsForDoctor(this.userData.id).subscribe(
      data => {
      const __this = this;
      this.events = this.getCalenderEvents(data);
      this.calendarOptions = {
        height: 400,
        themeSystem: 'bootstrap4',
        fixedWeekCount : false,
        defaultDate: new Date(),
        selectable: true,
        editable: false,
        eventLimit: true, // allow "more" link when too many events
        defaultView: 'month',
        events: this.events,
        eventClick: function(eventObj: any) {
          __this.setObjectAndSHowPopup(eventObj);
        }
      };
  });
}

setObjectAndSHowPopup(event: any) {
  this.modalRef = this.modalService.show(this.calEvent, {class: 'modal-sm'});
      setTimeout(function () {
        $('#eventTitle').html(event.title + '<br><br>Appointment start time: ' +
        event.start.format('YYYY-MM-DD HH:mm')
        + ' and end time: ' + event.end.format('YYYY-MM-DD HH:mm') + '.');
        }, 0);
}

getCalenderEvents(data: any): any {
  const events = [];
  data.forEach(d => {
     const schedule = d;
   events.push({
      title: d.remarks,
      start: this.getDateForEvent(schedule.date, schedule.startTime),
      end: this.getDateForEvent(schedule.date, schedule.endTime),
      color: '#ff5c56'
    });
  });
  return events;
}

getDateForEvent(date: string, time: string): string {
  const dateAndTime = date + 'T' + time;
  return dateAndTime;
}

  getAdditionalInfo1(info1: any) {
    if (info1.type === 'medicine') {
      const prefix = '<label>Times: </label>';
      switch (info1.additionalInfo1) {
        case '24' : return prefix + 'Once in a day';
        case '12' : return prefix + 'Two times in a day';
        case '8' : return prefix + 'Three times in a day';
        case '6' : return prefix + 'Four times in a day';
      }
    }
    return info1;
  }

  getAdditionalInfo2(info1: any) {
    if (info1.type === 'medicine') {
      const prefix = '<label>When: </label>';
      switch (info1.additionalInfo2) {
        case 'BeforeFood' : return prefix + 'Before food';
        case 'AfterFood' : return prefix + 'After food';
      }
    }
    return info1;
  }

  getAdditionalInfo3(info1: any) {
    if (info1.type === 'medicine' && info1.additionalInfo3 !== null) {
      const prefix = '<label>Duration: </label>' + info1.additionalInfo3 + ' days';
      return prefix;
    }
    return '';
  }

  updateDate() {
    const date = new Date();
    if (this.mode === 'year') {
      date.setFullYear(date.getFullYear() - 1);
    } else  {
      date.setDate(date.getDate() - 30);
    }
    this.dateForTrend = this.datePipe.transform(date, 'yyyy-MM-dd');
    this.loadTrendData();
  }

  get isDoctor() {
    return this.userData.userId.roles[0].roleName === 'DOCTOR';
  }

  getPrecDetails(prescHistory: any) {
    let contents = '';
    contents = contents + '<h4>Prescription on ' + prescHistory.date + ' at ' + prescHistory.startTime + '</h4>';
    contents = contents + '<br><br><b>Patient Name: </b>' + prescHistory.prescribedTo.firstName + ' ' +
    prescHistory.prescribedTo.lastName + '<br><br>';
    contents = contents + '<h4>Prescription Details</h4><hr>';
    const medicines = [];
    const tests = [];
    prescHistory.prescriptions.forEach(element => {
      if (element.type === 'medicine') {
        const htmlMedicine = '<b>Medicine: </b>' + element.name + '<br>' +
        '<b>Time period: </b>' + element.additionalInfo3 + ' days<br>' +
        '<b>Time to take: </b>' + this.getAdditionalInfo1Text(element.additionalInfo1) +
        '<b> When to take: </b>' + this.getAdditionalInfo2Text(element.additionalInfo2) + '<br>' +
        '<b>Side Effect of medicine: </b>' + (element.sideEffect ? element.sideEffect : 'Not any') + '<br>' +
        '<b>Any remark: </b>' + (element.remarks ? element.remarks : 'Not any') + '<hr>';
        medicines.push(htmlMedicine);
      } else {
        const testMedicine = '<b>Lab Test: </b>' + element.name + '<br>' +
        '<b>Any remark: </b>' + (element.remarks ? element.remarks : 'Not any') + '<hr>';
        tests.push(testMedicine);
      }
    });
    medicines.forEach(med => {
      contents = contents + med;
    });
    tests.forEach(test => {
      contents = contents + test;
    });
    contents = contents + '<hr><b>Note: </b>' + prescHistory.remarks + '<hr><br><br>';
    contents = contents + '<b>By: </b>Dr. ' + this.userData.firstName + ' ' +
    this.userData.lastName;
    this.modalRef = this.modalService.show(this.eventPopup);
    setTimeout(function () {
      $('#precDetail').html(contents);
      }, 0);
  }

  print() {
    let printContents, popupWin;
    printContents = document.getElementById('precDetail').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>Prescription Summary </title>` +
          `<style>
          //........Customized style.......
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }

  getAdditionalInfo2Text(info1: string) {
    switch (info1) {
      case 'BeforeFood' : return 'Before food';
      case 'AfterFood' : return 'After food';
    }
  return info1;
}
getAdditionalInfo1Text(info1: string) {
  switch (info1) {
    case '24' : return 'Once in a day';
    case '12' : return 'Two times in a day';
    case '8' : return 'Three times in a day';
    case '6' : return 'Four times in a day';
  }
return info1;
}

decline(): void {
  this.modalRef.hide();
}
}
