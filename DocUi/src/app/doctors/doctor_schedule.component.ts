import { Component, OnInit, Input } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { forEach } from 'lodash';
import { AuthenticationService } from '@app/core/authentication/authentication.service';
import { UserInfo } from '@app/shared/dataModels/userInfo';

import { thisExpression } from 'babel-types';
import { DoctorsService } from '@app/doctors/doctors.service';
import { DomSanitizer } from '@angular/platform-browser';
import { filter, clone } from 'lodash';
import { WeekDays } from '@app/shared/dataModels/constData';
import { NotificationService } from '@app/core/notification.service';

@Component({
  selector: 'app-doctor-schedule',
  templateUrl: './doctor_schedule.component.html',
  styleUrls: ['./doctors.component.scss']
})
export class DoctorScheduleComponent implements OnInit {

  @Input()
  patient: UserInfo;
  @Input()
  doctor: any;

  error;
  hospitals: any[];
  targetHospital: any;
  selectedHospital: any;
  workingDays: any;
  slots = [] as any[];
  appointments = [];
  selectedWeek;
  radioModel;
  reason;
  constructor(private route: ActivatedRoute, private docService: DoctorsService,
    private authenticationService: AuthenticationService,
    private domSanitizer: DomSanitizer, private datePipe: DatePipe,
    private notificationService: NotificationService, private router: Router) {
        this.route.params.subscribe(params => {
            console.log('After payment=' + params.scheduleId + ' and status=' + params.status);
            let message;
            if (params.scheduleId && params.status) {
              if (params.status === 'TXN_SUCCESS') {
                message = 'Your payment was successful and your appointment is booked with Dr. ' +
                this.doctor.firstName + ' ' + this.doctor.lastName + '.<br/>' +
                'Booked appointment will be displayed on your calender.';
              } else {
                message = 'Your payment was not successful and your appointment with' +
                ' Dr. ' + this.doctor.firstName + ' ' + this.doctor.lastName + 'is denied by system.' +
                '<br/>Please try again to enroll.';
              }
              this.notificationService.showNotification('Payment Status', message,
              params.status === 'TXN_SUCCESS' ? 'success' : 'error');
            }
          });
  }

  ngOnInit() {
      this.docService.getPatientAppointmentForDoctor(this.doctor.id).subscribe(
          appointments => {
            appointments.forEach(appointment => {
                const schedule = appointment;
                this.appointments.push(schedule.date + ' ' + schedule.startTime);
            });
            this.getHospitalDataForDoctor();
          },
          err => {
            // error in loading appointments
            this.getHospitalDataForDoctor();
          }
      );
    }

    getHospitalDataForDoctor() {
        this.docService.getHospitalForDoctor(this.doctor.id).subscribe(
            data => {
              this.hospitals = data;
            },
            err => {
              this.notificationService.showNotification('Warning', 'No associated hospital found for Dr. ' +
              this.doctor.firstName + ' ' + this.doctor.lastName + '.', 'warning');
                this.error = 'Doctor is not available for any hospital';
            }
        );
    }

    onSelect() {
        this.targetHospital = this.hospitals.filter(hospital => hospital.id === this.selectedHospital)[0];
        const availableDays = this.targetHospital.weekdays.split(',');
        this.workingDays = this.getWorkingDaysNo(WeekDays.filter(weekday =>
            availableDays.indexOf('' + weekday.id) >= 0));
        this.selectedWeek = undefined;
    }

    onSelectDay() {
        this.slots = [];
        const hospStartDateTime = new Date(+this.targetHospital.hospStartTime);
        const hospEndDateTime = new Date(+this.targetHospital.hospEndTime);

        const currentDate = new Date();

        hospStartDateTime.setDate(currentDate.getDate());
        hospStartDateTime.setMonth(currentDate.getMonth());
        hospStartDateTime.setFullYear(currentDate.getFullYear());

        hospEndDateTime.setDate(currentDate.getDate());
        hospEndDateTime.setMonth(currentDate.getMonth());
        hospEndDateTime.setFullYear(currentDate.getFullYear());

        const selectedDayDateStart = clone(new Date(+this.selectedWeek));
        const selectedDate = this.datePipe.transform(selectedDayDateStart, 'yyyy-MM-dd');

                // load blocked detail
                this.docService.getTimeSlotsBlockedForDate(this.doctor.id, selectedDate).subscribe(
                    data => {
                      const lstBlockedSlot = data;
                      this.prepareSlots(hospStartDateTime, hospEndDateTime, lstBlockedSlot, selectedDate);
                    }, err => {
                      this.prepareSlots(hospStartDateTime, hospEndDateTime, [], selectedDate);
                    });
    }

    prepareSlots(hospStartDateTime: Date, hospEndDateTime: Date, blockedSlots: any[], selectedDate: string) {
        const startSlotTime = hospStartDateTime.getTime();
        const slotPeriod = this.targetHospital.timeslot * 60000;
        let slotTimes = startSlotTime;
        while (slotTimes < hospEndDateTime.getTime()) {
            const slotTime = this.datePipe.transform(new Date(slotTimes), 'HH:mm');
            this.slots.push({
                time: slotTimes,
                disabled: (this.appointments.indexOf(selectedDate + ' ' + slotTime + ':00') > -1)
                  || this.checkFromBlockedTimes(slotTime, blockedSlots)
            });
            slotTimes = slotTimes + slotPeriod;
        }
      }

    checkFromBlockedTimes(slotTime: string, blockedSlots: any[]) {
        const slotSplit = slotTime.split(':');
        let isBlocked = false;
        for (let index = 0; index < blockedSlots.length; index++) {
          const bSlot = blockedSlots[index].startTime;
          const eSlot = blockedSlots[index].endTime;
          const startSplit = bSlot.split(':');
          const endSplit = eSlot.split(':');
          const slotH = +(slotSplit[0]);
          const slotM = +(slotSplit[1]);
          const startH = +(startSplit[0]);
          const startM = +(startSplit[1]);
          const endH = +(endSplit[0]);
          const endM = +(endSplit[1]);
          console.log('Slot: ' + slotH + '::' + startH + '::' + endH + '-Slot:' + slotM + '::' + startM + '::' + endM);
          if (slotH > startH && slotH < endH) {
            isBlocked = true;
            break;
          } else {
            if (slotH === startH && slotM >= startM) {
              isBlocked = true;
              break;
            }
            if (slotH === endH && slotM <= endM) {
              isBlocked = true;
              break;
            }
          }
        }
        return isBlocked;
      }

    private getWorkingDaysNo(workingDays: any[]) {
        const currentDate = new Date();
        currentDate.setHours(0);
        currentDate.setMinutes(0);
        currentDate.setSeconds(0);
        const slotDays = [];
        console.log(currentDate);
        workingDays.forEach(element => {
            console.log(element);
            const days7 = 7 * (1000 * 60 * 60 * 24);
            console.log('Current day::' + currentDate.getDay() + ' :: element:: ' + element.id);
            if (currentDate.getDay() <= element.id) {
                const daydiff = (element.id - currentDate.getDay()) * (1000 * 60 * 60 * 24);
                console.log('if daydiff::' + daydiff);
                slotDays.push(currentDate.getTime() + daydiff);
                slotDays.push(currentDate.getTime() + (daydiff + days7));
            } else {
                const daydiff = (currentDate.getDay() - element.id ) * (1000 * 60 * 60 * 24);
                console.log('else daydiff::' + daydiff);
                slotDays.push(currentDate.getTime() + (days7 - daydiff));
                slotDays.push(currentDate.getTime() + ((days7 * 2) - daydiff));
            }
        });
        console.log(slotDays);
        return slotDays;
    }

    bookSlot() {
        const dayToBook = new Date(+this.selectedWeek);
        const dateToBook = new Date(+this.radioModel);
        dayToBook.setHours(dateToBook.getHours());
        dayToBook.setMinutes(dateToBook.getMinutes(), 0, 0);
        const startTime = this.datePipe.transform(new Date(dayToBook.getTime()), 'HH:mm:ss');
        const endTime = this.datePipe.transform(
            new Date(dayToBook.getTime() + this.targetHospital.timeslot * 60000), 'HH:mm:ss');
        const slotDate = this.datePipe.transform(new Date(dayToBook.getTime()), 'yyyy/MM/dd');
        console.log(startTime);
        console.log(endTime);
        console.log(slotDate);
        if (this.targetHospital.paymentMode === 0) {
            const context = {
                date: this.datePipe.transform(new Date(dayToBook.getTime()), 'yyyy-MM-dd'),
                startTime: startTime,
                endTime: endTime,
                remarks: this.reason
            };
            this.docService.saveScheduleInCaseOfManualPayment(this.targetHospital.id, this.patient.id, context)
            .subscribe(data => {
                this.notificationService.showNotification('Success', 'Appointment booked successfully', 'success');
                this.router.navigate(['/schedule'], { replaceUrl: false });
            }, err => {
                this.notificationService.showNotification('Failure', 'Appointment booking failed', 'error');
            });
        } else {
            window.location.href =
            'http://35.185.51.218:8080/dist/paytm.html?patientId=' + this.patient.id +
            '&doctorHospitalId=' + this.targetHospital.id +
            '&startTime=' + startTime +
            '&endTime=' + endTime +
            '&remarks=' + this.reason +
            '&date=' + slotDate;
        }
    }

    isExpired(time: any) {
        const slotDate = new Date(time);
        const dateToBook = new Date(+this.selectedWeek);
        const currentDate = new Date();
        if (slotDate.getDay() === dateToBook.getDay()
        && slotDate.getMonth() === dateToBook.getMonth()
        && slotDate.getFullYear() === dateToBook.getFullYear()) {
            return slotDate.getHours() < currentDate.getHours()
            || (slotDate.getHours() === currentDate.getHours() && slotDate.getMinutes() < currentDate.getMinutes());
        }
        return false;
    }
}
