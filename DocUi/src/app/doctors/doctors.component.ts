import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { forEach, clone } from 'lodash';
import { AuthenticationService } from '@app/core/authentication/authentication.service';
import { UserInfo } from '@app/shared/dataModels/userInfo';
import { EmailFolderDetails } from '@app/email/dataModels/emailFolderDetails';
import { EmailInfo } from '@app/email/dataModels/emailInfo';
import { thisExpression } from 'babel-types';
import { DoctorsService } from '@app/doctors/doctors.service';
import { DomSanitizer } from '@angular/platform-browser';
import { ADMIN, PAGESIZE } from '@app/shared/dataModels/constData';
import { NotificationService } from '@app/core/notification.service';

@Component({
  selector: 'app-doctors',
  templateUrl: './doctors.component.html',
  styleUrls: ['./doctors.component.scss']
})
export class DoctorsComponent implements OnInit {

  userData: UserInfo;
  doctors = [];
  doctorForAppointment: any;
  searchTerm = '';
  scheduleTriggered = false;
  max = 5;
  rate = 3;
  listView = false;
  isAdminUser = false;
  page = 1;
  pageSize = PAGESIZE;
  totalRecord = 0;

  constructor(private route: ActivatedRoute, private docService: DoctorsService,
    private authenticationService: AuthenticationService,
    private domSanitizer: DomSanitizer, private notificationService: NotificationService) {
  }

  ngOnInit() {
    this.docService.sampleService().subscribe(data => {
      console.log(data);
    }
    )

    const savedCredentials = sessionStorage.getItem('credentials') || localStorage.getItem('credentials');
    if (savedCredentials) {
      const credentials = JSON.parse(savedCredentials);
      if (credentials.username === ADMIN) {
        this.loadDoctors(0);
        this.listView = true;
        this.isAdminUser = true;
      } else {
        this.authenticationService.getUserDetails(this.authenticationService.credentials.token)
        .subscribe(
          data => {
            this.userData = data.doctor ? data.doctor : data;
            this.loadDoctors(1);
          }
        );
      }
    }

  }

  loadDoctors(status: number) {
    this.docService.getDoctors(this.page, this.pageSize, status, 'asc', 'firstName').subscribe(
      response => {
        this.doctors = response.listOfObjects;
        this.totalRecord = response.totalRecord;
      }, error => {
        // error handling
      }
    );
  }

  scheduleAppointment(doctor: any) {
    this.scheduleTriggered = true;
    this.doctorForAppointment = doctor;
  }

  changeStatus(doctor: any) {
    const doctorToApprove = clone(doctor);
    const id = doctorToApprove.id;
    doctorToApprove.approvalFlag = doctorToApprove.approvalFlag === 1 ? 0 : 1;
    delete doctorToApprove.id;
    delete doctorToApprove.userId;
    this.docService.saveDoctor(id, doctorToApprove).subscribe(
      data => {
        this.notificationService.showNotification('Status Changed',
          'Doctor is ' + (doctorToApprove.approvalFlag === 1 ? 'approved.' : 'dis-approved'), 'success');
        this.loadDoctors(-1);
      }
    );
  }

  pageChanged($event: any) {
    this.page = $event.page;
    this.loadDoctors(this.isAdminUser ? -1 : 1);
  }
}
