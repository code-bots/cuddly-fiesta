import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, Headers, Response } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';


const credentialsKey = 'credentials';
export interface Credentials {
    // Customize received credentials here
    username: string;
    token: string;
  }

@Injectable()
export class DoctorsService {
    private _credentials: Credentials | null;

    constructor(private http: HttpClient) {
        const savedCredentials = sessionStorage.getItem(credentialsKey) || localStorage.getItem(credentialsKey);
        if (savedCredentials) {
          this._credentials = JSON.parse(savedCredentials);
        }
      }
      sampleService()
      {
        //https://jsonplaceholder.typicode.com/comments/1
        return this.http.get<any>('https://jsonplaceholder.typicode.com/comments/1');
      }
      getDoctors(page: number, pageSize: number, status: number, type: string, field: string) {
        return this.http.get<any>('/DocWeb/rest/api/doctorgrid?page=' + page + '&size=' + pageSize +
        (status !== -1 ? '&status=' +
        status : '')  + '&type=' + type + '&feild=' + field, this.getHeaderForRequest());
      }

      getHospitalForDoctor(docId: string) {
        return this.http.get<any>('/DocWeb/rest/api/hospital/' + docId,
        this.getHeaderForRequest());
      }

      getPrescriptionGivenByDoctor(docId: string) {
        return this.http.get<any>('/DocWeb/rest/api/prescription/doctor/' + docId,
        this.getHeaderForRequest());
      }

      getBlogByDoctor(docId: string) {
        // http://localhost:8080/DocWeb/rest/api/doctor/blog/{doctorId}
        return this.http.get<any>('/DocWeb/rest/api/doctor/blog/' + docId,
        this.getHeaderForRequest());
      }


      getPatientAppointmentForDoctor(doctorId: string) {
        const headers =  new Headers();
        headers.append('Authorization', 'bearer ' + this._credentials.token);
        return this.http.get<any>('/DocWeb/rest/api/schedule/doctor/' + doctorId,
        this.getHeaderForRequest());
      }

      // rest/api/schedule/{doctorHospitalId}/2018-05-09
      getVisitTrendForDoctor(doctorId: string, dateToCheck: string) {
        const headers =  new Headers();
        headers.append('Authorization', 'bearer ' + this._credentials.token);
        return this.http.get<any>('/DocWeb/rest/api/doctor/schedule/' + doctorId + '/' + dateToCheck,
        this.getHeaderForRequest());
      }

      saveScheduleInCaseOfManualPayment(hospitalId: string, patientId: string, context: any) {
        const headers =  new Headers();
        headers.append('Authorization', 'bearer ' + this._credentials.token);
        return this.http.post('/DocWeb/rest/api/schedule/' + hospitalId + '/' + patientId
          , context, this.getHeaderForRequest());
      }

      saveDoctor(doctorId: string, context: any) {
        const headers =  new Headers();
        headers.append('Authorization', 'bearer ' + this._credentials.token);
        return this.http.put('/DocWeb/rest/api/doctor/' + doctorId
          , context, this.getHeaderForRequest());
      }

      // http://localhost:8080/DocWeb/rest/api/block/{doctorId}?date=2018-05-08
  getTimeSlotsBlockedForDate(doctorId: string, date: string) {
    const headers =  new Headers();
    headers.append('Authorization', 'bearer ' + this._credentials.token);
    return this.http.get<any>('/DocWeb/rest/api/block/' + doctorId + '?date=' + date,
    this.getHeaderForRequest());
  }

  getHeaderForRequest() {
    const httpOptions = {
      headers: new HttpHeaders({ 'Authorization': 'bearer ' + this._credentials.token
    })};
    return httpOptions;
  }
}
