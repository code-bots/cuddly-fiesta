import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { Http, Headers, Response } from '@angular/http';
import { HttpHeaders } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';

export interface Credentials {
  // Customize received credentials here
  username: string;
  token: string;
}

const credentialsKey = 'credentials';

@Injectable()
export class ScheduleService {

  private _credentials: Credentials | null;

  constructor(private http: HttpClient) {
    const savedCredentials = sessionStorage.getItem(credentialsKey) || localStorage.getItem(credentialsKey);
    if (savedCredentials) {
      this._credentials = JSON.parse(savedCredentials);
    }
  }

 getCalenderEventsForPatient(patientId: string) {
    return this.http.get<any>('/DocWeb/rest/api/schedule/patient/' + patientId,
      this.getHeaderForRequest());
  }

  getCalenderEventsForDoctor(doctorId: string) {
    return this.http.get<any>('/DocWeb/rest/api/schedule/doc/' + doctorId,
      this.getHeaderForRequest());
  }

  getDoctorsByWeekday(weekDay: string) {
    return this.http.get<any>('/DocWeb/rest/api/weekdays/' + weekDay,
      this.getHeaderForRequest());
  }

  getPatientAppointmentForDoctor(doctorId: string) {
    return this.http.get<any>('/DocWeb/rest/api/schedule/doctor/' + doctorId,
    this.getHeaderForRequest());
  }

  saveScheduleInCaseOfManualPayment(hospitalId: string, patientId: string, context: any) {
    return this.http.post('/DocWeb/rest/api/schedule/' + hospitalId + '/' + patientId
      , context, this.getHeaderForRequest());
  }

  blockCalender(doctorId: string, context: any) {
    return this.http.post('/DocWeb/rest/api/block/' + doctorId
      , context, this.getHeaderForRequest());
  }

  getBlockedDatesForDoctor(doctorId: string) {
    return this.http.get('/DocWeb/rest/api/block/' + doctorId, this.getHeaderForRequest());
  }

  // http://localhost:8080/DocWeb/rest/api/block/{doctorId}?date=2018-05-08
  getTimeSlotsBlockedForDate(doctorId: string, date: string) {
    return this.http.get<any>('/DocWeb/rest/api/block/' + doctorId + '?date=' + date,
    this.getHeaderForRequest());
  }

  // http://localhost:8080/DocWeb/rest/api/block/{blockId}
  deleteBlockedevent(blockId: string) {
    return this.http.delete('/DocWeb/rest/api/block/' + blockId,
    this.getHeaderForRequest());
  }

  getHeaderForRequest() {
    const httpOptions = {
      headers: new HttpHeaders({ 'Authorization': 'bearer ' + this._credentials.token
    })};
    return httpOptions;
  }
}
