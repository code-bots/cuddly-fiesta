import { Component, OnInit, ViewChild,
  TemplateRef, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { Subject } from 'rxjs/Subject';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthenticationService } from '@app/core';
import { UserInfo } from '@app/shared/dataModels/userInfo';
import { ScheduleService } from '@app/schedule/schedule.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { TYPES_OF_SPECIALIZATION } from '@app/shared/dataModels/constData';
import { Router } from '@angular/router';
import { NotificationService } from '@app/core/notification.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.scss']
})
export class ScheduleComponent implements OnInit {

  userData: UserInfo;
  events;
  viewDate: Date = new Date();
  showCall = false;
  calendarOptions;
  modalRef: BsModalRef;
  bsModalRef: BsModalRef;
  dateToSchedule;
  toggleBlock = false;
  blockReason = 'Casual off';
  startTime: Date = new Date();
  endTime: Date = new Date();
  specializations = TYPES_OF_SPECIALIZATION;
  @ViewChild('template') modalContent: TemplateRef<any>;
  @ViewChild('eventDetail') eventPopup: TemplateRef<any>;
  @ViewChild('blockPopup') blockPopup: TemplateRef<any>;

  constructor(private modalService: BsModalService,
    private authenticationService: AuthenticationService,
    private notificationService: NotificationService,
    private scheduleService: ScheduleService, private cdr: ChangeDetectorRef,
    private router: Router, private datePipe: DatePipe
  ) {
  }

  ngOnInit() {
    this.authenticationService.getUserDetails(this.authenticationService.credentials.token)
    .subscribe(
      data => {
        this.userData = data.doctor ? data.doctor : data;
        this.loadCalenderEvents();
      }
    );
  }

  loadEventsForDoctor(blockedEvents: any) {
    const __this = this;
    this.scheduleService.getCalenderEventsForDoctor(this.userData.id).subscribe(
      data => {
        const blockedEs = this.getBlockedEvents(blockedEvents);
        this.events  = blockedEs.concat(this.getCalenderEvents(data));
        this.calendarOptions = {
         contentHeight: 600,
         themeSystem: 'bootstrap4',
         fixedWeekCount : false,
         defaultDate: this.viewDate,
         editable: true,
         selectable: true,
         eventLimit: true, // allow "more" link when too many events
         defaultView: 'month',
         header: {
           left: 'month,agendaWeek,agendaDay,listWeek',
           center: 'title',
           right: 'today prevYear,prev,next,nextYear'
         },
         footer: {
           center: '',
           right: 'prev,next'
         },
         eventClick: function(eventObj: any) {
           __this.setObjectAndSHowPopup(eventObj);
         },
         dayClick: function(date: any) {
         },
         events: this.events
       };
       this.showCall = true;
      }, err => {
        //
      }
    );
  }

  loadCalenderEvents() {
    const __this = this;
    if (this.userData.userId.roles[0].roleName === 'DOCTOR') {
      this.scheduleService.getBlockedDatesForDoctor(this.userData.id).subscribe(
        data => {
          this.loadEventsForDoctor(data);
        }, err => {

        }
      );
    }
    if (this.userData.userId.roles[0].roleName === 'PATIENT') {
      this.scheduleService.getCalenderEventsForPatient(this.userData.id).subscribe(
        data => {
        this.events = this.getCalenderEvents(data);
        this.calendarOptions = {
          contentHeight: 600,
          selectable: true,
          themeSystem: 'bootstrap4',
          fixedWeekCount : false,
          defaultDate: this.viewDate,
          editable: false,
          eventLimit: true, // allow "more" link when too many events
          defaultView: 'month',
          header: {
            left: 'month,agendaWeek,agendaDay,listWeek',
            center: 'title',
            right: 'today prevYear,prev,next,nextYear'
          },
          eventClick: function(eventObj: any) {
            __this.setObjectAndSHowPopup(eventObj);
          },
          dayClick: function(date: any) {
            __this.dateToSchedule = date;
            __this.modalRef = __this.modalService.show(__this.modalContent, {class: 'modal-sm'});
          },
          footer: {
            center: '',
            right: 'prev,next'
          },
          events: this.events
        };
        this.showCall = true;
        },
        err => console.log('error in loading events')
      );
    }
  }

  setObjectAndSHowPopup(event: any) {
    this.modalRef = this.modalService.show(this.eventPopup, {class: 'modal-sm'});
      if (event.type === 'blocked') {
        $('#unblock').show();
        $('#blockId').val(event.id);
        setTimeout(function () {
          $('#eventTitle').html(event.title + '<br><br>Calendar is blocked by start time: ' +
          event.start.format('YYYY-MM-DD HH:mm')
          + ' and end time: ' + event.end.format('YYYY-MM-DD HH:mm') + '. Do you want to unblock it ?');
          }, 0);
      } else {
        $('#unblock').hide();
        setTimeout(function () {
          $('#eventTitle').html(event.title + '<br><br>Appointment start time: ' +
          event.start.format('YYYY-MM-DD HH:mm')
          + ' and end time: ' + event.end.format('YYYY-MM-DD HH:mm') + '.');
          }, 0);
      }
  }

  getCalenderEvents(data: any): any {
    const events = [];
    data.forEach(d => {
       const schedule = d;
     events.push({
        title: this.getTitleForSchedule(schedule),
        start: this.getDateForEvent(schedule.date, schedule.startTime),
        end: this.getDateForEvent(schedule.date, schedule.endTime),
        color: '#ff5c56',
        type: 'schedule'
      });
    });
    return events;
  }

  getBlockedEvents(data: any): any {
    const events = [];
    data.forEach(d => {
    const blockedSchedule = d;
    events.push({
       title: blockedSchedule.reason,
       start: this.getDateForEvent(blockedSchedule.date, blockedSchedule.startTime),
       end: this.getDateForEvent(blockedSchedule.date, blockedSchedule.endTime),
       color: '#c5c5c5',
       type: 'blocked',
       id: blockedSchedule.id
     });
   });
    return events;
  }

  getTitleForSchedule(d: any) {
    if (this.userData.userId.roles[0].roleName === 'DOCTOR') {
      return 'Appointment with ' +
        d.patientId.firstName + ' ' + d.patientId.lastName +
        ' in ' +
    d.doctorHospitalId.name + ' hospital' +
    ' for ' + d.remarks + '. You can contact patient on contact no ' + d.patientId.mobileNumber + '.';
    } else {
      return 'You have appointment with "Dr.' +
      d.doctorHospitalId.doctorId.firstName + ' ' + d.doctorHospitalId.doctorId.lastName +
      '" at ' + d.doctorHospitalId.name + ' hospital. Contact ' + d.doctorHospitalId.doctorId.mobileNumber +
      ' no for any help regarding appointment.';
    }
  }

  getDateForEvent(date: string, time: string): string {
    const dateAndTime = date + 'T' + time;
    return dateAndTime;
  }

  confirm(): void {
    this.modalRef.hide();
    window.location.href = '#/calendar/' + this.dateToSchedule.format();
  }

  decline(): void {
    this.modalRef.hide();
  }

  unblock(): void {
    const val = $('#blockId').val() as string;
    this.scheduleService.deleteBlockedevent(val).subscribe(
      data => {
        console.log('Success in delete');
        $('#blockId').val('');
        this.modalRef.hide();
        location.reload();
      }, err => {
        console.log('Failure in delete');
        this.modalRef.hide();
      }
    );
  }


  blockCalender() {
    const context = {
      date : this.datePipe.transform(this.viewDate, 'yyyy-MM-dd'),
      startTime: this.datePipe.transform(this.startTime, 'HH:mm') + ':00',
      endTime: this.datePipe.transform(this.endTime, 'HH:mm') + ':00',
      reason: this.blockReason
    };
    this.scheduleService.blockCalender(this.userData.id, context).subscribe(
      response => {
        this.notificationService.showNotification('Success', 'Calender blocked successfully.', 'success');
        this.toggleBlock = !this.toggleBlock;
        location.reload();
      }, err => {
        this.notificationService.showNotification('Error', 'Error in blocking calender.', 'error');
      }
    );
  }

  get isDoctor() {
    return this.userData && this.userData.userId.roles[0].roleName === 'DOCTOR';
  }
}

