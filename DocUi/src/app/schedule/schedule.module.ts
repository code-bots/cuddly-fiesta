import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
// import { CalendarModule } from 'angular-calendar';
import { BrowserModule } from '@angular/platform-browser';
import { CoreModule } from '@app/core';
import { SharedModule } from '@app/shared';
import { ScheduleRoutingModule } from '@app/schedule/schedule-routing.module';
import { ScheduleComponent } from '@app/schedule/schedule.component';
import { ScheduleService } from '@app/schedule/schedule.service';
import { CalendarModule } from 'ap-angular2-fullcalendar';
import { ModalModule, ButtonsModule, PopoverModule, TimepickerModule, BsDatepickerModule } from 'ngx-bootstrap';
import { FormsModule } from '@angular/forms';
import { ToastyModule } from 'ng2-toasty';
import { ReactiveFormsModule } from '@angular/forms';
import { CalendarAppointmentComponent } from '@app/schedule/calendar-appointment.component';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    BrowserModule,
    CoreModule,
    SharedModule,
    ScheduleRoutingModule,
    CalendarModule.forRoot(),
    ModalModule.forRoot(),
    ButtonsModule.forRoot(),
    PopoverModule.forRoot(),
    ToastyModule.forRoot(),
    TimepickerModule.forRoot(),
    BsDatepickerModule.forRoot()
    ],
  declarations: [
    ScheduleComponent,
    CalendarAppointmentComponent
  ],
  providers : [ScheduleService]
})
export class ScheduleModule { }
