import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Route, extract } from '@app/core';
import { ScheduleComponent } from '@app/schedule/schedule.component';
import { CalendarAppointmentComponent } from '@app/schedule/calendar-appointment.component';


const routes: Routes = [
  Route.withShell([
    { path: 'schedule', component: ScheduleComponent, data: { title: extract('Schedule') } },
    { path: 'calendar/:dateToSchedule', component: CalendarAppointmentComponent,
    data: { title: extract('Calendar Appointment') } },
  ])
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class ScheduleRoutingModule { }
