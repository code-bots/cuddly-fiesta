import { Component, OnDestroy } from '@angular/core';
import { OnInit } from '@angular/core';
import { UserInfo } from '@app/shared/dataModels/userInfo';
import { AuthenticationService } from '@app/core';
import { ScheduleService } from '@app/schedule/schedule.service';
import { ActivatedRoute, Router } from '@angular/router';
import { TYPES_OF_SPECIALIZATION } from '@app/shared/dataModels/constData';
import { filter, clone } from 'lodash';
import { DatePipe } from '@angular/common';
import { NotificationService } from '@app/core/notification.service';

@Component({
  selector: 'app-cal-appointment',
  templateUrl: './calendar-appointment.html',
  styleUrls: ['./schedule.component.scss']
})
export class CalendarAppointmentComponent implements OnInit, OnDestroy {
  userData: UserInfo;
  dateToSchedule;
  typesOfDoc;
  availableDoctorsOnThatDay;
  specialization;
  filteredDoctorSpecializationWise;
  doctor;
  appointments = [];
  slots = [];
  sub;
  radioModel;
  selectedHospital;
  reason;
  constructor(private authenticationService: AuthenticationService,
    private scheduleService: ScheduleService, private route: ActivatedRoute, private datePipe: DatePipe,
    private notificationService: NotificationService, private router: Router
  ) {
    console.log('init construct');
   this.sub = this.route.params.subscribe(params => {
     console.log(params);
     this.availableDoctorsOnThatDay = undefined;
     this.typesOfDoc = undefined;
     if (params.dateToSchedule) {
      this.authenticationService.getUserDetails(this.authenticationService.credentials.token)
      .subscribe(
        data => {
          this.userData = data.doctor ? data.doctor : data;
        }
      );
      this.dateToSchedule = params.dateToSchedule;
      this.typesOfDoc = clone(TYPES_OF_SPECIALIZATION);
      const weekday = this.datePipe.transform(new Date(this.dateToSchedule), 'EEEE');
      this.scheduleService.getDoctorsByWeekday(this.getWeekDayNumber(weekday)).subscribe(
        data2 => {
          this.availableDoctorsOnThatDay = data2;
        }, err => {
          console.log('Data not available');
        }
      );
     }
    });
  }

  ngOnDestroy() {
    console.log('In destroy');
    this.dateToSchedule = undefined;
  }

  onSelect() {
    this.filteredDoctorSpecializationWise = this.availableDoctorsOnThatDay.
      filter(doctorHospital => doctorHospital.doctorId.title === this.specialization);
  }

  onSelectDoctor() {
    this.selectedHospital = this.availableDoctorsOnThatDay.
      filter(doctorHospital => doctorHospital.doctorId.id === this.doctor)[0];
    this.scheduleService.getPatientAppointmentForDoctor(this.doctor).subscribe(
      appointments => {
        appointments.forEach(appointment => {
          const schedule = appointment;
          this.appointments.push(schedule.date + ' ' + schedule.startTime);
        });
        this.slots = [];
        const hospStartDateTime = new Date(+this.selectedHospital.hospStartTime);
        const hospEndDateTime = new Date(+this.selectedHospital.hospEndTime);

        const currentDate = new Date();

        hospStartDateTime.setDate(currentDate.getDate());
        hospStartDateTime.setMonth(currentDate.getMonth());
        hospStartDateTime.setFullYear(currentDate.getFullYear());

        hospEndDateTime.setDate(currentDate.getDate());
        hospEndDateTime.setMonth(currentDate.getMonth());
        hospEndDateTime.setFullYear(currentDate.getFullYear());

        // load blocked detail
        this.scheduleService.getTimeSlotsBlockedForDate(this.doctor, this.dateToSchedule).subscribe(
          data => {
            const lstBlockedSlot = data;
            this.prepareSlots(hospStartDateTime, hospEndDateTime, lstBlockedSlot);
          }, err => {
            this.prepareSlots(hospStartDateTime, hospEndDateTime, []);
          });
      },
      err => {
        // error in loading appointments
      }
    );
  }

  prepareSlots(hospStartDateTime: Date, hospEndDateTime: Date, blockedSlots: any[]) {
    const startSlotTime = hospStartDateTime.getTime();
    const slotPeriod = this.selectedHospital.timeslot * 60000;
    let slotTimes = startSlotTime;
    while (slotTimes < hospEndDateTime.getTime()) {
        const slotTime = this.datePipe.transform(new Date(slotTimes), 'HH:mm');
        this.slots.push({
            time: slotTimes,
            disabled: (this.appointments.indexOf(this.dateToSchedule + ' ' + slotTime + ':00') > -1)
              || this.checkFromBlockedTimes(slotTime, blockedSlots)
        });
        slotTimes = slotTimes + slotPeriod;
    }
  }

  checkFromBlockedTimes(slotTime: string, blockedSlots: any[]) {
    const slotSplit = slotTime.split(':');
    let isBlocked = false;
    for (let index = 0; index < blockedSlots.length; index++) {
      const bSlot = blockedSlots[index].startTime;
      const eSlot = blockedSlots[index].endTime;
      const startSplit = bSlot.split(':');
      const endSplit = eSlot.split(':');
      const slotH = +(slotSplit[0]);
      const slotM = +(slotSplit[1]);
      const startH = +(startSplit[0]);
      const startM = +(startSplit[1]);
      const endH = +(endSplit[0]);
      const endM = +(endSplit[1]);
      console.log('Slot: ' + slotH + '::' + startH + '::' + endH + '-Slot:' + slotM + '::' + startM + '::' + endM);
      if (slotH > startH && slotH < endH) {
        isBlocked = true;
        break;
      } else {
        if (slotH === startH && slotM >= startM) {
          isBlocked = true;
          break;
        }
        if (slotH === endH && slotM <= endM) {
          isBlocked = true;
          break;
        }
      }
    }
    return isBlocked;
  }

  getWeekDayNumber(weekday: string) {
    switch (weekday) {
      case 'Sunday': return '0';
      case 'Monday': return '1';
      case 'Tuesday': return '2';
      case 'Wednesday': return '3';
      case 'Thursday': return '4';
      case 'Friday': return '5';
      case 'Saturday': return '6';
    }
  }

  bookSlot() {
    const dayToBook = new Date(this.dateToSchedule);
    const dateToBook = new Date(+this.radioModel);
    dayToBook.setHours(dateToBook.getHours());
    dayToBook.setMinutes(dateToBook.getMinutes(), 0, 0);
    const startTime = this.datePipe.transform(new Date(dayToBook.getTime()), 'HH:mm:ss');
    const endTime = this.datePipe.transform(
        new Date(dayToBook.getTime() + this.selectedHospital.timeslot * 60000), 'HH:mm:ss');
    const slotDate = this.datePipe.transform(new Date(dayToBook.getTime()), 'yyyy/MM/dd');
    if (this.selectedHospital.paymentMode === 0) {
        const context = {
            date: this.datePipe.transform(new Date(dayToBook.getTime()), 'yyyy-MM-dd'),
            startTime: startTime,
            endTime: endTime,
            remarks: this.reason
        };
        this.scheduleService.saveScheduleInCaseOfManualPayment(this.selectedHospital.id, this.userData.id, context)
        .subscribe(data => {
            this.notificationService.showNotification('Success', 'Appointment booked successfully', 'success');
            this.router.navigate(['/schedule'], { replaceUrl: true });
        }, err => {
            this.notificationService.showNotification('Failure', 'Appointment booking failed', 'error');
        });
    } else {
        window.location.href =
        'http://35.185.51.218:8080/dist/paytm.html?patientId=' + this.userData.id +
        '&doctorHospitalId=' + this.selectedHospital.id +
        '&startTime=' + startTime +
        '&endTime=' + endTime +
        '&remarks=' + this.reason +
        '&date=' + slotDate;
    }
}

  ngOnInit() {
  }
}
