import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { AuthenticationService } from '../core/authentication/authentication.service';
import { UserInfo } from '../shared/dataModels/userInfo';
import { DomSanitizer } from '@angular/platform-browser';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ChatService } from '@app/chat/chat.service';
import { filter, groupBy } from 'lodash';
import * as $ from 'jquery';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit {
  arrayOfKeys: string[];
  targetUser: any;
  userTwoFullName: any;
  userOneFullName: any;
  isLoading: boolean;
  userData: any;
  chatUsers: any;
  showChat =  false;
  messages = [] as any[];
  chatSessionId: string;
  searchTerm = '';
  chatWebSocket;
  MESSAGES_RENDERING_WAIT_TIME = 1000;
  currentMessage = '';
  contactClicked = false;
  contactsLoaded = false;
  _this_;
  constructor(private authenticationService: AuthenticationService,
    private chatService: ChatService,
    public domSanitizer: DomSanitizer) {
    if (!this.chatService.isConnected()) {
        this.chatService.connect();
    }
    this.authenticationService.getUserDetails(this.authenticationService.credentials.token)
      .subscribe(
        data => {
          this.userData = data.doctor ? data.doctor : data;
          this.isLoading = true;
          if (!this.isDoctor()) {
            // load tutor for student
            this.chatService.getDoctors().subscribe(
              doctorData => {
                let lstData: any;
                lstData = doctorData.listOfObjects;
                const dataToGroup = [];
                lstData.forEach(docData => {
                  if (docData.doctor.userId.enabled) {
                    dataToGroup.push({
                      name: docData.doctor.firstName + ' ' + docData.doctor.lastName,
                      pic: docData.doctor.profilePic,
                      email: docData.doctor.email,
                      id: docData.doctor.userId.id,
                      type: 'DOCTOR',
                      online: docData.doctor.userId.isPresent
                    });
                  }
                });
                this.chatUsers = groupBy(dataToGroup, function(user: any) {return user.name[0]; });
                this.arrayOfKeys = Object.keys(this.chatUsers);
                this.contactsLoaded = true;
              },
            );
          } else {
            // load student for tutor
            // http://localhost:8080/STMWeb/rest/api/tutor/assignments/student/{tutor_id}
            this.chatService.getPatients().subscribe(
              patientData => {
                let lstData: any;
                lstData = patientData.listOfObjects;
                const dataToGroup = [];
                lstData.forEach(patient => {
                  if (patient.userId.enabled) {
                    dataToGroup.push({
                      name: patient.firstName + ' ' + patient.lastName,
                      pic: patient.profilePic,
                      email: patient.email,
                      id: patient.userId.id,
                      type: 'PATIENT',
                      online: patient.userId.isPresent
                    });
                  }
                });
                this.chatUsers = groupBy(dataToGroup, function(user: any) {return user.name[0]; });
                this.arrayOfKeys = Object.keys(this.chatUsers);
                this.contactsLoaded = true;
              },
              err => {

              }
            );
          }
        }
      );
  }

  ngOnInit() {}

  loadChats(user: any) {
    this.targetUser = user;
    this.showChat = false;
    this.construct();
   // this.chatService.whenConnected(this.construct);
  }

  construct() {
    this.messages = [];
    this.chatService.establishChatSession(this.userData.userId.id, this.targetUser.id).subscribe(
      session => {
        this.chatSessionId = session.channelUuid;
        this.userOneFullName = session.userOneFullName;
        this.userTwoFullName = session.userTwoFullName;
        this.chatWebSocket = this.chatService.getSocket();
        const _this_ = this;
        this.chatWebSocket.subscribe('/topic/private.chat.' + this.chatSessionId, function (response: any) {
          _this_.addChatMessageToUI(JSON.parse(response.body), true);
          _this_.scrollToLatestChatMessage();
        });

        this.chatService.getExistingChatSessionMessages(this.chatSessionId).subscribe(
          data => {
            this.showChat = true;
            // this.messages = data;
            data.forEach(msg => {
              this.addChatMessageToUI(msg, true);
            });
          },
          error => {
            console.log('Error in loading the messages');
          }
        );
      }
    );
  }

  addChatMessageToUI(message: any, withForceApply: boolean) {
    this.messages
      .push({
        contents: message.contents,
        isFromRecipient: message.fromUserId !== this.userData.userId.id,
        author: (message.fromUserId === this.userData.userId.id) ?
          this.userData.firstName + ' ' + this.userData.lastName : this.targetUser.name,
        pic: message.fromUserId === this.userData.userId.id ?
          this.userData.profilePic : this.targetUser.pic
      });

   // if (withForceApply) { self.$apply(); }
  }

  getUserName(userEmail: string, isFromRecipient: boolean): any {
    if (!isFromRecipient) {
      return this.userData.firstName + ' ' + this.userData.lastName;
    } else {
      const user = filter(this.chatUsers, function(users: any) {
        return users.email === userEmail;
      });
      if (user && user.length) {
        return user[0].name;
      } else {
        return userEmail;
      }
    }
  }

  scrollToLatestChatMessage() {
    const chatContainer = $('#chat-area');
    setTimeout(function() {
      if (chatContainer.length > 0) { chatContainer.scrollTop(chatContainer[0].scrollHeight); }
    }, this.MESSAGES_RENDERING_WAIT_TIME);
  }

  sendChatMessage() {
    if (!this.currentMessage || this.currentMessage.trim() === '') {
      return;
    }

    this.chatWebSocket.send('/app/private.chat.' + this.chatSessionId, {}, JSON.stringify({
      fromUserId: this.userData.userId.id,
      toUserId: this.targetUser.id,
      contents: this.currentMessage
    }));

    this.currentMessage = null;
  }

  trackByFn(index: any, item: any) {
    return index; // or item.id
  }

  isDoctor() {
    return this.userData.userId.roles[0].roleName === 'DOCTOR';
  }
}
