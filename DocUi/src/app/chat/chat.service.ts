import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';

export interface Credentials {
  // Customize received credentials here
  username: string;
  token: string;
}

const credentialsKey = 'credentials';

@Injectable()
export class ChatService {

  private static WHEN_CONNECTED_CALLBACK_WAIT_INTERVAL = 1000;
  private _credentials: Credentials | null;
  private socket;

  constructor(private http: Http) {
    const savedCredentials = sessionStorage.getItem(credentialsKey) || localStorage.getItem(credentialsKey);
    if (savedCredentials) {
      this._credentials = JSON.parse(savedCredentials);
    }
  }

  getListOfStudentWhoHasTakenAssignment(tutorId: string) {
    return this.http.get('/DocWeb/rest/api/tutor/assignments/student/' + tutorId,
    {headers: this.getHeaderForRequest()}).map((res: Response) => res.json());
  }

  getDoctors() {
    return this.http.get('/DocWeb/rest/api/doctorgrid?status=1&page=1&size=999&type=asc&feild=firstName',
    {headers: this.getHeaderForRequest()})
    .map((res: Response) => res.json());
  }

  getPatients() {
    return this.http.get('/DocWeb/rest/api/patientgrid?page=1&size=999', {headers: this.getHeaderForRequest()})
    .map((res: Response) => res.json());
  }

  establishChatSession(userIdOne: string, userIdTwo: string ) {
    const data = {
      userIdOne: userIdOne,
      userIdTwo: userIdTwo
    };
    return this.http.put('/codebot-chat-1.0/api/private-chat/channel'
      , data, { headers: this.getHeaderForRequest() }).map((res: Response) => res.json());
  }

  getExistingChatSessionMessages(channelUuid: string ) {
    return this.http.get('/codebot-chat-1.0/api/private-chat/channel/' + channelUuid
      , { headers: this.getHeaderForRequest() }).map((res: Response) => res.json());
  }

  getHeaderForRequest() {
    const headers = new Headers();
    headers.append('Authorization', 'bearer ' + this._credentials.token);
    return headers;
  }

  connect() {
      const ws = new SockJS('/codebot-chat-1.0/ws');
      this.socket = Stomp.over(ws);
      // this.socket = Stomp.over(new SockJS('/ws'));
      this.socket.debug = null;
      this.socket.connect({}, function() {}, function() {
        // alert('You have disconnected, hit "OK" to reload.');
       window.location.reload();
      });
  }

  disconnect() {
    this.socket.disconnect();
    this.socket = null;
  }

  isConnected() {
      return (this.socket && this.socket.connected);
  }

  getSocket() {
    return this.socket;
  }

  whenConnected(_do: any) {
      const _this = this;
      setTimeout(
        function() {
          if (_this.isConnected()) {
            if (_do !== null) { _do(); }
            return;
          } else {
            _this.whenConnected(_do);
          }

        }, ChatService.WHEN_CONNECTED_CALLBACK_WAIT_INTERVAL);
    }
}
