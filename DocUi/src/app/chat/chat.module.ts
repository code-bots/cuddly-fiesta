import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { NgPipesModule } from 'ngx-pipes';
import { CoreModule } from '@app/core';
import { AlertModule, ButtonsModule } from 'ngx-bootstrap';
import { SharedModule } from '@app/shared';
import { ChatComponent } from '@app/chat/chat.component';
import { ChatService } from '@app/chat/chat.service';
import { ChatRoutingModule } from '@app/chat/chat-routing.module';


@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    CoreModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    ChatRoutingModule,
    NgPipesModule,
    ButtonsModule.forRoot()
  ],
  declarations: [
    ChatComponent
  ],
  providers: [ChatService]
})
export class ChatModule { }
