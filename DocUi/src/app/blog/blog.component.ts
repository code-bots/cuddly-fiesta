import { Component, OnInit } from '@angular/core';
import { BlogService } from '@app/blog/blog.service';
import { AuthenticationService } from '@app/core/authentication/authentication.service';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserInfo } from '@app/shared/dataModels/userInfo';
import { NotificationService } from '@app/core/notification.service';
@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.scss']
})
export class BlogComponent implements OnInit {

  blogForm: FormGroup;
  userData: UserInfo;
  constructor(private formBuilder: FormBuilder, private blogService: BlogService,
    private authenticationService: AuthenticationService,
    private notificationService: NotificationService,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.createForm();

    this.authenticationService.getUserDetails(this.authenticationService.credentials.token)
      .subscribe(
        data => {
          this.userData = data.doctor ? data.doctor : data;
        }
      );
  }

  createForm(): any {
    this.blogForm = this.formBuilder.group({
      title: ['', Validators.required],
      content: ['', Validators.required]
    });
  }

  save() {
    const postData = {
      title: this.blogForm.controls.title.value,
      content: this.blogForm.controls.content.value,
      tags: 'Sample',
      creation_date: new Date().getTime()
    };
    this.blogService.saveBlog(postData, this.userData.id).subscribe(data => {
      console.log('success');
      this.notificationService.showNotification('Blog Created Successfully', '', 'success');
      this.cancel();
    }, err => {
      console.log('Loading failed for data institute');
    });
  }

  cancel() {
    this.router.navigate(['/bloglist'], { replaceUrl: false });
  }


}
