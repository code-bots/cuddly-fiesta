import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, Headers, Response } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

const credentialsKey = 'credentials';
export interface Credentials {
  // Customize received credentials here
  username: string;
  token: string;
}
@Injectable()
export class BlogService {
  private _credentials: Credentials | null;

  constructor(private http: HttpClient) {
    const savedCredentials = sessionStorage.getItem(credentialsKey) || localStorage.getItem(credentialsKey);
    if (savedCredentials) {
      this._credentials = JSON.parse(savedCredentials);
    }
  }

  saveBlog(context: any, dotorId: string) {
    return this.http.post('/DocWeb/rest/api/blog/' + dotorId
      , context, this.getHeaderForRequest());
    // doctorId and blogData
    // http://localhost:8080/STMWeb/rest/api/blog/{doctorId}

  }


  saveBlogComment(context: any, blogId: string) {
    return this.http.post('/DocWeb/rest/api/comment/' + blogId, context,
    this.getHeaderForRequest());
    // http://localhost:8080/STMWeb/rest/api/comment/{blog_id}
  }

  getBlogs() {
    return this.http.get<Array<any>>('/DocWeb/rest/api/bloglist',
    this.getHeaderForRequest());
    // http://localhost:8080/STMWeb/rest/api/bloglist
  }

  getBlogsById(blog_Id: string) {
    return this.http.get('/DocWeb/rest/api/blog/' + blog_Id,
    this.getHeaderForRequest());
    // http://localhost:8080/STMWeb/rest/api/blog/{blog_id}
  }

  updateBlog(context: any, blogId: string) {
    return this.http.put('/DocWeb/rest/api/blog/' + blogId, context,
    this.getHeaderForRequest());
    // http://localhost:8080/DocWeb/rest/api/blog/{blogId}
  }

  getComments(blogId: string) {
    return this.http.get<any[]>('/DocWeb/rest/api/comments/' + blogId,
    this.getHeaderForRequest());
    //http://localhost:8080/STMWeb/rest/api/comments/{blog_id}
  }

  getParentComments(blogId: string) {
    return this.http.get('/DocWeb/rest/api/comments/' + blogId,
    this.getHeaderForRequest());
    //http://localhost:8080/STMWeb/rest/api/comments/{blog_id}
  }

  editComment(context: any, commentId: String) {
    return this.http.put('/DocWeb/rest/api/comment/' + commentId, context,
    this.getHeaderForRequest());
    //http://localhost:8080/DocWeb/rest/api/comment/{commentId}
  }

  deleteComment(commentId: String) {
    return this.http.delete('/DocWeb/rest/api/comment/' + commentId,
    this.getHeaderForRequest());
    //http://localhost:8080/DocWeb/rest/api/comment/{commentId}
  }

  deleteBlog(blogId: String) {
    const headerOptions = this.getHeaderForRequest() as any;
    headerOptions.responseType = 'text';
    return this.http.delete('/DocWeb/rest/api/blog/' + blogId, this.getHeaderForRequest());
    //http://localhost:8080/DocWeb/rest/api/comment/{commentId}
  }

  getHeaderForRequest() {
    const httpOptions = {
      headers: new HttpHeaders({ 'Authorization': 'bearer ' + this._credentials.token
    })};
    return httpOptions;
  }
}
