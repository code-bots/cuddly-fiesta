package com.codebots.dpm.reqres;

import java.sql.Time;
import java.util.Date;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonFormat;

public class PaymentInfo {
	
	UUID patientId;
	UUID doctorHospitalId;
	private Time startTime;
	private Time endTime;
	private String remarks;
	private Date date;
	
	
	
	public Time getStartTime() {
		return startTime;
	}
	public void setStartTime(Time startTime) {
		this.startTime = startTime;
	}
	public Time getEndTime() {
		return endTime;
	}
	public void setEndTime(Time endTime) {
		this.endTime = endTime;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd", timezone="UTC")
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public UUID getPatientId() {
		return patientId;
	}
	public void setPatientId(UUID patientId) {
		this.patientId = patientId;
	}
	public UUID getDoctorHospitalId() {
		return doctorHospitalId;
	}
	public void setDoctorHospitalId(UUID doctorHospitalId) {
		this.doctorHospitalId = doctorHospitalId;
	}
	
	
	
	
	
}
