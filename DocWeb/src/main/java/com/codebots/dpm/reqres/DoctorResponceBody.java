package com.codebots.dpm.reqres;

import java.util.List;

import com.codebots.dpm.spring.model.Doctor;
import com.codebots.dpm.spring.model.DoctorHospital;

public class DoctorResponceBody {

	private Doctor doctor;
	//private List<DoctorHospital> listOfHospital;
	private Long star;
	public Doctor getDoctor() {
		return doctor;
	}
	public void setDoctor(Doctor doctor) {
		this.doctor = doctor;
	}
	
	/*public List<DoctorHospital> getListOfHospital() {
		return listOfHospital;
	}
	public void setListOfHospital(List<DoctorHospital> listOfHospital) {
		this.listOfHospital = listOfHospital;
	}*/
	public Long getStar() {
		return star;
	}
	public void setStar(Long star) {
		this.star = star;
	}

}
