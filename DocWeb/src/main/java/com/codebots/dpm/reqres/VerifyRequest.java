package com.codebots.dpm.reqres;

public class VerifyRequest {

	private String emailOtp;
	private String smsOtp;
	public String getEmailOtp() {
		return emailOtp;
	}
	public void setEmailOtp(String emailOtp) {
		this.emailOtp = emailOtp;
	}
	public String getSmsOtp() {
		return smsOtp;
	}
	public void setSmsOtp(String smsOtp) {
		this.smsOtp = smsOtp;
	}
}
