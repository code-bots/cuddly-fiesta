package com.codebots.dpm.reqres;

import java.util.List;

import com.codebots.dpm.spring.model.Doctor;
import com.codebots.dpm.spring.model.DoctorHospital;

public class DoctorRequestBody {

	private Doctor registrationDetails;
	
	private String password;
	
	
	public Doctor getRegistrationDetails() {
		return registrationDetails;
	}
	public void setRegistrationDetails(Doctor registrationDetails) {
		this.registrationDetails = registrationDetails;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}	
}
