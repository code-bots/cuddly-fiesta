package com.codebots.dpm.reqres;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import com.codebots.dpm.spring.model.Prescription;
import com.codebots.dpm.spring.model.PrescriptionDetails;

public class PrescriptionRequestBody {

	
	private Prescription prescription;	
	private List<PrescriptionDetails> prescripionDetails;
	private String remarks;
	/*private HashMap<UUID,String> fileDetails;
	public HashMap<UUID, String> getFileDetails() {
		return fileDetails;
	}
	public void setFileDetails(HashMap<UUID, String> fileDetails) {
		this.fileDetails = fileDetails;
	}*/
	
	public List<PrescriptionDetails> getPrescripionDetails() {
		return prescripionDetails;
	}
	public void setPrescripionDetails(List<PrescriptionDetails> prescripionDetails) {
		this.prescripionDetails = prescripionDetails;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
	public Prescription getPrescription() {
		return prescription;
	}
	public void setPrescription(Prescription prescription) {
		this.prescription = prescription;
	}
	
}
