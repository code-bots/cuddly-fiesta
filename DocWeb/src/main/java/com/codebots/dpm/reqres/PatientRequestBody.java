package com.codebots.dpm.reqres;

import com.codebots.dpm.spring.model.Patient;

public class PatientRequestBody {
	private Patient registrationDetails;
	private String password;
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Patient getRegistrationDetails() {
		return registrationDetails;
	}
	public void setRegistrationDetails(Patient registrationDetails) {
		this.registrationDetails = registrationDetails;
	}
}
