package com.codebots.dpm.reqres;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import com.codebots.dpm.spring.model.Prescription;
import com.codebots.dpm.spring.model.PrescriptionDetails;

public class PrescriptionResponceBody {

	private Prescription prescriptionId;
	private List<PrescriptionDetails> prescriptionDetails;
	private HashMap<UUID,List<String>> listOfFiles;
	
	
	
	public Prescription getPrescriptionId() {
		return prescriptionId;
	}
	public void setPrescriptionId(Prescription prescriptionId) {
		this.prescriptionId = prescriptionId;
	}

	public List<PrescriptionDetails> getPrescriptionDetails() {
		return prescriptionDetails;
	}
	public void setPrescriptionDetails(List<PrescriptionDetails> prescriptionDetails) {
		this.prescriptionDetails = prescriptionDetails;
	}
	public HashMap<UUID, List<String>> getListOfFiles() {
		return listOfFiles;
	}
	public void setListOfFiles(HashMap<UUID, List<String>> listOfFiles) {
		this.listOfFiles = listOfFiles;
	}
	
	
	
	
	
	
	
}
