package com.codebots.dpm.reqres;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class ScheduleCountResponceBody {

	private Date date;
	private Long count;
	
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd", timezone="UTC")
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Long getCount() {
		return count;
	}
	public void setCount(Long count) {
		this.count = count;
	}
	
	
}
