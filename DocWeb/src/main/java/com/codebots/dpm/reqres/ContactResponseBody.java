package com.codebots.dpm.reqres;

import java.util.ArrayList;
import java.util.List;

public class ContactResponseBody {
	private List<Contact> students = new ArrayList<>();
	private List<Contact> tutors = new ArrayList<>(); 
	
	public List<Contact> getStudents() {
		return students;
	}
	public void setStudents(List<Contact> students) {
		this.students = students;
	}
	public List<Contact> getTutors() {
		return tutors;
	}
	public void setTutors(List<Contact> tutors) {
		this.tutors = tutors;
	}
	public void addStudentContact(String firstName, String lastName, String stmEmailId)
	{
		students.add(new Contact(firstName, lastName, stmEmailId));
	}
	public void addTutorContact(String firstName, String lastName, String stmEmailId)
	{
		tutors.add(new Contact(firstName, lastName, stmEmailId));
	}
}
class Contact
{
	private String firstName;
	private String lastName;
	private String stmEmailId;
	
	public Contact(String firstName, String lastName, String stmEmailId) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.stmEmailId = stmEmailId;
	}
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getStmEmailId() {
		return stmEmailId;
	}
	public void setStmEmailId(String stmEmailId) {
		this.stmEmailId = stmEmailId;
	}
}
