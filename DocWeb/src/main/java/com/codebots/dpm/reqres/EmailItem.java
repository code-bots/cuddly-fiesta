package com.codebots.dpm.reqres;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EmailItem {
	private String subject;
	private String body;
	private String from;
	private List<Receiver> receivers;
	private Date emailTime;
	public Date getEmailTime() {
		return emailTime;
	}
	public void setEmailTime(Date emailTime) {
		this.emailTime = emailTime;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public List<Receiver> getReceivers() {
		return receivers;
	}
	public void setReceivers(List<Receiver> receivers) {
		this.receivers = receivers;
	}
	public void addReceiver(String emailId, String type)
	{
		receivers.add(new Receiver(emailId, type));
	}
	public EmailItem(String subject, String body, String from) {
		super();
		this.subject = subject;
		this.body = body;
		this.from = from;
		receivers = new ArrayList<>();
	} 
}
class Receiver
{
	String emailId;
	String type;
	public Receiver(String emailId, String type) {
		super();
		this.emailId = emailId;
		this.type = type;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
}
