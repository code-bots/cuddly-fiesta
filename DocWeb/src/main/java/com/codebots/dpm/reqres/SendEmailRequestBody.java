package com.codebots.dpm.reqres;

public class SendEmailRequestBody {
	private String fromEmail;
	private String subject;
	private String body;
	private String[] toEmails;
	private String[] ccEmails;
	
	
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	
	public String getFromEmail() {
		return fromEmail;
	}
	public void setFromEmail(String fromEmail) {
		this.fromEmail = fromEmail;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public String[] getToEmails() {
		return toEmails;
	}
	public void setToEmails(String[] toEmails) {
		this.toEmails = toEmails;
	}
	public String[] getCcEmails() {
		return ccEmails;
	}
	public void setCcEmails(String[] ccEmails) {
		this.ccEmails = ccEmails;
	}
}
