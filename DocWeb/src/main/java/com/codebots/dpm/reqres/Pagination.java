package com.codebots.dpm.reqres;

import java.util.List;

public class Pagination {

	private List<Object> listOfObjects;
	private Long totalRecord;
	private Integer currentPage;
	private Integer pageSize;
	
	public List<Object> getListOfObjects() {
		return listOfObjects;
	}
	public void setListOfObjects(List<Object> listOfObjects) {
		this.listOfObjects = listOfObjects;
	}
	
	public Long getTotalRecord() {
		return totalRecord;
	}
	public void setTotalRecord(Long totalRecord) {
		this.totalRecord = totalRecord;
	}
	public Integer getCurrentPage() {
		return currentPage;
	}
	public void setCurrentPage(Integer currentPage) {
		this.currentPage = currentPage;
	}
	public Integer getPageSize() {
		return pageSize;
	}
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	
	
	
	
}
