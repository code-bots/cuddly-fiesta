package com.codebots.dpm.reqres;

import java.sql.Time;
import java.util.UUID;

public class BookedSlotsRes {


	private Time startTime;
	private Time endTime;
	public Time getStartTime() {
		return startTime;
	}
	public void setStartTime(Time startTime) {
		this.startTime = startTime;
	}
	public Time getEndTime() {
		return endTime;
	}
	public void setEndTime(Time endTime) {
		this.endTime = endTime;
	}
	
	
	
}
