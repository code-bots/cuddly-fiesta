package com.codebots.dpm.spring.dao;

import java.util.List;
import java.util.UUID;

import com.codebots.dpm.reqres.DoctorRequestBody;
import com.codebots.dpm.reqres.DoctorResponceBody;
import com.codebots.dpm.reqres.Pagination;
import com.codebots.dpm.reqres.PasswordChangeRequestBody;
import com.codebots.dpm.spring.exception.DPMAPIException;
import com.codebots.dpm.spring.model.Doctor;

public interface DoctorDao {

	Doctor save(DoctorRequestBody tutor) throws DPMAPIException;

	Pagination list(Integer status,int pageNumber,int pageSize,String type,String feild);

	Doctor update(Doctor tutor, UUID doctorId) throws DPMAPIException;
	
	DoctorResponceBody get(String userName);

	void resetPassword(PasswordChangeRequestBody passchange, UUID doctorId) throws DPMAPIException;
	List<Doctor> getByFilter(String query);
	
	List<Doctor> list(int pageNumber,int pageSize);
	
	Pagination listOfPatient(int pageNumber,int pageSize,String type,String feild,UUID doctorId);
	
	
}