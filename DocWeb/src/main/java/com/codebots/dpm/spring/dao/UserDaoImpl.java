package com.codebots.dpm.spring.dao;

import java.util.UUID;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.codebots.dpm.communication.EmailNotifier;
import com.codebots.dpm.communication.SendSMS;
import com.codebots.dpm.reqres.VerifyRequest;
import com.codebots.dpm.spring.controller.RegistrationController;
import com.codebots.dpm.spring.exception.DPMAPIException;
import com.codebots.dpm.spring.model.Doctor;
import com.codebots.dpm.spring.model.Patient;
import com.codebots.dpm.spring.model.User;

@Repository
@Transactional
public class UserDaoImpl implements UserDao{
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void resendVerificationDetails(String type, UUID userId) throws DPMAPIException
	{
		String firstName = null;
		String emailId = null;
		String lastName = null;
		String mobile = null;
		User user = null;
		if(type.equals("patient"))
		{
			Patient patient = sessionFactory.getCurrentSession().get(Patient.class, userId);
			firstName = patient.getFirstName();
			emailId = patient.getEmail();
			lastName = patient.getLastName();
			user = patient.getUserId();
			mobile = patient.getMobileNumber();
		}else if(type.equals("doctor"))
		{
			Doctor doctor = sessionFactory.getCurrentSession().get(Doctor.class, userId);
			firstName = doctor.getFirstName();
			lastName = doctor.getLastName();
			emailId = doctor.getEmail();
			user = doctor.getUserId();
			mobile = doctor.getMobileNumber();
		}else
		{
			throw new DPMAPIException("0056", new String[]{type}, null);
		}
		String emailContent = RegistrationController.EMAIL_CONTENT;
		String emailbody = emailContent.replace("@FirstName@", firstName)
				.replace("@LastName@", lastName)
				.replace("@EMAIL@", emailId)
				.replace("@OTP@", user.getEmail_OTP());
		String smsContent = RegistrationController.SMS_CONTENT;
		String smsBody = smsContent.replace("@FIRSTNAME@", firstName)
				.replace("@OTP@",user.getSms_OTP());
		EmailNotifier.sendEmailNotification(emailId, "Please verify your email", emailbody);
		SendSMS.sendSms(smsContent, mobile);
	}

	@Override
	public void setUnsetPresence(Integer id, boolean value) {
		User user = sessionFactory.getCurrentSession().get(User.class, id);
		user.setIsPresent(value);
	}

	@Override
	public String verifydetailsMethod(VerifyRequest verifyDetails,Integer uid) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from " + User.class.getName() + " where id = :uid";
		Query query = session.createQuery(hql);
		query.setParameter("uid", uid);
		User u = (User) query.uniqueResult();
		String eOtp = u.getEmail_OTP();
		String sOtp = u.getSms_OTP();
		
		if(eOtp.equals(verifyDetails.getEmailOtp()) && sOtp.equals(verifyDetails.getSmsOtp()))
		{
			String hql1 = "update " + User.class.getName() + " SET isEnabled = '1' where id = :uid";
			Query query1 = session.createQuery(hql1);
			query1.setParameter("uid", uid);
			query1.executeUpdate();
			return "success";
		}
		else
		{
			return "Please try again,Entered values are incorrect";
		}
		
	}

	@Override
	public VerifyRequest resendOTP(Integer uid) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from " + User.class.getName() + " where id = :uid";
		Query query = session.createQuery(hql);
		query.setParameter("uid", uid);
		User u = (User) query.uniqueResult();
		String eOtp = u.getEmail_OTP();
		String sOtp = u.getSms_OTP();
		VerifyRequest vr = new VerifyRequest();
		vr.setEmailOtp(eOtp);
		vr.setSmsOtp(sOtp);
		
		return vr;
	}
	
	@Override
	public User getUserDetails(Integer uID)
	{
		Session session = sessionFactory.getCurrentSession();
		String hql = "from " + User.class.getName() + " where id = :uid";
		Query query = session.createQuery(hql);
		query.setParameter("uid", uID);
		User u = (User) query.uniqueResult();
		return u;
	}
	
}
