package com.codebots.dpm.spring.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.codebots.dpm.reqres.ContactResponseBody;
import com.codebots.dpm.spring.model.Patient;
import com.codebots.dpm.spring.model.Doctor;

@Repository
@Transactional
public class EmailDaoImpl implements EmailDao{
	@Autowired
	private SessionFactory sessionfactory;
	
	@Override
	public ContactResponseBody getEmailContacts()
	{
		Session session = sessionfactory.getCurrentSession();
		List<Object[]> resultStudents = session
				.createQuery("select firstName, lastName, dpmEmailId from " + Patient.class.getName()).list();
		ContactResponseBody responseBody = new ContactResponseBody();
		for (Object[] objects : resultStudents) {
			responseBody.addStudentContact((String)objects[0],(String) objects[1],(String) objects[2]);
		}
		List<Object[]> resultTutors = session
				.createQuery("select firstName, lastName, dpmEmailId from " + Doctor.class.getName()).list();
		for (Object[] objects : resultTutors) {
			responseBody.addTutorContact((String)objects[0],(String) objects[1],(String) objects[2]);
		}
		
		return responseBody;
	}
	
}
