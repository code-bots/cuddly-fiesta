package com.codebots.dpm.spring.model;

import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;

@Entity(name = "doctor_hospital")
public class DoctorHospital {

	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
	private UUID id;
	@OneToOne(targetEntity = Doctor.class, cascade = CascadeType.ALL)
	@JoinColumn(name = "doctorId", referencedColumnName = "id", updatable=false)
	private Doctor doctorId;
	private String latitude;
	private String longitude;
	private String hospStartTime;
	private String hospEndTime;
	private String weekdays;
	private String name;
	private long fees;
	private Integer timeslot;
	private Integer paymentMode;

	

	
	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public Doctor getDoctorId() {
		return doctorId;
	}

	public void setDoctorId(Doctor doctorId) {
		this.doctorId = doctorId;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getWeekdays() {
		return weekdays;
	}

	public void setWeekdays(String weekdays) {
		this.weekdays = weekdays;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getFees() {
		return fees;
	}

	public void setFees(long fees) {
		this.fees = fees;
	}

	public String getHospStartTime() {
		return hospStartTime;
	}

	public void setHospStartTime(String hospStartTime) {
		this.hospStartTime = hospStartTime;
	}

	public String getHospEndTime() {
		return hospEndTime;
	}

	public void setHospEndTime(String hospEndTime) {
		this.hospEndTime = hospEndTime;
	}

	public Integer getTimeslot() {
		return timeslot;
	}

	public void setTimeslot(Integer timeslot) {
		this.timeslot = timeslot;
	}

	public Integer getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(Integer paymentMode) {
		this.paymentMode = paymentMode;
	}

	

}
