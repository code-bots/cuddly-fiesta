package com.codebots.dpm.spring.controller;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codebots.dpm.spring.dao.FeedbackDao;
import com.codebots.dpm.spring.exception.DPMAPIException;
import com.codebots.dpm.spring.model.Feedback;

@RestController
@RequestMapping("/rest/api")
public class FeedbackController {

	
	@Autowired
	private FeedbackDao feedbackdao;
	
	@PostMapping("/feedback/{scheduleId}")
	public ResponseEntity<Feedback> postFeedback(@RequestBody Feedback feedback,@PathVariable("scheduleId")UUID scheduleId) throws DPMAPIException
	{
		try
		{
			Feedback fb = feedbackdao.saveFeedback(feedback, scheduleId);
			return ResponseEntity.ok().body(fb);
		}
		catch(Exception e)
		{
			throw new DPMAPIException("0088", new String[] { e.getMessage() }, e);
		}

	}
}
