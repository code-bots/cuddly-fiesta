package com.codebots.dpm.spring.controller;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codebots.dpm.reqres.ResponseMessage;
import com.codebots.dpm.spring.dao.BlogDao;
import com.codebots.dpm.spring.dao.CommentDao;
import com.codebots.dpm.spring.exception.DPMAPIException;
import com.codebots.dpm.spring.model.Blog;
import com.codebots.dpm.spring.model.Comments;

@RestController
@RequestMapping("/rest/api")
public class BlogController {
	
	@Autowired
	private BlogDao blogDao;
	
	@Autowired
	private CommentDao comDao;
	
	
	@PostMapping("/blog/{doctorId}")
	public ResponseEntity<Blog> saveBlog(@RequestBody Blog blog,@PathVariable("doctorId") UUID tutor_id) throws DPMAPIException
	{
		try
		{
		Blog blg = blogDao.save(blog, tutor_id);
		return ResponseEntity.ok().body(blg);
		}
		catch(Exception e)
		{
			throw new DPMAPIException("0061", new String[] {e.getMessage()}, e);
		}
	}
	
	@DeleteMapping("/blog/{blog_id}")
	public ResponseEntity<ResponseMessage> deleteBlog(@PathVariable("blog_id") UUID blog_id) throws DPMAPIException
	{
		try
		{
			blogDao.deleteBlog(blog_id);
			ResponseMessage msg = new ResponseMessage();
			msg.setMsg("Successfully Deleted Blog");
			return ResponseEntity.ok().body(msg);
		}
		catch(Exception e)
		{
			throw new DPMAPIException("0064", new String[] {e.getMessage()}, e);
		}
	}
	
	@PostMapping("comment/{blog_id}")
	public ResponseEntity<Comments> saveComment(@RequestBody Comments com,@PathVariable("blog_id") UUID blog_id) throws DPMAPIException
	{
		try
		{
			Comments comment = comDao.saveComments(com, blog_id);
			return ResponseEntity.ok().body(comment);
		}
		catch(Exception e)
		{
			throw new DPMAPIException("0065", new String[] {e.getMessage()}, e);
		}
	}
	
	
	@GetMapping("doctor/blog/{doctorId}")
	public ResponseEntity<List<Blog>> getByTutorBlog(@PathVariable("doctorId") UUID doctorId) throws DPMAPIException
	{
		try
		{
			List<Blog> blogs = blogDao.getBlogByDoctor(doctorId);
			return ResponseEntity.ok().body(blogs);
		}
		catch(Exception e)
		{
			throw new DPMAPIException("0062", new String[] {e.getMessage()}, e);
		}
	}
	
	@GetMapping("blog/{id}")
	public ResponseEntity<Blog> getByBlogId(@PathVariable("id") UUID blog_id) throws DPMAPIException
	{
		try
		{
			Blog blog = blogDao.getBogById(blog_id);
			return ResponseEntity.ok().body(blog);
		}
		catch(Exception e)
		{
			throw new DPMAPIException("0073", new String[] {e.getMessage()}, e);
		}
	} 
	
	@GetMapping("/bloglist")
	public ResponseEntity<List<Blog>> getAll() throws DPMAPIException
	{
		try
		{
			List<Blog> blogs = blogDao.getAllBlogs();
			return ResponseEntity.ok().body(blogs);
		}
		catch(Exception e)
		{
			throw new DPMAPIException("0063", new String[] {e.getMessage()}, e);
		}
	}
	
	@GetMapping("/comments/{blog_id}")
	public ResponseEntity<List<Comments>> getAllCommentsByBlog(@PathVariable("blog_id") UUID blog_id) throws DPMAPIException
	{
		try
		{
			List<Comments> coms = comDao.listByBlogs(blog_id);
			return ResponseEntity.ok().body(coms);
		}
		catch(Exception e)
		{
			throw new DPMAPIException("0066", new String[] {e.getMessage()}, e);
		}
	}
	
	
	@GetMapping("/parentComments/{blog_id}")
	public ResponseEntity<List<Comments>> getParentCmt(@PathVariable("blog_id") UUID blog_id) throws DPMAPIException
	{
		try
		{
			List<Comments> coms = comDao.parentCmt(blog_id);
			return ResponseEntity.ok().body(coms);
		}
		catch(Exception e)
		{
			throw new DPMAPIException("0067", new String[] {e.getMessage()}, e);
		}
	}
	
	
	
	@GetMapping("/comment/{commentId}")
	public ResponseEntity<Comments> getCommentById(@PathVariable("commentId") UUID commentId) throws DPMAPIException
	{
		try
		{
			Comments com = comDao.getCommentById(commentId);
			return ResponseEntity.ok().body(com);
		}
		catch(Exception e)
		{
			throw new DPMAPIException("0068", new String[] {e.getMessage()}, e);
		}
	}
	
	@GetMapping("/commentchain/{parentCommentId}")
	public ResponseEntity<List<Comments>> getAllCommentsByParent(@PathVariable("parentCommentId") UUID parentCommentId) throws DPMAPIException
	{
		try
		{
			List<Comments> coms = comDao.listByParant(parentCommentId);
			return ResponseEntity.ok().body(coms);
		}
		catch(Exception e)
		{
			throw new DPMAPIException("0069", new String[] {e.getMessage()}, e);
		}
	}
	
	@PutMapping("comment/{commentId}")
	public ResponseEntity<Comments> editComment(@RequestBody Comments comment,@PathVariable("commentId") UUID commentId) throws DPMAPIException
	{
		try
		{
			Comments com = comDao.updateComment(comment, commentId);
			return ResponseEntity.ok().body(com);
		}
		catch(Exception e)
		{
			throw new DPMAPIException("0068", new String[] {e.getMessage()}, e);
		}
	}
	
	@PutMapping("blog/{blogId}")
	public ResponseEntity<Blog> editBlog(@RequestBody Blog blog,@PathVariable("blogId") UUID blogId) throws DPMAPIException
	{
		try
		{
			Blog bl = blogDao.updateBlog(blog, blogId);
			return ResponseEntity.ok().body(bl);
		}
		catch(Exception e)
		{
			throw new DPMAPIException("0070", new String[] {e.getMessage()}, e);
		}
	}
	
	
	@DeleteMapping("comment/{commentId}")
	public ResponseEntity<ResponseMessage> deleteComment(@PathVariable("commentId") UUID commentId) throws DPMAPIException
	{
		try
		{
			comDao.deleteComment(commentId);
			ResponseMessage msg = new ResponseMessage();
			msg.setMsg("Comment Deleted");
			return ResponseEntity.ok().body(msg);
		}
		catch(Exception e)
		{
			throw new DPMAPIException("0069", new String[] {e.getMessage()}, e);
		}
	}
	
	
	
}
