package com.codebots.dpm.spring.dao;

import java.util.List;
import java.util.UUID;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Repository;

import com.codebots.dpm.spring.model.Blog;
import com.codebots.dpm.spring.model.Doctor;

@Repository
@PropertySource("classpath:application.properties")
@PropertySource("classpath:mailbox.properties")
@Transactional
public class BlogDaoImple  implements BlogDao{

	@Autowired
	private SessionFactory sessionfactory;
	

	@Override
	public Blog save(Blog blog, UUID doctorId) {
		Session session = sessionfactory.getCurrentSession();
		Doctor doc =  session.byId(Doctor.class).load(doctorId);
		blog.setDoctorId(doc);
		sessionfactory.getCurrentSession().save(blog);
		
		return blog;
	}

	@Override
	public List<Blog> getBlogByDoctor(UUID doctorId) {
		Session session = sessionfactory.getCurrentSession();
		String hql = "from " + Blog.class.getName() + " where doctorId.id= :doctor order by creation_date desc";
		Query query = session.createQuery(hql);
		query.setParameter("doctor",doctorId);
		List<Blog> list = query.list();
		return list;

	}

	@Override
	public List<Blog> getAllBlogs() {
		Session session = sessionfactory.getCurrentSession();
		String hql = "from " + Blog.class.getName() + " order by creation_date desc";
		Query query = session.createQuery(hql);
		List<Blog> list = query.list();
		return list;
	}

	@Override
	public Blog getBogById(UUID id) {
		Session session = sessionfactory.getCurrentSession();
		Blog blog = session.byId(Blog.class).load(id);
		return blog;
	}

	@Override
	public Blog updateBlog(Blog blog, UUID blogId) {
		Session session = sessionfactory.getCurrentSession();
		
		Blog bl = session.get(Blog.class, blogId);
		
		bl.setContent(blog.getContent());
		bl.setTitle(blog.getTitle());
		bl.setTags(blog.getTags());
		
		session.saveOrUpdate(bl);		
		return bl;
	}

	@Override
	public void deleteBlog(UUID blog_id) {
	Session session = sessionfactory.getCurrentSession();
	String hql = "delete from " + Blog.class.getName() + " where id = :blogId";
	Query query = session.createQuery(hql).setParameter("blogId", blog_id);
	query.executeUpdate();	// TODO Auto-generated method stub
		
	}

}
