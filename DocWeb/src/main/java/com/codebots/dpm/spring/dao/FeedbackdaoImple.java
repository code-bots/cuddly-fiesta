package com.codebots.dpm.spring.dao;

import java.util.UUID;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Repository;

import com.codebots.dpm.spring.model.Feedback;
import com.codebots.dpm.spring.model.Schedule;

@Repository
@PropertySource("classpath:application.properties")
@PropertySource("classpath:mailbox.properties")
@Transactional
public class FeedbackdaoImple implements FeedbackDao{

	@Autowired
	private SessionFactory sessionfactory;
	
	@Override
	public Feedback saveFeedback(Feedback feedback, UUID scheduleId) {
		Session session = sessionfactory.getCurrentSession();
		Schedule sch = session.byId(Schedule.class).load(scheduleId);
		feedback.setScheduleId(sch);
		sessionfactory.getCurrentSession().save(feedback);
		return feedback;
	}
	
	
	

}
