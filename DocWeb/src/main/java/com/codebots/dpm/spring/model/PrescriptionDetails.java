package com.codebots.dpm.spring.model;

import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;

@Entity(name = "prescriptionDetails")
public class PrescriptionDetails 
{
	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(name="UUID",strategy="org.hibernate.id.UUIDGenerator")
	private UUID id;
	
	@OneToOne(targetEntity=Prescription.class,cascade=CascadeType.ALL)
	@JoinColumn(name="prescriptionId",referencedColumnName="id")
	private Prescription prescriptionId;
	
	private String type;
	private String name;
	private String additionalInfo1;
	private String additionalInfo2;
	private String additionalInfo3;
	private String sideEffect;
	private String remarks;
	public UUID getId() {
		return id;
	}
	public void setId(UUID id) {
		this.id = id;
	}
	
	public Prescription getPrescriptionId() {
		return prescriptionId;
	}
	public void setPrescriptionId(Prescription prescriptionId) {
		this.prescriptionId = prescriptionId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAdditionalInfo1() {
		return additionalInfo1;
	}
	public void setAdditionalInfo1(String additionalInfo1) {
		this.additionalInfo1 = additionalInfo1;
	}
	public String getAdditionalInfo2() {
		return additionalInfo2;
	}
	public void setAdditionalInfo2(String additionalInfo2) {
		this.additionalInfo2 = additionalInfo2;
	}
	
	public String getAdditionalInfo3() {
		return additionalInfo3;
	}
	public void setAdditionalInfo3(String additionalInfo3) {
		this.additionalInfo3 = additionalInfo3;
	}
	public String getSideEffect() {
		return sideEffect;
	}
	public void setSideEffect(String sideEffect) {
		this.sideEffect = sideEffect;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
	
	
	
	
}