package com.codebots.dpm.spring.dao;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Repository;

import com.codebots.dpm.reqres.PrescriptionRequestBody;
import com.codebots.dpm.reqres.PrescriptionResponceBody;
import com.codebots.dpm.spring.model.Doctor;
import com.codebots.dpm.spring.model.DoctorHospital;
import com.codebots.dpm.spring.model.Patient;
import com.codebots.dpm.spring.model.Prescription;
import com.codebots.dpm.spring.model.PrescriptionDetails;
import com.codebots.dpm.spring.model.Schedule;


@Repository
@PropertySource("classpath:application.properties")
@PropertySource("classpath:mailbox.properties")
@Transactional
public class PrescriptionDaoImple implements PrescriptionDao {

	@Autowired
	private SessionFactory sessionfactory;
	
	@Autowired org.springframework.core.env.Environment env;

	

	@Override
	public PrescriptionResponceBody savePre(PrescriptionRequestBody prescription,UUID scheduleId) {
		Session session = sessionfactory.getCurrentSession();
		
		List<PrescriptionDetails> preDet = new ArrayList<PrescriptionDetails>();
		
		
		Prescription preDao = prescription.getPrescription();
		Schedule schedule = session.get(Schedule.class, scheduleId);
		preDao.setScheduleId(schedule);
		
		for(PrescriptionDetails preDetails : prescription.getPrescripionDetails())
		{
			preDetails.setPrescriptionId(preDao);	
			preDet.add(preDetails);
			sessionfactory.getCurrentSession().save(preDetails);
		}
		
		sessionfactory.getCurrentSession().save(preDao);
		
		PrescriptionResponceBody preResponce = new PrescriptionResponceBody();
		preResponce.setPrescriptionId(preDao);
		preResponce.setPrescriptionDetails(preDet);
		
		
		return preResponce;
	}
	
	@Override
	public List<Prescription> getAllPrescription() {
		Session session = sessionfactory.getCurrentSession();
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Prescription> cq = cb.createQuery(Prescription.class);
		Root<Prescription> root = cq.from(Prescription.class);
		cq.select(root);
		Query<Prescription> query = session.createQuery(cq);
		return query.getResultList();
	}
	
	@Override
	public PrescriptionResponceBody getAllPrescriptionDetails(UUID preId) {
		
		Session session = sessionfactory.getCurrentSession();
			
		Prescription pre = session.byId(Prescription.class).load(preId);
		
		String hql = "from "+ PrescriptionDetails.class.getName() + " where prescriptionId.id = :preId";
		Query query = session.createQuery(hql);
		query.setParameter("preId", preId);
		List<PrescriptionDetails> listPreDetails = query.list();		
		 

		HashMap<UUID, List<String>> fileMap = new HashMap<UUID, List<String>>();
		
		for(PrescriptionDetails pred : listPreDetails)
		{
			List<String> namesOfFile = new ArrayList<String>();
			File directory = new File(env.getProperty("upload") + pred.getId()); 
			if(directory.exists())
			{
				File[] fList = directory.listFiles();
				for (File file : fList){
		         	String name = file.getName();
		         	namesOfFile.add(name);			         	
		         }	
				fileMap.put(pred.getId(), namesOfFile);
			}
			
		}
		
		 PrescriptionResponceBody preRes = new PrescriptionResponceBody();
		 preRes.setPrescriptionId(pre);
		 preRes.setPrescriptionDetails(listPreDetails);
		 preRes.setListOfFiles(fileMap);
		 
		return preRes;
	}


	@Override
	public List<PrescriptionResponceBody> getPreByPatient(UUID patientId) {
		Session session = sessionfactory.getCurrentSession();
		List<PrescriptionResponceBody> listofResponce = new ArrayList<PrescriptionResponceBody>();
		
		List<UUID> listOfPrescription = this.getPreIdOfPatient(patientId);
		for(UUID prescriptionID : listOfPrescription)
		{
			PrescriptionResponceBody preResponceBody = new PrescriptionResponceBody();
			Prescription prescription = session.byId(Prescription.class).load(prescriptionID);
			
			
			String hql = "from "+ PrescriptionDetails.class.getName() + " where prescriptionId.id = :preId";
			Query query = session.createQuery(hql);
			query.setParameter("preId", prescriptionID);
			List<PrescriptionDetails> preDetails = query.list();
	
			
			
			HashMap<UUID, List<String>> fileMap = new HashMap<UUID, List<String>>();
			
			for(PrescriptionDetails pred : preDetails)
			{
				List<String> namesOfFile = new ArrayList<String>();
				File directory = new File(env.getProperty("upload") + pred.getId()); 
				if(directory.exists())
				{
					File[] fList = directory.listFiles();
					for (File file : fList){
			         	String name = file.getName();
			         	namesOfFile.add(name);			         	
			         }		
					fileMap.put(pred.getId(), namesOfFile);
				}
				
			}
			preResponceBody.setPrescriptionId(prescription);
			preResponceBody.setPrescriptionDetails(preDetails);
			preResponceBody.setListOfFiles(fileMap);
			
			listofResponce.add(preResponceBody);
			
		}
		return listofResponce;
	}

	@Override
	public List<PrescriptionDetails> getTestPatient(UUID patientId,String type) {
		Session session = sessionfactory.getCurrentSession();
		String hql = "FROM "+ PrescriptionDetails.class.getName() +  " pred," + Prescription.class.getName() + " pre,"
				+ Schedule.class.getName() + " sc," + Patient.class.getName() +  
				" 	pet WHERE  pred.prescriptionId = pre.id   AND pre.scheduleId = sc.id  AND sc.patientId = pet.id  AND pet.id = :petient AND pred.type = :type";
		Query query = session.createQuery(hql);
		query.setParameter("petient", patientId);
		query.setParameter("type", type);
		List<PrescriptionDetails> list = query.list();		
		return list;
	}
	
	@Override
	public void deletePrescription(UUID prescriptionDetailId) {
		Session session = sessionfactory.getCurrentSession();
		String hql = "delete " + PrescriptionDetails.class.getName() + " where id = :preDetId";
		Query query = session.createQuery(hql);
		query.setParameter("preDetId", prescriptionDetailId);
		query.executeUpdate();
	}

	@Override
	public PrescriptionDetails updatePreDetails(PrescriptionDetails preDetails,UUID preDetId) {
		Session session = sessionfactory.getCurrentSession();
		
		PrescriptionDetails preDet = session.get(PrescriptionDetails.class, preDetId);
		
		preDet.setName(preDetails.getName());
		preDet.setAdditionalInfo1(preDetails.getAdditionalInfo1());
		preDet.setAdditionalInfo2(preDetails.getAdditionalInfo2());
		preDet.setAdditionalInfo3(preDetails.getAdditionalInfo3());
		preDet.setType(preDetails.getType());
		preDet.setSideEffect(preDetails.getSideEffect());
		preDet.setRemarks(preDetails.getRemarks());
		
		session.saveOrUpdate(preDet);
				return preDet;
	}

	


	@Override
	public List<PrescriptionResponceBody> getPreByDoctor(UUID doctorId) {
		Session session = sessionfactory.getCurrentSession();
		List<PrescriptionResponceBody> listofResponce = new ArrayList<PrescriptionResponceBody>();
		
		List<UUID> listOfPrescription = this.getPreIdOfDoctor(doctorId);
		for(UUID prescriptionID : listOfPrescription)
		{
			PrescriptionResponceBody preResponceBody = new PrescriptionResponceBody();
			Prescription prescription = session.byId(Prescription.class).load(prescriptionID);
			
			
			String hql = "from "+ PrescriptionDetails.class.getName() + " where prescriptionId.id = :preId";
			Query query = session.createQuery(hql);
			query.setParameter("preId", prescriptionID);
			List<PrescriptionDetails> preDetails = query.list();
	
			
			HashMap<UUID, List<String>> fileMap = new HashMap<UUID, List<String>>();
			
			for(PrescriptionDetails pred : preDetails)
			{
				List<String> namesOfFile = new ArrayList<String>();
				File directory = new File(env.getProperty("upload") + pred.getId()); 
				if(directory.exists())
				{
					File[] fList = directory.listFiles();
					for (File file : fList){
			         	String name = file.getName();
			         	namesOfFile.add(name);			         	
			         }		
					fileMap.put(pred.getId(), namesOfFile);
				}
				
			}
			preResponceBody.setPrescriptionId(prescription);
			preResponceBody.setPrescriptionDetails(preDetails);
			preResponceBody.setListOfFiles(fileMap);
			
			listofResponce.add(preResponceBody);
			
		}
		return listofResponce;
	}
	

	@Override
	public List<UUID> getPreIdOfPatient(UUID patientId) {

		Session session = sessionfactory.getCurrentSession();
		
		String hql = "select pre.id from "+ Prescription.class.getName() + " pre," +  Schedule.class.getName() + " sch,"+ Patient.class.getName() + " p "
				+ "where pre.scheduleId = sch.id and sch.patientId = p.id and p.id = :patientId";
		Query query = session.createQuery(hql);
		query.setParameter("patientId", patientId);
		List<UUID> list = query.getResultList();

		return list;
	}

	@Override
	public List<UUID> getPreIdOfDoctor(UUID doctorId) {
		Session session = sessionfactory.getCurrentSession();
		String hql = "select pre.id from "+ Prescription.class.getName() + " pre," +  Schedule.class.getName() + " sch,"+ DoctorHospital.class.getName() + " docHos,"
					+ Doctor.class.getName() + " doc where pre.scheduleId = sch.id and sch.doctorHospitalId = docHos.id and docHos.doctorId = doc.id and doc.id = :docID";
				
		Query query = session.createQuery(hql);
		query.setParameter("docID", doctorId);
		List<UUID> list = query.getResultList();

		return list;

	}

	@Override
	public PrescriptionResponceBody getBySchedule(UUID scheduleId) {
		Session session = sessionfactory.getCurrentSession();
		
		PrescriptionResponceBody preResponce = new PrescriptionResponceBody();
		
		String hql = "from " + Prescription.class.getName() + " where scheduleId.id = :scheduleId";
		Query query = session.createQuery(hql);
		query.setParameter("scheduleId", scheduleId);
		Prescription pre = (Prescription) query.uniqueResult();
		
		
		String hql1 = "from "+ PrescriptionDetails.class.getName() + " where prescriptionId.id = :preId";
		Query query1 = session.createQuery(hql1);
		query1.setParameter("preId", pre.getId());
		List<PrescriptionDetails> preDetails = query1.list();
		
		HashMap<UUID, List<String>> fileMap = new HashMap<UUID, List<String>>();
		
		for(PrescriptionDetails pred : preDetails)
		{
			List<String> namesOfFile = new ArrayList<String>();
			File directory = new File(env.getProperty("upload") + pred.getId()); 
			if(directory.exists())
			{
				File[] fList = directory.listFiles();
				for (File file : fList){
		         	String name = file.getName();
		         	namesOfFile.add(name);			         	
		         }		
				fileMap.put(pred.getId(), namesOfFile);
			}
			
		}
			
		preResponce.setPrescriptionId(pre);
		preResponce.setPrescriptionDetails(preDetails);
		preResponce.setListOfFiles(fileMap);
		return preResponce;
	}






	
	

}
