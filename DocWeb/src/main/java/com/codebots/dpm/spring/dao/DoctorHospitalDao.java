package com.codebots.dpm.spring.dao;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.codebots.dpm.spring.model.DoctorHospital;

public interface DoctorHospitalDao {

	DoctorHospital save(DoctorHospital doctorHospital,UUID doctorId);
	DoctorHospital update(DoctorHospital tutorInstitute,UUID doctorHospitalId);
	List<DoctorHospital> getByDoctor(UUID doctorId);
	List<DoctorHospital> getAll();
	List<DoctorHospital> getByFilter(String param);
	DoctorHospital getById(UUID docHosId);
	List<DoctorHospital> getByDayDoctor(UUID doctorId,String weekdays);
	List<DoctorHospital> getByDay(String weekdays);
}
