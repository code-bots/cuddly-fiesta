package com.codebots.dpm.spring.dao;


import java.util.List;
import java.util.UUID;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Repository;

import com.codebots.dpm.spring.model.Surgery;
import com.codebots.dpm.spring.model.Patient;


@Repository
@PropertySource("classpath:application.properties")
@Transactional
public class SurgeryDaoImpl implements SurgeryDao {

	@Autowired
	private SessionFactory sessionfactory;

	
	
	@Override
	public Surgery save(Surgery surgery, UUID patientId) {
		Patient patient = sessionfactory.getCurrentSession().get(Patient.class, patientId);
		surgery.setPatientId(patient);
		sessionfactory.getCurrentSession().save(surgery);
		return surgery;
	}

	@Override
	public List<Surgery> list() {
		Session session = sessionfactory.getCurrentSession();
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Surgery> cq = cb.createQuery(Surgery.class);
		Root<Surgery> root = cq.from(Surgery.class);
		cq.select(root);
		Query<Surgery> query = session.createQuery(cq);
		return query.getResultList();
	}

	@Override
	public List<Surgery> getbyId(UUID patientId) {
		Session session = sessionfactory.getCurrentSession();
		Query query = sessionfactory.getCurrentSession().createQuery(" from " + Surgery.class.getName() + " where patientId.id = :pid order by datePerformed desc");
		query.setParameter("pid", patientId);
		List<Surgery> list = query.list();
		return list;
	}

	@Override
	public Surgery getSpecific(UUID surgeryId) {
		Session session = sessionfactory.getCurrentSession();
		String hql = "from " + Surgery.class.getName() + " where id = :surgrey";
		Query query = session.createQuery(hql);
		query.setParameter("surgrey", surgeryId);
		Surgery surg  = (Surgery) query.uniqueResult();
		return surg;
	}

	@Override
	public int delete(UUID patientId, UUID surgeryId) {
		Session session = sessionfactory.getCurrentSession();
		Query query = session.createQuery("delete from surgery where id = :sid and patientId = :pid");
		query.setParameter("sid", surgeryId);
		query.setParameter("pid", patientId);
		int result = query.executeUpdate();
		return result;
	}


	@Override
	public void update(UUID patientId,UUID surgeryId, Surgery surgery) {
		Session session = sessionfactory.getCurrentSession();
		String hql = "from " + Surgery.class.getName() + " where id = :sid and patientId.id = :pid ";
		Query query = session.createQuery(hql);
		query.setParameter("sid", surgeryId);
		query.setParameter("pid", patientId);
		Surgery ad = (Surgery) query.uniqueResult();
		ad.setDatePerformed(surgery.getDatePerformed());
		ad.setHospitalName(surgery.getHospitalName());
		ad.setSurgeryName(surgery.getSurgeryName());
		session.saveOrUpdate(ad);
	}

}
