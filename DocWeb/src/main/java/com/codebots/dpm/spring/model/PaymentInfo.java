package com.codebots.dpm.spring.model;

import java.util.Date;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;


@Entity(name = "payment_info")
public class PaymentInfo {

	
	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(name="UUID",strategy="org.hibernate.id.UUIDGenerator")
	private UUID id;

	@OneToOne(targetEntity=Schedule.class,cascade=CascadeType.ALL)
	@JoinColumn(name="scheduleId",referencedColumnName="id", updatable=false)
	private Schedule scheduleId;
	
	
	@OneToOne(targetEntity=Patient.class,cascade=CascadeType.ALL)
	@JoinColumn(name="patientId",referencedColumnName="id", updatable=false)
	private Patient patientId;

	@OneToOne(targetEntity=DoctorHospital.class,cascade=CascadeType.ALL)
	@JoinColumn(name="doctorHosId",referencedColumnName="id", updatable=false)
	private DoctorHospital doctorHosId;
	
	
	
	private Date txn_time;
	private Long amount;
	private String txn_id;
	private String app_status;
	private String respcode;
	private String gateway_status;
	private String respmsg;
	public UUID getId() {
		return id;
	}
	public void setId(UUID id) {
		this.id = id;
	}
	
	
	public Patient getPatientId() {
		return patientId;
	}
	public void setPatientId(Patient patientId) {
		this.patientId = patientId;
	}
	public DoctorHospital getDoctorHosId() {
		return doctorHosId;
	}
	public void setDoctorHosId(DoctorHospital doctorHosId) {
		this.doctorHosId = doctorHosId;
	}
	public Schedule getScheduleId() {
		return scheduleId;
	}
	public void setScheduleId(Schedule scheduleId) {
		this.scheduleId = scheduleId;
	}
	public Date getTxn_time() {
		return txn_time;
	}
	public void setTxn_time(Date txn_time) {
		this.txn_time = txn_time;
	}
	public Long getAmount() {
		return amount;
	}
	public void setAmount(Long amount) {
		this.amount = amount;
	}
	public String getTxn_id() {
		return txn_id;
	}
	public void setTxn_id(String txn_id) {
		this.txn_id = txn_id;
	}
	public String getApp_status() {
		return app_status;
	}
	public void setApp_status(String app_status) {
		this.app_status = app_status;
	}
	public String getRespcode() {
		return respcode;
	}
	public void setRespcode(String respcode) {
		this.respcode = respcode;
	}
	public String getGateway_status() {
		return gateway_status;
	}
	public void setGateway_status(String gateway_status) {
		this.gateway_status = gateway_status;
	}
	public String getRespmsg() {
		return respmsg;
	}
	public void setRespmsg(String respmsg) {
		this.respmsg = respmsg;
	}
	
	
	
}
