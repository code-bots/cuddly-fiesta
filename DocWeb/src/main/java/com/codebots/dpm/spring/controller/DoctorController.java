package com.codebots.dpm.spring.controller;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codebots.dpm.reqres.Pagination;
import com.codebots.dpm.reqres.PasswordChangeRequestBody;
import com.codebots.dpm.reqres.ResponseMessage;
import com.codebots.dpm.spring.dao.DoctorDao;
import com.codebots.dpm.spring.dao.DoctorHospitalDao;
import com.codebots.dpm.spring.exception.DPMAPIException;
import com.codebots.dpm.spring.model.Doctor;
import com.codebots.dpm.spring.model.DoctorHospital;

@RestController
@RequestMapping("/rest/api")
public class DoctorController {

	@Autowired
	private DoctorDao doctorDao;

	@Autowired
	private DoctorHospitalDao doctorHospitalDao;
	

	@GetMapping("/doctorgrid")
	public ResponseEntity<Pagination> listDoctorGrid(@RequestParam(value = "status",required = false) Integer status,
													 @RequestParam(value = "page",required = false) Integer pageNumber,
													 @RequestParam(value = "size",required = false) Integer pageSize,
													 @RequestParam(value = "type",required = false) String type,
													 @RequestParam(value = "feild",required = false) String feild) throws DPMAPIException {
		try {
			Pagination page = doctorDao.list(status,pageNumber,pageSize,type,feild);
			return ResponseEntity.ok().body(page);
		} catch (Exception e) {
			throw new DPMAPIException("0012", new String[] { e.getMessage() }, e);
		}
	}

	/*
	 * @GetMapping("/questionSet/{tutor_id}") public
	 * ResponseEntity<List<QuestionSet>> getQuestionSet(@PathVariable("tutor_id")
	 * UUID tutor_id) { List<QuestionSet> list = queSetDao.getById(tutor_id); return
	 * ResponseEntity.ok().body(list); }
	 */

	@PutMapping("/doctor/{doctor_id}")
	public ResponseEntity<Doctor> updateDoctor(@PathVariable("doctor_id") UUID id, @RequestBody Doctor doctor)
			throws DPMAPIException {
		try {
		
			doctor.setId(id);
			doctorDao.update(doctor, id);
			return new ResponseEntity<Doctor>(doctor, HttpStatus.OK);
		} catch (Exception e) {
			throw new DPMAPIException("0013", new String[] { e.getMessage() }, e);
		}

	}

	@PutMapping("doctor/changepassword/{doctor_id}")
	public ResponseEntity<ResponseMessage> resetPassword(@PathVariable("doctor_id") UUID doctorId,
			@RequestBody PasswordChangeRequestBody passchange) throws DPMAPIException {
		try {
			doctorDao.resetPassword(passchange, doctorId);
			ResponseMessage msg = new ResponseMessage();
			msg.setMsg("Doctor's Password updated Successfully");
			return ResponseEntity.ok().body(msg);
		} catch (Exception e) {
			throw new DPMAPIException("0014", new String[] { e.getMessage() }, e);
		}
	}

	@PostMapping("hospital/{doctor_id}")
	public ResponseEntity<DoctorHospital> saveHospital(@RequestBody DoctorHospital docHospital,
			@PathVariable("doctor_id") UUID doctor_id) throws DPMAPIException {
		try {
			DoctorHospital tut = doctorHospitalDao.save(docHospital, doctor_id);
			return ResponseEntity.ok().body(tut);
		} catch (Exception e) {
			throw new DPMAPIException("0015", new String[] { e.getMessage() }, e);
		}
	}

	@PutMapping("hospital/{hospital_id}")
	public ResponseEntity<DoctorHospital> updateHospital(@RequestBody DoctorHospital docHospital,
			@PathVariable("hospital_id") UUID hospitalId) throws DPMAPIException {
		try {
			docHospital.setId(hospitalId);
			DoctorHospital doctorHospitalUpdatedObject = doctorHospitalDao.update(docHospital, hospitalId);
			return ResponseEntity.ok().body(doctorHospitalUpdatedObject);
		} catch (Exception e) {
			throw new DPMAPIException("0016", new String[] { e.getMessage() }, e);
		}
	}

	@GetMapping("hospital/{doctor_id}")
	public ResponseEntity<List<DoctorHospital>> getByDoctorHospital(@PathVariable("doctor_id") UUID doctor_id)
			throws DPMAPIException {
		try {
			List<DoctorHospital> list = doctorHospitalDao.getByDoctor(doctor_id);
			return ResponseEntity.ok().body(list);
		} catch (Exception e) {
			throw new DPMAPIException("0017", new String[] { e.getMessage() }, e);
		}

	}
	
	@GetMapping("doctor/hospital")
	public ResponseEntity<List<DoctorHospital>> getALlDoctorHospital() throws DPMAPIException
	{
		try
		{
			List<DoctorHospital> list = doctorHospitalDao.getAll();
			return ResponseEntity.ok().body(list);
		}
		catch(Exception e)
		{
			throw new DPMAPIException("0017", new String[] { e.getMessage() }, e);
		}
		
	}

	
	@GetMapping("hospital")
	public ResponseEntity<List<DoctorHospital>> getByFilter(@RequestParam(value = "query") String query) throws DPMAPIException
	{
		try
		{
			List<DoctorHospital> list = doctorHospitalDao.getByFilter(query);
			return ResponseEntity.ok().body(list);
		}
		catch(Exception e)
		{
			throw new DPMAPIException("0024", new String[] { e.getMessage() }, e);
		}
		
		
	}
	
	
	@GetMapping("doctor")
	public ResponseEntity<List<Doctor>> getDoctorByFilter(@RequestParam(value = "query") String query) throws DPMAPIException
	{
		try
		{
			List<Doctor> list = doctorDao.getByFilter(query);
			return ResponseEntity.ok().body(list);
		}
		catch(Exception e)
		{
			throw new DPMAPIException("0023", new String[] { e.getMessage() }, e);
		}
	}
	
	
	@GetMapping("/weekdays/{doctorId}/{weekdays}")
	public ResponseEntity<List<DoctorHospital>> getByWeekdays(@PathVariable("doctorId") UUID docId,@PathVariable("weekdays") String weekdays) throws DPMAPIException
	{
		try
		{
			List<DoctorHospital> list = doctorHospitalDao.getByDayDoctor(docId, weekdays);
			return ResponseEntity.ok().body(list);
		}catch(Exception e)
		{
			throw new DPMAPIException("0033", new String[] { e.getMessage() }, e);
		}

	}
	
	@GetMapping("/weekdays/{weekdays}")
	public ResponseEntity<List<DoctorHospital>> getByWeekdays(@PathVariable("weekdays") String weekdays) throws DPMAPIException
	{
		try
		{
			List<DoctorHospital> list = doctorHospitalDao.getByDay(weekdays);
			return ResponseEntity.ok().body(list);
		}catch(Exception e)
		{
			throw new DPMAPIException("0033", new String[] { e.getMessage() }, e);
		}

	}

	@GetMapping("/doctor/patientlist/{doctorId}")
	public ResponseEntity<Pagination> listOfPatientByDoctor(@PathVariable("doctorId") UUID doctorId,
	  													    @RequestParam(value = "page",required = false) Integer pageNumber,
														    @RequestParam(value = "size",required = false) Integer pageSize,
														    @RequestParam(value = "type",required = false) String type,
														    @RequestParam(value = "feild",required = false) String feild) throws DPMAPIException
	{
		try
		{
		Pagination page = doctorDao.listOfPatient(pageNumber, pageSize, type, feild,doctorId);
		return ResponseEntity.ok().body(page);
		}
		catch(Exception e)	
		{
			throw new DPMAPIException("0087", new String[] { e.getMessage() }, e);
		}
	}
}