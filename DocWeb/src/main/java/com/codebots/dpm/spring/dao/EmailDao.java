package com.codebots.dpm.spring.dao;

import com.codebots.dpm.reqres.ContactResponseBody;

public interface EmailDao {

	ContactResponseBody getEmailContacts();

}
