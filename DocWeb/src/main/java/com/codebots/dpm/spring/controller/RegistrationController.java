package com.codebots.dpm.spring.controller;


import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codebots.dpm.communication.EmailNotifier;
import com.codebots.dpm.communication.SendSMS;
import com.codebots.dpm.reqres.DoctorRequestBody;
import com.codebots.dpm.reqres.PatientRequestBody;
import com.codebots.dpm.spring.dao.DoctorDao;
import com.codebots.dpm.spring.dao.PatientDao;
import com.codebots.dpm.spring.dao.UserDao;
import com.codebots.dpm.spring.exception.DPMAPIException;
import com.codebots.dpm.spring.model.Doctor;
import com.codebots.dpm.spring.model.Patient;

@RestController
@RequestMapping("/rest/registration")
public class RegistrationController {

	@Autowired
	private PatientDao patientDao;
	
	@Autowired
	private DoctorDao doctorDao;
	
	@Autowired
	private UserDao userDao;
	
	public static String EMAIL_CONTENT = "Dear @FirstName@ @LastName@,	<BR><BR>"
			+ "Welcome! You've entered @EMAIL@ as the contact email address for your profile."
			+ " In order to verify your email address, all you have to do is simply provide OTP after login:<BR>"
			+ "<b>Your OTP is :</b> @OTP@<BR>"
			+ "Please <a href=\"http://35.185.51.218:8080/dist/\">click here</a> to login and verify verify your email address<BR><BR>"
			+ "Please note, this OTP will expire in 24 hours. After this, you will need to resubmit the request to add/change your email address.<BR>"
			+ "<b>Why did I get this email?</b><BR>"
			+ "You receive this email whenever you provide a new email address or change your email address in your account.<BR>"
			+ "If you did not initiate this request, please contact the customer care center at 123 456 789.<BR>" 
			+ "This is an automated email for providing important information. Therefore, please do not reply to this email.<BR>"
			+ "In case you want to get in touch with us, you can email us at customercare@sss.in or call us directly at 123 456 789.<BR><BR>"
			+ "Thanks,<BR>"
			+ "Team";

	public static String SMS_CONTENT ="Dear @FIRSTNAME@, Please verify your mobile with AtoZ portal." + 
									  "Your Mobile verification code is @OTP@. Valid for 30 minutes.";
	
	
	
	@PostMapping("/resend/{type}/{userId}")
	public ResponseEntity<?> resendVerificationDetails(@PathVariable("type") String type, @PathVariable("userId") UUID id) throws DPMAPIException
	{	
		
		try
		{
		userDao.resendVerificationDetails(type, id);
		return  ResponseEntity.ok().body("OK");
		}
		catch(Exception e)
		{
			throw new DPMAPIException("0051", new String[] {e.getMessage()}, e);
		}
	}
	
	@PostMapping("/patient")
	public ResponseEntity<?> savePatient(@RequestBody PatientRequestBody patient) throws DPMAPIException
	{    
		Patient patientObject = null;
		try
		{
			patientObject = patientDao.save(patient);
		}catch(Exception e)
		{
			throw new DPMAPIException("0001", new String[] {e.getMessage()}, e);
		}
		try{
			String emailbody = EMAIL_CONTENT.replace("@FirstName@", patientObject.getFirstName())
					.replace("@LastName@", patientObject.getLastName())
					.replace("@EMAIL@",patientObject.getEmail())
					.replace("@OTP@", patientObject.getUserId().getEmail_OTP());
			EmailNotifier.sendEmailNotification(patientObject.getEmail(), "Please verify your email", emailbody);
			String smsBody = SMS_CONTENT.replace("@FIRSTNAME@", patientObject.getFirstName())
										.replace("@OTP@",patientObject.getUserId().getSms_OTP());
			SendSMS.sendSms(smsBody,patientObject.getMobileNumber());
		
		}catch(Exception e)
		{
			//eat exception
		}
		return ResponseEntity.ok().body(patientObject);
	}
	
	@PostMapping("/doctor")
	public ResponseEntity<Doctor> saveDoctor(@RequestBody DoctorRequestBody doctor) throws DPMAPIException
	{
		Doctor doctorObject = null;
		try
		{
			doctorObject = doctorDao.save(doctor);
			
		}catch(Exception e)
		{
			throw new DPMAPIException("0002", new String[] {e.getMessage()},e);
		}
		
		try{
			String emailbody = EMAIL_CONTENT.replace("@FirstName@", doctorObject.getFirstName())
					.replace("@LastName@", doctorObject.getLastName())
					.replace("@EMAIL@",doctorObject.getEmail())
					.replace("@OTP@", doctorObject.getUserId().getEmail_OTP());
			EmailNotifier.sendEmailNotification(doctorObject.getEmail(), "Please verify your email", emailbody);
			String smsBody = SMS_CONTENT.replace("@FIRSTNAME@", doctorObject.getFirstName())
										.replace("@OTP@",doctorObject.getUserId().getSms_OTP());
			SendSMS.sendSms(smsBody,doctorObject.getMobileNumber());
		
		}catch(Exception e)
		{
			//eat exception;
		}
		return ResponseEntity.ok().body(doctorObject);
	}
	

}
