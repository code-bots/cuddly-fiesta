
package com.codebots.dpm.spring.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.security.auth.message.config.AuthConfigFactory.RegistrationContext;
import javax.servlet.Registration;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.stereotype.Repository;

import com.codebots.dpm.common.Utility;
import com.codebots.dpm.communication.EmailNotifier;
import com.codebots.dpm.communication.EmailUserGeneration;
import com.codebots.dpm.communication.SendSMS;
import com.codebots.dpm.reqres.DoctorRequestBody;
import com.codebots.dpm.reqres.DoctorResponceBody;
import com.codebots.dpm.reqres.Pagination;
import com.codebots.dpm.reqres.PasswordChangeRequestBody;
import com.codebots.dpm.spring.controller.RegistrationController;
import com.codebots.dpm.spring.exception.DPMAPIException;
import com.codebots.dpm.spring.model.Doctor;
import com.codebots.dpm.spring.model.DoctorHospital;
import com.codebots.dpm.spring.model.Feedback;
import com.codebots.dpm.spring.model.Patient;
import com.codebots.dpm.spring.model.Role;
import com.codebots.dpm.spring.model.Schedule;
import com.codebots.dpm.spring.model.User;

@Repository
@PropertySource("classpath:application.properties")
@PropertySource("classpath:mailbox.properties")
@Transactional
public class DoctorDaoImpl implements DoctorDao {

	@Autowired
	private SessionFactory sessionfactory;

	@Autowired
	private Environment env;

	@Override
	public Doctor save(DoctorRequestBody doctor) throws DPMAPIException {

		Session session = sessionfactory.getCurrentSession();
		String email = doctor.getRegistrationDetails().getEmail();
		String mob = doctor.getRegistrationDetails().getMobileNumber();
		
	/*	String hqlEmail = "select email from doctor where email = :email";
		Query queryEmail = session.createQuery(hqlEmail);
		queryEmail.setParameter("email", email);
		List<String> listOfEmail = queryEmail.list();
		*/
		String hqlMobile = "select mobileNumber from doctor where mobileNumber = :mobile";
		Query queryMobile = session.createQuery(hqlMobile);
		queryMobile.setParameter("mobile", mob);
		List<String> listOfMobile = queryMobile.list();
		
		String hqlMobile1 = "select mobileNumber from patient where mobileNumber = :mobile";
		Query queryMobile1 = session.createQuery(hqlMobile1);
		queryMobile1.setParameter("mobile", mob);
		List<String> listOfMobile1 = queryMobile1.list();
		
		String hqlUserName = "select username from " + User.class.getName() + " where username = :uname";
		Query queryUsrname = session.createQuery(hqlUserName);
		queryUsrname.setParameter("uname", doctor.getRegistrationDetails().getEmail());
		List<String> listUser = queryUsrname.list();
		
		
		if (listUser.size() > 0) {
			throw new DPMAPIException("0036", new String[] { email }, null);
		}
		else if(listOfMobile.size() > 0)
		{
			throw new DPMAPIException("0037", new String[] { mob }, null);
		}
		/*else if(listUser.size() > 0)
		{
			throw new DPMAPIException("0036", new String[] { email }, null);
		}*/
		else if(listOfMobile1.size() > 0)
		{
			throw new DPMAPIException("0037", new String[] { mob }, null);
		}
		else {
				User user = new User();
				
				user.setUsername(doctor.getRegistrationDetails().getEmail());
			
				ShaPasswordEncoder encoder = new ShaPasswordEncoder(
						env.getProperty("security.encoding-strength", Integer.class));
				String encodedPassword = encoder.encodePassword(doctor.getPassword(), null);
				user.setPassword(encodedPassword);
	
				List<Role> role = new ArrayList<>();
				Role tutorrole = sessionfactory.getCurrentSession().get(Role.class, 1);
				role.add(tutorrole);
				user.setRoles(role);
				user.setEnabled(false);
				user.setEmail_OTP(Utility.getRandomOTP());
				user.setSms_OTP(Utility.getRandomOTP());
				user.setIsPresent(false);
				sessionfactory.getCurrentSession().save(user);
				
	
				// mailbox registration
				EmailUserGeneration userGeneration = new EmailUserGeneration();
				String emailId = userGeneration.generateEmailUser(env.getProperty("domain"),env.getProperty("userpassword"));
				//String emailId = System.currentTimeMillis() + "@stm.com";
				Doctor doctorDao = doctor.getRegistrationDetails();
				doctorDao.setUserId(user);
				doctorDao.setDpmEmailId(emailId);
				doctorDao.setApprovalFlag(0);
				sessionfactory.getCurrentSession().save(doctorDao);
				return doctor.getRegistrationDetails();
			}
		}
	
	
	@Override
	public DoctorResponceBody get(String userName) {
		
		
		Doctor doc = (Doctor) sessionfactory.getCurrentSession()
				.createQuery(" from " + Doctor.class.getName() + " where email='" + userName + "'").list().get(0);
		DoctorResponceBody docRes = new DoctorResponceBody();
		
		Session session = sessionfactory.getCurrentSession();
		String hql = "select avg(fb.star) from " + Feedback.class.getName() 
						+ " fb," + Schedule.class.getName() 
						+ " sch," + DoctorHospital.class.getName() + " dochos," 
						+ Doctor.class.getName() + " doc where "
						+ "fb.scheduleId = sch.id and sch.doctorHospitalId = dochos.id "
						+ "and dochos.doctorId = doc.id	and doc.id = :docId"
						+ " group by doc.id ";
					
		Query query = session.createQuery(hql);
		query.setParameter("docId", doc.getId());
		Double star = (Double) query.uniqueResult();
		if(star == null)
		{
			docRes.setStar((long) 0);
		}
		else
		{
			docRes.setStar(star.longValue());
		}
		
		docRes.setDoctor(doc);
	
		
		
		return docRes;
	}

	@Override
	public Pagination list(Integer status,int pageNumber,int pageSize,String type,String feild)
	{
		
		if(status == null && type == null && feild == null)
		{
			Session session = sessionfactory.getCurrentSession();
			String hql = "from " + Doctor.class.getName();
			
			
			Query query = session.createQuery(hql);
					
			if(pageNumber == 1)
			{		
				query.setFirstResult(pageNumber-1);
				query.setMaxResults(pageSize);
			}
			else
			{
				int startRow = (pageNumber - 1) * pageSize;
				query.setFirstResult(startRow);
				query.setMaxResults(pageSize);
			}
			System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++"+query.getQueryString()+"++++++++++++++++++++++++++++++++++++++++++++++++++++");
			List<Object> list = query.list();
			List<Object> listOfDoctors = new ArrayList<>();
			for(Object obj : list)
			{
				DoctorResponceBody docResBody = new DoctorResponceBody();
				docResBody.setDoctor((Doctor) obj);
				
				
				Session session1 = sessionfactory.getCurrentSession();
				String hql1 = "select avg(fb.star) from " + Feedback.class.getName() 
								+ " fb," + Schedule.class.getName() 
								+ " sch," + DoctorHospital.class.getName() + " dochos," 
								+ Doctor.class.getName() + " doc where "
								+ "fb.scheduleId = sch.id and sch.doctorHospitalId = dochos.id "
								+ "and dochos.doctorId = doc.id	and doc.id = :docId"
								+ " group by doc.id ";
							
				Query query1 = session1.createQuery(hql1);
				query1.setParameter("docId", ((Doctor) obj).getId());
				Double star = (Double) query1.uniqueResult();
				if(star == null)
				{
					docResBody.setStar((long) 0);
				}
				else
				{
					docResBody.setStar(star.longValue());
				}
				listOfDoctors.add(docResBody);
			}
		
			Query countQuery = session.createQuery("select COUNT(*) from " + Doctor.class.getName() );
			long result = (Long) countQuery.list().get(0);
			Pagination page = new Pagination();
			page.setListOfObjects(listOfDoctors);
			page.setCurrentPage(pageNumber);
			page.setPageSize(pageSize);
			page.setTotalRecord(result);
			
			
			return page;
			
		}
		else 
		{	
				Session session = sessionfactory.getCurrentSession();
				String hql = "from " + Doctor.class.getName()  + " where approvalFlag = :status order by " + feild + " " + type;
				Query query = session.createQuery(hql);
				query.setParameter("status", status);
				if(pageNumber == 1)
				{		
					query.setFirstResult(pageNumber-1);
					query.setMaxResults(pageSize);
				}
				else
				{
					int startRow = (pageNumber - 1) * pageSize;
					query.setFirstResult(startRow);
					query.setMaxResults(pageSize);
				}
				System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++"+query.getQueryString()+"++++++++++++++++++++++++++++++++++++++++++++++++++++");
				List<Object> list = query.list();
				List<Object> listOfDoctors = new ArrayList<>();
				for(Object obj : list)
				{
					DoctorResponceBody docResBody = new DoctorResponceBody();
					docResBody.setDoctor((Doctor) obj);
					
					
					Session session1 = sessionfactory.getCurrentSession();
					String hql1 = "select avg(fb.star) from " + Feedback.class.getName() 
									+ " fb," + Schedule.class.getName() 
									+ " sch," + DoctorHospital.class.getName() + " dochos," 
									+ Doctor.class.getName() + " doc where "
									+ "fb.scheduleId = sch.id and sch.doctorHospitalId = dochos.id "
									+ "and dochos.doctorId = doc.id	and doc.id = :docId"
									+ " group by doc.id ";
								
					Query query1 = session1.createQuery(hql1);
					query1.setParameter("docId", ((Doctor) obj).getId());
					Double star = (Double) query1.uniqueResult();
					if(star == null)
					{
						docResBody.setStar((long) 0);
					}
					else
					{
						docResBody.setStar(star.longValue());
					}
					listOfDoctors.add(docResBody);
				}
			
				Query countQuery = session.createQuery("select COUNT(*) from " + Doctor.class.getName()   + " where approvalFlag = :status");
				countQuery.setParameter("status", status);
				long result = (Long) countQuery.list().get(0);
				Pagination page = new Pagination();
				page.setListOfObjects(listOfDoctors);
				page.setCurrentPage(pageNumber);
				page.setPageSize(pageSize);
				page.setTotalRecord(result);		
				return page;
		}
		
	}

	@Override
	public Doctor update(Doctor doctor, UUID doctorId) throws DPMAPIException {
		String emailOTP = Utility.getRandomOTP();
		String smsOTP = Utility.getRandomOTP();
		
		Session session = sessionfactory.getCurrentSession();
		Doctor docDO = session.byId(Doctor.class).load(doctorId);

		if(!docDO.getEmail().equals(doctor.getEmail()) || !docDO.getMobileNumber().equals(doctor.getMobileNumber()))
		{
			String email = doctor.getEmail();
			docDO.setMobileNumber(doctor.getMobileNumber());
			docDO.setEmail(doctor.getEmail());
			String hql = "UPDATE " + User.class.getName() + " us SET us.isEnabled = 0,us.email_OTP = " + emailOTP + ",us.sms_OTP = " +
					smsOTP + ",us.username = '" + email + "' where us.id = " + docDO.getUserId().getId();
			// 
			Query query = session.createQuery(hql);
			query.executeUpdate();

			try
			{
				String emailContent = RegistrationController.EMAIL_CONTENT;
				String emailbody = emailContent.replace("@FirstName@", doctor.getFirstName())
						.replace("@LastName@", doctor.getLastName())
						.replace("@EMAIL@",email)
						.replace("@OTP@", emailOTP);

				EmailNotifier.sendEmailNotification(email, "Please verify your email", emailbody);
				
				String smsContent = RegistrationController.SMS_CONTENT;
				String smsBody = smsContent.replace("@FIRSTNAME@", doctor.getFirstName())
						.replace("@OTP@",smsOTP);
				String mobile = doctor.getMobileNumber();
				SendSMS.sendSms(smsBody, mobile);
				
			}
			catch(Exception e)
			{
				
			}
		}
		
		docDO.setFirstName(doctor.getFirstName());
		docDO.setLastName(doctor.getLastName());
		
		docDO.setDob(doctor.getDob());
		docDO.setQualification(doctor.getQualification());
		docDO.setProfilePic(doctor.getProfilePic());
		docDO.setLicNo(doctor.getLicNo());
		docDO.setMedicalCouncil(doctor.getMedicalCouncil());
		docDO.setAwards(doctor.getAwards());
		docDO.setTitle(doctor.getTitle());
		docDO.setGender(doctor.getGender());
		docDO.setAddressLine(doctor.getAddressLine());
		docDO.setLocality(doctor.getLocality());
		docDO.setCity(doctor.getCity());
		docDO.setPincode(doctor.getPincode());
		docDO.setState(doctor.getState());
		docDO.setApprovalFlag(doctor.getApprovalFlag());
		session.saveOrUpdate(docDO);
		
		
		
		return docDO;
	}

	@Override
	public void resetPassword(PasswordChangeRequestBody passchange, UUID doctorId) throws DPMAPIException {
		Session session = sessionfactory.getCurrentSession();
		Doctor tut = session.get(Doctor.class, doctorId);
		String original_pass = tut.getUserId().getPassword();
		// origional password
		String old_pass_by_user = passchange.getOld_pass();

		ShaPasswordEncoder encoder = new ShaPasswordEncoder(
				env.getProperty("security.encoding-strength", Integer.class));
		// entered old_pass (encoded)
		String old_Password_by_user = encoder.encodePassword(old_pass_by_user, null);

		if (old_Password_by_user.equals(original_pass)) {
			ShaPasswordEncoder encoder2 = new ShaPasswordEncoder(
					env.getProperty("security.encoding-strength", Integer.class));
			String new_encodedPassword = encoder2.encodePassword(passchange.getNew_pass(), null);
			Query query = session.createQuery("update app_user set password = :newpass where id = :user_id");
			query.setParameter("newpass", new_encodedPassword);
			query.setParameter("user_id", tut.getUserId().getId());
			query.executeUpdate();
		} else {
			throw new DPMAPIException("0010", new String[] {}, null);
		}

	}

	@Override
	public List<Doctor> getByFilter(String que) {
		Session session = sessionfactory.getCurrentSession();
		String hql = "from " + Doctor.class.getName() + " where " + que;
		System.out.println("------------------------------------------------");
		System.out.println(hql);
		System.out.println("------------------------------------------------");
		Query query = session.createQuery(hql);
		List<Doctor> list = query.list();
		return list;
	}

	public void test(UUID doctorId,UUID hospitalid,Date date)	
	{
		Session session = sessionfactory.getCurrentSession();
		String hql = "from " + Schedule.class.getName() + " where doctorId.id = :doctor and doctorHospitalId.id = :docHosID";
		Query query = session.createQuery(hql);
		query.setParameter("doctor", doctorId);
		query.setParameter("docHosID", hospitalid);
		
		List<Schedule> list = query.list();
		
	}

	@Override
	public List<Doctor> list(int pageNumber,int pageSize) {
		
		Session session = sessionfactory.getCurrentSession();
		String hql = "select COUNT(*) from doctor";
		Query query = session.createQuery(hql);
		long result = (Long) query.list().get(0);
		Pagination page = new Pagination();
		page.setTotalRecord(result);
		page.setCurrentPage(pageNumber);
		page.setPageSize(pageSize);
		
	
		
		
		
		return null;
	}


	@Override
	public Pagination listOfPatient(int pageNumber, int pageSize, String type, String feild,UUID doctorId) {
		
		if(type == null || feild == null)
		{
			Session session = sessionfactory.getCurrentSession();
			String hql = "select DISTINCT sc.patientId from " + Schedule.class.getName() + " sc," + DoctorHospital.class.getName() + " dh," + Doctor.class.getName() + 
		     " d  WHERE sc.doctorHospitalId = dh.id AND dh.doctorId = d.id  AND d.id = :docId AND sc.status = 1" ;
			
			Query query = session.createQuery(hql);
			query.setParameter("docId", doctorId);
			if(pageNumber == 1)
			{		
				query.setFirstResult(pageNumber-1);
				query.setMaxResults(pageSize);
			}
			else
			{
				int startRow = (pageNumber - 1) * pageSize;
				query.setFirstResult(startRow);
				query.setMaxResults(pageSize);
			}
			
			List<Object> list = query.list();
			
			String hql1 = "select count(DISTINCT sc.patientId) from " + Schedule.class.getName() + " sc," + DoctorHospital.class.getName() + " dh," + Doctor.class.getName() + 
				     " d  WHERE sc.doctorHospitalId = dh.id AND dh.doctorId = d.id  AND d.id = :docId AND sc.status = 1" ;
					
			Query query1 = session.createQuery(hql1);
			query1.setParameter("docId", doctorId);
			Long result = (Long) query1.list().get(0);
			
			
			Pagination page = new Pagination();
			page.setListOfObjects(list);
			page.setPageSize(pageSize);
			page.setCurrentPage(pageNumber);
			page.setTotalRecord(result);
			
			
			return page;
		}
		else
		{
			Session session = sessionfactory.getCurrentSession();
			String hql = "select DISTINCT p from " + Schedule.class.getName() + " sc," + DoctorHospital.class.getName() + " dh," + Doctor.class.getName() + 
				     " d," + Patient.class.getName() + " p WHERE sc.doctorHospitalId = dh.id AND dh.doctorId = d.id  AND d.id = :docId AND p.id = sc.patientId AND sc.status = 1 order by p." + feild + " " + type;
			
			Query query = session.createQuery(hql);
			query.setParameter("docId", doctorId);
			System.out.println("******************************" + query.getQueryString() + "**********************");
			if(pageNumber == 1)
			{		
				query.setFirstResult(pageNumber-1);
				query.setMaxResults(pageSize);
			}
			else
			{
				int startRow = (pageNumber - 1) * pageSize;
				query.setFirstResult(startRow);
				query.setMaxResults(pageSize);
			}
			
			List<Object> list = query.list();
			
			String hql1 = "select count(DISTINCT sc.patientId) from " + Schedule.class.getName() + " sc," + DoctorHospital.class.getName() + " dh," + Doctor.class.getName() + 
				     " d  WHERE sc.doctorHospitalId = dh.id AND dh.doctorId = d.id  AND d.id = :docId AND sc.status = 1" ;
					
			Query query1 = session.createQuery(hql1);
			query1.setParameter("docId", doctorId);
			long result = (Long) query1.list().get(0);
			
			
			Pagination page = new Pagination();
			page.setListOfObjects(list);
			page.setPageSize(pageSize);
			page.setCurrentPage(pageNumber);
			page.setTotalRecord(result);
			
			
			return page;
		}
	}
	
	
	
}