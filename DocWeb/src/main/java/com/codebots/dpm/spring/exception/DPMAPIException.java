package com.codebots.dpm.spring.exception;

import java.text.MessageFormat;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class DPMAPIException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final String API_EXCEPTION_BASE = "ApiException";
	private static final String ERROR_PREFIX = "DPM - ";
	private String errorCode;
	private String[] args;

	public DPMAPIException(final String errorCode, final String[] args, final Throwable baseException) {
		super(errorCode, baseException);
		this.errorCode = errorCode;
		this.args = args;
	}

	public String getErrorCode() {
		return this.errorCode;
	}

	@Override
	public String getMessage() {
		return getLocalizedMessage();
	}

	@Override
	public String getLocalizedMessage() {
		try {
			ResourceBundle res = ResourceBundle.getBundle(API_EXCEPTION_BASE);
			String messageTemplate = res.getString(errorCode);

			if (messageTemplate != null) {
				MessageFormat msgFormatter = new MessageFormat(res.getString(errorCode));
				return ERROR_PREFIX + errorCode + ": " + msgFormatter.format(args);
			} else
				return (ERROR_PREFIX + errorCode + ": Missing Exception Message Resource File "
						+ super.getClass().getName());
		} catch (MissingResourceException me) {
			return (ERROR_PREFIX + errorCode + ": Missing Exception Message Resource File "
					+ super.getClass().getName());
		} catch (Throwable e) {
			return ("Error during exception message formating:" + e.getClass().getName()
					+ "\n Base error message is:\n" + this.getClass().getName());
		}
	}
}
