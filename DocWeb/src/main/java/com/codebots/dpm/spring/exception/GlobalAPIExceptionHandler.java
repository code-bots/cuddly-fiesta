package com.codebots.dpm.spring.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * This is global api exception handler. It handles responses of all exceptions.
 *
 * 
 */
@ControllerAdvice
public class GlobalAPIExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(DPMAPIException.class)
	public ResponseEntity<DPMAPIError> exceptionHandler(DPMAPIException e) {
		String stackTrace = org.apache.commons.lang.exception.ExceptionUtils.getFullStackTrace(e);
		DPMAPIError apiError = new DPMAPIError(e.getMessage(), "", e.getErrorCode(), stackTrace);
		return new ResponseEntity<DPMAPIError>(apiError, HttpStatus.INTERNAL_SERVER_ERROR);
	}
/*
	@ExceptionHandler(STMAPIException.class)
	@ResponseBody
	public STMAPIError handleException(final HttpServletRequest request, final STMAPIException e) {
		String stackTrace = org.apache.commons.lang.exception.ExceptionUtils.getFullStackTrace(e);
		return new STMAPIError(e.getMessage(), "", e.getErrorCode(), stackTrace);
	}
*/
}
