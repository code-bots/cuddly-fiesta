package com.codebots.dpm.spring.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codebots.dpm.reqres.VerifyRequest;
import com.codebots.dpm.spring.dao.DoctorDao;
import com.codebots.dpm.spring.dao.PatientDao;
import com.codebots.dpm.spring.dao.UserDao;

@RestController
@RequestMapping("/rest/api")
public class UserController {

	@Autowired
	private PatientDao patientDao;
	
	@Autowired
	private DoctorDao doctorDao;
	
	@Autowired
	private UserDao userDao;
	
	

	
	@GetMapping("/userInfo")
	public ResponseEntity<?> get() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String userName = authentication.getName();
		SimpleGrantedAuthority authority = (SimpleGrantedAuthority) new ArrayList(authentication.getAuthorities()).get(0);
		String type = authority.getAuthority();
		if(type.equals("DOCTOR"))
		{
			return ResponseEntity.ok().body(doctorDao.get(userName));
		}else{
			return ResponseEntity.ok().body(patientDao.get(userName));
		}
	}
	
	@PutMapping("user/availability/{type}")
	public ResponseEntity setAvailability(@PathVariable("type") boolean value)
	{
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String userName = authentication.getName();
		SimpleGrantedAuthority authority = (SimpleGrantedAuthority) new ArrayList(authentication.getAuthorities()).get(0);
		String type = authority.getAuthority();
		Integer id;
		if(type.equals("DOCTOR"))
		{
			id = doctorDao.get(userName).getDoctor().getUserId().getId();
		}else{
			id = patientDao.get(userName).getUserId().getId();
		}
		userDao.setUnsetPresence(id, value);
		return ResponseEntity.ok().build();
	}
	
	@PostMapping("/verifydetails/{userId}")
	public ResponseEntity<?> verify(@RequestBody VerifyRequest verifydetails,@PathVariable("userId") Integer uid) 
	{
		String status = userDao.verifydetailsMethod(verifydetails, uid);
	//	SendSMS.sendSms("Welcome! You’re successfully registered with AtoZ portal.");
		return ResponseEntity.ok().body(status);
	}
	
	@GetMapping("/resend/{userId}")
	public ResponseEntity<?> resend(@PathVariable("userId") Integer uid) 
	{
		//VerifyRequest vr = userDao.resendOTP(uid);
		return ResponseEntity.ok().body("OK");
	}
	

}