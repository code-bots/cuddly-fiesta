package com.codebots.dpm.spring.dao;

import java.util.List;
import java.util.UUID;

import com.codebots.dpm.spring.model.Comments;



public interface CommentDao {

	Comments saveComments(Comments comment,UUID blog_id);
	Comments getCommentById(UUID commentId);
	List<Comments>	listByBlogs(UUID blog_id);
	List<Comments> listByParant(UUID parentcomment_id);
	Comments updateComment(Comments comment,UUID commentId);
	void deleteComment(UUID comment_id);
	List<Comments> parentCmt(UUID blog_id);
	
}
