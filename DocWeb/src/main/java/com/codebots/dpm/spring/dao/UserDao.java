package com.codebots.dpm.spring.dao;

import java.util.UUID;

import com.codebots.dpm.reqres.VerifyRequest;
import com.codebots.dpm.spring.exception.DPMAPIException;
import com.codebots.dpm.spring.model.User;

public interface UserDao {

	void resendVerificationDetails(String type, UUID id) throws DPMAPIException;
	void setUnsetPresence(Integer id, boolean value);
	String verifydetailsMethod(VerifyRequest verifyDetails,Integer uid);
	VerifyRequest resendOTP(Integer uid);
	User getUserDetails(Integer uID);
}
