package com.codebots.dpm.spring.model;

import java.util.Date;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity(name = "comments")
public class Comments {

	
	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(name="UUID",strategy="org.hibernate.id.UUIDGenerator")
	private UUID id;
	
	@OneToOne(targetEntity=Blog.class,cascade=CascadeType.ALL)
	@JoinColumn(name="blogId",referencedColumnName="id")
	private Blog blogId;
	
	@Column(columnDefinition = "TEXT")
	private String description;
	
	private UUID auther_id;
	private Date comment_date;	
	private UUID parentcomment_id;
	public UUID getId() {
		return id;
	}
	public void setId(UUID id) {
		this.id = id;
	}
	public Blog getBlogId() {
		return blogId;
	}
	public void setBlogId(Blog blogId) {
		this.blogId = blogId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public UUID getAuther_id() {
		return auther_id;
	}
	public void setAuther_id(UUID auther_id) {
		this.auther_id = auther_id;
	}
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss", timezone="IST")
	public Date getComment_date() {
		return comment_date;
	}
	public void setComment_date(Date comment_date) {
		this.comment_date = comment_date;
	}
	public UUID getParentcomment_id() {
		return parentcomment_id;
	}
	public void setParentcomment_id(UUID parentcomment_id) {
		this.parentcomment_id = parentcomment_id;
	}
	
	
	
}
