package com.codebots.dpm.spring.exception;

/**
 * 
 *
 * 
 */
public class DPMAPIError {

	private String errorMessage;
	private String infoToUser;
	private String errorCode;
	private String stackTrace;

	public DPMAPIError(final String errorMessage, final String infoToUser, final String errorCode,
			final String stackTrace) {
		super();
		this.errorMessage = errorMessage;
		this.infoToUser = infoToUser;
		this.errorCode = errorCode;
		this.stackTrace = stackTrace;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(final String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getInfoToUser() {
		return infoToUser;
	}

	public void setInfoToUser(final String infoToUser) {
		this.infoToUser = infoToUser;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(final String errorCode) {
		this.errorCode = errorCode;
	}

	public String getStackTrace() {
		return stackTrace;
	}

	public void setStackTrace(final String stackTrace) {
		this.stackTrace = stackTrace;
	}

}
