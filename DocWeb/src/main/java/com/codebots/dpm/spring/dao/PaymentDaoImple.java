package com.codebots.dpm.spring.dao;

import java.util.List;
import java.util.UUID;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Repository;

import com.codebots.dpm.spring.model.PaymentInfo;

@Repository
@PropertySource("classpath:application.properties")
@PropertySource("classpath:mailbox.properties")
@Transactional
public class PaymentDaoImple implements PaymentDao{

	@Autowired
	private SessionFactory sessionfactory;
	
	
	@Override
	public PaymentInfo save(PaymentInfo payment) {
		sessionfactory.getCurrentSession().save(payment);	
		return payment;
	}

	@Override
	public List<PaymentInfo> list() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PaymentInfo updatePayment(PaymentInfo payInfo, UUID id) {
		Session session = sessionfactory.getCurrentSession();
		Query query = session.createQuery("from " + PaymentInfo.class.getName() + " where id = :id").setParameter("id",id);
		
		PaymentInfo pay = (PaymentInfo) query.uniqueResult();
		pay.setTxn_id(payInfo.getTxn_id());
		pay.setRespcode(payInfo.getRespcode());
		pay.setGateway_status(payInfo.getGateway_status());
		pay.setTxn_time(payInfo.getTxn_time());
		pay.setApp_status(payInfo.getApp_status());
		pay.setRespmsg(payInfo.getRespmsg());
		session.saveOrUpdate(pay);
		
		return pay;
	}

	
}
