package com.codebots.dpm.spring.dao;

import java.util.List;
import java.util.UUID;

import com.codebots.dpm.spring.model.Blog;

public interface BlogDao {

	
	Blog save(Blog blog,UUID doctorId);
	List<Blog> getBlogByDoctor(UUID doctorId);
	List<Blog> getAllBlogs();
	Blog getBogById(UUID id);
	Blog updateBlog(Blog blog,UUID blogId);
	void deleteBlog(UUID blog_id);
	
}
