package com.codebots.dpm.spring.model;

import java.sql.Time;
import java.util.Date;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity(name = "schedule")
public class Schedule {

	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(name="UUID",strategy="org.hibernate.id.UUIDGenerator")
	private UUID id;
	private Date date;
	@OneToOne(targetEntity=Patient.class,cascade=CascadeType.ALL)
	@JoinColumn(name="patientId",referencedColumnName="id")
	private Patient patientId;
	
	@OneToOne(targetEntity=DoctorHospital.class,cascade=CascadeType.ALL)
	@JoinColumn(name="doctorHospitalId",referencedColumnName="id")
	private DoctorHospital doctorHospitalId;
	
	private Time startTime;
	private Time endTime;
	private String remarks;
	private Integer status;
	private String schStatus;
	
	
	public String getSchStatus() {
		return schStatus;
	}
	public void setSchStatus(String schStatus) {
		this.schStatus = schStatus;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public UUID getId() {
		return id;
	}
	public void setId(UUID id) {
		this.id = id;
	}
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd", timezone="UTC")
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	
	public Patient getPatientId() {
		return patientId;
	}
	public void setPatientId(Patient patientId) {
		this.patientId = patientId;
	}
	public DoctorHospital getDoctorHospitalId() {
		return doctorHospitalId;
	}
	public void setDoctorHospitalId(DoctorHospital doctorHospitalId) {
		this.doctorHospitalId = doctorHospitalId;
	}
	public Time getStartTime() {
		return startTime;
	}
	public void setStartTime(Time startTime) {
		this.startTime = startTime;
	}
	public Time getEndTime() {
		return endTime;
	}
	public void setEndTime(Time endTime) {
		this.endTime = endTime;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
	
	
	
}
