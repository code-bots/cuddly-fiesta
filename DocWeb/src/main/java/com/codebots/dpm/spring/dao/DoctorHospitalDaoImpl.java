package com.codebots.dpm.spring.dao;

import java.util.List;
import java.util.UUID;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Repository;

import com.codebots.dpm.spring.model.Doctor;
import com.codebots.dpm.spring.model.DoctorHospital;
@Repository
@PropertySource("classpath:application.properties")
@PropertySource("classpath:mailbox.properties")
@Transactional
public class DoctorHospitalDaoImpl implements DoctorHospitalDao {

	@Autowired
	private SessionFactory sessionfactory;
	
	@Override
	public DoctorHospital save(DoctorHospital doctorHospital, UUID DoctorId) {
		Session session = sessionfactory.getCurrentSession();
		Doctor doctor = session.byId(Doctor.class).load(DoctorId);
		doctorHospital.setDoctorId(doctor);
		sessionfactory.getCurrentSession().save(doctorHospital);
		return doctorHospital;
	}

	@Override
	public DoctorHospital update(DoctorHospital doctorHospital, UUID doctorHospitalId) {
		Session session = sessionfactory.getCurrentSession();
		DoctorHospital docHospital = session.byId(DoctorHospital.class).load(doctorHospitalId);
		
		
		docHospital.setWeekdays(doctorHospital.getWeekdays());
		docHospital.setName(doctorHospital.getName());
		docHospital.setLongitude(doctorHospital.getLongitude());
		docHospital.setLatitude(doctorHospital.getLatitude());
		docHospital.setHospStartTime(doctorHospital.getHospStartTime());
		docHospital.setHospEndTime(doctorHospital.getHospEndTime());
		docHospital.setFees(doctorHospital.getFees());
		docHospital.setTimeslot(doctorHospital.getTimeslot());
		docHospital.setPaymentMode(doctorHospital.getPaymentMode());
		session.saveOrUpdate(docHospital);
		
		return docHospital;
	}

	@Override
	public List<DoctorHospital> getByDoctor(UUID doctorId) {
		Session session = sessionfactory.getCurrentSession();
		String hql = "from " + DoctorHospital.class.getName() + " where doctorId.id = :docId and doctorId.approvalFlag = 1";
		Query query = session.createQuery(hql);
		query.setParameter("docId",doctorId);
		List<DoctorHospital> list = query.list();
		return list;
	}

	@Override
	public List<DoctorHospital> getAll() {
		Session session = sessionfactory.getCurrentSession();
		/*CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<DoctorHospital> cq = cb.createQuery(DoctorHospital.class);
		Root<DoctorHospital> root = cq.from(DoctorHospital.class);
		cq.select(root);
		Query<DoctorHospital> query = session.createQuery(cq);*/
		String hql = "from " + DoctorHospital.class.getName() + " where doctorId.approvalFlag = 1";
		Query query = session.createQuery(hql);
		List<DoctorHospital> list = query.list();
		
		return list;
	}

	@Override
	public List<DoctorHospital> getByFilter(String filter) {
		
		Session session = sessionfactory.getCurrentSession();
		String hql = "from " + DoctorHospital.class.getName() + " where " + filter;
		System.out.println("------------------------------------------------");
		System.out.println(hql);
		System.out.println("------------------------------------------------");
		Query query = session.createQuery(hql);
		List<DoctorHospital> list = query.list();
		
		return list;
	}

	@Override
	public DoctorHospital getById(UUID docHosId) {
		Session session = sessionfactory.getCurrentSession();
		DoctorHospital docHos = session.byId(DoctorHospital.class).load(docHosId);
		return docHos;
	}

	@Override
	public List<DoctorHospital> getByDayDoctor(UUID doctorId, String weekdays) {
		Session session = sessionfactory.getCurrentSession();
		String hql = "from " + DoctorHospital.class.getName() + "  where weekdays like :day and  doctorId.id = :id";
		//  and  doctorId.id = :id
		//String d = "'%" + weekdays + "%'";
		Query query = session.createQuery(hql);
		query.setParameter("day","%" + weekdays + "%");
		query.setParameter("id",doctorId);
		
		List<DoctorHospital> list = query.list();
		return list;
	}

	@Override
	public List<DoctorHospital> getByDay(String weekdays) {
		Session session = sessionfactory.getCurrentSession();
		String hql = "from " + DoctorHospital.class.getName() + "  where weekdays like :day";
		Query query = session.createQuery(hql);
		query.setParameter("day","%" + weekdays + "%");
		
		List<DoctorHospital> list = query.list();
		return list;
	}

	
	
	
	
	
}
