package com.codebots.dpm.spring.dao;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import com.codebots.dpm.reqres.BookedSlotsRes;
import com.codebots.dpm.reqres.ScheduleCountResponceBody;
import com.codebots.dpm.spring.exception.DPMAPIException;
import com.codebots.dpm.spring.model.Schedule;



public interface ScheduleDao {

	
	Schedule saveSch(Schedule sche,UUID docHosId,UUID patientId);
	List<Schedule> list() throws ParseException;
	Schedule updateSchedule(Schedule sche,UUID scheduleId) throws ParseException, DPMAPIException;
	Schedule getById(UUID scheduleId);
	List<Schedule> getByDoctorDate(UUID doctorId,Date date);
	List<Schedule> getByDoctor(UUID doctorId);
	List<Schedule> getByPatient(UUID patientId);
	
	List<BookedSlotsRes> listBookSlots(UUID doctorHosId,Date d);
	
	List<Schedule> getByDoctorDateTest(UUID doctorId,Date fromdate,Date toDate);
	void updateScheduleStatus(String status,UUID schId);
	void updateStatus(int status,UUID schId);
	void demoTest();
	void sendReminder();
	
	List<ScheduleCountResponceBody> countSchedule(Date date,UUID doctorHospitalId);
	List<ScheduleCountResponceBody> countScheduleDoctorWise(Date date,UUID doctorId);
	
	
}
