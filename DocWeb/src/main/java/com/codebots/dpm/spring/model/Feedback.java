package com.codebots.dpm.spring.model;

import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;

@Entity(name = "feedback")
public class Feedback {

	
	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(name="UUID",strategy="org.hibernate.id.UUIDGenerator")
	private UUID id;
	@OneToOne(targetEntity=Schedule.class,cascade=CascadeType.ALL)
	@JoinColumn(name="scheduleId",referencedColumnName="id")
	private Schedule scheduleId;
	private String content;
	private Long star;
	public UUID getId() {
		return id;
	}
	public void setId(UUID id) {
		this.id = id;
	}
	public Schedule getScheduleId() {
		return scheduleId;
	}
	public void setScheduleId(Schedule scheduleId) {
		this.scheduleId = scheduleId;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Long getStar() {
		return star;
	}
	public void setStar(Long star) {
		this.star = star;
	}
	 
	
	
}
