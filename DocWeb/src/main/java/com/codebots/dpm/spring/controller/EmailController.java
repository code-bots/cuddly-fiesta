package com.codebots.dpm.spring.controller;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codebots.dpm.communication.EmailMessage;
import com.codebots.dpm.communication.EmailStoreReader;
import com.codebots.dpm.reqres.ContactResponseBody;
import com.codebots.dpm.reqres.EmailFolderDetails;
import com.codebots.dpm.reqres.EmailItem;
import com.codebots.dpm.reqres.SendEmailRequestBody;
import com.codebots.dpm.spring.dao.EmailDao;
import com.codebots.dpm.spring.exception.DPMAPIException;

@RestController
@Transactional
@PropertySource("classpath:mailbox.properties")
@RequestMapping("/rest/api/email")
public class EmailController {

	@Autowired
	private EmailDao emailDao;

	@Autowired
	Environment env;

	@PostMapping("/send")
	public ResponseEntity<?> sendEmail(@RequestBody SendEmailRequestBody requestBody) {
		EmailMessage message = new EmailMessage(requestBody.getFromEmail(), requestBody.getToEmails(),
				requestBody.getCcEmails(), requestBody.getSubject(), requestBody.getBody(),
				env.getProperty("mail.smtp.host"), env.getProperty("mail.smtp.port"));
		message.send();
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@GetMapping("/emailItems/{emailId:.+}/folder/{folderName}")
	public ResponseEntity<List<EmailItem>> getEmailItems(@PathVariable("emailId") String emailId, @PathVariable("folderName") String folderName, @RequestParam("startIndex") int startIndex, @RequestParam("endIndex") int endIndex) throws DPMAPIException {
		EmailStoreReader reader = new EmailStoreReader(env.getProperty("mail.smtp.host"), env.getProperty("mail.imap.port", Integer.class), emailId, env.getProperty("userpassword"));
		return new ResponseEntity<List<EmailItem>>(reader.getFolderItems(folderName, startIndex, endIndex), HttpStatus.OK);
	}

	@GetMapping("/contacts")
	public ResponseEntity<ContactResponseBody> getAllContacts() {
		ContactResponseBody responseBody = emailDao.getEmailContacts();
		return new ResponseEntity<ContactResponseBody>(responseBody, HttpStatus.OK);
	}

	@GetMapping("/folder-details/{emailId:.+}/list")
	public ResponseEntity<List<EmailFolderDetails>> getFolderDetails(@PathVariable("emailId") String emailId) throws DPMAPIException {
		EmailStoreReader reader = new EmailStoreReader(env.getProperty("mail.smtp.host"), env.getProperty("mail.imap.port", Integer.class), emailId, env.getProperty("userpassword"));
		return new ResponseEntity<List<EmailFolderDetails>>(reader.getFolderDetails(), HttpStatus.OK);
	}

}
