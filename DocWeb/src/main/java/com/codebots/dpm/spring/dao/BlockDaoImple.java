package com.codebots.dpm.spring.dao;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Repository;

import com.codebots.dpm.spring.model.Block;
import com.codebots.dpm.spring.model.Doctor;

@Repository
@PropertySource("classpath:application.properties")
@PropertySource("classpath:mailbox.properties")
@Transactional
public class BlockDaoImple implements BlockDao{

	
	@Autowired
	private SessionFactory sessionfactory;
	
	@Override
	public Block saveBlock(Block block,UUID doctorId) {
		
		Session session = sessionfactory.getCurrentSession();
		Doctor doctor = session.get(Doctor.class, doctorId);
		block.setDoctorId(doctor);
		sessionfactory.getCurrentSession().save(block);
			
		return block;
	}

	@Override
	public List<Block> getBlock(UUID doctorId, Date date) {
		
		Session session = sessionfactory.getCurrentSession();
		if(date == null)
		{
			String hql = "from " + Block.class.getName() + " where doctorId.id = :docId";			
			Query query = session.createQuery(hql);
			query.setParameter("docId", doctorId);
			List<Block> list = query.list();
			return list;	
		}
		else
		{			
			String hql = "from " + Block.class.getName() + " where date = :date and doctorId.id = :docId";			
			Query query = session.createQuery(hql);
			query.setParameter("date", date);
			query.setParameter("docId", doctorId);
			List<Block> list = query.list();
			return list;	
		}
		
		
	}

	@Override
	public void deleteBlock(UUID blockId) {
		Session session = sessionfactory.getCurrentSession();
		String hql = "delete from " + Block.class.getName() + " where id = :blckId";
		Query query = session.createQuery(hql);
		query.setParameter("blckId", blockId);
		query.executeUpdate();
		
	}

}
