package com.codebots.dpm.spring.config;


import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import com.mysql.cj.jdbc.MysqlDataSource;


@Configuration
@PropertySource("classpath:db.properties")
@EntityScan("com.codebots.stm.spring.model")
@EnableJpaRepositories("com.codebots.dpm.spring.crudrepo")
public class DataSourceConfig {


	@Autowired
	private Environment env;
	
	@Bean
	public DataSource dataSource() {
		final MysqlDataSource dataSource = new MysqlDataSource();
		dataSource.setUrl(env.getProperty("mysql.url"));
	    dataSource.setUser(env.getProperty("mysql.user"));
	    dataSource.setPassword(env.getProperty("mysql.password"));
	    return dataSource;
	}
	
	
	

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource){
        LocalContainerEntityManagerFactoryBean entityManagerFactory = new LocalContainerEntityManagerFactoryBean();
        entityManagerFactory.setDataSource(dataSource);
        entityManagerFactory.setPackagesToScan(new String[]{"com.codebots.dpm.spring.model"});
        JpaVendorAdapter jpaVendorAdapter = new HibernateJpaVendorAdapter();
        entityManagerFactory.setJpaVendorAdapter(jpaVendorAdapter);
        return entityManagerFactory;
    } 
/*
    @Bean
    public PlatformTransactionManager transactionManager(EntityManagerFactory entityManagerFactory){
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory);
        return transactionManager;
    }
  */
    /*
    @Bean
    public EntityManager entityManager() {
        return entityManagerFactory(dataSource()).getObject().createEntityManager();
    }
    */
}