package com.codebots.dpm.spring.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.hibernate.validator.internal.util.CollectionHelper.Partitioner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.stereotype.Repository;

import com.codebots.dpm.common.Utility;
import com.codebots.dpm.communication.EmailNotifier;
import com.codebots.dpm.communication.EmailUserGeneration;
import com.codebots.dpm.communication.SendSMS;
import com.codebots.dpm.reqres.Pagination;
import com.codebots.dpm.reqres.PasswordChangeRequestBody;
import com.codebots.dpm.reqres.PatientRequestBody;
import com.codebots.dpm.spring.controller.RegistrationController;
import com.codebots.dpm.spring.exception.DPMAPIException;
import com.codebots.dpm.spring.model.Role;
import com.codebots.dpm.spring.model.Doctor;
import com.codebots.dpm.spring.model.Patient;
import com.codebots.dpm.spring.model.User;

@Repository
@PropertySource("classpath:application.properties")
@PropertySource("classpath:mailbox.properties")
@Transactional
public class PatientDaoImpl implements PatientDao {

	@Autowired
	private SessionFactory sessionfactory;
	
	@Autowired
	private Environment env;
	
	@Override
	public Patient save(PatientRequestBody patient) throws DPMAPIException {
		
		Session session = sessionfactory.getCurrentSession();
		String email = patient.getRegistrationDetails().getEmail();
		String mob = patient.getRegistrationDetails().getMobileNumber();
		
		
	/*	String hql = "select email from patient where email = :email";
		Query query = session.createQuery(hql);
		query.setParameter("email",email);
		List<String> listOfEmail = query.list();
		*/
		
		String hqlMobile = "select mobileNumber from patient where mobileNumber = :mobile";
		Query queryMobile = session.createQuery(hqlMobile);
		queryMobile.setParameter("mobile", mob);
		List<String> listOfMobile = queryMobile.list();
		
		String hqlForDocMob = "select mobileNumber from  " + Doctor.class.getName() + " where mobileNumber = :mobile";
		Query queryForDocMob = session.createQuery(hqlForDocMob);
		queryForDocMob.setParameter("mobile", mob);
		List<String> listOfDocMob = queryForDocMob.list();
		
		
		String hqlUserName = "select username from " + User.class.getName() + " where username = :uname";
		Query queryUsrname = session.createQuery(hqlUserName);
		queryUsrname.setParameter("uname", patient.getRegistrationDetails().getEmail());
		List<String> listUser = queryUsrname.list();
		
		
		if (listUser.size() > 0) {
			throw new DPMAPIException("0036", new String[] { email }, null);
		}
		else if(listOfMobile.size() > 0)
		{
			throw new DPMAPIException("0037", new String[] { mob }, null);
		}
	/*	else if(listUser.size() > 0)
		{
			throw new DPMAPIException("0036", new String[] { email }, null);
		}*/
		else if(listOfDocMob.size() > 0)
		{
			throw new DPMAPIException("0037", new String[] { mob }, null);
		}
		else
		{
			EmailUserGeneration userGeneration = new EmailUserGeneration();
			String emailId = userGeneration.generateEmailUser(env.getProperty("domain"), env.getProperty("userpassword"));
			//String emailId = System.currentTimeMillis() + "@stm.com";
			// user object
			User user = new User();
			user.setUsername(patient.getRegistrationDetails().getEmail());
			ShaPasswordEncoder encoder = new ShaPasswordEncoder(env.getProperty("security.encoding-strength", Integer.class));
			String encodedPassword = encoder.encodePassword(patient.getPassword(), null);
			user.setPassword(encodedPassword);
			user.setEnabled(false);
			user.setEmail_OTP(Utility.getRandomOTP());
			user.setSms_OTP(Utility.getRandomOTP());
			//assigning student 
			List<Role> role = new ArrayList<>();
			Role studentRole = sessionfactory.getCurrentSession().get(Role.class, 2);
			role.add(studentRole);
			user.setRoles(role);
			user.setIsPresent(false);
			//save operations
			sessionfactory.getCurrentSession().save(user);
			Patient studentDao = patient.getRegistrationDetails();
			studentDao.setUserId(user);
			studentDao.setDpmEmailId(emailId);
			sessionfactory.getCurrentSession().save(studentDao);
			
			//otp and email send
			
			return patient.getRegistrationDetails();
		}

	}

	@Override
	public void resetPassword(PasswordChangeRequestBody passchange,UUID patientId) throws DPMAPIException {
		
		Session session = sessionfactory.getCurrentSession();
		Patient student = session.get(Patient.class, patientId);
		String original_pass = student.getUserId().getPassword();
		//origional password
		String old_pass_by_user = passchange.getOld_pass();
		
		ShaPasswordEncoder encoder = new ShaPasswordEncoder(env.getProperty("security.encoding-strength", Integer.class));
		//entered old_pass (encoded)
		String old_Password_by_user = encoder.encodePassword(old_pass_by_user, null);
		
		
		if(old_Password_by_user.equals(original_pass))
		{
			ShaPasswordEncoder encoder2 = new ShaPasswordEncoder(env.getProperty("security.encoding-strength", Integer.class));
			String new_encodedPassword = encoder2.encodePassword(passchange.getNew_pass(), null);
			Query query = session.createQuery("update app_user set password = :newpass where id = :user_id");
			query.setParameter("newpass",  new_encodedPassword);
			query.setParameter("user_id",  student.getUserId().getId());
			query.executeUpdate();
		}
		else
		{
			throw new DPMAPIException("0010", new String[] {}, null);
		}
	}

	
	@Override
	public Patient get(UUID patientId) {
		return sessionfactory.getCurrentSession().get(Patient.class, patientId);
	}

	@Override
	public Patient get(String userName)
	{
		return (Patient) sessionfactory.getCurrentSession().createQuery(" from " + Patient.class.getName() + " where email='" + userName + "'").list().get(0);
	}
	
	@Override
	public Pagination list(int pageNumber,int pageSize,String type,String feild) {
		
		if(type == null && feild == null)
		{
			Session session = sessionfactory.getCurrentSession();
			String hql = "from " + Patient.class.getName();
			Query query = session.createQuery(hql);
					
			if(pageNumber == 1)
			{		
				query.setFirstResult(pageNumber-1);
				query.setMaxResults(pageSize);
			}
			else
			{
				int startRow = (pageNumber - 1) * pageSize;
				query.setFirstResult(startRow);
				query.setMaxResults(pageSize);
			}
			System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++"+query.getQueryString()+"++++++++++++++++++++++++++++++++++++++++++++++++++++");
			List<Object> list = query.list();
			
			Query countQuery = session.createQuery("select COUNT(*) from " + Patient.class.getName());
			long result = (Long) countQuery.list().get(0);
			Pagination page = new Pagination();
			page.setListOfObjects(list);
			page.setCurrentPage(pageNumber);
			page.setPageSize(pageSize);
			page.setTotalRecord(result);
			
			
			return page;
		}
		else
		{
			Session session = sessionfactory.getCurrentSession();
			String hql = "from " + Patient.class.getName() + " order by  " + feild + " " + type;
			Query query = session.createQuery(hql);
					
			if(pageNumber == 1)
			{		
				query.setFirstResult(pageNumber-1);
				query.setMaxResults(pageSize);
			}
			else
			{
				int startRow = (pageNumber - 1) * pageSize;
				query.setFirstResult(startRow);
				query.setMaxResults(pageSize);
			}
			System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++"+query.getQueryString()+"++++++++++++++++++++++++++++++++++++++++++++++++++++");
			List<Object> list = query.list();
			
			Query countQuery = session.createQuery("select COUNT(*) from " + Patient.class.getName());
			long result = (Long) countQuery.list().get(0);
			Pagination page = new Pagination();
			page.setListOfObjects(list);
			page.setCurrentPage(pageNumber);
			page.setPageSize(pageSize);
			page.setTotalRecord(result);
			
			
			return page;
		}

	}

	
	@Override
	public Patient update(UUID patientId,Patient patient) throws DPMAPIException
	{
		
		String emailOTP = Utility.getRandomOTP();
		String smsOTP = Utility.getRandomOTP();
		
		Session session = sessionfactory.getCurrentSession();
		Patient patientDO = session.byId(Patient.class).load(patientId);
		
		if(!patientDO.getMobileNumber().equals(patient.getMobileNumber()) ||  !patientDO.getEmail().equals(patient.getEmail()))
		{
			String email = patient.getEmail();
			String hql = "UPDATE " + User.class.getName() + " us SET us.isEnabled = 0,us.email_OTP = " + emailOTP + ",us.sms_OTP = " +
					smsOTP + ",us.username = '" + email + "' where us.id = " + patientDO.getUserId().getId();
			
			Query query = session.createQuery(hql);
			query.executeUpdate();
			try
			{
				String emailContent = RegistrationController.EMAIL_CONTENT;
				String emailbody = emailContent.replace("@FirstName@", patient.getFirstName())
						.replace("@LastName@", patient.getLastName())
						.replace("@EMAIL@",email)
						.replace("@OTP@", emailOTP);

				EmailNotifier.sendEmailNotification(email, "Please verify your email", emailbody);
				
				String smsContent = RegistrationController.SMS_CONTENT;
				String smsBody = smsContent.replace("@FIRSTNAME@", patient.getFirstName())
						.replace("@OTP@",smsOTP);
				String mobile = patient.getMobileNumber();
				SendSMS.sendSms(smsBody, mobile);
				
				
			}
			catch(Exception e)
			{
				
			}
		}
		
		patientDO.setEmail(patient.getEmail());
		patientDO.setFirstName(patient.getFirstName());
		patientDO.setLastName(patient.getLastName());
		patientDO.setDob(patient.getDob());
		patientDO.setAddress(patient.getAddress());
		patientDO.setMobileNumber(patient.getMobileNumber());
		patientDO.setEmergencyContact(patient.getEmergencyContact());
		patientDO.setRemindPref(patient.getRemindPref());
		patientDO.setProfilePic(patient.getProfilePic());
		patientDO.setHeight(patient.getHeight());
		patientDO.setWeight(patient.getWeight());
		patientDO.setExistingDesease(patient.getExistingDesease());
		patientDO.setGender(patient.getGender());
		patientDO.setLocality(patient.getLocality());
		patientDO.setCity(patient.getCity());
		patientDO.setPincode(patient.getPincode());
		patientDO.setState(patient.getState());
		patientDO.setFamilyHistory(patient.getFamilyHistory());
		patientDO.setAllergic(patient.getAllergic());
		patientDO.setRemindPref(patient.getRemindPref());
		session.saveOrUpdate(patientDO);
		return patientDO;
		
	}
	
	@Override
	public void delete(UUID student_id) {
		Session session = sessionfactory.getCurrentSession();
		Patient student = session.byId(Patient.class).load(student_id);
		session.delete(student);
		
	}

	@Override
	public String getUsername(UUID patientId) {
		Session session = sessionfactory.getCurrentSession();
		String hql = "select email from student where id = :pid";
		Query query = session.createQuery(hql);
		query.setParameter("pid", patientId);
		String username = (String) query.uniqueResult();
		return username;
	}
}
