package com.codebots.dpm.spring.dao;

import java.util.List;
import java.util.UUID;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Repository;

import com.codebots.dpm.spring.model.Blog;
import com.codebots.dpm.spring.model.Comments;

@Repository
@PropertySource("classpath:application.properties")
@PropertySource("classpath:mailbox.properties")
@Transactional
public class CommentDaoImple  implements CommentDao{

	@Autowired
	private SessionFactory sessionfactory;
	
	@Override
	public Comments saveComments(Comments comment, UUID blog_id) {
		
		Session session = sessionfactory.getCurrentSession();
		Blog blog = session.get(Blog.class,blog_id);
		comment.setBlogId(blog);
		sessionfactory.getCurrentSession().save(comment);
		return comment;
	}

	@Override
	public List<Comments> listByBlogs(UUID blog_id) {
		Session session = sessionfactory.getCurrentSession();
		String hql = "from "+  Comments.class.getName() + " where blogId.id = :blog order by comment_date";
		Query query = session.createQuery(hql);
		query.setParameter("blog", blog_id);
		List<Comments> list = query.list();
		return list;
	}

	
	@Override
	public List<Comments> parentCmt(UUID blog_id) {
		Session session = sessionfactory.getCurrentSession();
		String hql = "from "+  Comments.class.getName() + " where blogId.id = :blog and  parentcomment_id  IS NULL order by comment_date";
		Query query = session.createQuery(hql);
		query.setParameter("blog", blog_id);
		List<Comments> list = query.list();
		return list;
	}
	
	@Override
	public List<Comments> listByParant(UUID parentcomment_id) {
		Session session = sessionfactory.getCurrentSession();
		String hql = "from "+  Comments.class.getName() + " where parentcomment_id = :parentId order by comment_date";
		Query query = session.createQuery(hql);
		query.setParameter("parentId", parentcomment_id);
		List<Comments> list = query.list();
		return list;
	}

	@Override
	public Comments updateComment(Comments comment,UUID commentId) {
		Session session = sessionfactory.getCurrentSession();
		Comments com = session.get(Comments.class,commentId);
		
		com.setComment_date(comment.getComment_date());
		com.setDescription(comment.getDescription());
		
		session.saveOrUpdate(com);
		return com;
	}

	@Override
	public void deleteComment(UUID comment_id) {
		
		Session session = sessionfactory.getCurrentSession();
		String hql = "delete from " + Comments.class.getName() + " where id = :commentId";
		Query query = session.createQuery(hql).setParameter("commentId", comment_id);
		query.executeUpdate();
	}

	@Override
	public Comments getCommentById(UUID commentId) {
		Session session = sessionfactory.getCurrentSession();
		Comments com = session.byId(Comments.class).load(commentId);
		return com;
	}

}
