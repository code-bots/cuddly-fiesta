
package com.codebots.dpm.spring.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codebots.dpm.communication.EmailNotifier;
import com.codebots.dpm.communication.SendSMS;
import com.codebots.dpm.reqres.BookedSlotsRes;
import com.codebots.dpm.reqres.ResponseMessage;
import com.codebots.dpm.reqres.ScheduleCountResponceBody;
import com.codebots.dpm.spring.dao.PrescriptionDao;
import com.codebots.dpm.spring.dao.ScheduleDao;
import com.codebots.dpm.spring.exception.DPMAPIException;
import com.codebots.dpm.spring.model.Schedule;


@RestController
@RequestMapping("/rest/api")
public class ScheduleController {

	
	@Autowired
	private ScheduleDao scheduleDao;
	
	@Autowired
	private PrescriptionDao preDao;
	
	
	public static String EMAIL_CONTENT = "Dear @FirstName@ @LastName@,	<BR><BR>"
			+ "Your appointment at @Time@ on @DAY@,@MONTH@ @Date@, @YEAR@ is confirmed with Dr.@ABCD@." + "Please be on time. <BR><BR>"
			+ "Please contact 123 456789  for any assitance on appointment."
			+ "This is an automated email for providing important information. Therefore, please do not reply to this email.<BR>"
			+ "In case you want to get in touch with us, you can email us at customercare@sss.in or call us directly at 123 456 789.<BR><BR>"
			+ "Thanks,<BR>"
			+ "Team";
	
	
	public static String SMS_CONTENT = "Dear @FirstName@ ,Your appointment at @Time@ on @DAY@,@MONTH@ @Date@, @YEAR@ is confirmed with Dr.@ABCD@.Please be on time.";
	
	/*@PostMapping("/sms")
	public ResponseEntity<?> sendSSS() 
	{
		SendSMS obj = new SendSMS();
		String test =  obj.sendSms("hello","7405450312");
		return ResponseEntity.ok().body(test);
	
	}*/
	
	
	@PostMapping("schedule/{doctorHospitalId}/{patientId}")
	public ResponseEntity<Schedule> saveSchedule(@RequestBody Schedule schedule,@PathVariable("doctorHospitalId")UUID docHosId,@PathVariable("patientId")UUID patID) throws DPMAPIException 
	{
		Schedule sche = null;
		try
		{
			schedule.setStatus(1);
			sche = scheduleDao.saveSch(schedule, docHosId, patID);
			
		}catch(Exception e)
		{
			throw new DPMAPIException("0019", new String[] { e.getMessage() }, e);
		}	
		try{
			
			 DateFormat dateFormat = new SimpleDateFormat("dd");
			 String date = dateFormat.format(sche.getDate());
			 
			 DateFormat dateFormat1 = new SimpleDateFormat("EEEE");
			 String day = dateFormat1.format(sche.getDate());
			 
			 DateFormat dateFormat2 = new SimpleDateFormat("MMM");
			 String month = dateFormat2.format(sche.getDate());
			 
			 DateFormat dateFormat3 = new SimpleDateFormat("YYYY");
			 String year = dateFormat3.format(sche.getDate());
			 
			String emailbody = EMAIL_CONTENT.replace("@FirstName@", sche.getPatientId().getFirstName())
					.replace("@LastName@", sche.getPatientId().getLastName())
					.replace("@Date@",date)
					.replace("@DAY@",day)
					.replace("@MONTH@",month)			
					.replace("@YEAR@",year)	
					.replace("@ABCD@",sche.getDoctorHospitalId().getDoctorId().getFirstName())
					.replace("@Time@",sche.getStartTime().toString());
			
			String smsBody = SMS_CONTENT.replace("@FirstName@", sche.getPatientId().getFirstName())
					.replace("@Date@",date)
					.replace("@DAY@",day)
					.replace("@MONTH@",month)			
					.replace("@YEAR@",year)	
					.replace("@ABCD@",sche.getDoctorHospitalId().getDoctorId().getFirstName())
					.replace("@Time@",sche.getStartTime().toString());
			
			String email = sche.getPatientId().getEmail();
			
			System.out.println("**************************************************************" + emailbody + "**************************************************************");
			EmailNotifier.sendEmailNotification(email,"Appointment confirmation",emailbody);
			String mobile = sche.getPatientId().getMobileNumber();
			SendSMS.sendSms(smsBody,mobile);
		}catch(Exception e)
		{
			throw new DPMAPIException("0085", new String[] { e.getMessage() }, e);
		}
		return ResponseEntity.ok().body(sche);
	}
	
	
	@GetMapping("schedulelist")
	public ResponseEntity<List<Schedule>> scheduleList() throws DPMAPIException
	{
		
		try
		{	
			List<Schedule> list = scheduleDao.list();
			return ResponseEntity.ok().body(list);
		}
		catch(Exception e)
		{
			throw new DPMAPIException("0020", new String[] { e.getMessage() }, e);
		}
		
	}
	
	@GetMapping("/schedule/doc/{doctorId}")
	public ResponseEntity<List<Schedule>> listByDoctor(@PathVariable("doctorId") UUID doctorId) throws DPMAPIException
	{
		try
		{
			List<Schedule> list = scheduleDao.getByDoctor(doctorId);
			return ResponseEntity.ok().body(list);
		}
		catch(Exception e)
		{
			throw new DPMAPIException("0025", new String[] { e.getMessage() }, e);
		}
		
	}
			// {format:(/format/[^/]+?)?}
			
	@GetMapping("/schedule/doctor/{doctorId}")
	public ResponseEntity<List<Schedule>> listByDoctorDate(@PathVariable("doctorId") UUID doctorId,
														   @RequestParam(value = "todate",required =false) @DateTimeFormat(pattern="yyyy-MM-dd") Date toDate,
														   @RequestParam(value = "fromdate",required =false) @DateTimeFormat(pattern="yyyy-MM-dd") Date fromDate) throws DPMAPIException
	{
		try
		{
			List<Schedule> list = scheduleDao.getByDoctorDateTest(doctorId,fromDate,toDate);
			return ResponseEntity.ok().body(list);
		}
		catch(Exception e)
		{
			throw new DPMAPIException("0025", new String[] { e.getMessage() }, e);
		}
		
	}
	
	
	
	/*@GetMapping("/schedule/doctor/{doctorId}")
	public ResponseEntity<List<Schedule>> listByDoctorDate(@PathVariable("doctorId") UUID doctorId,@RequestParam(value = "date",required =false) @DateTimeFormat(pattern="yyyy-MM-dd") Date date) throws DPMAPIException
	{
		try
		{
			List<Schedule> list = scheduleDao.getByDoctorDateTest(doctorId,date);
			return ResponseEntity.ok().body(list);
		}
		catch(Exception e)
		{
			throw new DPMAPIException("0025", new String[] { e.getMessage() }, e);
		}
		
	}*/
	
	
	
	@GetMapping("/bookedslots/{doctorId}")
	public ResponseEntity<List<BookedSlotsRes>> getBookedSlots(@PathVariable("doctorId") UUID doctorId,@RequestParam("date") @DateTimeFormat(pattern="yyyy-MM-dd") Date date) throws DPMAPIException
	{
		try
		{
			List<BookedSlotsRes> list = scheduleDao.listBookSlots(doctorId,date);
			return ResponseEntity.ok().body(list);
		}
		catch(Exception e)
		{
			throw new DPMAPIException("0032", new String[] { e.getMessage() }, e);
		}
		
	}
	
	
	@GetMapping("/schedule/{doctorHospitalId}/{date}")
	public ResponseEntity<List<ScheduleCountResponceBody>> getCountDateWise(@PathVariable("doctorHospitalId") UUID docHosId,@PathVariable("date") @DateTimeFormat(pattern = "yyyy-MM-dd") Date date) throws DPMAPIException
	{
		try
		{
		List<ScheduleCountResponceBody> list = scheduleDao.countSchedule(date, docHosId);
		return ResponseEntity.ok().body(list);
		}
		catch(Exception e)
		{
			throw new DPMAPIException("0035", new String[] { e.getMessage() }, e);
		}
	}
	@GetMapping("doctor/schedule/{doctorId}/{date}")
	public ResponseEntity<List<ScheduleCountResponceBody>> getCountDateDoctorWise(@PathVariable("doctorId") UUID doctorId,@PathVariable("date") @DateTimeFormat(pattern = "yyyy-MM-dd") Date date) throws DPMAPIException
	{
		try
		{
		List<ScheduleCountResponceBody> list = scheduleDao.countScheduleDoctorWise(date, doctorId);
		return ResponseEntity.ok().body(list);
		}
		catch(Exception e)
		{
			throw new DPMAPIException("0035", new String[] { e.getMessage() }, e);
		}
	}
	
	@GetMapping("/schedule/patient/{patientId}")
	public ResponseEntity<List<Schedule>> listByPatient(@PathVariable("patientId") UUID patientId) throws DPMAPIException
	{
		try
		{
			List<Schedule> list = scheduleDao.getByPatient(patientId);
			return ResponseEntity.ok().body(list);
		}
		catch(Exception e)
		{
			throw new DPMAPIException("0026", new String[] { e.getMessage() }, e);
		}
		
	}
	
	@PutMapping("schedule/{id}")
	public ResponseEntity<Schedule> editSchedule(@RequestBody Schedule sche,@PathVariable("id") UUID id) throws DPMAPIException
	{
		
		try
		{
			sche.setId(id);
			Schedule schedule = scheduleDao.updateSchedule(sche, id);
			return ResponseEntity.ok().body(schedule);
		}
		catch(Exception e)
		{
			throw new DPMAPIException("0021", new String[] { e.getMessage() }, e);
		}
		
	}
	
	
	@PutMapping("/update")
	public ResponseEntity<ResponseMessage> updateTest() throws DPMAPIException
	{
		
		try
		{
			scheduleDao.demoTest();
			ResponseMessage msg = new ResponseMessage();
			msg.setMsg("Schedule Updated Successfully");
			return ResponseEntity.ok().body(msg);
		}
		catch(Exception e)
		{
			throw new DPMAPIException("0021", new String[] { e.getMessage() }, e);
		}
		
	}
	
	@PutMapping("/update/status/{scheduleId}/{status}")
	public ResponseEntity<?> updateTest(@PathVariable("scheduleId") UUID scheduleId,@PathVariable("status") String status) throws DPMAPIException
	{
		
		try
		{
			scheduleDao.updateScheduleStatus(status, scheduleId);
			ResponseMessage msg = new ResponseMessage();
			msg.setMsg("Schedule Updated Successfully");
			return ResponseEntity.ok().body(msg);
		
		}
		catch(Exception e)
		{
			throw new DPMAPIException("0093", new String[] { e.getMessage() }, e);
		}
		
	}
	
}