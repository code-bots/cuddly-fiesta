package com.codebots.dpm.spring.controller;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codebots.dpm.communication.EmailNotifier;
import com.codebots.dpm.communication.SendSMS;
import com.codebots.dpm.reqres.PrescriptionRequestBody;
import com.codebots.dpm.reqres.PrescriptionResponceBody;
import com.codebots.dpm.reqres.ResponseMessage;
import com.codebots.dpm.spring.dao.PrescriptionDao;
import com.codebots.dpm.spring.exception.DPMAPIException;
import com.codebots.dpm.spring.model.Prescription;
import com.codebots.dpm.spring.model.PrescriptionDetails;

@RestController
@RequestMapping("/rest/api")
public class PrescriptionController {

	
	@Autowired
	private PrescriptionDao preDao;
	
	public static String EMAIL_CONTENT = "Dear @FirstName@ @LastName@,	<BR><BR>"
			+ "Thanks you for your visit. Please login to  AtoZ portal to find visit summary.<br><br>"
			+ "This is an automated email for providing important information. Therefore, please do not reply to this email.<BR>"
			+ "In case you want to get in touch with us, you can email us at customercare@sss.in or call us directly at 123 456 789.<BR><BR>"
			+ "Thanks,<BR>"
			+ "Team";
	
	public static String SMS_CONTENT = "Dear @FirstName@,Thanks you for your visit. Please login to  AtoZ portal to find visit summary";

	@PostMapping("/prescription/{scheduleId}")
	public ResponseEntity<PrescriptionResponceBody> savePrescription(@RequestBody PrescriptionRequestBody preReq,@PathVariable("scheduleId") UUID scheduleId) throws DPMAPIException
	{
		PrescriptionResponceBody pre = null;
		try {
			 pre = preDao.savePre(preReq, scheduleId);
		
		} catch (Exception e) {
			throw new DPMAPIException("0027", new String[] { e.getMessage() }, e);
		}
		try
		{
			String emailbody = EMAIL_CONTENT.replace("@FirstName@", pre.getPrescriptionId().getScheduleId().getPatientId().getFirstName())
					.replace("@LastName@", pre.getPrescriptionId().getScheduleId().getPatientId().getLastName());
			String smsBody = SMS_CONTENT.replace("@FirstName@", pre.getPrescriptionId().getScheduleId().getPatientId().getFirstName());
			String email = pre.getPrescriptionId().getScheduleId().getPatientId().getEmail();
			EmailNotifier.sendEmailNotification(email, "Visit summary", emailbody);
			String mobile = pre.getPrescriptionId().getScheduleId().getPatientId().getMobileNumber();
			SendSMS.sendSms(smsBody,mobile);
		}
		catch(Exception e)	
		{
			throw new DPMAPIException("0086", new String[] { e.getMessage() }, e);
		}
		return ResponseEntity.ok().body(pre);
		
		
	}
	
	@GetMapping("/list/prescription")
	public ResponseEntity<List<Prescription>> getAllPrescription() throws DPMAPIException
	{
		try
		{
			List<Prescription> list = preDao.getAllPrescription();
			return ResponseEntity.ok().body(list);
		}
		catch(Exception e)
		{
			throw new DPMAPIException("0090", new String[] { e.getMessage() }, e);
		}
		
	}
	
	@GetMapping("/prescriptiondetails/{prescriptionId}")
	public ResponseEntity<PrescriptionResponceBody> getAllPreDetails(@PathVariable("prescriptionId") UUID prescriptionId) throws DPMAPIException
	{
		try
		{
			PrescriptionResponceBody list = preDao.getAllPrescriptionDetails(prescriptionId);
			return ResponseEntity.ok().body(list);
		}
		catch(Exception e)
		{
			throw new DPMAPIException("0030", new String[] { e.getMessage() }, e);
		}
	}
/*	
	@GetMapping("/prescription/{patientId}")
	public ResponseEntity<List<PrescriptionDetails>> getByPatient(@PathVariable("patientId") UUID patientId) throws DPMAPIException
	{
		try
		{
			List<PrescriptionDetails> list = preDao.getPreByPatient(patientId);
			return ResponseEntity.ok().body(list);
		}
		catch(Exception e)
		{
			throw new DPMAPIException("0030", new String[] { e.getMessage() }, e);
		}
	}
	*/
	
	@GetMapping("/prescription/patient/{patientId}")
	public ResponseEntity<List<PrescriptionResponceBody>> getByPatient(@PathVariable("patientId") UUID patientId) throws DPMAPIException
	{
		try
		{	
			
			List<PrescriptionResponceBody>  list = preDao.getPreByPatient(patientId);
			return ResponseEntity.ok().body(list);
		}
		catch(Exception e)
		{
			throw new DPMAPIException("0091", new String[] { e.getMessage() }, e);
		}
	}
	

	@GetMapping("/prescription/doctor/{doctorId}")
	public ResponseEntity<List<PrescriptionResponceBody>> getByDoctor(@PathVariable("doctorId") UUID doctorId) throws DPMAPIException
	{
		try
		{	
			List<PrescriptionResponceBody>  list = preDao.getPreByDoctor(doctorId);
			return ResponseEntity.ok().body(list);
		}
		catch(Exception e)
		{
			throw new DPMAPIException("0092", new String[] { e.getMessage() }, e);
		}
	}
	
	@GetMapping("/prescription/{type}/{patientId}")
	public ResponseEntity<List<PrescriptionDetails>> getByType(@PathVariable("patientId") UUID patientId,@PathVariable("type") String type) throws DPMAPIException
	{
		try
		{
			List<PrescriptionDetails> list = preDao.getTestPatient(patientId, type);
			return ResponseEntity.ok().body(list);
		}
		catch(Exception e)
		{
			throw new DPMAPIException("0031", new String[] { e.getMessage() }, e);
		}
	}
	
	
	@GetMapping("/prescription/schedule/{scheduleId}")
	public ResponseEntity<PrescriptionResponceBody> getByScheduleID(@PathVariable("scheduleId") UUID scheduleId) throws DPMAPIException
	{
		try
		{
		PrescriptionResponceBody prescriptionResponce = preDao.getBySchedule(scheduleId);
		return ResponseEntity.ok().body(prescriptionResponce);
		}
		catch(Exception e)
		{
			throw new DPMAPIException("0034", new String[] { e.getMessage() }, e);
		}
	}
	
	
	@PutMapping("/prescription/{prescriptionId}")
	public ResponseEntity<PrescriptionDetails> editPrescription(@RequestBody PrescriptionDetails pre,@PathVariable("prescriptionId") UUID prescriptionId) throws DPMAPIException
	{
		try {
			PrescriptionDetails preDet = preDao.updatePreDetails(pre, prescriptionId);
			return ResponseEntity.ok().body(preDet);
		} catch (Exception e) {
			throw new DPMAPIException("0028", new String[] { e.getMessage() }, e);
		}
	}
	
	@DeleteMapping("/prescription/{prescriptionDetId}")
	public ResponseEntity<ResponseMessage> delatePrescription(@PathVariable("prescriptionDetId") UUID prescriptionDetId) throws DPMAPIException
	{
		try {
			preDao.deletePrescription(prescriptionDetId);
			ResponseMessage msg = new ResponseMessage();
			msg.setMsg("Successfully Deleted Prescription");
			return ResponseEntity.ok().body(msg);
		} catch (Exception e) {
			throw new DPMAPIException("0029", new String[] { e.getMessage() }, e);
		}
	}
	
	
	

	
}
