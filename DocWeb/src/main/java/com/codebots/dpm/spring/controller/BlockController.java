package com.codebots.dpm.spring.controller;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codebots.dpm.reqres.ResponseMessage;
import com.codebots.dpm.spring.dao.BlockDao;
import com.codebots.dpm.spring.exception.DPMAPIException;
import com.codebots.dpm.spring.model.Block;

@RestController
@RequestMapping("/rest/api")
public class BlockController {

	@Autowired
	private BlockDao blockDao;
	
	@PostMapping("/block/{doctorId}")
	public ResponseEntity<Block> postBlock(@RequestBody Block block,@PathVariable("doctorId") UUID doctorId) throws DPMAPIException
	{
		try
		{
			Block b = blockDao.saveBlock(block, doctorId);
			return ResponseEntity.ok().body(b);
		}
		catch(Exception e)
		{
			throw new DPMAPIException("0095", new String[] { e.getMessage() }, e);
		}
	}
	
	
	@GetMapping("/block/{doctorId}")
	public ResponseEntity<List<Block>> getBlockDoctorWise(@PathVariable("doctorId") UUID doctorId
														 ,@RequestParam(value = "date",required =false) @DateTimeFormat(pattern="yyyy-MM-dd") Date date) throws DPMAPIException
	{
		try
		{
			List<Block> list = blockDao.getBlock(doctorId, date);
			return ResponseEntity.ok().body(list);	
		}
		catch(Exception e)
		{
			throw new DPMAPIException("0096", new String[] { e.getMessage() }, e);
		}
	}
	
	@DeleteMapping("block/{blockId}")
	public ResponseEntity<ResponseMessage> removeBlock(@PathVariable("blockId") UUID blockId) throws DPMAPIException
	{
		try
		{
		blockDao.deleteBlock(blockId);
		ResponseMessage msg = new ResponseMessage();
		msg.setMsg("block removed");
		return ResponseEntity.ok().body(msg);
		}
		catch(Exception e)
		{
			throw new DPMAPIException("0097", new String[] { e.getMessage() }, e);
		}
	}
	
}
