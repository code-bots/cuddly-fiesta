package com.codebots.dpm.spring.crudrepo;

import java.util.UUID;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.codebots.dpm.spring.model.Role;
@Repository
public interface RoleRepository extends CrudRepository<Role, Integer> {
}