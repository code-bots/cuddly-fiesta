package com.codebots.dpm.spring.dao;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Repository;

import com.codebots.dpm.reqres.BookedSlotsRes;
import com.codebots.dpm.reqres.ScheduleCountResponceBody;
import com.codebots.dpm.spring.exception.DPMAPIException;
import com.codebots.dpm.spring.model.Doctor;
import com.codebots.dpm.spring.model.DoctorHospital;
import com.codebots.dpm.spring.model.Patient;
import com.codebots.dpm.spring.model.Schedule;

@Repository
@PropertySource("classpath:application.properties")
@PropertySource("classpath:mailbox.properties")
@Transactional
public class ScheduleDaoImple implements ScheduleDao{

	
	@Autowired
	private SessionFactory sessionfactory;
	


	@Override
	public Schedule saveSch(Schedule sche, UUID docHosId, UUID patientId) {
		Session session = sessionfactory.getCurrentSession();
		DoctorHospital dochos = session.byId(DoctorHospital.class).load(docHosId);
		Patient patient = session.byId(Patient.class).load(patientId); 
		sche.setPatientId(patient);
		sche.setDoctorHospitalId(dochos);

		sessionfactory.getCurrentSession().save(sche);
		return sche;
	}
	
	@Override
	public void sendReminder()
	{
		Date date = new Date();
		Session session = sessionfactory.getCurrentSession();
		String hql = "from "+Schedule.class.getName() + " where datediff("+ date  +",date) = 1";
		Query query = session.createQuery(hql);
		List<Schedule> list = query.list();
		System.out.println(list);
	}
	
	@Override
	public List<Schedule> list() throws ParseException {
		Session session = sessionfactory.getCurrentSession();
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Schedule> cq = cb.createQuery(Schedule.class);
		Root<Schedule> root = cq.from(Schedule.class);
		cq.select(root);
		Query<Schedule> query = session.createQuery(cq);
		return query.getResultList();

		
	}

	@Override
	public Schedule updateSchedule(Schedule sche, UUID scheduleId) throws ParseException, DPMAPIException 
	{
	
		Session session = sessionfactory.getCurrentSession();
		String hql = "from " + Schedule.class.getName()  + " where id = :schId";
		Query query = session.createQuery(hql);
		query.setParameter("schId", scheduleId);
		Schedule schedule = (Schedule) query.uniqueResult();
		
		Date currentDate = new Date();
		SimpleDateFormat dateFormate = new SimpleDateFormat("yyyy-MM-dd");
		String cDate = dateFormate.format(currentDate);
		String sDate = dateFormate.format(schedule.getDate());
		
		Date date1 = dateFormate.parse(cDate);
		Date date2 = dateFormate.parse(sDate);
	    
		
		if(date2.compareTo(date1) > 0)
		{
			schedule.setDate(sche.getDate());
			schedule.setEndTime(sche.getEndTime());
			schedule.setStartTime(sche.getStartTime());
			schedule.setRemarks(sche.getRemarks());
			schedule.setSchStatus(sche.getSchStatus());
			session.saveOrUpdate(schedule);
			return schedule;
		}
		else if(date2.compareTo(date1) == 0)
		{
			Date date = new Date();
		    String timeFormate = "HH:mm";
		    DateFormat timrFormate = new SimpleDateFormat(timeFormate);
		    String currTime= timrFormate.format(date);
		    String oriTime= timrFormate.format(schedule.getStartTime());
		    
		    Date time1 = timrFormate.parse(oriTime);
			Date time2 = timrFormate.parse(currTime);
			long difference = (time1.getHours() - time2.getHours())/(60 * 60 * 1000) % 24;
			
			if(difference < 6)
			{
				String diff = Long.toString(difference);
				throw new DPMAPIException("0010", new String[] { diff }, null);
			}
			else 
			{
			schedule.setDate(sche.getDate());
			schedule.setEndTime(sche.getEndTime());
			schedule.setStartTime(sche.getStartTime());
			schedule.setRemarks(sche.getRemarks());
			schedule.setSchStatus(sche.getSchStatus());
			session.saveOrUpdate(schedule);
			return schedule;
			}
		}
		else
		{
			throw new DPMAPIException("0010", new String[] { }, null);
		}
				
	}


	@Override
	public Schedule getById(UUID scheduleId) {
		Session session = sessionfactory.getCurrentSession();
		Schedule schedule = session.byId(Schedule.class).load(scheduleId);
		return schedule;
	}


	@Override
	public List<Schedule> getByDoctorDateTest(UUID doctorId,Date fromDate,Date toDate) {	
		if(toDate == null && fromDate == null) 
		{
			List<Schedule> list = getByDoctor(doctorId);
			return list;
		}
		else
		{
			Session session = sessionfactory.getCurrentSession();		
			String hql = "select sc from " + Schedule.class.getName() + " sc," + DoctorHospital.class.getName() + " dh," + Doctor.class.getName() + 
					     " d  WHERE sc.doctorHospitalId = dh.id AND dh.doctorId = d.id  AND d.id = :docId And sc.date >= :fromDate and sc.date <= :toDate and sc.status = 1" ;
			Query query = session.createQuery(hql);
			query.setParameter("docId", doctorId);
			query.setParameter("toDate", toDate);	
			query.setParameter("fromDate", fromDate);	
			System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" + query.getQueryString()+ "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
			List<Schedule> schList = query.list();
			return schList;
		}
	}
	@Override
	public List<Schedule> getByDoctorDate(UUID doctorId,Date date) {	
		
			Session session = sessionfactory.getCurrentSession();		
			String hql = "from " + Schedule.class.getName() + " sc," + DoctorHospital.class.getName() + " dh," + Doctor.class.getName() + 
					     " d  WHERE sc.doctorHospitalId = dh.id AND dh.doctorId = d.id  AND d.id = :docId And sc.date = :d and sc.status = 1" ;
			Query query = session.createQuery(hql);
			query.setParameter("docId", doctorId);
			query.setParameter("d", date);
			List<Schedule> schList = query.list();
			return schList;
		
	}
	
	
	
	@Override
	public List<Schedule> getByPatient(UUID patientId)
	{
		Session session = sessionfactory.getCurrentSession();
		String hql = "from " + Schedule.class.getName() + " where patientId.id = :patId and status = 1";
		Query query = session.createQuery(hql);
		query.setParameter("patId", patientId);
		List<Schedule> listSche	 = query.list();
		return listSche;
	}


	@Override
	public List<Schedule> getByDoctor(UUID doctorId) {
		Session session = sessionfactory.getCurrentSession();		
		String hql = "select sc from " + Schedule.class.getName() + " sc," + DoctorHospital.class.getName() + " dh," + Doctor.class.getName() + 
				     " d  WHERE sc.doctorHospitalId = dh.id AND dh.doctorId = d.id  AND d.id = :docId and sc.status = 1" ;
		Query query = session.createQuery(hql);
		query.setParameter("docId", doctorId);
		
		List<Schedule> schList = query.list();
		return schList;
	}


	@Override
	public List<BookedSlotsRes> listBookSlots(UUID doctorHospitalId,Date date) {
		Session session = sessionfactory.getCurrentSession();
		String hql = "select sc.startTime,sc.endTime from "+ Schedule.class.getName() + " sc,"  + DoctorHospital.class.getName() + " dochos WHERE sc.doctorHospitalId = dochos.id "
					+ "AND dochos.id = :dochosId And sc.date = :date and sc.status = '1'";	
		
		Query query = session.createQuery(hql);
		query.setParameter("dochosId", doctorHospitalId);
		query.setParameter("date", date);
		//query.setParameter("status", '1');
		List<BookedSlotsRes> list = query.list();
		return list;
	}


	@Override
	public void updateScheduleStatus(String status,UUID schId) {
		Session session = sessionfactory.getCurrentSession();
		String hql = "update " + Schedule.class.getName() + " set schStatus = :status where id = :id";
		Query query = session.createQuery(hql);
		query.setParameter("status", status);
		query.setParameter("id", schId);
		int result = query.executeUpdate();	
		
	}


	@Override
	public void demoTest() {
		Session session = sessionfactory.getCurrentSession();		
		String hql = "update " + Schedule.class.getName() + " set status = 3 where TIMEDIFF(paymentStart,startTime) = '00:10:00' and status = 0";
		Query query = session.createQuery(hql);
		query.executeUpdate();
		
	}

	@Override
	public void updateStatus(int status, UUID schId) {
		Session session = sessionfactory.getCurrentSession();
		String hql = "update " + Schedule.class.getName() + " set status = :status where id = :id";
		Query query = session.createQuery(hql);
		query.setParameter("status", status);
		query.setParameter("id", schId);
		int result = query.executeUpdate();	
		
	}

	@Override
	public List<ScheduleCountResponceBody> countSchedule(Date date,UUID doctorHospitalId) {
		Session session = sessionfactory.getCurrentSession();
		String hql = "SELECT DATE,COUNT(DATE) FROM " + Schedule.class.getName() + " where doctorHospitalId.id = :docHosId GROUP BY DATE HAVING DATE >= :date";
		Query query = session.createQuery(hql);
		query.setParameter("date", date);
		query.setParameter("docHosId", doctorHospitalId);	
		List<ScheduleCountResponceBody> list = query.list();	
		return list;
	}

	@Override
	public List<ScheduleCountResponceBody> countScheduleDoctorWise(Date date, UUID doctorId) {
		Session session = sessionfactory.getCurrentSession();
		String hql = "select  date,count(date) from " + Schedule.class.getName() + " sc," + DoctorHospital.class.getName() + " dh," + Doctor.class.getName() + 
			     " d  WHERE sc.doctorHospitalId = dh.id AND dh.doctorId = d.id  AND d.id = :docId  GROUP BY DATE HAVING DATE >= :date";
		Query query = session.createQuery(hql);
		query.setParameter("date", date);
		query.setParameter("docId", doctorId);
		List<ScheduleCountResponceBody> list = query.list();
		return list;	
	}


}
