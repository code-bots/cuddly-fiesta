package com.codebots.dpm.spring.controller;

import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import com.codebots.dpm.communication.EmailNotifier;
import com.codebots.dpm.communication.SendSMS;
import com.codebots.dpm.spring.exception.DPMAPIException;
import com.codebots.dpm.spring.model.Schedule;

@Configuration
@EnableScheduling
@Transactional
public class ScheduleJob {

	
	public static String EMAIL_CONTENT = "Dear @FirstName@ @LastName@,	<BR><BR>"
			+ "This is to remind you of your appointment at  @Time@ on @DAY@,@MONTH@ @Date@, @YEAR@ with Dr.@ABCD@." + "Please be on time. <BR><BR>"
			+ "Please contact 123 456789  for any assitance on appointment."
			+ "This is an automated email for providing important information. Therefore, please do not reply to this email.<BR>"
			+ "In case you want to get in touch with us, you can email us at customercare@sss.in or call us directly at 123 456 789.<BR><BR>"
			+ "Thanks,<BR>"
			+ "Team";
	
	
	public static String SMS_CONTENT = "Dear @FirstName@ ,This is to remind you of your appointment at  @Time@ on @DAY@,@MONTH@ @Date@, @YEAR@ with Dr.@ABCD@.Please be on time.";
	
	
	public static String EMAIL_CONTENT_OVERDUE = "Dear @FirstName@ @LastName@,	<BR><BR>"
			+ "Your appointment at  @Time@ on @DAY@,@MONTH@ @Date@, @YEAR@ is overdue <br><br>"
			+  "Please visit <a href=\"http://35.185.51.218:8080/dist/\">DOCWEB</a>to book new appointmnet.<BR><BR>"
			+ "Please contact 123 456789  for any assitance on appointment."
			+ "This is an automated email for providing important information. Therefore, please do not reply to this email.<BR>"
			+ "In case you want to get in touch with us, you can email us at customercare@sss.in or call us directly at 123 456 789.<BR><BR>"
			+ "Thanks,<BR>"
			+ "Team";
	
	public static String SMS_CONTENT_OVERDUE = "Dear @FirstName@ ," + "Your appointment at  @Time@ on @DAY@,@MONTH@ @Date@, @YEAR@ is overdue"
			+  "Please visit http://35.185.51.218:8080/dist/ to book new appointmnet.<BR><BR>";
	
	
	@Autowired
	private SessionFactory sessionfactory;
	// */10 * * * *
	@Scheduled(cron="0 */10 * ? * *")
    public void demoServiceMethod()
    {
		Session session = sessionfactory.getCurrentSession();		
		String hql = "update " + Schedule.class.getName() + " set status = 3 where TIMEDIFF(paymentStart,startTime) = '00:10:00' and status = 0";
		Query query = session.createQuery(hql);
		query.executeUpdate();
		System.out.println("-----------------------------status Updated---------------------------------");
    }
	
	
	@Scheduled(cron="0 0 19 * * ?")
	public void sendReminder() throws ParseException, DPMAPIException
	{
		
		System.out.println("----------------------------------Reminder------------------------------------");
		Session session = sessionfactory.getCurrentSession();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String today = dateFormat.format(new Date());
		Date cDate;
		cDate  = dateFormat.parse(today); 
		String hql = "from " + Schedule.class.getName() + " where DATEDIFF(date,:today) = 1";
		Query query = session.createQuery(hql);
		query.setParameter("today", cDate);
		List<Schedule> list = query.list();
		
		for(Schedule sch : list)
		{
			String email = sch.getPatientId().getEmail();
			 DateFormat dateFormatt = new SimpleDateFormat("dd");
			 String date = dateFormatt.format(sch.getDate());
			 
			 DateFormat dateFormat1 = new SimpleDateFormat("EEEE");
			 String day = dateFormat1.format(sch.getDate());
			 
			 DateFormat dateFormat2 = new SimpleDateFormat("MMM");
			 String month = dateFormat2.format(sch.getDate());
			 
			 DateFormat dateFormat3 = new SimpleDateFormat("YYYY");
			 String year = dateFormat3.format(sch.getDate());
			String emailbody = EMAIL_CONTENT.replace("@FirstName@", sch.getPatientId().getFirstName())
					.replace("@LastName@", sch.getPatientId().getLastName())
					.replace("@Date@",date)
					.replace("@DAY@",day)
					.replace("@MONTH@",month)			
					.replace("@YEAR@",year)	
					.replace("@ABCD@",sch.getDoctorHospitalId().getDoctorId().getFirstName())
					.replace("@Time@",sch.getStartTime().toString());
			
			String smsBody = SMS_CONTENT.replace("@FirstName@", sch.getPatientId().getFirstName())
					.replace("@Date@",date)
					.replace("@DAY@",day)
					.replace("@MONTH@",month)			
					.replace("@YEAR@",year)	
					.replace("@ABCD@",sch.getDoctorHospitalId().getDoctorId().getFirstName())
					.replace("@Time@",sch.getStartTime().toString());
			
			EmailNotifier.sendEmailNotification(email,"Visit summary",emailbody);
			SendSMS.sendSms(smsBody,sch.getPatientId().getMobileNumber());
		}
	}
	
	
	@Scheduled(cron="0 0 19 * * ?")
	public void dueAppointmentDate() throws ParseException, DPMAPIException
	{
		
		System.out.println("----------------------------------Reminder------------------------------------");
		Session session = sessionfactory.getCurrentSession();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String today = dateFormat.format(new Date());
		Date cDate;
		cDate  = dateFormat.parse(today); 
		String hql = "from " + Schedule.class.getName() + " where DATEDIFF(:today,date) >= 1";
		Query query = session.createQuery(hql);
		query.setParameter("today", cDate);
		List<Schedule> list = query.list();
	
		for(Schedule sch : list)
		{
			String email = sch.getPatientId().getEmail();
			 DateFormat dateFormatt = new SimpleDateFormat("dd");
			 String date = dateFormatt.format(sch.getDate());
			 
			 DateFormat dateFormat1 = new SimpleDateFormat("EEEE");
			 String day = dateFormat1.format(sch.getDate());
			 
			 DateFormat dateFormat2 = new SimpleDateFormat("MMM");
			 String month = dateFormat2.format(sch.getDate());
			 
			 DateFormat dateFormat3 = new SimpleDateFormat("YYYY");
			 String year = dateFormat3.format(sch.getDate());
			 
			String emailbody = EMAIL_CONTENT_OVERDUE.replace("@FirstName@", sch.getPatientId().getFirstName())
					.replace("@LastName@", sch.getPatientId().getLastName())
					.replace("@Date@",date)
					.replace("@DAY@",day)
					.replace("@MONTH@",month)			
					.replace("@YEAR@",year)	
					.replace("@Time@",sch.getStartTime().toString());
			
			String smsBody = SMS_CONTENT_OVERDUE.replace("@FirstName@", sch.getPatientId().getFirstName())
					.replace("@Date@",date)
					.replace("@DAY@",day)
					.replace("@MONTH@",month)			
					.replace("@YEAR@",year)	
					.replace("@Time@",sch.getStartTime().toString());
			
			EmailNotifier.sendEmailNotification(email,"Appointment Overdue",emailbody);
			SendSMS.sendSms(smsBody,sch.getPatientId().getMobileNumber());
		}
	}
	
	public Time getPaymentTime(UUID scheduleId)
	{
		Session session = sessionfactory.getCurrentSession();
		String hql = "select paymentTime from " + Schedule.class.getName() + " where id = :schId ";
		Query query = session.createQuery(hql);
		query.setParameter("schId", scheduleId);
		Time time = (Time) query.uniqueResult();
		return time;
	}
	
}