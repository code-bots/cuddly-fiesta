package com.codebots.dpm.spring.dao;

import java.util.List;
import java.util.UUID;

import com.codebots.dpm.reqres.PrescriptionRequestBody;
import com.codebots.dpm.reqres.PrescriptionResponceBody;
import com.codebots.dpm.spring.model.Prescription;
import com.codebots.dpm.spring.model.PrescriptionDetails;

public interface PrescriptionDao {

	
	PrescriptionResponceBody savePre(PrescriptionRequestBody prescription,UUID scheduleId);
	List<Prescription> getAllPrescription();	
	PrescriptionResponceBody getAllPrescriptionDetails(UUID preId);
	
	//List<PrescriptionDetails> getPreByPatient(UUID patientId);
	List<PrescriptionResponceBody> getPreByPatient(UUID patientId);
	List<PrescriptionResponceBody> getPreByDoctor(UUID doctorId);
	List<PrescriptionDetails> getTestPatient(UUID patientId,String test);
	void deletePrescription(UUID prescriptionDetailId);
	PrescriptionDetails updatePreDetails(PrescriptionDetails preDetails,UUID preDetId);

	List<UUID> getPreIdOfPatient(UUID patientId);
	List<UUID> getPreIdOfDoctor(UUID doctorId);
	
	PrescriptionResponceBody getBySchedule(UUID scheduleId);
	
	
	
}
