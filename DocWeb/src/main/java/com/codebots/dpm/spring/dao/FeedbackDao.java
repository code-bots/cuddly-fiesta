package com.codebots.dpm.spring.dao;

import java.util.UUID;

import com.codebots.dpm.spring.model.Feedback;

public interface FeedbackDao {

	public Feedback saveFeedback(Feedback feedback,UUID scheduleId);
	
}
