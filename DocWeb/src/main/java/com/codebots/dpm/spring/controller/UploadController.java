package com.codebots.dpm.spring.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.codebots.dpm.reqres.ResponseMessage;
import com.codebots.dpm.spring.exception.DPMAPIException;

@PropertySource("classpath:application.properties")
@Controller
public class UploadController {

	@Autowired org.springframework.core.env.Environment env;

	
	private static final Logger logger = LoggerFactory
			.getLogger(UploadController.class);
	
    @GetMapping("/")
    public String index() {
        return "This is API for DPM project";
    }

    @RequestMapping(value = "/tobase64", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<String> singleFileUpload(@RequestParam("file") MultipartFile file,
                                   RedirectAttributes redirectAttributes) throws DPMAPIException {

        if (file.isEmpty()) {
            throw new DPMAPIException("0037", new String[] {"File is empty"}, null);
        }

        try {
            // Get the file and save it somewhere
            byte[] bytes = file.getBytes();
            return new ResponseEntity<String>(new String(Base64.getEncoder().encode(bytes)), HttpStatus.OK);
        } catch (IOException e) {
            throw new DPMAPIException("0037", new String[] {e.getMessage()}, e);
        }

    }
    
    @RequestMapping(value = "/report/{prescriptionDetailId}", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<ResponseMessage> reportUpload(@PathVariable("prescriptionDetailId") UUID id,@RequestParam("file") MultipartFile file) throws DPMAPIException {

    //	String path = env.getProperty("upload") + id;
    	//System.out.println("*********************************************************************" + path);
    	//String path = "/home/parth/work/apache-tomcat-9.0.8/bin/prescription/" + id;
    	String path = env.getProperty("upload")+ id;
    	System.out.println(path);
    	 File dir = new File(path);
    	    boolean successful = dir.mkdir();
    	    
        	if (!file.isEmpty()) {
    			try {
    				byte[] bytes = file.getBytes();
    				if (!dir.exists())
    					dir.mkdirs();

    				// Create the file on server
    				File serverFile = new File(dir.getAbsolutePath()
    						+ File.separator + file.getOriginalFilename());
    				BufferedOutputStream stream = new BufferedOutputStream(
    						new FileOutputStream(serverFile));
    				stream.write(bytes);
    				stream.close();

    				logger.info("Server File Location="
    						+ serverFile.getAbsolutePath());
//return new ResponseEntity<String>(new String(Base64.getEncoder().encode(bytes)), HttpStatus.OK);
    				ResponseMessage msg = new ResponseMessage();
    				msg.setMsg("You successfully uploaded file " + file.getName());
    				return ResponseEntity.ok().body(msg);
    				
    				//return new ResponseEntity<String>("You successfully uploaded file " + file.getName(), HttpStatus.OK);
    			} catch (Exception e) {
    				ResponseMessage msg = new ResponseMessage();
    				msg.setMsg("You failed to upload  => " +   file.getName() );
    				return ResponseEntity.ok().body(msg);
    				//return new ResponseEntity<String>("You failed to upload  => " +   file.getName() + " "+ e.getMessage(), HttpStatus.OK);
    			}
    		} else {
    			
    			ResponseMessage msg = new ResponseMessage();
				msg.setMsg("You failed to upload  because the file " +   file.getName() + " was empty.");
				return ResponseEntity.ok().body(msg);
    			//return new ResponseEntity<String>("You failed to upload  because the file " +   file.getName() + " was empty.", HttpStatus.OK);
    		}
    	   
    	    
    }
    
    
    @RequestMapping(value = "/download/{prescriptionDetailId}/{fileName:.+}", method = RequestMethod.GET)
    public void downloadPDFResource( HttpServletRequest request,
                                     HttpServletResponse response,
                                     @PathVariable("prescriptionDetailId") String preDetId,
                                     @PathVariable("fileName") String fileName)
    {
    	//request.getServletContext().getRealPath("/bin/prescription/pdf/");
        String dataDirectory1 = env.getProperty("upload")+ preDetId + "/" ;
        String dataDirectory ="/home/parth/work/apache-tomcat-9.0.8/bin/prescription/" + preDetId + "/" ;
        Path file = Paths.get(dataDirectory, fileName);
        if (Files.exists(file))
        {
            response.setContentType("application/octet-stream");
            response.addHeader("Content-Disposition", "attachment; filename="+fileName);
            try
            {
                Files.copy(file, response.getOutputStream());
                response.getOutputStream().flush();
            }
            catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
    }