package com.codebots.dpm.spring.dao;

import java.util.List;
import java.util.UUID;

import com.codebots.dpm.spring.model.Surgery;

public interface SurgeryDao {

	Surgery save(Surgery academic,UUID patientId);
	List<Surgery> list();
	List<Surgery> getbyId(UUID patientId);
	Surgery getSpecific(UUID surgeryId);
	int delete(UUID student_id,UUID surgeryId);
	void update(UUID student_id,UUID surgeryId,Surgery surgery);
}
