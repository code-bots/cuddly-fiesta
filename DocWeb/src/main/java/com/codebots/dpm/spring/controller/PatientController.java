package com.codebots.dpm.spring.controller;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codebots.dpm.reqres.Pagination;
import com.codebots.dpm.reqres.PasswordChangeRequestBody;
import com.codebots.dpm.reqres.ResponseMessage;
import com.codebots.dpm.spring.dao.PatientDao;
import com.codebots.dpm.spring.dao.SurgeryDao;
import com.codebots.dpm.spring.exception.DPMAPIException;
import com.codebots.dpm.spring.model.Patient;
import com.codebots.dpm.spring.model.Surgery;

@RestController
@RequestMapping("/rest/api")
public class PatientController {

	@Autowired
	private PatientDao patientDao;

	@Autowired
	private SurgeryDao surgeryDao;

	@GetMapping("/patientgrid")
	public ResponseEntity<Pagination> listPatientGrid(@RequestParam(value = "page",required = false) Integer pageNumber,
													  @RequestParam(value = "size",required = false) Integer pageSize,
													  @RequestParam(value = "type",required = false) String type,
													  @RequestParam(value = "feild",required = false) String feild) throws DPMAPIException {
		try {
			Pagination page = patientDao.list(pageNumber,pageSize,type,feild);
			return ResponseEntity.ok().body(page);
		} catch (Exception e) {
			throw new DPMAPIException("0003", new String[] { e.getMessage() }, e);
		}
	}

	@PutMapping("patient/changepassword/{patient_id}")
	public ResponseEntity<?> resetPassword(@PathVariable UUID patientId,
			@RequestBody PasswordChangeRequestBody passchange) throws DPMAPIException {
		try {
			patientDao.resetPassword(passchange, patientId);
			ResponseMessage msg = new ResponseMessage();
			msg.setMsg("Successfully Changed Password");
			return ResponseEntity.ok().body(msg);
		} catch (Exception e) {
			throw new DPMAPIException("0004", new String[] { e.getMessage() }, e);
		}
	}

	// update patient
	@PutMapping("/patient/{patient_id}")
	public ResponseEntity<Patient> updatePatient(@PathVariable("patient_id") UUID id, @RequestBody Patient patient)
			throws DPMAPIException {
		try {
			patient.setId(id);
			patientDao.update(id, patient);
			return new ResponseEntity<Patient>(patient, HttpStatus.OK);
		} catch (Exception e) {
			throw new DPMAPIException("0005", new String[] { e.getMessage() }, e);
		}
	}

	@GetMapping("/surgerydetails")
	public ResponseEntity<List<Surgery>> listSurgeries() throws DPMAPIException {
		try {
			List<Surgery> surgeries = surgeryDao.list();
			return ResponseEntity.ok().body(surgeries);
		} catch (Exception e) {
			throw new DPMAPIException("0006", new String[] { e.getMessage() }, e);
		}
	}

	@PostMapping("/patient/{patient_id}/surgery")
	public ResponseEntity<?> saveSurgery(@RequestBody Surgery surgery, @PathVariable("patient_id") UUID patientId)
			throws DPMAPIException {
		try {
			Surgery surg = surgeryDao.save(surgery, patientId);
			return ResponseEntity.ok().body(surg);
		} catch (Exception e) {
			throw new DPMAPIException("0007", new String[] { e.getMessage() }, e);
		}
	}

	// get all surgery of patient
	@GetMapping("/patient/{patient_id}/surgeries")
	public ResponseEntity<List<Surgery>> getByPatientId(@PathVariable("patient_id") UUID patientId)
			throws DPMAPIException {
		try {
			List<Surgery> surgeries = surgeryDao.getbyId(patientId);
			return ResponseEntity.ok().body(surgeries);
		} catch (Exception e) {
			throw new DPMAPIException("0008", new String[] { e.getMessage() }, e);
		}

	}

	// get patient wise surgery single object
	@GetMapping("surgery/{surgery_id}")
	public ResponseEntity<Surgery> getSpecificSurgery(@PathVariable("surgery_id") UUID surgeryId) throws DPMAPIException {
		try {
			Surgery surgery = surgeryDao.getSpecific(surgeryId);
			return ResponseEntity.ok().body(surgery);
		} catch (Exception e) {
			throw new DPMAPIException("0009", new String[] { e.getMessage() }, e);
		}
	}

	// delete surgery patient
	@DeleteMapping("/patient/{patient_id}/surgery/{surgery_id}")
	public ResponseEntity<ResponseMessage> deleteSurgery(@PathVariable("patient_id") UUID patientId,
			@PathVariable("surgery_id") UUID surgeryId) throws DPMAPIException {
		try {
			int result = surgeryDao.delete(patientId, surgeryId);
			ResponseMessage msg = new ResponseMessage();
			msg.setMsg("Successfully Deleted Surgery");
			return ResponseEntity.ok().body(msg);
		} catch (Exception e) {
			throw new DPMAPIException("0010", new String[] { e.getMessage() }, e);
		}
	}

	// update surgery
	@PutMapping("/patient/{patient_id}/surgery/{surgery_id}")
	public ResponseEntity<ResponseMessage> updateSurgery(@PathVariable("patient_id") UUID patientId,
			@PathVariable("surgery_id") UUID surgeryId, @RequestBody Surgery surgery) throws DPMAPIException {
		try {
			surgery.setId(surgeryId);
			surgeryDao.update(patientId, surgeryId, surgery);
			ResponseMessage msg = new ResponseMessage();
			msg.setMsg("Successfully updated surgery");
			return ResponseEntity.ok().body(msg);
		} catch (Exception e) {
			throw new DPMAPIException("0011", new String[] { e.getMessage() }, e);
		}
	}

}
