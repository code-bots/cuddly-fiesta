package com.codebots.dpm.spring.dao;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import com.codebots.dpm.spring.model.Block;

public interface BlockDao {

	
	Block saveBlock(Block block,UUID doctorId);
	List<Block> getBlock(UUID doctorId,Date date);
	void deleteBlock(UUID blockId);
	
}
