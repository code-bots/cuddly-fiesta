package com.codebots.dpm.spring.controller;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.View;

import com.codebots.dpm.common.PaymentConstants;
import com.codebots.dpm.spring.dao.DoctorHospitalDao;
import com.codebots.dpm.spring.dao.PatientDao;
import com.codebots.dpm.spring.dao.PaymentDao;
import com.codebots.dpm.spring.dao.ScheduleDao;
import com.codebots.dpm.spring.exception.DPMAPIException;
import com.codebots.dpm.spring.model.DoctorHospital;
import com.codebots.dpm.spring.model.Patient;
import com.codebots.dpm.spring.model.Schedule;
import com.paytm.pg.merchant.CheckSumServiceHelper;



@PropertySource("classpath:payment.properties")
@Controller
public class PaymentServlet {

	
	@Autowired
	private Environment env;
	
	@Autowired
	private PaymentDao paymentDao;
	
	@Autowired
	private ScheduleDao schedulDao;
	
	@Autowired
	private PatientDao patientDao;
	
	@Autowired 
	private DoctorHospitalDao docHosDao;
	
	@RequestMapping(value = "/paymentResult", method = RequestMethod.POST, produces="text/html")
	public void paymentResult(HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		Enumeration<String> paramNames = request.getParameterNames();

		Map<String, String[]> mapData = request.getParameterMap();
		TreeMap<String,String> parameters = new TreeMap<String,String>();
		String paytmChecksum =  "";
		while(paramNames.hasMoreElements()) {
			String paramName = (String)paramNames.nextElement();
			if(paramName.equals("CHECKSUMHASH")){
				paytmChecksum = mapData.get(paramName)[0];
			}else{
				parameters.put(paramName,mapData.get(paramName)[0]);
			}
		}
		boolean isValideChecksum = false;
		com.codebots.dpm.spring.model.PaymentInfo paymentInfo = new com.codebots.dpm.spring.model.PaymentInfo();
		paymentInfo.setGateway_status(parameters.get("STATUS"));
		paymentInfo.setRespcode(parameters.get("RESPCODE"));
		paymentInfo.setRespmsg(parameters.get("RESPMSG"));
		paymentInfo.setTxn_id(parameters.get("TXNID"));
		paymentInfo.setApp_status("Completed");
		com.codebots.dpm.spring.model.PaymentInfo updatedPaymentInfo = paymentDao.updatePayment(paymentInfo, UUID.fromString(parameters.get("ORDERID")));
		
		Schedule sch = updatedPaymentInfo.getScheduleId();
		System.out.println("this is test :: ------------------------------------" + parameters.get("STATUS"));
		if(parameters.get("STATUS").equals("TXN_SUCCESS"))
		{
			schedulDao.updateStatus(1, sch.getId());
		}
		else
		{
			schedulDao.updateStatus(2, sch.getId());
			//sch.setStatus(1);
			//schedulDao.saveSch(sch, updatedPaymentInfo.getDoctorHosId().getId(), updatedPaymentInfo.getPatientId().getId());
		}
		
	
		
		
		
		isValideChecksum = CheckSumServiceHelper.getCheckSumServiceHelper().verifycheckSum(env.getProperty(PaymentConstants.MERCHANT_KEY),parameters,paytmChecksum);
		if(!isValideChecksum){
			throw new DPMAPIException("0076", new String[] {paymentInfo.getTxn_id()}, null);
		}
		response.setContentType("application/json;charset=UTF-8");
		ObjectMapper mapper = new ObjectMapper();
// 		response.getWriter().write(mapper.writeValueAsString(updatedPaymentInfo));
 		response.sendRedirect(env.getProperty("APP_REDIRECT_AFTER_PAYMENT") + updatedPaymentInfo.getScheduleId().getId().toString() + "/" + parameters.get("STATUS"));

 		
		
	}
	
	@RequestMapping(value = "/payment", method = RequestMethod.POST, produces="text/html")
    public void proccedPayment(HttpServletRequest request, @Validated com.codebots.dpm.reqres.PaymentInfo paymentDetails,
                                   HttpServletResponse response) throws Exception {
		
		//load patient details
		//load doctorhospital details
		//load price
		
		
		Patient patient = patientDao.get(paymentDetails.getPatientId());
		DoctorHospital docHos = docHosDao.getById(paymentDetails.getDoctorHospitalId());
		String custId = paymentDetails.getPatientId().toString();
		String amount = String.valueOf(docHos.getFees());
		String emailId = patient.getEmail();
		String mobileNumber = patient.getMobileNumber();
		
		Schedule sch = new Schedule();
		sch.setDoctorHospitalId(docHos);
		sch.setPatientId(patient);
		sch.setDate(paymentDetails.getDate());
		sch.setStartTime(paymentDetails.getStartTime());
		sch.setEndTime(paymentDetails.getEndTime());
		sch.setRemarks(paymentDetails.getRemarks());
		sch.setStatus(0);
			
			com.codebots.dpm.spring.model.PaymentInfo paymentInfo = new com.codebots.dpm.spring.model.PaymentInfo();
			paymentInfo.setDoctorHosId(docHos);
			paymentInfo.setPatientId(patient);
			paymentInfo.setAmount(docHos.getFees());
			
			paymentInfo.setApp_status("Initialized");
			//create schedule
			
//			Schedule sch = new Schedule();
//			sch.setDoctorHospitalId(docHos);
//			sch.setPatientId(patient);
//			sch.setDate(paymentDetails.getDate());
//			sch.setStartTime(paymentDetails.getStartTime());
//			sch.setEndTime(paymentDetails.getEndTime());
//			sch.setRemarks(paymentDetails.getRemarks());
//			sch.setStatus(0);

			paymentInfo.setScheduleId(sch);

			com.codebots.dpm.spring.model.PaymentInfo savedPaymentInfo = paymentDao.save(paymentInfo);
			
			String orderId = savedPaymentInfo.getId().toString();
			
			request.setAttribute(View.RESPONSE_STATUS_ATTRIBUTE, HttpStatus.TEMPORARY_REDIRECT);
			TreeMap<String,String> parameters = new TreeMap<String,String>();
			parameters.put(PaymentConstants.MID,env.getProperty(PaymentConstants.MID));
			parameters.put(PaymentConstants.CHANNEL_ID,env.getProperty(PaymentConstants.CHANNEL_ID));
			parameters.put(PaymentConstants.INDUSTRY_TYPE_ID,env.getProperty(PaymentConstants.INDUSTRY_TYPE_ID));
			parameters.put(PaymentConstants.WEBSITE,env.getProperty(PaymentConstants.WEBSITE));
			parameters.put(PaymentConstants.MOBILE_NO,mobileNumber);
			parameters.put(PaymentConstants.EMAIL,emailId);
			parameters.put(PaymentConstants.CALLBACK_URL, env.getProperty(PaymentConstants.CALLBACK_URL));
			parameters.put(PaymentConstants.ORDER_ID, orderId);
			parameters.put(PaymentConstants.CUST_ID, custId);
			parameters.put(PaymentConstants.TXN_AMOUNT, amount);
						
			String checkSum =  CheckSumServiceHelper.getCheckSumServiceHelper().genrateCheckSum(env.getProperty(PaymentConstants.MERCHANT_KEY), parameters);

			StringBuilder outputHtml = new StringBuilder();
			outputHtml.append("<!DOCTYPE html PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>");
			outputHtml.append("<html>");
			outputHtml.append("<head>");
			outputHtml.append("<title>Merchant Check Out Page</title>");
			outputHtml.append("</head>");
			outputHtml.append("<body>");
			outputHtml.append("<center><h1>Please do not refresh this page...</h1></center>");
			outputHtml.append("<form method='post' action='"+ env.getProperty(PaymentConstants.PAYTM_URL) +"' name='f1'>");
			outputHtml.append("<table border='1'>");
			outputHtml.append("<tbody>");

			for(Map.Entry<String,String> entry : parameters.entrySet()) {
				String key = entry.getKey();
				String value = entry.getValue();
				System.out.println(key + ":" + value);
				outputHtml.append("<input type='hidden' name='"+key+"' value='" +value+"'>");	
			}	  

			outputHtml.append("<input type='hidden' name='CHECKSUMHASH' value='"+checkSum+"'>");
			outputHtml.append("</tbody>");
			outputHtml.append("</table>");
			outputHtml.append("<script type='text/javascript'>");
			outputHtml.append("document.f1.submit();");
			outputHtml.append("</script>");
			outputHtml.append("</form>");
			outputHtml.append("</body>");
			outputHtml.append("</html>");
			response.setContentType("text/html;charset=UTF-8");
			response.getWriter().write(outputHtml.toString());	
	
		
	}
	
	
	
	
	
}
