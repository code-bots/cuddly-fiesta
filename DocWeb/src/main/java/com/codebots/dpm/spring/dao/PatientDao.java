package com.codebots.dpm.spring.dao;


import java.util.UUID;

import com.codebots.dpm.reqres.Pagination;
import com.codebots.dpm.reqres.PasswordChangeRequestBody;
import com.codebots.dpm.reqres.PatientRequestBody;
import com.codebots.dpm.spring.exception.DPMAPIException;
import com.codebots.dpm.spring.model.Patient;

public interface PatientDao {

	Patient get(UUID id);
	Pagination list(int pageNumber,int pageSize,String type,String feild);
	Patient update(UUID id,Patient patient) throws DPMAPIException;
	void delete(UUID id);
	void resetPassword(PasswordChangeRequestBody passchange,UUID patient)throws DPMAPIException;
	String getUsername(UUID patient_id);
	Patient save(PatientRequestBody patient)
			throws DPMAPIException;
	Patient get(String userName);
	
}
