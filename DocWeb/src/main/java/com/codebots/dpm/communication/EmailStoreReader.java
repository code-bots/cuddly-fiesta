package com.codebots.dpm.communication;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.Session;

import com.codebots.dpm.reqres.EmailFolderDetails;
import com.codebots.dpm.reqres.EmailItem;
import com.codebots.dpm.spring.exception.DPMAPIException;

public class EmailStoreReader {
	
	private String hostName;
	private int port;
	private String userName;
	private String password;
	
	public EmailStoreReader(String hostName, int port, String userName, String password) {
		this.hostName = hostName;
		this.port = port;
		this.userName = userName;
		this.password = password;
	}
	
	public List<EmailFolderDetails> getFolderDetails() throws DPMAPIException
	{
		Properties props = System.getProperties();
		props.setProperty("mail.store.protocol", "imap");
		props.put("mail.smtp.socketFactory.fallback", "true"); 
		List<EmailFolderDetails> eFolders = new ArrayList<>();
		try {
		    Session session = Session.getDefaultInstance(props, null);
		    javax.mail.Store store = session.getStore("imap");
		    store.connect(hostName, port, userName, password);
		    javax.mail.Folder[] folders = store.getDefaultFolder().list("*");
		    for (javax.mail.Folder folder : folders) {
		        if ((folder.getType() & javax.mail.Folder.HOLDS_MESSAGES) != 0) {
		        	EmailFolderDetails fld = new EmailFolderDetails();
		        	fld.setFolderName(folder.getFullName());
		        	fld.setTotalCount(folder.getMessageCount());
		        	fld.setUnReadCount(folder.getUnreadMessageCount());
		        	eFolders.add(fld);
		        }
		    }
		} catch (MessagingException e) {
			throw new DPMAPIException("0053", new String[] {e.getMessage()}, e);
		}
		return eFolders;
	}
	
	public List<EmailItem> getFolderItems(String folderName, int startIndex, int endIndex) throws DPMAPIException
	{
		Properties props = System.getProperties();
		props.setProperty("mail.store.protocol", "imap");
		props.put("mail.smtp.socketFactory.fallback", "true");
		List<EmailItem> emailItems = new ArrayList<>();
		try {
		    Session session = Session.getDefaultInstance(props, null);
		    javax.mail.Store store = session.getStore("imap");
		    store.connect(hostName, port, userName, password);
		    javax.mail.Folder folder = store.getFolder(folderName);
		    folder.open(Folder.READ_ONLY);
		    int maxCount = folder.getMessageCount();
		    if(endIndex > maxCount) endIndex = maxCount; 
		    Message[] msgs = folder.getMessages(startIndex, endIndex);
		    for (Message message : msgs) {
				EmailItem item = new EmailItem(message.getSubject(), String.valueOf(message.getContent()), message.getFrom()[0].toString());
				if(folderName.equals("Sent"))
				{
					item.setEmailTime(message.getSentDate());
				}else {
					item.setEmailTime(message.getReceivedDate());
				}
				javax.mail.Address[] toAddrs = message.getRecipients(RecipientType.TO); 
				if(toAddrs!=null){
					for (javax.mail.Address rAddress : toAddrs)
					{
						item.addReceiver(rAddress.toString(), "TO");
					}
				}
				javax.mail.Address[] ccAddrs = message.getRecipients(RecipientType.CC);
				if(ccAddrs!=null)
				for (javax.mail.Address rAddress : ccAddrs)
				{
					item.addReceiver(rAddress.toString(), "CC");
				}
				emailItems.add(item);
			}
		} catch (MessagingException | IOException e) {
			throw new DPMAPIException("0054", new String[] {e.getMessage()}, e);
		}
		return emailItems;
	}
	
	
	public static void main(String[] args) throws DPMAPIException {
		EmailStoreReader reader = new EmailStoreReader("localhost", 143, "1526226989728@stm.com", "a1b2c3x0y9z8");
		reader.getFolderItems("INBOX", 1, 10);
	}
}
