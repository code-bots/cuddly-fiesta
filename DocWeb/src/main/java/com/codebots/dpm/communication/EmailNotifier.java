package com.codebots.dpm.communication;

import java.util.Enumeration;
import java.util.Properties;
import java.util.ResourceBundle;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.codebots.dpm.spring.exception.DPMAPIException;

public class EmailNotifier {
	public static void main(String[] args) throws DPMAPIException {
		EmailNotifier.sendEmailNotification("gcchavda@gmail.com", "test subject", "<b>test</b>");
	}
	public static void sendEmailNotification(String toAddress, String subject, String body) throws DPMAPIException
	{
		String username = null;
		String password = null;
		Properties props = new Properties();
		ResourceBundle bundle = ResourceBundle.getBundle("notification_creds");
		Enumeration<String> propKeys = bundle.getKeys();
		
		while(propKeys.hasMoreElements())
		{
			String key = propKeys.nextElement();
			String value = bundle.getString(key);
			if(key.equals("mail.username"))
			{
				username = value;
			}else if(key.equals("mail.password"))
			{
				password = value;
			}else
			{
				props.put(key, value);
			}
		}
		final String user = username;
		final String pass = password;
		Session session = Session.getInstance(props,
		  new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(user, pass);
			}
		  });

		try {

			Message message = new MimeMessage(session);
			message.setContent(body, "text/html; charset=utf-8");
			message.setFrom(new InternetAddress(user));
			message.setRecipients(Message.RecipientType.TO,
				InternetAddress.parse(toAddress));
			message.setSubject(subject);
//			message.setText(body);
			Transport.send(message);
		} catch (MessagingException e) {
			throw new DPMAPIException("0055", new String[]{e.getMessage()},e);
		}
	}
}
