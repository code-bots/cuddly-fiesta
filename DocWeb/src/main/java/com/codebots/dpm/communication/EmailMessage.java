package com.codebots.dpm.communication;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class EmailMessage {
	private String sender;
	private String[] to;
	private String[] cc;
	private String subject;
	private String body;
	private String smtpHost;
	private String smtpPort;
	
	public EmailMessage(String sender, String[] to, String[] cc, String subject, String body, String smtpHost, String smtpPort) {
		this.sender = sender;
		this.to = to;
		this.cc = cc;
		this.subject = subject;
		this.body = body;
		this.smtpHost = smtpHost;
		this.smtpPort = smtpPort;
	}

	public void send() {
		Properties mailServerProperties = new Properties();
		mailServerProperties.put("mail.smtp.host", this.smtpHost);
		mailServerProperties.put("mail.smtp.port", this.smtpPort);

		Session session = Session.getDefaultInstance(mailServerProperties);

		MimeMessage messageToSend = new MimeMessage(session);

		try {
			messageToSend.setFrom(new InternetAddress(this.sender));
			for (String toAddress : to) {
				messageToSend.addRecipient(Message.RecipientType.TO, new InternetAddress(toAddress));	
			}
			for (String ccAddress : cc) {
				messageToSend.addRecipient(Message.RecipientType.CC, new InternetAddress(ccAddress));	
			}
			messageToSend.setSubject(this.subject);
			messageToSend.setText(this.body);

			Transport.send(messageToSend);
		} catch (MessagingException ex) {
			ex.printStackTrace();
		}
	}

}