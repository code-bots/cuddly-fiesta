package com.codebots.dpm.common;

public class PaymentConstants {
	public static final String MID = "MID";
	public static final String CHANNEL_ID = "CHANNEL_ID";
	public static final String INDUSTRY_TYPE_ID = "INDUSTRY_TYPE_ID";
	public static final String WEBSITE = "WEBSITE";
	public static final String MOBILE_NO = "MOBILE_NO";
	public static final String EMAIL = "EMAIL";
	public static final String CALLBACK_URL = "CALLBACK_URL";
	public static final String ORDER_ID = "ORDER_ID";
	public static final String CUST_ID = "CUST_ID";
	public static final String TXN_AMOUNT = "TXN_AMOUNT";
	public static final String MERCHANT_KEY = "MERCHANT_KEY";
	public static final String PAYTM_URL = "PAYTM_URL";
}
