/*
SQLyog Community v13.0.1 (64 bit)
MySQL - 5.7.22-0ubuntu0.16.04.1 : Database - dpmdb
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`dpmdb` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `dpmdb`;

/*Tdable structure for table `app_role` */

DROP TABLE IF EXISTS `app_role`;

CREATE TABLE `app_role` (
  `id` int(11) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `role_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `app_role` */

insert  into `app_role`(`id`,`description`,`role_name`) values 
(1,'Doctor User, for specific permissions on resources','DOCTOR'),
(2,'Patient User, for patient related resource access','PATIENT'),
(3,'Admin User, for admin related resource access','ADMIN');

/*Table structure for table `app_user` */

DROP TABLE IF EXISTS `app_user`;

CREATE TABLE `app_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `isEnabled` tinyint(1) NOT NULL DEFAULT '0',
  `email_OTP` varchar(6) NOT NULL,
  `sms_OTP` varchar(6) NOT NULL,
  `isPresent` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unq_email` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;


INSERT INTO `app_user` (`id`,`password`,`username`,`isEnabled`,`email_OTP`,`sms_OTP`)
VALUES (10,'88d4266fd4e6338d13b845fcf289579d209c897823b9217da3e161936f031589','admin@docweb.com',1,00,00);

INSERT INTO `user_role` values (10,3);

/*Data for the table `app_user` */

/*Table structure for table `chatChannel` */

DROP TABLE IF EXISTS `chatChannel`;

CREATE TABLE `chatChannel` (
  `id` varchar(40) NOT NULL,
  `userIdOne` int(11) NOT NULL,
  `userIdTwo` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userIdOne` (`userIdOne`),
  KEY `userIdTwo` (`userIdTwo`),
  CONSTRAINT `chatChannel_ibfk_1` FOREIGN KEY (`userIdOne`) REFERENCES `app_user` (`id`),
  CONSTRAINT `chatChannel_ibfk_2` FOREIGN KEY (`userIdTwo`) REFERENCES `app_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `chatChannel` */

/*Table structure for table `chatMessage` */

DROP TABLE IF EXISTS `chatMessage`;

CREATE TABLE `chatMessage` (
  `id` int(40) NOT NULL AUTO_INCREMENT,
  `authorUserId` int(11) NOT NULL,
  `recipientUserId` int(11) NOT NULL,
  `contents` text NOT NULL,
  `timeSent` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `authorUserId` (`authorUserId`),
  KEY `recipientUserId` (`recipientUserId`),
  CONSTRAINT `chatMessage_ibfk_1` FOREIGN KEY (`authorUserId`) REFERENCES `app_user` (`id`),
  CONSTRAINT `chatMessage_ibfk_2` FOREIGN KEY (`recipientUserId`) REFERENCES `app_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

/*Data for the table `chatMessage` */

/*Table structure for table `doctor` */

DROP TABLE IF EXISTS `doctor`;

CREATE TABLE `doctor` (
  `id` varchar(36) NOT NULL,
  `firstName` varchar(255) NOT NULL,
  `lastName` varchar(255) NOT NULL,
  `qualification` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobileNumber` varchar(15) NOT NULL,
  `licNo` varchar(40) NOT NULL,
  `medicalCouncil` varchar(255) DEFAULT NULL,
  `awards` varchar(255) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `gender` varchar(6) NOT NULL,
  `addressLine` varchar(200) NOT NULL,
  `locality` varchar(30) NOT NULL,
  `city` varchar(50) NOT NULL,
  `pincode` varchar(8) NOT NULL,
  `state` varchar(50) NOT NULL,
  `userId` int(11) NOT NULL,
  `dpmEmailId` varchar(50) NOT NULL,
  `profilePic` blob,
  `title` varchar(255) DEFAULT NULL,
  `approvalFlag` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_docuser_user1` (`userId`),
  CONSTRAINT `fk_docuser_user1` FOREIGN KEY (`userId`) REFERENCES `app_user` (`id`)
) ENGINE=InnoDB;
/*Data for the table `doctor` */

/*Table structure for table `doctor_hospital` */

DROP TABLE IF EXISTS `doctor_hospital`;

CREATE TABLE `doctor_hospital` (
  `id` varchar(36) NOT NULL,
  `doctorId` varchar(36) NOT NULL,
  `latitude` varchar(50) NOT NULL,
  `longitude` varchar(50) NOT NULL,
  `hospStartTime` varchar(25) NOT NULL,
  `hospEndTime` varchar(25) NOT NULL,
  `weekdays` varchar(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `fees` bigint(20) NOT NULL,
  `timeslot` int(11) NOT NULL,
  `paymentMode` tinyint(5) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_doctor_1` (`doctorId`),
  CONSTRAINT `fk_doctor_1` FOREIGN KEY (`doctorId`) REFERENCES `doctor` (`id`)
) ENGINE=InnoDB;

/*Data for the table `doctor_hospital` */

/*Table structure for table `patient` */

DROP TABLE IF EXISTS `patient`;

CREATE TABLE `patient` (
  `id` varchar(36) NOT NULL,
  `firstName` varchar(255) NOT NULL,
  `lastName` varchar(255) NOT NULL,
  `dob` date NOT NULL,
  `height` varchar(8) DEFAULT NULL,
  `weight` varchar(8) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `mobileNumber` varchar(15) NOT NULL,
  `email` varchar(255) NOT NULL,
  `emergencyContact` varchar(15) DEFAULT NULL,
  `userId` int(11) NOT NULL,
  `existingDesease` varchar(1000) DEFAULT NULL,
  `familyHistory` varchar(1000) DEFAULT NULL,
  `allergic` varchar(1000) DEFAULT NULL,
  `remindPref` int(11) NOT NULL,
  `dpmEmailId` varchar(255) NOT NULL,
  `profilePic` blob,
  `gender` varchar(6) NOT NULL,
  `locality` varchar(30) NOT NULL,
  `city` varchar(50) NOT NULL,
  `pincode` varchar(8) NOT NULL,
  `state` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_patient_usr1` (`userId`),
  CONSTRAINT `fk_patient_usr1` FOREIGN KEY (`userId`) REFERENCES `app_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `patient` */

/*Table structure for table `surgery` */

DROP TABLE IF EXISTS `surgery`;

CREATE TABLE `surgery` (
  `id` varchar(36) NOT NULL,
  `patientId` varchar(36) NOT NULL,
  `datePerformed` date NOT NULL,
  `hospitalName` varchar(255) NOT NULL,
  `surgeryName` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_patient_1` (`patientId`),
  CONSTRAINT `fk_patient_1` FOREIGN KEY (`patientId`) REFERENCES `patient` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `surgery` */


/*Table structure for table `user_role` */

DROP TABLE IF EXISTS `user_role`;

CREATE TABLE `user_role` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  KEY `fk_app_user1` (`user_id`),
  KEY `fk_app_rile1` (`role_id`),
  CONSTRAINT `fk_app_rile1` FOREIGN KEY (`role_id`) REFERENCES `app_role` (`id`),
  CONSTRAINT `fk_app_user1` FOREIGN KEY (`user_id`) REFERENCES `app_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `payment_info` (
  `id` varchar(36) NOT NULL,
  `scheduleId` varchar(36) NOT NULL,
  `patientId` varchar(36) NOT NULL,
  `doctorHosId` varchar(36) NOT NULL,
  `txn_time` datetime DEFAULT NULL,
  `amount` bigint(20) NOT NULL,
  `txn_id` varchar(255) DEFAULT NULL,
  `app_status` varchar(20) DEFAULT NULL,
  `respcode` varchar(5) DEFAULT NULL,
  `gateway_status` varchar(50) DEFAULT NULL,
  `respmsg` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_payment_schedule` (`scheduleId`),
  KEY `fk_payment_patient` (`patientId`),
  KEY `fk_payment_docHos` (`doctorHosId`),
  CONSTRAINT `fk_payment_docHos` FOREIGN KEY (`doctorHosId`) REFERENCES `doctor_hospital` (`id`),
  CONSTRAINT `fk_payment_patient` FOREIGN KEY (`patientId`) REFERENCES `patient` (`id`),
  CONSTRAINT `fk_payment_schedule` FOREIGN KEY (`scheduleId`) REFERENCES `schedule` (`id`)
) ENGINE=InnoDB;



CREATE TABLE `schedule` (
  `id` varchar(36) NOT NULL,
  `date` date NOT NULL,
  `doctorHospitalId` varchar(36) NOT NULL,
  `patientId` varchar(36) NOT NULL,
  `startTime` time NOT NULL,
  `endTime` time NOT NULL,
  `remarks` varchar(10000) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `paymentStart` time DEFAULT NULL,
  `schStatus` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_schedule_DocHosId` (`doctorHospitalId`),
  KEY `fk_schedule_patient` (`patientId`),
  CONSTRAINT `fk_schedule_DocHosId` FOREIGN KEY (`doctorHospitalId`) REFERENCES `doctor_hospital` (`id`),
  CONSTRAINT `fk_schedule_patient` FOREIGN KEY (`patientId`) REFERENCES `patient` (`id`)
) ENGINE=InnoDB;

CREATE TABLE `blog` (
  `id` varchar(36) NOT NULL,
  `title` varchar(500) NOT NULL,
  `doctorId` varchar(36) NOT NULL,
  `content` text NOT NULL,
  `tags` varchar(500) NOT NULL,
  `creation_date` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_blog_doctorId` (`doctorId`),
  CONSTRAINT `fk_blog_doctorId` FOREIGN KEY (`doctorId`) REFERENCES `doctor` (`id`)
) ENGINE=InnoDB;

CREATE TABLE `comments` (
  `id` varchar(36) NOT NULL,
  `blogId` varchar(36) NOT NULL,
  `description` text NOT NULL,
  `auther_id` varchar(36) NOT NULL,
  `comment_date` datetime NOT NULL,
  `parentcomment_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_comments_blog` (`blogId`),
  CONSTRAINT `fk_comments_blog` FOREIGN KEY (`blogId`) REFERENCES `blog` (`id`)
) ENGINE=InnoDB;


CREATE TABLE `prescription` (
  `id` varchar(36) NOT NULL,
  `scheduleId` varchar(36) NOT NULL,
  `remarks` varchar(500) DEFAULT NULL,
  `nextAppointmentDate` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_prescription_schedule` (`scheduleId`),
  CONSTRAINT `fk_prescription_schedule` FOREIGN KEY (`scheduleId`) REFERENCES `schedule` (`id`)
) ENGINE=InnoDB;


CREATE TABLE `prescriptionDetails` (
  `id` varchar(36) NOT NULL,
  `prescriptionId` varchar(36) NOT NULL,
  `type` varchar(50) NOT NULL,
  `name` varchar(500) NOT NULL,
  `additionalInfo1` varchar(50) DEFAULT NULL,
  `additionalInfo2` varchar(50) DEFAULT NULL,
  `sideEffect` varchar(500) DEFAULT NULL,
  `remarks` varchar(500) NOT NULL,
  `additionalInfo3` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_prescriptionDetails_prescription` (`prescriptionId`),
  CONSTRAINT `fk_prescriptionDetails_prescription` FOREIGN KEY (`prescriptionId`) REFERENCES `prescription` (`id`)
) ENGINE=InnoDB;

CREATE TABLE `feedback` (
  `id` varchar(36) NOT NULL,
  `scheduleId` varchar(36) NOT NULL,
  `content` varchar(1000) NOT NULL,
  `star` bigint(20) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_feedback_schedule` (`scheduleId`),
  CONSTRAINT `fk_feedback_schedule` FOREIGN KEY (`scheduleId`) REFERENCES `schedule` (`id`)
) ENGINE=InnoDB;


CREATE TABLE `block` (
  `id` varchar(36) NOT NULL,
  `doctorId` varchar(36) NOT NULL,
  `date` date NOT NULL,
  `startTime` time NOT NULL,
  `endTime` time NOT NULL,
  `reason` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_block_doctorId` (`doctorId`),
  CONSTRAINT `fk_block_doctorId` FOREIGN KEY (`doctorId`) REFERENCES `doctor` (`id`)
) ENGINE=InnoDB; 

/*Data for the table `user_role` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;