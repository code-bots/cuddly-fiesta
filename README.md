# cuddly-fiesta
# STMWeb
This is web service for STM project. 

# install maven on your local machine
https://maven.apache.org/download.cgi

# Create/Change as per your local configuration
1. db.properties [MySQL URL, User, Password]
2. Create database with name stmdb and execute db.sql file containing tables for the application

# Run following commands to work in development mode
1. CD STMWeb [Go inside STMWeb directory]
2. mvn clean install
3. mvn tomcat7:run
