import { Component, OnInit, ViewChild,
  TemplateRef, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { Subject } from 'rxjs/Subject';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { AuthenticationService } from '@app/core';
import { UserInfo } from '@app/shared/dataModels/userInfo';
import { ScheduleService } from '@app/schedule/schedule.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { Router } from '@angular/router';
import { NotificationService } from '@app/core/notification.service';
import { DatePipe } from '@angular/common';
import { AssignmentService } from '@app/assignments/assignment.service';
import { StudentService } from '@app/student/student.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.scss']
})
export class ScheduleComponent implements OnInit {

 
  userData: UserInfo;
  events;
  viewDate: Date = new Date();
  showCall = false;
  calendarOptions;
  modalRef: BsModalRef;
  bsModalRef: BsModalRef;
@ViewChild('template') modalContent: TemplateRef<any>;
@ViewChild('eventDetail') eventPopup: TemplateRef<any>;
  startTime: Date = new Date();
  endTime: Date = new Date();

  assignmentDetails: Array<any> = [];

  constructor(private modalService: BsModalService,
    private authenticationService: AuthenticationService,
    private notificationService: NotificationService,
    private scheduleService: ScheduleService, private cdr: ChangeDetectorRef,
    private router: Router, private datePipe: DatePipe,
    private assignmentService : AssignmentService,
    private studentService : StudentService
  ) {
  }

  ngOnInit() {
    this.authenticationService.getUserDetails(this.authenticationService.credentials.token)
    .subscribe(
      data => {
        this.userData =  data;
      }
    );
    console.log("Init called by " + this.userData.first_name);
    this.loadCalenderEvents();
  }


  loadAssignmentForTutor()
  {
    this.assignmentService.getAssignmentsForTutor(this.userData.tutor_id,'','').subscribe(
      data =>{
        this.assignmentDetails = data;
        console.log("Assignment for tutor Loaded");
      }
    );
  }

  loadCalenderEvents() {
    console.log("one");
    const __this = this;
    console.log("one");
    if (this.userData.user_id.roles[0].roleName === 'STUDENT') {
      console.log("one");
      this.studentService.getAssignmentOfStudent(this.userData.student_id).subscribe(
        data => {
          console.log("Enter in calender")
        this.events = this.getCalenderEvents(data);
        console.log("called");
        this.calendarOptions = {
          contentHeight: 600,
          selectable: true,
      //    themeSystem: 'bootstrap4',
          fixedWeekCount : false,
          defaultDate: this.viewDate,
          editable: false,
          eventLimit: true, 
          defaultView: 'month',
          header: {
            left: 'month,agendaWeek,agendaDay,listWeek',
            center: 'title',
            right: 'today prevYear,prev,next,nextYear'
          },
          footer: {
            center: '',
            right: 'prev,next'
          },
          eventClick: function(eventObj: any) {
            __this.setObjectAndSHowPopup(eventObj);
          },
          dayClick: function(date: any) {
          },
          events: this.events
        };
        console.log("fine");
        this.showCall = true;
        },
        err => console.log('error in loading events')
      );
    }
    else
    {
      console.log("Enter in Doctor");
      this.assignmentService.getAssignmentsForTutor(this.userData.tutor_id,'','').subscribe(
        data => {
          console.log("Enter in calender doctor")
        this.events = this.getCalenderEvents(data);
        console.log("called..");
        this.calendarOptions = {
          contentHeight: 600,
          selectable: true,
      //    themeSystem: 'bootstrap4',
          fixedWeekCount : false,
          defaultDate: this.viewDate,
          editable: false,
          eventLimit: true, 
          defaultView: 'month',
          header: {
            left: 'month,agendaWeek,agendaDay,listWeek',
            center: 'title',
            right: 'today prevYear,prev,next,nextYear'
          },
          footer: {
            center: '',
            right: 'prev,next'
          },eventClick: function(eventObj: any) {
            __this.setObjectAndSHowPopup(eventObj);
          },
          dayClick: function(date: any) {
          },
          events: this.events
        };
        console.log("fine..");
        this.showCall = true;
        },
        err => console.log('error in loading events')
      );
    }
    console.log("Again fine..");
  }


  setObjectAndSHowPopup(event: any) {
    this.modalRef = this.modalService.show(this.eventPopup, {class: 'modal-sm'});
    setTimeout(function () {
      $('#eventTitle').html(event.title);
      }, 0);
  }
  getCalenderEvents(data: any): any {
    const events = [];
    if (this.userData.user_id.roles[0].roleName === 'TUTOR')
    {
      data.forEach(d => {
        const schedule = d;
      events.push({
          title: this.getTitleForSchedule(schedule),
          start: this.getDateForEvent(schedule.due_date),
        // end: this.getDateForEvent(schedule.assignment_id.due_date),
          color: '#ff5c56',
          type: 'schedule'
        });
      });
    }
    else
    {
      data.forEach(d => {
        const schedule = d;
      events.push({
          title: this.getTitleForSchedule(schedule),
          start: this.getDateForEvent(schedule.assignment_id.due_date),
        // end: this.getDateForEvent(schedule.assignment_id.due_date),
          color: '#ff5c56',
          type: 'schedule'
        });
      });
  }
  //  console.log(event);
    return events;
  }



  getTitleForSchedule(d: any) {
    if (this.userData.user_id.roles[0].roleName === 'TUTOR') {
      return ' Due Date of : ' + d.title  +  '(' + d.due_date + ')';
    } else {
      console.log('Due Date of ' + d.assignment_id.title + ' is ' + d.assignment_id.due_date);
      return ' Due Date of : ' + d.assignment_id.title +  '(' + d.assignment_id.due_date + ')';
    }
  }
  decline(): void {
    this.modalRef.hide();
  }

  getDateForEvent(date: string): string {
    const actualDate = date;
    console.log("actual date " + actualDate);
    return actualDate;
  }
}