import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';


import { CoreModule } from '@app/core';
import { SharedModule } from '@app/shared';

import { ScheduleRoutingModule } from '@app/schedule/schedule-routing.module';
import { ScheduleComponent } from '@app/schedule/schedule.component';
import { ScheduleService } from '@app/schedule/schedule.service';
import { CalendarModule } from 'ap-angular2-fullcalendar';
import { ModalModule, ButtonsModule, PopoverModule, TimepickerModule, BsDatepickerModule } from 'ngx-bootstrap';
import { ToastyModule } from 'ng2-toasty';
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    BrowserModule,
     ScheduleRoutingModule,
    CoreModule,
    SharedModule,
    CalendarModule.forRoot(),
    ModalModule.forRoot(),
    ButtonsModule.forRoot(),
    PopoverModule.forRoot(),
    ToastyModule.forRoot(),
    TimepickerModule.forRoot(),
    BsDatepickerModule.forRoot()
  ],
  declarations: [
    ScheduleComponent
  ],
  providers : [ScheduleService,
               DatePipe]
})
export class ScheduleModule { }
