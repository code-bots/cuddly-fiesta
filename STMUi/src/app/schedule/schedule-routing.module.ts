import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Route, extract } from '@app/core';
import { ScheduleComponent } from '@app/schedule/schedule.component';


const routes: Routes = [
  Route.withShell([
    { path: 'schedule', component: ScheduleComponent, data: { title: extract('Schedule') } }
  ])
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class ScheduleRoutingModule { }
