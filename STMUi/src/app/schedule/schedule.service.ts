import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { Http, Headers, Response } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';

export interface Credentials {
  // Customize received credentials here
  username: string;
  token: string;
}

const credentialsKey = 'credentials';

@Injectable()
export class ScheduleService {

  private _credentials: Credentials | null;

  constructor(private http: HttpClient) {
    const savedCredentials = sessionStorage.getItem(credentialsKey) || localStorage.getItem(credentialsKey);
    if (savedCredentials) {
      this._credentials = JSON.parse(savedCredentials);
    }
  }

  saveAssignment(context: any, tutor_id: string, institute_id: string) {
    return this.http.post('/STMWeb/rest/api/assignment/' + tutor_id
    + '/' + institute_id
      , context, this.getHeaderForRequest());
  }

  getCalenderEventsForStudents(studentId: string) {
   
    return this.http.get<any>('/STMWeb/rest/api/schedule/student/' + studentId,
    this.getHeaderForRequest());
  }

  getCalenderEventsForTutor(tutorId: string) {
   
    return this.http.get<any>('/STMWeb/rest/api/schedule/tutor/' + tutorId,
      this.getHeaderForRequest());
  }


  getHeaderForRequest() {
    const httpOptions = {
      headers: new HttpHeaders({ 'Authorization': 'bearer ' + this._credentials.token
    })};
    return httpOptions;
  }
}
