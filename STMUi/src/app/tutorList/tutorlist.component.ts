import { Component, OnInit,OnChanges } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthenticationService } from '@app/core/authentication/authentication.service';
import { UserInfo } from '@app/shared/dataModels/userInfo';
import { TutorListService } from '@app/tutorList/tutorList.service';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { ADMIN } from '@app/shared/dataModels/constData';
import { BlogService } from '@app/blog/blog.service';
import { clone } from 'lodash';
import { ProfileService } from '@app/profile/profile.service';
import { NotificationService } from '@app/core/notification.service';
import { ExcelService } from '@app/shared/excelService';

@Component({
    selector: 'app-tutorlist',
    templateUrl: './tutorList.component.html',
    styleUrls: ['./tutorlist.component.scss']
})
export class TutorListComponent implements OnInit,OnChanges {
    
    userData: UserInfo;
    tutors: Array<any> = [];
    page = 1;
    pageSize = 500;
    isAdminUser = false;
    totalRecord = 0;
    searchTerm = '';

    constructor(private router: Router,private authenticationService: AuthenticationService,
      private tutorListService : TutorListService,private excelService:ExcelService,private domSanitizer: DomSanitizer,private profileService : ProfileService, private notificationService: NotificationService) {
       }

    ngOnChanges()
    {
      console.log("ngOnChanges called");
    }

    ngOnInit() {
      const savedCredentials = sessionStorage.getItem('credentials') || localStorage.getItem('credentials');
      if (savedCredentials) {
        const credentials = JSON.parse(savedCredentials);
        if (credentials.username === ADMIN) {
          console.log("login admin");
          this.loadTutorsAd();
          this.isAdminUser=true;
        } else {
          this.authenticationService.getUserDetails(this.authenticationService.credentials.token)
          .subscribe(
            data => {
              this.userData = data;
              this.loadTutors(1);
            });
        }
    }
      }

      loadTutorsAd() {
        this.tutorListService.loadForAdmin(this.page, this.pageSize).subscribe(
          response => {
            this.tutors = response.listOfObjects;
            this.totalRecord = response.totalRecord;
          }, error => {
            // error handling
          }
        );
      }

      loadTutors(status : number) {
        this.tutorListService.getTutors(status,this.page, this.pageSize, 'asc', 'first_name').subscribe(
          response => {
            this.tutors = response.listOfObjects;
            this.totalRecord = response.totalRecord;
          }, error => {
            // error handling
          }
        );
      }

      flag : number;
      
      changeStatus(tutor : any)
      {
         const id = tutor.tutor_id;
        if(tutor.approvalFlag === 1)
        {
          this.flag = 0;
        }
        else
        {
          this.flag = 1;
        }
        this.tutorListService.approvTutor(id,this.flag).subscribe
        (
          data =>
            {
              this.notificationService.showNotification('Status Changed',
          'Tutor is ' + (tutor.approvalFlag === 1 ? 'approved.' : 'dis-approved'), 'success');
            this.loadTutorsAd();
            }
        );
        
      }
      pageChanged($event: any) {
        this.page = $event.page;
        this.loadTutors(this.isAdminUser ? -1 : 1);
      }
    
      gotToPatientTimeLine(patient: any) {
        this.router.navigate(['/tutorlist', patient.id], { replaceUrl: false });
      }

      getPrint() {
        this.tutorListService.exportTutor(1).subscribe(data=>{
          console.log(data);
          this.excelService.exportAsExcelFile(data, 'Tutor');
        })
        
     }

}   

