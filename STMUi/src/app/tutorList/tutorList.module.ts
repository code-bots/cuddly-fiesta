import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { TranslateModule } from "@ngx-translate/core";
import { CoreModule } from "@app/core";
import { SharedModule } from "@app/shared";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { TreeModule } from 'angular-tree-component';
import { TutorListComponent } from "@app/tutorList/tutorlist.component";
import { TutorListService } from "@app/tutorList/tutorList.service";
import { TutorListRoutingModule } from "@app/tutorList/tutorList.routing";
import { NgPipesModule } from "ngx-pipes";
import { AlertModule, BsDatepickerModule, ButtonsModule, TimepickerModule, ModalModule,
    PopoverModule, RatingModule, PaginationModule, TabsModule } from 'ngx-bootstrap';
import { ToastyModule } from "ng2-toasty";
@NgModule({
    imports: [
      CommonModule,
      NgPipesModule,
      TranslateModule,
      CoreModule,
      SharedModule,
      FormsModule,
      ReactiveFormsModule,
      TutorListRoutingModule,
      PaginationModule.forRoot(),
      ButtonsModule.forRoot(),
      PaginationModule.forRoot(),
      RatingModule.forRoot(),
      ToastyModule.forRoot()
    ],
    declarations: [
        TutorListComponent
    ],
    providers: [
        TutorListService
    ]
  })
  export class TutorListModule { }