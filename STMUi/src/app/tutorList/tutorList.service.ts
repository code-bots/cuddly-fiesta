import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, Headers, Response } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
const credentialsKey = 'credentials';
export interface Credentials {
    // Customize received credentials here
    username: string;
    token: string;
  }

@Injectable()
export class TutorListService {
    private _credentials: Credentials | null;

    constructor(private http: HttpClient) {
      const savedCredentials = sessionStorage.getItem(credentialsKey) || localStorage.getItem(credentialsKey);
      if (savedCredentials) {
        this._credentials = JSON.parse(savedCredentials);
      }
    }

      getHeaderForRequest() {
    const httpOptions = {
      headers: new HttpHeaders({ 'Authorization': 'bearer ' + this._credentials.token
    })};
    return httpOptions;
  }


      approvTutor(tutorId : string,status : number)
      {
        return this.http.put('/STMWeb/rest/api/approv/tutor/' + tutorId + '/' + status , {},this.getHeaderForRequest())
      }


      getTutors(status : number,page: number, pageSize: number, type: string, field: string)
      {
        return this.http.get<any>('/STMWeb/rest/api/tutorgrid?page=' + page + '&size=' + pageSize +
         '&type=' + type + '&feild=' + field + '&status=' + status, this.getHeaderForRequest());    
        //http://localhost:8080/STMWeb/rest/api/tutorgrid
      }

      loadForAdmin(page: number, pageSize: number)
      {
        console.log('/STMWeb/rest/api/tutorgrid?page=' + page + '&size=' + pageSize);
        return this.http.get<any>('/STMWeb/rest/api/tutorgrid?page=' + page + '&size=' + pageSize , this.getHeaderForRequest());    
        //http://localhost:4200/STMWeb/rest/api/tutorgrid?page=1&size=50&status=
      }
     
      exportTutor(status : number)
      {
        return this.http.get<any>('/STMWeb/rest/api/print/tutor?status=' + status , this.getHeaderForRequest());    
      }

}
