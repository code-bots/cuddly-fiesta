import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Route, extract } from '@app/core';
import { TutorListComponent } from '@app/tutorList/tutorlist.component';


const routes: Routes = [
    Route.withShell([
      { path: 'tutorlist', component: TutorListComponent, data: { title: extract('Tutor') } }
    ])
  ];
  
  @NgModule({
    imports: [RouterModule.forChild(routes)],   
    exports: [RouterModule],
    providers: []
  })
  export class TutorListRoutingModule { }
  