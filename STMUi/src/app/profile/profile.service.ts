import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, Headers, Response } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

export interface Credentials {
  // Customize received credentials here
  username: string;
  token: string;
}

const credentialsKey = 'credentials';

@Injectable()
export class ProfileService {

  private _credentials: Credentials | null;

  constructor(private http: HttpClient) {
    const savedCredentials = sessionStorage.getItem(credentialsKey) || localStorage.getItem(credentialsKey);
    if (savedCredentials) {
      this._credentials = JSON.parse(savedCredentials);
    }
  }

  getHeaderForRequest() {
    const httpOptions = {
      headers: new HttpHeaders({ 'Authorization': 'bearer ' + this._credentials.token
    })};
    return httpOptions;
  }

 

  saveAcademic(academicId : string,context: any, student_id: string) {
    if(academicId !== '-1')
    {
      return this.http.put('/STMWeb/rest/api/student/' + student_id + '/academics/'+ academicId
      , context, this.getHeaderForRequest());
    }
    else{
      return this.http.post('/STMWeb/rest/api/student/' + student_id + '/academics'
      , context, this.getHeaderForRequest());
    }
    
  }

  getAcademicsForStudent(student_id: string) {
    return this.http.get<any[]>('/STMWeb/rest/api/student/' + student_id + '/academics',
    this.getHeaderForRequest());
  }

  getOTPForForgetPassword(contex:any) {
    return this.http.post('/STMWeb/rest/api/forget',contex,
    this.getHeaderForRequest());
  }

  saveInstitute(instituteId : string,context: any, tutor_id: string) {
    if(instituteId !== '-1')
    {
      return this.http.put('/STMWeb/rest/api/institute/' + instituteId,context,
      this.getHeaderForRequest());
    }
    else
    {
      return this.http.post('/STMWeb/rest/api/institute/' + tutor_id
      , context, this.getHeaderForRequest());
    }
    
  }


  // saveHospital(hospitalId: string, context: any, docId: string) {
  //   const headers =  new Headers();
  //   headers.append('Authorization', 'bearer ' + this._credentials.token);
  //   if (hospitalId !== '-1') {
  //     return this.http.put('/DocWeb/rest/api/hospital/' + hospitalId
  //     , context, this.getHeaderForRequest());
  //   } else {
  //     return this.http.post('/DocWeb/rest/api/hospital/' + docId
  //     , context, this.getHeaderForRequest());
  //   }
  // }


  getInstituteForTutor(tutor_id: string) {
    return this.http.get<Array<any>>('/STMWeb/rest/api/institute/' + tutor_id,
    this.getHeaderForRequest());
  }

  updateTutorProfile(tutor_id : string,context : any)
  {
    return this.http.put('/STMWeb/rest/api/tutor/' + tutor_id,context,
    this.getHeaderForRequest());

    // http://localhost:8080/STMWeb/rest/api/tutor/{tutorId}
  }


  updateInstitute(instituteId : string,context : any)
  {
    return this.http.put('/STMWeb/rest/api/institute/' + instituteId,context,
    this.getHeaderForRequest());

    // http://localhost:8080/STMWeb/rest/api/institute/{tutorId}
  }



  updateStudentProfile(student_id : string,context : any)
  {
    return this.http.put('/STMWeb/rest/api/student/' + student_id,context,
    this.getHeaderForRequest());

    // http://localhost:8080/STMWeb/rest/api/student/{studentId}
  }


}
