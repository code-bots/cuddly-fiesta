import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { AuthenticationService } from '../core/authentication/authentication.service';
import { UserInfo } from '../shared/dataModels/userInfo';
import { DomSanitizer } from '@angular/platform-browser';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ProfileService } from '@app/profile/profile.service';
import { NotificationService } from '@app/core/notification.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss', '../assignments/form.scss']
})
export class ProfileComponent implements OnInit {

  quote: string;
  isLoading: boolean;
  dateVisit = new Date();
  userData: UserInfo;
  editSummaryDetails = false;
  tempUserSummary: string;
  addUpForm: FormGroup;
  fromYear = [] as number[];
  toYear = [] as number[];
  showForm = false;
  lat = 28.532616848211614 as number;
  lng = 77.25288623303231 as number;
  startTime: Date = new Date();
  endTime: Date = new Date();
  weekModal = {
    sun: false,
    mon: true,
    tue: true,
    wed: true,
    thu: true,
    fri: true,
    sat: false
  };
  academics = [] as any[];
  loadingA = false;
  institutes = [] as any[];
  instituteEdit;
  academicsEdit;
  studentRegistrationForm: FormGroup;
  tutorRegistrationForm: FormGroup;
  constructor(private authenticationService: AuthenticationService, private domSanitizer: DomSanitizer,
    private formBuilder: FormBuilder, private profileService: ProfileService,
    private notificationService: NotificationService, private router: Router) {
  }

  ngOnInit() {
    this.authenticationService.getUserDetails(this.authenticationService.credentials.token)
      .subscribe(
        data => {
          this.userData = data;
          this.isLoading = true;
          let startYear = new Date().getFullYear() - 30;
          const endYear = new Date().getFullYear();
          for (startYear; startYear <= endYear; startYear++) {
            this.fromYear.push(+startYear);
            this.toYear.push(+startYear);
          }
          console.log(this.fromYear);
          this.loadingA = true;
          if (this.userData.student_id) {
            this.profileService.getAcademicsForStudent(this.userData.student_id).subscribe(
              data1 => {
                this.academics = data1;
                this.loadingA = false;
                console.log(this.academics);
              }, err => {
                console.log('error in loading academics');
              }
            );
          } else {
            this.profileService.getInstituteForTutor(this.userData.tutor_id).subscribe(
              data1 => {
                this.institutes = data1;
                this.loadingA = false;
                console.log(this.institutes);
              }, err => {
                console.log('error in loading institute');
              }
            );
          }
          this.createForm();
          this.createEditForm();
        }
      );
  }

  onLocationChoose($event: any) {
    this.lat = $event.coords.lat;
    this.lng = $event.coords.lng;
    console.log(this.lat + '::' + this.lng);
  }

  createForm() {
    if (this.userData.tutor_id) {
      this.addUpForm = this.formBuilder.group({
        inst_startTime: '',
        inst_endTime: '',
        fees: ['', Validators.required],
        sun: ['false', Validators.required],
        mon: ['true', Validators.required],
        tue: ['true', Validators.required],
        wed: ['true', Validators.required],
        thu: ['true', Validators.required],
        fri: ['true', Validators.required],
        sat: ['false', Validators.required],
        name: ['', Validators.required]
      });
    } else {
      this.addUpForm = this.formBuilder.group({
        school: ['', Validators.required],
        from_year: ['', Validators.required],
        to_year: '-1',
        degree: ['', Validators.required],
        grade: null,
        description: null
      });
    }
  }


  editAcademic(academics : any)
  {
    this.academicsEdit = academics;
    if(this.academicsEdit)
    {
      this.addUpForm.controls['school'].setValue(this.academicsEdit.school);
      this.addUpForm.controls['from_year'].setValue(this.academicsEdit.from_year);
      this.addUpForm.controls['to_year'].setValue(this.academicsEdit.to_year);
      this.addUpForm.controls['degree'].setValue(this.academicsEdit.degree);
      this.addUpForm.controls['grade'].setValue(this.academicsEdit.grade);
      this.addUpForm.controls['description'].setValue(this.academicsEdit.description);
      this.showForm = true;
    }

  }


  editInstitute(institute : any)
  {
    this.instituteEdit = institute;

    if(this.instituteEdit)
    {
      this.startTime = this.getTime(this.instituteEdit.inst_startTime);
      this.endTime = this.getTime(this.instituteEdit.inst_endTime);
      this.addUpForm.controls['fees'].setValue(this.instituteEdit.fees);
      this.lat = +this.instituteEdit.latitude;
      this.lng = +this.instituteEdit.longitude;
      this.addUpForm.controls['name'].setValue(this.instituteEdit.name);
      this.addUpForm.controls['sun'].setValue(this.instituteEdit.weekdays.indexOf('0') !== -1);
      this.addUpForm.controls['mon'].setValue(this.instituteEdit.weekdays.indexOf('1') !== -1);
      this.addUpForm.controls['tue'].setValue(this.instituteEdit.weekdays.indexOf('2') !== -1);
      this.addUpForm.controls['wed'].setValue(this.instituteEdit.weekdays.indexOf('3') !== -1);
      this.addUpForm.controls['thu'].setValue(this.instituteEdit.weekdays.indexOf('4') !== -1);
      this.addUpForm.controls['fri'].setValue(this.instituteEdit.weekdays.indexOf('5') !== -1);
      this.addUpForm.controls['sat'].setValue(this.instituteEdit.weekdays.indexOf('6') !== -1);
      this.showForm = true;
    }
  }


  getTime(time: any) {
    const readDate = new Date(+time);
    const currentDate = new Date();
    currentDate.setHours(readDate.getHours());
    currentDate.setMinutes(readDate.getMinutes());
    return currentDate;
  }

  createEditForm() {
    if (this.userData.tutor_id) {
      this.tutorRegistrationForm = this.formBuilder.group({
            first_name: [this.userData.first_name, Validators.required],
            last_name: [this.userData.last_name, Validators.required],
            email: [this.userData.email, Validators.required],
            address: [this.userData.address,Validators.required],
            locality:[this.userData.locality, Validators.required],
            city:[this.userData.city, Validators.required],
            state:[this.userData.state, Validators.required],
            pincode:[this.userData.pincode, Validators.required],
            mobile_number: [this.userData.mobile_number,[Validators.required, Validators.pattern('^((\\+91-?)|0)?[0-9]{10}$')]],
            dob: [this.userData.dob, Validators.required],
            qualification: [this.userData.qualification, Validators.required],
            profile_pic :this.userData.profile_pic

      });
    } else {
      this.studentRegistrationForm = this.formBuilder.group({
        first_name: [this.userData.first_name, Validators.required],
            last_name: [this.userData.last_name, Validators.required],
            blood_group: [this.userData.blood_group, Validators.required],
            food_preference: [this.userData.food_preference, Validators.required],
            address: [this.userData.address],
            locality:[this.userData.locality],
            city:[this.userData.city],
            state:[this.userData.state],
            pincode:[this.userData.pincode],
            mobile_number: [this.userData.mobile_number,[Validators.required, Validators.pattern('^((\\+91-?)|0)?[0-9]{10}$')]],
            email: [this.userData.email, Validators.required],
            dob: [this.userData.dob, Validators.required],
            emergency_contact: [this.userData.emergency_contact, Validators.required],
            father_name:[this.userData.father_name],
            father_qualification: [this.userData.father_qualification],
            father_dob: [this.userData.father_dob],
            father_profession: [this.userData.father_profession],
            mother_name:[this.userData.mother_name],
            mother_qualification: [this.userData.mother_qualification],
            mother_dob: [this.userData.mother_dob],
            mother_profession: [this.userData.mother_profession],
            remind_pref: ['1', Validators.required],
            profile_pic :this.userData.profile_pic
      });
    }
  }


  editSummary() {
    if (this.editSummaryDetails) {
      this.editSummaryDetails = false;
      return;
    }
    this.editSummaryDetails = true;
  }



  public updateSummary() {
    // call api to save the user data and on success perform the following steps
    if (this.userData.tutor_id) {
      const tutToSave = {
                first_name: this.tutorRegistrationForm.value.first_name,
                last_name: this.tutorRegistrationForm.value.last_name,
                address: this.tutorRegistrationForm.value.address,
                locality : this.tutorRegistrationForm.value.locality,
                city : this.tutorRegistrationForm.value.city,
                state : this.tutorRegistrationForm.value.state,
                pincode : this.tutorRegistrationForm.value.pincode,
                mobile_number: this.tutorRegistrationForm.value.mobile_number,
                email: this.tutorRegistrationForm.value.email,
                dob: this.tutorRegistrationForm.value.dob,
                qualification: this.tutorRegistrationForm.value.qualification,
                profile_pic: this.userData.profile_pic
      };

      const needRedirectionToLogin = this.userData.mobile_number !== tutToSave.mobile_number 
                                      || this.userData.email !== tutToSave.email;

      this.profileService.updateTutorProfile(this.userData.tutor_id, tutToSave).subscribe(
        data => {
          this.notificationService.showNotification('Success', 'Dr.' + tutToSave.first_name + ' ' + tutToSave.last_name +
            ' details successfully updated.', 'success');

            if (needRedirectionToLogin) {
              sessionStorage.removeItem('credentials');
              localStorage.removeItem('credentials');
              this.router.navigateByUrl(`/login`);
            } else {
              location.reload();
            }
          this.editSummaryDetails = false;
        }, err => {
          this.notificationService.showNotification('Error', 'Dr.' + tutToSave.first_name + ' ' + tutToSave.last_name +
            ' details not updated.', 'error');
        }
      );
    } else {
      const studentTobeSaved = {
        first_name: this.studentRegistrationForm.value.first_name,
        last_name: this.studentRegistrationForm.value.last_name,
        blood_group: this.studentRegistrationForm.value.blood_group,
        address: this.studentRegistrationForm.value.address,
        locality:this.studentRegistrationForm.value.locality,
        city:this.studentRegistrationForm.value.city,
        state:this.studentRegistrationForm.value.state,
        pincode:this.studentRegistrationForm.value.pincode,
        mobile_number: this.studentRegistrationForm.value.mobile_number,
        email: this.studentRegistrationForm.value.email,
        dob: this.studentRegistrationForm.value.dob,
        emergency_contact: this.studentRegistrationForm.value.emergency_contact,
        father_name : this.studentRegistrationForm.value.father_name,
        father_qualification: this.studentRegistrationForm.value.father_qualification,
        father_dob: this.studentRegistrationForm.value.father_dob,
        father_profession: this.studentRegistrationForm.value.father_profession,
        mother_name: this.studentRegistrationForm.value.mother_name,
        mother_qualification: this.studentRegistrationForm.value.mother_qualification,
        mother_dob: this.studentRegistrationForm.value.mother_dob,
        mother_profession: this.studentRegistrationForm.value.mother_profession,
        remind_pref: this.studentRegistrationForm.value.remind_pref,
        food_preference: this.studentRegistrationForm.value.food_preference,
        middel_name: 'test',
        profile_pic: this.userData.profile_pic
      };

      const needRedirectionToLogin = this.userData.mobile_number !== studentTobeSaved.mobile_number 
                                      || this.userData.email !== studentTobeSaved.email;
      this.profileService.updateStudentProfile(this.userData.student_id, studentTobeSaved).subscribe(
        data => {
          this.notificationService.showNotification('Success', 'Mr.' + studentTobeSaved.first_name + ' ' +
          studentTobeSaved.last_name + ' details successfully updated.', 'success');
          if (needRedirectionToLogin) {
            sessionStorage.removeItem('credentials');
            localStorage.removeItem('credentials');
            this.router.navigateByUrl(`/login`);
          } else {
            location.reload();
          }
          this.editSummaryDetails = false;
        }, err => {
          this.notificationService.showNotification('Error', 'Mr.' + studentTobeSaved.first_name  + ' ' +
          studentTobeSaved.last_name  + ' details not updated.', 'error');
        }
      );
    }
  }





  save() {
    const saveContext = this.addUpForm.value;
    if (this.userData.tutor_id) {
      saveContext.latitude = this.lat;
      saveContext.longitude = this.lng;
      saveContext.weekdays = this.getWeekdays();
      saveContext.inst_startTime = this.startTime.getTime();
      saveContext.inst_endTime = this.endTime.getTime();
      delete saveContext.sun;
      delete saveContext.mon;
      delete saveContext.tue;
      delete saveContext.wed;
      delete saveContext.thu;
      delete saveContext.fri;
      delete saveContext.sat;
      this.profileService.saveInstitute(this.instituteEdit ? this.instituteEdit.id : '-1'
                        ,saveContext, this.userData.tutor_id).subscribe(
        result => {
          this.showForm = false;
          this.loadingA = true;
          this.instituteEdit = undefined;
          this.profileService.getInstituteForTutor(this.userData.tutor_id).subscribe(
            data => {
              this.institutes = data;
              this.loadingA = false;
            }
          );
        }, err => {
          console.log('error in adding institute');
        }
      );
    } else {
      this.profileService.saveAcademic(this.academicsEdit ? this.academicsEdit.academic_id : '-1'
                                   ,saveContext, this.userData.student_id).subscribe(
        result => {
          this.showForm = false;
          this.loadingA = true;
          this.academicsEdit = undefined;
          this.profileService.getAcademicsForStudent(this.userData.student_id).subscribe(
            data => {
              this.academics = data;
              this.loadingA = false;
            }
          );
        },
        err => {
          console.log('error in adding academic');
        }
      );
    }
  }

  /** Called when user clicks on edit icon for summary */
  editUserSummary() {
    if (this.editSummaryDetails) {
      this.editSummaryDetails = false;
      return;
    }
    this.tempUserSummary = this.userData ? this.userData.aboutme : '';
    this.editSummaryDetails = true;
  }


  private getWeekdays(): string {
    const days = [];
    if (this.addUpForm.value.sun) {
      days.push(1);
    }
    if (this.addUpForm.value.mon) {
      days.push(2);
    }
    if (this.addUpForm.value.tue) {
      days.push(3);
    }
    if (this.addUpForm.value.wed) {
      days.push(4);
    }
    if (this.addUpForm.value.thu) {
      days.push(5);
    }
    if (this.addUpForm.value.fri) {
      days.push(6);
    }
    if (this.addUpForm.value.sat) {
      days.push(7);
    }
    return days.toString();
  }

  private getUIFriendlyWeeks(weekNos: string) {
    const weekNosArr = weekNos.split(',');
    const returnWeekNames = [];
    weekNosArr.forEach(weekNo => {
      returnWeekNames.push(this.getWeekName(weekNo));
    });
    return returnWeekNames.toString();
  }

  private getWeekName(weekNo: string) {
    switch (weekNo) {
      case '1': return 'Sun';
      case '2': return 'Mon';
      case '3': return 'Tue';
      case '4': return 'Wed';
      case '5': return 'Thu';
      case '6': return 'Fri';
      case '7': return 'Sat';
    }
  }
}
