import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { CoreModule } from '@app/core';
import { AlertModule, ButtonsModule, TimepickerModule, BsDatepickerModule } from 'ngx-bootstrap';
import { AgmCoreModule } from '@agm/core';
import { SharedModule } from '@app/shared';
import { ProfileRoutingModule } from '@app/profile/profile-routing.module';
import { ProfileComponent } from '@app/profile/profile.component';
import { ProfileService } from '@app/profile/profile.service';


@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    CoreModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    ProfileRoutingModule,
    ButtonsModule.forRoot(),
    TimepickerModule.forRoot(),
    BsDatepickerModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyD6Ouxewhop7JfB5KU0CyMf9IwtERmHlvE'
    })
  ],
  declarations: [
    ProfileComponent
  ],
  providers: [ProfileService]
})
export class ProfileModule { }
