import { Component, OnInit, Input } from '@angular/core';

import { Question, eAnswer } from '../dataModels/question-set.model';
import { TestResultsService } from '@app/test-results/test-results.service';

@Component({
  selector: 'app-add-question-grid',
  templateUrl: './add-question-grid.component.html',
  styleUrls: ['./add-question-grid.component.scss']
})
export class AddQuestionGridComponent implements OnInit {
  @Input() questionSetId: string;
  public answerList = [
    {title: eAnswer.a, value: eAnswer.a},
    {title: eAnswer.b, value: eAnswer.b},
    {title: eAnswer.c, value: eAnswer.c},
    {title: eAnswer.d, value: eAnswer.d},
  ];

  public settings = {
    columns: {
      question: {
        title: 'Question',
        width: '20%',
        editor: {
          type: 'textarea'
        }
      },
      option_A: {
        title: 'Option 1',
        width: '15%',
        editor: {
          type: 'textarea'
        }
      },
      option_B: {
        title: 'Option 2',
        width: '15%',
        editor: {
          type: 'textarea'
        }
      },
      option_C: {
        title: 'Option 3',
        width: '15%',
        editor: {
          type: 'textarea'
        }
      },
      option_D: {
        title: 'Option 4',
        width: '15%',
        editor: {
          type: 'textarea'
        }
      },
      correct_ans: {
        title: 'Answer',
        width: '10%',
        type: 'html',
        editor: {
          type: 'list',
          config: {
            list: this.answerList
          },
        },
        filter: {
          type: 'list',
          config: {
            list: this.answerList
          },
        }
      },
    },
    actions: {
      position: 'right',
      delete: true
    },
    add: {
      confirmCreate: true
    },
    edit: {
      confirmSave: true
    }
    // ,
    // delete: {
    //   confirmDelete: true
    // }
  };

  public data = [];

  constructor(private testResultsService: TestResultsService) { }

  ngOnInit() {
    this.getQuestionList();
  }

  public getQuestionList() {
    if (this.questionSetId && this.questionSetId.trim() !== '') {
      this.testResultsService.getQuestionListByQustionSetId(this.questionSetId)
        .subscribe(
          (result) => {
            this.data = result;
          },
          (error) => {
            console.log('Get Question failed', error);
          }
        );
    }
  }

  public onCreateConfirm(event): void {
    const question: Question = event.newData;
    this.testResultsService.saveQuestion(question, this.questionSetId)
      .subscribe(
        (result) => {
          event.confirm.resolve();
        },
        (error) => {
          event.confirm.reject();
          console.log('Question not save successfully', error);
        }
      );
  }

  public onEditConfirm(event): void {
    const question: Question = event.newData;
    console.log("questionId : " + question );
    this.testResultsService.updateQuestion(question, question.id)
      .subscribe(
        (result) => {
          event.confirm.resolve();
        },
        (error) => {
          event.confirm.reject();
          console.log('Question not update successfully', error);
        }
      );
  }

  public onDeleteConfirm(event): void {
    console.log("in");
    console.log(event)
 
    this.testResultsService.deleteQuestion(event.data.id).subscribe(data=>
      console.log("deleted"));
    
  }
}
