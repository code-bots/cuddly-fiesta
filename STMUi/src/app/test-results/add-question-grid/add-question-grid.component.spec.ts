import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddQuestionGridComponent } from './add-question-grid.component';

describe('AddQuestionGridComponent', () => {
  let component: AddQuestionGridComponent;
  let fixture: ComponentFixture<AddQuestionGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddQuestionGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddQuestionGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
