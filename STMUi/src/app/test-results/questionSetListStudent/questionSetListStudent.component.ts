import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { StudentListService } from '@app/studentList/studentList.service';
import { NotificationService } from '@app/core/notification.service';
import { UserInfo } from '@app/shared/dataModels/userInfo';
import { AuthenticationService } from '@app/core/authentication/authentication.service';
import { TestResultsService } from '@app/test-results/test-results.service';


@Component({
    selector: 'app-questionSetListStudent',
    templateUrl: './questionSetListStudent.component.html',
})
export class QuestionSetListStudentComponent implements OnInit {

  questionSetList : Array<any>;
    userData : UserInfo;
    searchTerm = '';
    
    constructor(private router: Router, private examService: TestResultsService,
        private authenticationService: AuthenticationService,
        private domSanitizer: DomSanitizer, private notificationService: NotificationService) {
      }

    ngOnInit() {
        const savedCredentials = sessionStorage.getItem('credentials') || localStorage.getItem('credentials');
        if (savedCredentials) {
            this.authenticationService.getUserDetails(this.authenticationService.credentials.token)
            .subscribe(
              data => {
                this.userData = data;
                console.log("Id : " + this.userData.student_id);
                this.loadQuestionSetForStudent();
              }
            );
          }
      }

      loadQuestionSetForStudent()
      {
          this.examService.getQuestionSetForStudent().subscribe(
            data =>
            {
              this.questionSetList = data;
              console.log("QuestionSet loaded");
            }
          )
      }

      gotoView(id: string)
      {
        this.router.navigate(['/question-paper', id ], { replaceUrl: true });
      
      }
}