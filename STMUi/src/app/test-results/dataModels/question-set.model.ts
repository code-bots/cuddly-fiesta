export class QuestionSet {
    public name: string;
    public subject: string;
    public creation_date: Date;
    public no_of_questions: number;
    public is_payment: boolean;
}

export class Question {
    public id: string;
    public question: string;
    public option_A: string;
    public option_B: string;
    public option_C: string;
    public option_D: string;
    public correct_ans: string;
    public question_set_id: QuestionSet;
}


export enum eAnswer {
    a = 'Option 1',
    b = 'Option 2',
    c = 'Option 3',
    d = 'Option 4',
}