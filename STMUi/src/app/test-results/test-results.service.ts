import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { Http, Headers, Response } from '@angular/http';
import { HttpHeaders } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';

export interface Credentials {
    // Customize received credentials here
    username: string;
    token: string;
}

const credentialsKey = 'credentials';
const auth = 'Authorization';
const key = 'bearer ';

@Injectable()
export class TestResultsService {

  private _credentials: Credentials | null;

  constructor(private http: HttpClient) {
    const savedCredentials = sessionStorage.getItem(credentialsKey) || localStorage.getItem(credentialsKey);
    if (savedCredentials) {
      this._credentials = JSON.parse(savedCredentials);
    }
  }

    getHeaderForRequest() {
      const httpOptions = {
        headers: new HttpHeaders({ 'Authorization': 'bearer ' + this._credentials.token
      })};
      return httpOptions;
    }

  // GET : http://localhost:8080/STMWeb/rest/api/subjects
  public getSubjects() {
    return this.http.get<any[]>('/STMWeb/rest/api/subjects',
    this.getHeaderForRequest());
  }


  // GET : http://localhost:8080/STMWeb/rest/api/questionsetlist/{tutor_id}
  public getQuestionSetListByTutorId(tutorId: string, startIndex: number, endIndex: number) {
    return this.http.get<any[]>('/STMWeb/rest/api/questionsetlist/' + tutorId,
      // + '?startIndex=' + startIndex + '&endIndex=' + endIndex,
      this.getHeaderForRequest());
  }

  // GET : http://localhost:8080/STMWeb/rest/api/questionSet/{questionSetId}
  public getQuestionSetByQuestionSetId(questionSetId: string) {
    return this.http.get<any>('/STMWeb/rest/api/questionSet/' + questionSetId,
    this.getHeaderForRequest());
  }

  // POST : http://localhost:8080/STMWeb/rest/api/exam/questionSet/{tutor_id}/{subject_id}
  public saveQuestionSet(context: any, tutorId: string,subjectId : string) {
    return this.http.post('/STMWeb/rest/api/exam/questionSet/' + tutorId + '/' +subjectId
      , context, this.getHeaderForRequest());
  }

  // PUT : http://localhost:8080/STMWeb/rest/api/questionSet/{questionSetId}
  public updateQuestionSet(context: any, questionSetId: string) {
    return this.http.put('/STMWeb/rest/api/questionSet/' + questionSetId
      , context, this.getHeaderForRequest());
  }


  // GET : http://localhost:8080/STMWeb/rest/api/questionlist/{qustionSetId}
  public getQuestionListByQustionSetId(qustionSetId: string) {
    return this.http.get<any>('/STMWeb/rest/api/questionlist/' + qustionSetId,
    this.getHeaderForRequest());
  }

  // POST : http://localhost:8080/STMWeb/rest/api/question/{questionSetId}
  public saveQuestion(context: any, questionSetId: string) {
    return this.http.post('/STMWeb/rest/api/question/' + questionSetId
      , context, this.getHeaderForRequest());
  }

  // PUT : http://localhost:8080/STMWeb/rest/api/question/{questionId}
  public updateQuestion(context: any, questionId: string) {
    return this.http.put('/STMWeb/rest/api/question/' + questionId
      , context, this.getHeaderForRequest());
  }

  //GET : http://localhost:8080/STMWeb/rest/api/questionSet/list
  public getQuestionSetForStudent()
  {
    return this.http.get<any[]>('/STMWeb/rest/api/questionSet/list', this.getHeaderForRequest());
  }

  //GET : http://localhost:8080/STMWeb/rest/api/questionSet/{questionSetId}
  public getQuestionSetById(queSetId : string)
  {
    return this.http.get<any[]>('/STMWeb/rest/api/questionSet/' + queSetId, this.getHeaderForRequest());
  }

  //GET : http://localhost:8080/STMWeb/rest/api/questionlist/{qustionSetId}
  public getQuestionByQuestionSet(queSetId : string)
  {
    return this.http.get<any[]>('/STMWeb/rest/api/questionlist/' + queSetId, this.getHeaderForRequest());
  }

  public getResultByTutor(tutorId : string)
  {
    return this.http.get<any[]>('/STMWeb/rest/api/result/tutor/' + tutorId, this.getHeaderForRequest());
  }
  ///result/tutor/{tutorId}

  //GET : http://localhost:8080/STMWeb/rest/api/ans/{qustionId}
  public getCorrectAns(queId : string)
  {
    return this.http.get<any>('/STMWeb/rest/api/ans/' + queId,this.getHeaderForRequest());
  }

  // POST : http://localhost:8080/STMWeb/rest/api/student/{student_id}/{question_id}
  public saveAnsByStudent(context: any, stuId: string,queId : string) {
    return this.http.post('/STMWeb/rest/api/student/' + stuId + '/' + queId
      , context, this.getHeaderForRequest());
  }

 // POST :  http://localhost:8080/STMWeb/rest/api/result/{Student_id}/{QuestionSetId}
 public saveResult(context: any, stuId: string,queSetId : string) {
  return this.http.post('/STMWeb/rest/api/result/' + stuId + '/' + queSetId
    , context, this.getHeaderForRequest());
}
  ///get : selected/ans/{questionset}/{student}

  public getSelectedAns(queSetId : string,stuId : string)
  {
    return this.http.get<any[]>('/STMWeb/rest/api/selected/ans/' + queSetId + '/' + stuId, 
        this.getHeaderForRequest());
  }

  public changeStatus(queSetId : string,stuId : string)
  {
    return this.http.put('/STMWeb/rest/api/status/change/' + queSetId + '/' + stuId
    , {}, this.getHeaderForRequest());
  }
  //delete : question/{questionId}

  public deleteQuestion(queId : string)
  {
    return this.http.delete('/STMWeb/rest/api/question/' + queId , this.getHeaderForRequest());
  }
}