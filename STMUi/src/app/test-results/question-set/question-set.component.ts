import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { TestResultsService } from '../test-results.service';
import { AuthenticationService } from '@app/core/authentication/authentication.service';
import { UserInfo } from '@app/shared/dataModels/userInfo';
import { QuestionSet } from './../dataModels/question-set.model';

@Component({
  selector: 'app-question-set',
  templateUrl: './question-set.component.html',
  styleUrls: ['./question-set.component.scss']
})
export class QuestionSetComponent implements OnInit {
  public questionSetForm: FormGroup;
  public courses = [] as any[];
  public subjects = [] as any[];

  private userData: UserInfo;
   isNewRecord: boolean;
  public questionSetId: string;

  constructor(private authenticationService: AuthenticationService,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private testResultsService: TestResultsService) {
      this.loadSubjects(false);
      this.createForm();
    }

  ngOnInit() {
   // this.userData = this.authenticationService.getUserInfo();
    this.authenticationService.getUserDetails(this.authenticationService.credentials.token)
    .subscribe(
      data => {
        this.userData = data;
      }
    );
    console.log("tutorId = " + this.userData.tutor_id)
    this.route.params.subscribe(params => {
      if (params['id'] === 'create') {
        // create new one
        this.isNewRecord = true;
      } else {
        // edit already exists
        this.isNewRecord = false;
        this.questionSetId = params['id'];
      }
    });
    console.log(this.isNewRecord + " as");
  }

  public loadSubjects(hideForm: boolean) {
    this.testResultsService.getSubjects()
    .subscribe(
      (data) => {
        this.subjects = data;
        if (!this.isNewRecord) {
          this.fillControl(this.questionSetId);
        }
      },
      (err) => {
        console.log('Loading failed for data subject');
      }
    );
  }

  private createForm() {
    this.questionSetForm = this.formBuilder.group({
      name: ['', Validators.required],
      subject: ['', Validators.required],
      subject_id:null,
      subjectName:[''],
      no_of_questions: ['', Validators.required]
    });
  }

  private fillControl(questionSetId) {
    if (questionSetId && questionSetId.trim() !== '') {
      this.testResultsService.getQuestionSetByQuestionSetId(questionSetId)
        .subscribe(
          (result) => {
            console.log('result', result);
            console.log(result.subject_id.name)
            this.questionSetForm.patchValue({
              name: result.name,
              subject: result.subject_id.name,
              subjectName : result.subject_id.name,
              no_of_questions: result.no_of_questions
            });
          },
          (error) => {
            console.log('error', error);
          }
        );
    }
  }

  public onCreateClick() {

    console.log("in");
    if (this.questionSetForm.valid) {
      const postData = {      
        name: this.questionSetForm.value.name,
        creation_date: new Date().getTime(),
        no_of_questions: this.questionSetForm.value.no_of_questions,
        is_payment: false
      };

      console.log("subject Id : " + this.questionSetForm.value.subject);

      if (this.isNewRecord) {
        // Save
        this.testResultsService.saveQuestionSet(postData, this.userData.tutor_id, this.questionSetForm.value.subject)
          .subscribe(
            (result) => {
              console.log('Question Set created', result);
              this.router.navigate(['/question-set-list'], { replaceUrl: false });
            },
            (error) => {
              console.log('Question Set creation failed', error);
            }
          );
      } else {
        // Update
      }
    } else {
      // display message form not valid
    }
  }
  onUpdateClick()
  {
    const postData = {      
      name: this.questionSetForm.value.name,
      creation_date: new Date().getTime(),
      no_of_questions: this.questionSetForm.value.no_of_questions,
      is_payment: false
    };
    this.testResultsService.updateQuestionSet(postData, this.questionSetId)
    .subscribe(
      (result) => {
        console.log('Question Set updated', result);
        this.router.navigateByUrl('/question-set-list');
      },
      (error) => {
        console.log('Question Set updation failed', error);
      }
    );
  }

  public onCancelClick() {
    this.router.navigateByUrl('/question-set-list');
  }

}
