import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { Ng2SmartTableModule } from 'ng2-smart-table';

import { PaginationModule, ButtonsModule, RatingModule } from 'ngx-bootstrap';

import { CoreModule } from '@app/core';
import { SharedModule } from '@app/shared';

import { TestResultsRoutingModule } from './test-results-routing.module';

import { QuestionSetListComponent } from './question-set-list/question-set-list.component';
import { QuestionSetComponent } from './question-set/question-set.component';
import { AddQuestionGridComponent } from './add-question-grid/add-question-grid.component';
import { QuestionSetListStudentComponent } from '@app/test-results/questionSetListStudent/questionSetListStudent.component';
import { TestResultsService } from './test-results.service';
import { ToastyModule } from 'ng2-toasty';
import { NgPipesModule } from 'ngx-pipes';
import { TranslateModule } from '@ngx-translate/core';
import { QuestionPaperComponent } from '@app/test-results/questionPaper/questionPaper.component';


@NgModule({
  imports: [
    CommonModule,
    NgPipesModule,
    TranslateModule,
    CoreModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    Ng2SmartTableModule,
    TestResultsRoutingModule,
    PaginationModule.forRoot(),
    ButtonsModule.forRoot(),
    RatingModule.forRoot(),
    ToastyModule.forRoot()
  ],
  declarations: [
    QuestionSetListComponent,
    QuestionSetComponent,
    AddQuestionGridComponent,
    QuestionSetListStudentComponent,
    QuestionPaperComponent
  ],
  providers: [TestResultsService, DatePipe]
})
export class TestResultsModule { }