import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Route, extract } from '@app/core';
import { QuestionSetComponent } from './question-set/question-set.component';
import { QuestionSetListComponent } from './question-set-list/question-set-list.component';
import { QuestionSetListStudentComponent } from '@app/test-results/questionSetListStudent/questionSetListStudent.component';
import { QuestionPaperComponent } from '@app/test-results/questionPaper/questionPaper.component';

const routes: Routes = [
    Route.withShell([
      { path: 'question-set-list', component: QuestionSetListComponent,
      data: { title: extract('Question Set List') }},
      { path: 'question-set-list/:id', component: QuestionSetComponent,
      data: { title: extract('Create Question Set')}},
      { path: 'question-paper/:id', component: QuestionPaperComponent,
      data: { title: extract('Question Paper')}},
      { path: 'stu-quesionset-list', component: QuestionSetListStudentComponent, data: { title: extract('QuestionSet') } },
    //   { path: 'assignment', component: CreateAssignmentComponent,
    //   data: { title: extract('Create Assignment') }}
    ])
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    providers: []
})
export class TestResultsRoutingModule { }
