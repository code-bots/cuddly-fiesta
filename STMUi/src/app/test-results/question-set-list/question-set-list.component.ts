import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { QuestionSet } from '../dataModels/question-set.model';

import { TestResultsService } from '@app/test-results/test-results.service';
import { AuthenticationService } from '@app/core/authentication/authentication.service';
import { UserInfo } from '@app/shared/dataModels/userInfo';

@Component({
  selector: 'app-question-set-list',
  templateUrl: './question-set-list.component.html',
  styleUrls: ['./question-set-list.component.scss']
})
export class QuestionSetListComponent implements OnInit {
  public questionSetList: QuestionSet[] = [];
  public currentPage: number;
  public startPageIndex: number;
  public endPageIndex: number;
  public recordsPerPage = 10;
  searchTerm='';
  private userData: UserInfo;

  constructor(private authenticationService: AuthenticationService,
    private router: Router,
    private testResultsService: TestResultsService) { }

  ngOnInit() {
    this.authenticationService.getUserDetails(this.authenticationService.credentials.token)
    .subscribe(
      data => {
        this.userData = data;
      }
    );
    this.getQuestionSetList();
  }

  public goTo(route: string, id: string) {
    this.router.navigate([route, id]);
  }

  private getQuestionSetList() {
    this.endPageIndex = this.startPageIndex + (this.recordsPerPage - 1);
    this.testResultsService.getQuestionSetListByTutorId(this.userData.tutor_id, this.startPageIndex, this.endPageIndex)
      .subscribe(
        (result) => {
          this.questionSetList = result;
        },
        (error) => {
          console.log('Get Question Set failed', error);
        }
      );
  }

  public pageChanged($event: any) {
    this.startPageIndex = ($event.page * $event.itemsPerPage) - ($event.itemsPerPage - 1);
    this.getQuestionSetList();
  }

}
