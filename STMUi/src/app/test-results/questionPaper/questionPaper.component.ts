import { Component, OnInit, Input } from '@angular/core';
import { find} from 'lodash';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';
import { NotificationService } from '@app/core/notification.service';
import { UserInfo } from '@app/shared/dataModels/userInfo';
import { AuthenticationService } from '@app/core/authentication/authentication.service';
import { TestResultsService } from '@app/test-results/test-results.service';



@Component({
    selector: 'app-questionPaper',
    templateUrl: './questionPaper.component.html'
})
export class QuestionPaperComponent implements OnInit {

    userData : UserInfo;
    questionsetId : string;
    radioName : string;
    questionSet : any;
    questionform : FormGroup;
    isDisable : boolean;
    correctAns : any;
    isDisabled : boolean = false;
    isSelected : any;
    sample : any;
    constructor(private router: ActivatedRoute,private route : Router,private authenticationService: AuthenticationService,
        private domSanitizer: DomSanitizer, private notificationService: NotificationService
        ,private examService : TestResultsService,private formBuilder: FormBuilder,) {
      }
      cancle()
      {
        this.route.navigate(['/question-set-list'], { replaceUrl: false });
      }
      
    ngOnInit() {

      this.router.params.subscribe(params => {
        const savedCredentials = sessionStorage.getItem('credentials') || localStorage.getItem('credentials');
        if (savedCredentials) {
            this.authenticationService.getUserDetails(this.authenticationService.credentials.token)
            .subscribe(
              data => {
                this.userData = data;
                this.questionsetId = params.id;
                console.log("queSetId : " + this.questionsetId);
                this.loadQuestions();
              }
            );
          }
        });
      }
     
      loadQuestions()
      {
        this.examService.getQuestionByQuestionSet(this.questionsetId).subscribe
        (
          data =>
          {
            
            console.log("Question Loaded");
            console.log("object" + this.questionSet);
            this.examService.getSelectedAns(this.questionsetId,this.userData.student_id).subscribe
            (
              res => {
                console.log("selected ans loaded " + res );
                for(let item of res)
                {
                  this.isSelected = find(data,function(o) {return item.question_id.id == o.id;});
                  console.log("data is " + this.isSelected.question);
                  this.isSelected.disabled = true;
                  console.log("selected ans :: " + item.student_choice);
                  this.isSelected.selectedAns = item.student_choice;
                }
              }
            );
            console.log("---");
            this.questionSet = data;
          }
        );

      }

      createForm(): any
      {
        
      }

      saveAns(question: any)
      {
        console.log("Question Id : " + question.id);
        question.hasClicked = true;
        this.examService.getCorrectAns(question.id).subscribe
        (
          data => {
            const postData = {
              student_choice: question.selectedAns,
              correct_ans: data.message,
              status:0
            };
            this.examService.saveAnsByStudent(postData,this.userData.student_id,question.id).subscribe
            (
            data2 => {
                  console.log("Answer saved");
                  question.disabled = true;
              },
            err2 => {
              question.hasClicked = false;
            }
            );
          },
          err1 => {
            question.hasClicked = false;
          }
        );
      }

      submitTest()
      {
        console.log("queSetId :: " + this.questionsetId + " studentId :: " + this.userData.student_id)
        const postData ={
          result_date : new Date().getTime()
        }
        this.examService.saveResult(postData,this.userData.student_id,this.questionsetId).subscribe
        (
          data =>
          {
            this.examService.changeStatus(this.questionsetId,this.userData.student_id).subscribe
            (
              res =>
              {
                console.log("status changes");
              }
            );
            console.log("test submit");
          }
        )
        this.route.navigate(['/stu-dashboard'], { replaceUrl: false });
      }

      radioChangeHandler($event: any, question: any)
      {
        question.selectedAns = $event.target.value;
      //  event.target.disabled = true;
        console.log("ans :: " +  question.selectedAns);
        console.log("questionId :: " + question.id);
      }
}
