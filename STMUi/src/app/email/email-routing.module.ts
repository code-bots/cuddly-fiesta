import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Route, extract } from '@app/core';
import { EmailComponent } from './email.component';

const routes: Routes = [
  Route.withShell([
    { path: 'email/:folder', component: EmailComponent, data: { title: extract('Email') } }
  ])
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class EmailRoutingModule { }
