import { ContactInfo } from './contactInfo';
export class Contacts {
    students: ContactInfo[];
    tutors: ContactInfo[];
}
