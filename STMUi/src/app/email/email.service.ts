import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { Http, Headers, Response } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';


const credentialsKey = 'credentials';
export interface Credentials {
    // Customize received credentials here
    username: string;
    token: string;
  }

@Injectable()
export class EmailService {
    private _credentials: Credentials | null;
    constructor(private http: HttpClient) {
      const savedCredentials = sessionStorage.getItem(credentialsKey) || localStorage.getItem(credentialsKey);
      if (savedCredentials) {
        this._credentials = JSON.parse(savedCredentials);
      }
    }

    getHeaderForRequest() {
      const httpOptions = {
        headers: new HttpHeaders({ 'Authorization': 'bearer ' + this._credentials.token
      })};
      return httpOptions;
    }

      getContacts() {
        // http://localhost:8080/STMWeb/rest/api/email/contacts
        return this.http.get<any>('/STMWeb/rest/api/email/contacts',
        this.getHeaderForRequest());
      }

      getEmailFolderDetails(stmEmailId: string) {
        // http://localhost:8080/STMWeb/rest/api/email/folder-details/{stmEmailId}/list
        return this.http.get<any[]>('/STMWeb/rest/api/email/folder-details/' + stmEmailId + '/list',
        this.getHeaderForRequest());
      }

      getEmailItemsFromFolder(stmEmailId: string, folder: string, startIndex: number, endIndex: number) {
        // http://localhost:8080/STMWeb/rest/api/email/emailItems/{stmEmailId}/folder/INBOX?startIndex=1&endIndex=10
        return this.http.get<any[]>('/STMWeb/rest/api/email/emailItems/' + stmEmailId +
        '/folder/' + folder + '?startIndex=' + startIndex + '&endIndex=' + endIndex,
        this.getHeaderForRequest());
      }

      sendEmail(context: any) {
        // http://localhost:8080/STMWeb/rest/api/email/send
        return this.http.post('/STMWeb/rest/api/email/send', context,
        this.getHeaderForRequest());
      }

}
