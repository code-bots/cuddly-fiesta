import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Route, extract } from '@app/core';
import { BlogComponent } from './blog.component';
import { BloglistComponent } from '@app/bloglist/bloglist.component';
import { ViewBlogComponent } from '@app/bloglist/viewblog.component';
import { BlogEditComponent } from '@app/blog/blogedit.component';

const routes: Routes = [
  Route.withShell([
    { path: 'blog', component: BlogComponent, data: { title: extract('Blog') } },
    { path: 'bloglist', component: BloglistComponent, data: { title: extract('BlogList') } },
    { path: 'blogedit/:id', component: BlogEditComponent, data: { title: extract('BlogEdit') } },
    { path: 'viewblog/:id', component: ViewBlogComponent, data: { title: extract('ViewBlog') } }
  ])
];

@NgModule({
  imports: [RouterModule.forChild(routes)],   
  exports: [RouterModule],
  providers: []
})
export class BlogRoutingModule { }
