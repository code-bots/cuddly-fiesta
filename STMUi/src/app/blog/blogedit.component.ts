import { Component, OnInit, Input, Output } from '@angular/core';
import { BlogService } from '@app/blog/blog.service';
import { AuthenticationService } from '@app/core/authentication/authentication.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserInfo } from '@app/shared/dataModels/userInfo';
import { NotificationService } from '@app/core/notification.service';
@Component({
  selector: 'app-blog',
  templateUrl: './blogedit.component.html',
  styleUrls: ['./blog.component.scss']
})
export class BlogEditComponent implements OnInit {

  blogForm: FormGroup;
  userData: UserInfo;
  blog: any;
  @Input() data: string;
  constructor(private formBuilder: FormBuilder, private blogService: BlogService,
    private authenticationService: AuthenticationService,
    private notificationService: NotificationService,
    private router: Router, private route: ActivatedRoute
  ) {
  }

  ngOnInit() {
    this.createForm();
    this.route.params.subscribe(params => {
      this.authenticationService.getUserDetails(this.authenticationService.credentials.token)
        .subscribe(
          data => {
            this.userData = data;// ? data.doctor : data;
            const blogId = params.id;
            this.loadBlog(blogId);
          }
        );
    });
  }

  createForm(): any {
    this.blogForm = this.formBuilder.group({
      title: ['', Validators.required]
    });
  }

  update() {
    const dataToSave = {
      title: this.blogForm.controls.title.value,
      content: this.data,
      tags: ''
    };
    this.blogService.updateBlog(dataToSave, this.blog.id).subscribe(
      data => {
        this.notificationService.showNotification('Success', 'Blog saved successfully', 'success');
        this.cancel();
      }, err => {
        this.notificationService.showNotification('Success', 'Error in saving blog due to: ' + err, 'error');
      }
    );
  }

  cancel() {
    this.router.navigate(['/bloglist'], { replaceUrl: false });
  }

  loadBlog(blogId: string) {
    this.blogService.getBlogsById(blogId).subscribe(
      response => {
        this.blog = response;
        this.blogForm.controls.title.setValue(this.blog.title);
        this.data = this.blog.content;
      }, error => {
      }
    );
  }

  

}