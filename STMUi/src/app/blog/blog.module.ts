import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { TranslateModule } from "@ngx-translate/core";
import { CoreModule } from "@app/core";
import { SharedModule } from "@app/shared";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { BlogRoutingModule } from "@app/blog/blog.routing";
import { NgxEditorModule } from "ngx-editor";
import { BlogComponent } from "@app/blog/blog.component";
import { BlogService } from "@app/blog/blog.service";
import { BloglistComponent } from "@app/bloglist/bloglist.component";
import { ViewBlogComponent } from "@app/bloglist/viewblog.component";
import { BlogEditComponent } from "@app/blog/blogedit.component";
import { TreeModule } from 'angular-tree-component';
@NgModule({
    imports: [
      CommonModule,
      TranslateModule,
      CoreModule,
      SharedModule,
      FormsModule,
      ReactiveFormsModule,
      BlogRoutingModule,
      NgxEditorModule,
      TreeModule
    ],
    declarations: [
      BlogComponent,BloglistComponent,ViewBlogComponent,BlogEditComponent
    ],
    providers: [
      BlogService
    ]
  })
  export class BlogModule { }