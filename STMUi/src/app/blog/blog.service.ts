import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, Headers, Response } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

const credentialsKey = 'credentials';
export interface Credentials {
    // Customize received credentials here
    username: string;
    token: string;
  }

@Injectable()
export class BlogService {
    private _credentials: Credentials | null;

    constructor(private http: HttpClient) {
      const savedCredentials = sessionStorage.getItem(credentialsKey) || localStorage.getItem(credentialsKey);
      if (savedCredentials) {
        this._credentials = JSON.parse(savedCredentials);
      }
    }

      getHeaderForRequest() {
        const httpOptions = {
          headers: new HttpHeaders({ 'Authorization': 'bearer ' + this._credentials.token
        })};
        return httpOptions;
      }

      saveBlog(context: any, tutor_id: string)
      {       
        return this.http.post('/STMWeb/rest/api/blog/' + tutor_id
        , context, this.getHeaderForRequest());    
      }

      getBlogsByTutor(tutor_id: string)
      {       

        return this.http.get<Array<any>>('/STMWeb/rest/api/tutor/blog/' + tutor_id
        ,  this.getHeaderForRequest());    
        //tutorId
        //http://localhost:8080/STMWeb/rest/api/blog/{tutor_id}

      }


      saveBlogComment(context : any,blogId : string)
      {
        return this.http.post('/STMWeb/rest/api/comment/'+ blogId , context, this.getHeaderForRequest());    
        //http://localhost:8080/STMWeb/rest/api/comment/{blog_id} 
      }


      getBlogs()
      { 
        return this.http.get<any[]>('/STMWeb/rest/api/bloglist',  this.getHeaderForRequest());    
        //http://localhost:8080/STMWeb/rest/api/bloglist
      }

      getBlogsById(blog_Id : string)
      {
        return this.http.get('/STMWeb/rest/api/blog/'+ blog_Id ,  this.getHeaderForRequest());    
        // http://localhost:8080/STMWeb/rest/api/blog/{blog_id}
      }
      

      getComments(blogId : string)
      {
        return this.http.get<any[]>('/STMWeb/rest/api/comments/'+ blogId ,  this.getHeaderForRequest());    
        //http://localhost:8080/STMWeb/rest/api/comments/{blog_id}
      }

      getParentComments(blogId : string)
      {
        return this.http.get<any[]>('/STMWeb/rest/api/commentchain/'+ blogId ,  this.getHeaderForRequest());
        //http://localhost:8080/STMWeb/rest/api/commentchain/{parentCommentId}
      }

      approveTutor(tutorId: string)
      {
    
        console.log("token : " + this._credentials.token);
        return this.http.put('/STMWeb/rest/api/approv/tutor/' + tutorId,
          this.getHeaderForRequest());    
        //http://localhost:8080/STMWeb/rest/api/tutorgrid
      }

      updateBlog(context :any ,blogId : string )
      {

        return this.http.put('/STMWeb/rest/api/blog/'+ blogId , context, this.getHeaderForRequest());
        // http://localhost:8080/STMWeb/rest/api/blog/{blog_id}
      }

      deleteBlog(blogId : string)
      {
        return this.http.delete('/STMWeb/rest/api/blog/'+ blogId , this.getHeaderForRequest());

        // http://localhost:8080/DocWeb/rest/api/blog/{blog_id}
      }

      editComment(context : any,cmtId : string )
      {
        return this.http.put('/STMWeb/rest/api/comment/'+ cmtId , context, this.getHeaderForRequest());

        // http://localhost:8080/STMWeb/rest/api/comment/{commentId}
      }

      deleteComment(cmtId : string)
      {
        return this.http.delete('/STMWeb/rest/api/comment/'+ cmtId ,  this.getHeaderForRequest());
        // http://localhost:8080/STMWeb/rest/api/comment/{commentId}

      }

      // getHeaderForRequest() {
      //   const httpOptions = {
      //     headers: new HttpHeaders({ 'Authorization': 'bearer ' + this._credentials.token
      //   })};
      //   return httpOptions;
      // }


}
