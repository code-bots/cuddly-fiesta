import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { AuthenticationService } from '../core/authentication/authentication.service';
import { UserInfo } from '../shared/dataModels/userInfo';
import { DomSanitizer } from '@angular/platform-browser';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ProfileService } from '@app/profile/profile.service';
import { ChatService } from '@app/chat/chat.service';
import { NotificationService } from '@app/core/notification.service';
import { filter, groupBy } from 'lodash';
import * as $ from 'jquery';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss', '../assignments/form.scss']
})
export class ChatComponent implements OnInit {
  arrayOfKeys: string[];
  targetUser: any;
  userTwoFullName: any;
  userOneFullName: any;
  isLoading: boolean;
  userData: any;
  chatUsers: any;
  showChat =  false;
  messages = [] as any[];
  chatSessionId: string;
  searchTerm = '';
  chatWebSocket;
  MESSAGES_RENDERING_WAIT_TIME = 1000;
  currentMessage = '';
  contactClicked = false;
  contactsLoaded = false;
  _this_;
  constructor(private authenticationService: AuthenticationService,
    private chatService: ChatService,
    private notificationService: NotificationService,
    public domSanitizer: DomSanitizer) {
    if (!this.chatService.isConnected()) {
        this.chatService.connect();
    }
    this.authenticationService.getUserDetails(this.authenticationService.credentials.token)
      .subscribe(
        data => {
          this.userData = data;
          this.isLoading = true;
          if (this.userData.student_id) {
            // load tutor for student
            this.chatService.getTutor().subscribe(
              tutorData => {
                let lstData: any;
                lstData = tutorData.listOfObjects;
                const dataToGroup = [];
                lstData.forEach(tutData => {
                  if (tutData.tutor.user_id.enabled)
                  { dataToGroup.push({
                    name: tutData.tutor.first_name + ' ' + tutData.tutor.last_name,
                    pic: tutData.tutor.profile_pic,
                    email: tutData.tutor.email,
                    id: tutData.tutor.user_id.id,
                    type: 'tutor',
                    online: tutData.tutor.user_id.isPresent
                  });}
                 
                });
                this.chatUsers = groupBy(dataToGroup, function(user: any) {return user.name[0]; });
                this.arrayOfKeys = Object.keys(this.chatUsers);
                this.contactsLoaded = true;
              },
            );
          } else {
            // load student for tutor
            // http://localhost:8080/STMWeb/rest/api/tutor/assignments/student/{tutor_id}
            this.chatService.getStudent().subscribe(
              studentsData => {
                let lstData: any[];
                lstData = studentsData.listOfObjects;
                const dataToGroup = [];
                lstData.forEach(student => {
                  if(student.user_id.enabled)
                  {dataToGroup.push({
                    name: student.first_name + ' ' + student.last_name,
                    pic: student.profile_pic,
                    email: student.email,
                    id: student.user_id.id,
                    type: 'student',
                    online: student.user_id.isPresent
                  });
                }
                  
                });
                this.chatUsers = groupBy(dataToGroup, function(user: any) {return user.name[0]; });
                this.arrayOfKeys = Object.keys(this.chatUsers);
                this.contactsLoaded = true;
              },
              err => {

              }
            );
          }
        }
      );
  }

  ngOnInit() {}

  loadChats(user: any) {
    this.targetUser = user;
   // this.messages = [];
    this.showChat = false;
    this.construct();
   // this.chatService.whenConnected(this.construct);
  }

  construct() {
    this.messages = []; 
    this.chatService.establishChatSession(this.userData.user_id.id, this.targetUser.id).subscribe(
      session => {
        this.chatSessionId = session.channelUuid;
        this.userOneFullName = session.userOneFullName;
        this.userTwoFullName = session.userTwoFullName;
        this.chatWebSocket = this.chatService.getSocket();
        const _this_ = this;
        this.chatWebSocket.subscribe('/topic/private.chat.' + this.chatSessionId, function (response: any) {
          _this_.addChatMessageToUI(JSON.parse(response.body), true);
          _this_.scrollToLatestChatMessage();
        });

        this.chatService.getExistingChatSessionMessages(this.chatSessionId).subscribe(
          data => {
            this.showChat = true;
            // this.messages = data;
            data.forEach(msg => {
              this.addChatMessageToUI(msg, true);
            });
          },
          error => {
            console.log('Error in loading the messages');
          }
        );
      }
    );
  }

  addChatMessageToUI(message: any, withForceApply: boolean) {
    this.messages
      .push({
        contents: message.contents,
        isFromRecipient: message.fromUserId !== this.userData.user_id.id,
        author: (message.fromUserId === this.userData.user_id.id) ?
          this.userData.first_name + ' ' + this.userData.last_name : this.targetUser.name,
        pic: message.fromUserId === this.userData.user_id.id ?
          this.userData.profile_pic : this.targetUser.pic
      });

   // if (withForceApply) { self.$apply(); }
  }

  getUserName(userEmail: string, isFromRecipient: boolean): any {
    if (!isFromRecipient) {
      return this.userData.first_name + ' ' + this.userData.last_name;
    } else {
      const user = filter(this.chatUsers, function(users: any) {
        return users.email === userEmail;
      });
      if (user && user.length) {
        return user[0].name;
      } else {
        return userEmail;
      }
    }
  }

  scrollToLatestChatMessage() {
    const chatContainer = $('#chat-area');
    setTimeout(function() {
      if (chatContainer.length > 0) { chatContainer.scrollTop(chatContainer[0].scrollHeight); }
    }, this.MESSAGES_RENDERING_WAIT_TIME);
  }

  sendChatMessage() {
    if (!this.currentMessage || this.currentMessage.trim() === '') {
      return;
    }

    this.chatWebSocket.send('/app/private.chat.' + this.chatSessionId, {}, JSON.stringify({
      fromUserId: this.userData.user_id.id,
      toUserId: this.targetUser.id,
      contents: this.currentMessage
    }));

    this.currentMessage = null;
  }

  trackByFn(index: any, item: any) {
    return index; // or item.id
  }
}
