import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthenticationService } from '@app/core';
import { AssignmentService } from '@app/assignments/assignment.service';
import { NotificationService } from '@app/core/notification.service';

@Component({
  selector: 'app-assignment-create',
  templateUrl: './create-assignment.component.html',
  styleUrls: ['./assignment.component.scss', './form.scss']
})
export class CreateAssignmentComponent implements OnInit {

  assignmentForm: FormGroup;
  quote: string;
  isLoading: boolean;
  userData: any;
  dateVisit = new Date();
  institutes = [] as any[];
  courses = [] as any[];
  subjects = [] as any[];
  addCourse = false;
  addSubject = false;

  constructor(private formBuilder: FormBuilder,
    private authenticationService: AuthenticationService,
    private assignmentService: AssignmentService,
    private notificationService: NotificationService,
    private router: Router) {
      this.authenticationService.getUserDetails(this.authenticationService.credentials.token)
      .subscribe(
        data => {
          this.userData = data;
          this.isLoading = true;
          this.assignmentService.getInstituteForTutor(this.userData.tutor_id).subscribe(
            data1 => {
              this.institutes = data1;
            }, err => {
              this.notificationService.showNotification('Loading error', 'Unable to load institutes.', 'error');
            }
          );
          this.loadCourses(false);
          this.createForm();
        }
      );
  }

  private loadCourses(hideForm: boolean) {
    this.assignmentService.getCourses().subscribe(
      data => {
        this.courses = data;
        if (hideForm) {
          this.addCourse = false;
        }
      }, err => {
        this.notificationService.showNotification('Loading error', 'Unable to load courses.', 'error');
      }
    );
  }

  private loadSubjects(hideForm: boolean) {
    this.assignmentService.getSubjectForCourse(this.assignmentForm.value.coure_id).subscribe(
      data => {
        this.subjects = data;
        if (hideForm) {
          this.addSubject = false;
        }
      }, err => {
        this.notificationService.showNotification('Loading error', 'Unable to load subjects.', 'error');
      }
    );
  }

  private createForm() {
    this.assignmentForm = this.formBuilder.group({
      title: ['Sample Title' + new Date().getTime(), Validators.required],
      description: '',
      type: ['', Validators.required],
      due_date: ['', Validators.required],
      institute: null,
      coure_id: null,
      course_name: ['', Validators.required],
      course_desc: null,
      subject_id: null,
      sub_name: ['', Validators.required],
      sub_desc: null,
      payment: false
    });
  }

  ngOnInit() {
    this.isLoading = true;
  }

  save(withSchedule: boolean) {
    const assignment = {
      assignment: {
        title: this.assignmentForm.value.title,
        description: this.assignmentForm.value.description,
        type: this.assignmentForm.value.type,
        due_date: this.assignmentForm.value.due_date,
        payment: this.assignmentForm.value.payment ? 1 : 0,
        creation_date: new Date(),
        status: 4
      },
      subjectId: this.assignmentForm.value.subject_id
    };
    this.assignmentService.saveAssignment(assignment, this.userData.tutor_id,
      this.assignmentForm.value.institute).subscribe(
      res => {
        console.log('Assignment created');
        this.notificationService.showNotification('Create success',
        'Assignment ' + this.assignmentForm.value.title + ' created.', 'success');
        this.router.navigate(['/assignments'], { replaceUrl: false });
      },
      err => {
        console.log('Assignment creation failed');
      }
    );
  }

  cancel() {
    this.router.navigate(['/assignments'], { replaceUrl: false });
  }

  saveCourse() {
    const course = {
      name: this.assignmentForm.value.course_name,
      description: this.assignmentForm.value.course_desc
    };
    this.assignmentService.saveCourse(course).subscribe(
      data => {
        this.loadCourses(true);
        this.notificationService.showNotification('Create success',
        'Course ' + this.assignmentForm.value.course_name + ' created.', 'success');
      }
    );
  }

  saveSubject() {
    const subject = {
      name: this.assignmentForm.value.sub_name,
      description: this.assignmentForm.value.sub_desc
    };
    this.assignmentService.saveSubject(subject, this.assignmentForm.value.coure_id).subscribe(
      data => {
        this.loadSubjects(true);
        this.notificationService.showNotification('Create success',
        'Subject ' + this.assignmentForm.value.sub_name + ' created.', 'success');
      }
    );
  }

}
