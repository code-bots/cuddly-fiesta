import { Component, OnInit, TemplateRef } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { DragulaService } from 'ng2-dragula';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '@app/core';
import { AssignmentService } from '@app/assignments/assignment.service';
import { DomSanitizer } from '@angular/platform-browser';
import { FormBuilder } from '@angular/forms';
import { keyBy, filter } from 'lodash';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { existentialTypeParam } from 'babel-types';
import * as $ from 'jquery';
import { DatePipe } from '@angular/common';
import { Subscription } from 'rxjs';
import { ProfileService } from '@app/profile/profile.service';
@Component({
  selector: 'app-assignment-student',
  templateUrl: './assignment-student.component.html',
  styleUrls: ['./assignment.component.scss']
})
export class AssignmentStudentComponent implements OnInit {

  assignments;
  assignmentsStudent;
  assignmentBoards;
  quote: string;
  isLoading: boolean;
  userData;
  currDate : any; 
  assignmentsData = [];
  paymentData;
  modalRef: BsModalRef;
  assignmentToPay;
  paymentRef: TemplateRef<any>;
  status: string;
  dateVisit = [new Date(), new Date()];
  startDateFilter: string;
  endDateFilter: string;
  subs = new Subscription();

  //http://localhost:4200/STMWeb/rest/api/change/assignment/status/042a2e46-2c46-4fc4-be55-26a71a46a6ec/2
  constructor(private dragulaService: DragulaService,
              private router: Router,
              private authenticationService: AuthenticationService,
              private assignmentService: AssignmentService,
              private domSanitizer: DomSanitizer,
              private modalService: BsModalService,
              private formBuilder: FormBuilder,
              private profileServie : ProfileService,
              private route: ActivatedRoute,
              private datePipe: DatePipe) {
    console.log('in constructor');

    this.dragulaService.drop.subscribe(value => {
      
      // studentId,boardId,assignmentId
    
      console.log("id is = " + value[2]['id']);
      console.log("studentID : " +  this.userData.student_id);
      console.log("assStuID "  + value[1]['id']);
     

      this.assignmentService.updateStatus(value[1]['id'],value[2]['id']).subscribe
      (
        res => 
        {
          console.log("status updated");
        }
      );
    });    
    
       
    this.route.params.subscribe(params => {
      console.log('After payment=' + params.assignmentId + ' and status=' + params.status);
      if (params.assignmentId && params.status) {
        this.modalRef = this.modalService.show(this.paymentRef);
      }
    });
  }

  ngOnInit() {
    this.authenticationService.getUserDetails(this.authenticationService.credentials.token)
    .subscribe(
      data => {
        this.currDate = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
        console.log("cuur"  + this.currDate);
        console.log("first")
        this.userData = data;
        this.isLoading = true;
        this.loadAssignmentMeta();
      }
    );
  }

  loadAssignmentMeta() {
    this.assignmentService.getAssignmentBoards(1).subscribe(
      data => {
        this.assignmentBoards = data;
        this.loadAllAssignments();
      }, err => {
        console.log('Error in loading boards');
      }
    );
  }

  loadAllAssignments() {
    this.assignmentService.getAllAssignments().subscribe(
      data => {
        this.assignments = data;
        this.assignmentBoards.forEach(board => {
         if (board.id !== 0) {
            this.loadBoardAssignmentsForStudent(board);
         }
        });
      }
    );
  }

  loadBoardAssignmentsForStudent(board: any) {
    this.assignmentService.getAssignmentsForStudentByStatus(this.userData.student_id, board.id,this.startDateFilter,this.endDateFilter).subscribe(
      data => {
        const assignmentsStudentByStatus = data;
        // index studentAssignment by "assignmentId"
        const lookup = keyBy(assignmentsStudentByStatus, function(studentAssignment: any) {
          return studentAssignment.assignment_id.id;
        });
        // find all assignments where "assignmentId" not exists in index
        this.assignments = filter(this.assignments, function(assignment: any) {
          return !lookup[assignment.id];
        });
        const dataToCheck = filter(this.assignmentsData, function(assignmentData: any) {
          return assignmentData.board.id === board.id;
        });
        if (dataToCheck.length) {
          dataToCheck[0].assignments = data;
        } else {
          this.assignmentsData.push({
            board: board,
            assignments: data,
            loadingDone: true
          });
        }
      },
      err => {
        console.log('Loading of assignments failed');
      }
    );
  }

  onValueChange($event: any) {
    console.log($event);
    if ($event === null) {
      return;
    }
    if ($event) {
      this.startDateFilter = this.datePipe.transform($event[0], 'yyyy-MM-dd'),
      console.log(this.startDateFilter);
      this.endDateFilter = this.datePipe.transform($event[1], 'yyyy-MM-dd'),
      console.log(this.endDateFilter);
      this.loadAssignmentMeta();
    }
  }

  getAssignmentForBoard(board: any) {
    const data = filter(this.assignmentsData, function(assignmentData: any) {
      return assignmentData.board.id === board.id;
    });
    if (data && data.length) {
      return data[0].assignments;
    } else {
      return [];
    }
  }

  getIdForUI(entity: any) {
    switch (entity.type) {
      case 1: return 'ASS-' + entity.assNumber;
      case 2: return 'SEM-' + entity.assNumber;
      case 3: return 'FRE-' + entity.assNumber;
      case 4: return 'CON-' + entity.assNumber;
    }
    return 'N/A';
  }

  getBatchForUI(entity: any) {
    switch (entity.type) {
      case 1: return 'Assignment';
      case 2: return 'Seminar';
      case 3: return 'Free Lecture';
      case 4: return 'Consulting';
    }
  }

  enroll(assignment: any, template: TemplateRef<any>) {



    if(assignment.payment === 0)
    {
      const postdata = {
        "completion_date":new Date().getTime(),
	      "status":"1",
	      "tutor_ratings":3
      }
      this.assignmentService.enrollAssignmentToStudent(postdata,assignment.id,this.userData.student_id)
      .subscribe(
          data => {
            this.assignments = data;
            console.log("updated Successfully");
            this.assignmentService.changeStatus(assignment.id).subscribe
            (
              data =>
              {
                console.log("status updated" + data);
                
              }
            );
          }
        );
        this.loadAssignmentMeta();
    }
    else
    {
      window.location.href =
                'http://35.185.51.218:8080/dist/payment.html?assignmentId=' + assignment.id +
                '&studentId=' + this.userData.student_id;
                this.assignmentToPay = assignment;
                this.paymentRef = template;
      console.log('enrol after payment');
      this.assignmentService.changeStatus(assignment.id).subscribe
            (
              data =>
              {
                console.log("status updated" + data);
                
              }
            );
    }
  }

}
