import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Route, extract } from '@app/core';
import { AssignmentComponent } from './assignment.component';
import { AssignmentStudentComponent } from '@app/assignments/assignment-student.component';
import { AssignmentScheduleComponent } from '@app/assignments/schedule-assignment.component';
import { CreateAssignmentComponent } from '@app/assignments/create-assignment.component';

const routes: Routes = [
  Route.withShell([
    { path: 'assignments', component: AssignmentComponent, data: { title: extract('Tutor Assignments') } },
    { path: 'student-assignments', component: AssignmentStudentComponent,
    data: { title: extract('Student Assignments') }},
    { path: 'schedule/:assId', component: AssignmentScheduleComponent,
    data: { title: extract('Schedule Assignment')}},
    { path: 'assignment', component: CreateAssignmentComponent,
    data: { title: extract('Create Assignment') }},
    { path: 'student-assignments/:assignmentId/:status',
      component: AssignmentStudentComponent, data: { title: extract('Student Assignments') } }
  ])
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class AssignmentRoutingModule { }
