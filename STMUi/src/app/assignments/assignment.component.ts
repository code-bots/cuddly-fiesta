import { Component, OnInit, TemplateRef } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { Router } from '@angular/router';
import { AssignmentService } from '@app/assignments/assignment.service';
import { chain, groupBy, toPairs, zip, assign, filter } from 'lodash';
import { DomSanitizer } from '@angular/platform-browser';
import { AuthenticationService } from '@app/core';
import { UserInfo } from '@app/shared/dataModels/userInfo';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DatePipe } from '@angular/common';
@Component({
  selector: 'app-assignment',
  templateUrl: './assignment.component.html',
  styleUrls: ['./assignment.component.scss']
})
export class AssignmentComponent implements OnInit {

  quote: string;
  isLoading: boolean;
  dateVisit = [new Date(), new Date()];
  userData: UserInfo;
  assignments;
  assignmentBoards;
  modalRef: BsModalRef;
  scheduleForm: FormGroup;
  assignmentToBeSchedule: any;
  startTime = new Date();
  endTime = new Date();
  dueDatedAssignemts : any;

  startDateFilter: string;
  endDateFilter: string;
  constructor(private router: Router,
    private authenticationService: AuthenticationService,
    private assignmentService: AssignmentService,
    private domSanitizer: DomSanitizer,
    private modalService: BsModalService,
    private formBuilder: FormBuilder,
    private datePipe: DatePipe
  ) {
  }

  ngOnInit() {
    this.authenticationService.getUserDetails(this.authenticationService.credentials.token)
    .subscribe(
      data => {
        this.userData = data;
        this.isLoading = true;
        this.loadAssignmentMeta();
        this.getDueDatedAssignments();
        this.createForm();
        this.updateStatusOfComplated();
      }
    );
  }

  createForm() {
    this.scheduleForm = this.formBuilder.group({
      title: ['Schedule-' + new Date().getTime(), Validators.required],
      range: ['', Validators.required]
    });
  }

  loadAssignmentMeta() {
    this.assignmentService.getAssignmentBoards(this.userData.tutor_id ? 2 : 1).subscribe(
      data => {
        this.assignmentBoards = data;
        this.loadAssignmentList();
      }, err => {
        console.log('Error in loading boards');
      }
    );
  }

  updateStatusOfComplated()
  {
    this.assignmentService.checkAndUpdatedComplated().subscribe(
      data => {
        console.log("status updated to complated");
      }
    );
  }

  loadAssignmentList() {
    this.assignmentService.getAssignmentsForTutor(this.userData.tutor_id,this.startDateFilter,this.endDateFilter).subscribe(
      data => {
        this.assignments = data;
      },
      err => {
        console.log('Loading of assignments failed');
      }
    );
  }
  
  getDueDatedAssignments()
  {
    this.assignmentService.getDueDatedAssignments().subscribe
    (
      data => {
        this.dueDatedAssignemts = data;
        console.log("due dated assignments loaded");
      }
    )
  }



  onValueChange($event: any) {
    console.log($event);
    if ($event === null) {
      return;
    }
    if ($event) {
      this.startDateFilter = this.datePipe.transform($event[0], 'yyyy-MM-dd'),
      console.log(this.startDateFilter);
      this.endDateFilter = this.datePipe.transform($event[1], 'yyyy-MM-dd'),
      console.log(this.endDateFilter);
      this.loadAssignmentList();
    }
  }
  getAssignmentForBoard(boardId: number) {
    return filter( this.assignments , function(o: any) { return o.status === boardId; });
  }

  goTo(route: string) {
    this.router.navigate(['/' + route], { replaceUrl: false });
  }

  getIdForUI(entity: any) {
    switch (entity.type) {
      case 1: return 'ASS-' + entity.assNumber;
      case 2: return 'SEM-' + entity.assNumber;
      case 3: return 'FRE-' + entity.assNumber;
      case 4: return 'CON-' + entity.assNumber;
    }
    return 'N/A';
  }

  getBatchForUI(entity: any) {
    switch (entity.type) {
      case 1: return 'Assignment';
      case 2: return 'Seminar';
      case 3: return 'Free Lecture';
      case 4: return 'Consulting';
    }
  }

  openModal(template: TemplateRef<any>, assignment: any) {
    this.assignmentToBeSchedule = assignment;
    this.loadAssignmentAndPopulateForm(assignment.id, template);
    this.modalRef = this.modalService.show(template);
  }

  loadAssignmentAndPopulateForm(assignmentId: string, template: TemplateRef<any>) {
    // TODO need getScheduleForAssignment service
    // this.assignmentService.getScheduleForAssignment(assignmentId).subscribe(data => {
    //   if (data) {
    //     this.populateForm(data, template);
    //   } else {
    //     this.modalRef = this.modalService.show(template);
    //   }
    // }, error => {
    //   console.log('Error in loading the schedule');
    // });
  }

  populateForm(schedule: any, template: TemplateRef<any>) {
    this.scheduleForm.setValue({
      title:    schedule.title,
      range: [ schedule.start_date, schedule.end_date]
   });
   this.startTime = schedule.start_date;
   this.endTime = schedule.end_date;
  }
  
  

  saveSchedule() {
    const s_date = new Date(this.scheduleForm.value.range[0]);
    const s_date_format = s_date.getFullYear() + '-'
    + (s_date.getMonth() > 8 ? (s_date.getMonth() + 1) : '0' + (s_date.getMonth() + 1)) + '-'
    + (s_date.getDate() > 9 ?  s_date.getDate() : '0' + s_date.getDate());

    const e_date = new Date(this.scheduleForm.value.range[1]);
    const e_date_format = e_date.getFullYear() + '-'
    + (e_date.getMonth() > 8 ? (e_date.getMonth() + 1) : '0' + (e_date.getMonth() + 1)) + '-'
    + (e_date.getDate() > 9 ?  e_date.getDate() : '0' + e_date.getDate());

    const start_date = s_date_format + ' ' +
      (this.startTime.getHours() > 9 ? this.startTime.getHours() : '0' + this.startTime.getHours())
      + ':' + (this.startTime.getMinutes() > 9 ? this.startTime.getMinutes() : '0' + this.startTime.getMinutes())
      + ':00';

    const end_date = e_date_format + ' ' +
      (this.endTime.getHours() > 9 ? this.endTime.getHours() : '0' + this.endTime.getHours())
      + ':' + (this.endTime.getMinutes() > 9 ? this.endTime.getMinutes() : '0' + this.endTime.getMinutes())
      + ':00';

    const schedule = {
        title: this.scheduleForm.value.title,
        start_date: start_date,
        end_date: end_date,
        asignment_type: this.assignmentToBeSchedule.type
    };
    this.assignmentService.saveSchedule(schedule, this.assignmentToBeSchedule.id).subscribe(
      data => {
        this.modalRef.hide();
        this.modalRef = null;
      },
      error => {
        console.log('Error in saving schedule for assignment' + this.assignmentToBeSchedule.title);
      }
    );
  }

}
