import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, Headers, Response } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
const credentialsKey = 'credentials';
export interface Credentials {
    // Customize received credentials here
    username: string;
    token: string;
  }



@Injectable()
export class AssignmentService {

  private _credentials: Credentials | null;

    constructor(private http: HttpClient) {
        const savedCredentials = sessionStorage.getItem(credentialsKey) || localStorage.getItem(credentialsKey);
        if (savedCredentials) {
          this._credentials = JSON.parse(savedCredentials);
        }
      }

  getHeaderForRequest() {
    const httpOptions = {
      headers: new HttpHeaders({ 'Authorization': 'bearer ' + this._credentials.token
    })};
    return httpOptions;
  }

  saveAssignment(context: any, tutor_id: string, institute_id: string) {
    // const headers =  new Headers();
    // headers.append('Authorization', 'bearer ' + this._credentials.token);
    return this.http.post('/STMWeb/rest/api/assignment/' + tutor_id
    + '/' + institute_id
      , context,this.getHeaderForRequest());
  }

  enrollAssignmentToStudent(context: any, assignment_id: string, student_id: string) {
    // const headers =  new Headers();
    // headers.append('Authorization', 'bearer ' + this._credentials.token);
    return this.http.post('/STMWeb/rest/api/assignmentslist/' + assignment_id
    + '/' + student_id
      , context, this.getHeaderForRequest());
  }


  getAllAssignments() {
    // const headers =  new Headers();
    // headers.append('Authorization', 'bearer ' + this._credentials.token);
    return this.http.get<any>('/STMWeb/rest/api/assignments', this.getHeaderForRequest());
  }

  getAssignmentsForTutor(tutor_id: string,from : string,to:string) {
    // const headers =  new Headers();
    // headers.append('Authorization', 'bearer ' + this._credentials.token);
    return this.http.get<any>('/STMWeb/rest/api/tutor/' + tutor_id
    + '/assignments?from='+(from ?  from : '') + '&to=' + (to ?  to : ''), this.getHeaderForRequest());
  }

  getAssignmentsForTutorByStatus(tutor_id: string,status : number,from : string,to:string) {
    // const headers =  new Headers();
    // headers.append('Authorization', 'bearer ' + this._credentials.token);
    return this.http.get<any>('/STMWeb/rest/api/tutor/' + tutor_id
    + '/assignments/'+ status + '?from='+(from ?  from : '') + '&to=' + (to ?  to : ''), this.getHeaderForRequest());
  }

  getAssignmentsForStudentByStatus(student_id: string, status: number,from : string,to :string) {
    // const headers =  new Headers();
    // headers.append('Authorization', 'bearer ' + this._credentials.token);
    return this.http.get<any>('/STMWeb/rest/api/student/' + student_id + '/assignmentlist/' + status +'?from=' +(from ?  from : '')+'&to='  + (to ?  to : '')
    , this.getHeaderForRequest());
  }

  updateStatus(stuASsId : string,status : number)
  {
    return this.http.put('/STMWeb/rest/api/update/status/' + stuASsId + '/' + status,{},
    this.getHeaderForRequest());
  }

  checkAndUpdatedComplated()
  {
    return this.http.put('/STMWeb/rest/api/check/complated',{},
    this.getHeaderForRequest());
  }


  getAssignmentBoards(type: number) {
    // const headers =  new Headers();
    // headers.append('Authorization', 'bearer ' + this._credentials.token);
    return this.http.get<any>('/STMWeb/rest/api/assignment/statusmeta/' + type,
    this.getHeaderForRequest());
  }

  getInstituteForTutor(tutor_id: string) {
    // const headers =  new Headers();
    // headers.append('Authorization', 'bearer ' + this._credentials.token);
    return this.http.get<any>('/STMWeb/rest/api/institute/' + tutor_id,
    this.getHeaderForRequest());
  }

  getCourses() {
    // const headers =  new Headers();
    // headers.append('Authorization', 'bearer ' + this._credentials.token);
    return this.http.get<any>('/STMWeb/rest/api/course/list',
    this.getHeaderForRequest());
  }

  getScheduleForAssignment(assignmentId: string) {
    // const headers =  new Headers();
    // headers.append('Authorization', 'bearer ' + this._credentials.token);
    return this.http.get<any>('/STMWeb/rest/api/assignmentbyid/' + assignmentId,
    this.getHeaderForRequest());
  }

  saveCourse(context: any) {
    // const headers =  new Headers();
    // headers.append('Authorization', 'bearer ' + this._credentials.token);
    return this.http.post('/STMWeb/rest/api/course'
      , context, this.getHeaderForRequest());
  }

  getSubjectForCourse(course_id: string) { 
    // const headers =  new Headers();
    // headers.append('Authorization', 'bearer ' + this._credentials.token);
    return this.http.get<any>('/STMWeb/rest/api/course/' + course_id + '/subject',
    this.getHeaderForRequest());
  }
  getStudentDetailsWhoEnrolls(tutId: string) {
    //http://localhost:8080/STMWeb/rest/api/student/assignmentlist/{tuorId}
    // const headers =  new Headers();
    // headers.append('Authorization', 'bearer ' + this._credentials.token);
    return this.http.get<any>('/STMWeb/rest/api/student/assignmentlist/' + tutId
    , this.getHeaderForRequest());
  }

  saveSubject(context: any, course_id: string) {
    // const headers =  new Headers();
    // headers.append('Authorization', 'bearer ' + this._credentials.token);
    return this.http.post('/STMWeb/rest/api/course/' + course_id + '/subject'
      , context, this.getHeaderForRequest());
  }

  saveSchedule(context: any, assignment_id: string) {
    // const headers =  new Headers();
    // headers.append('Authorization', 'bearer ' + this._credentials.token);
    return this.http.post('/STMWeb/rest/api/schedule/' + assignment_id
      , context, this.getHeaderForRequest());
  }

  payment(context: any) {
    const formData: FormData = new FormData();
    formData.set('studentId', context.studentId);
    formData.set('assignmentId', context.assignmentId);
    const headers =  new Headers();
    headers.set('Content-Type', 'application/x-www-form-urlencoded');
    return this.http.post('/STMWeb/payment1'
      , formData).map((res: Response) => res.json());
  }

  changeStatus(assignmentId : string )
  {
    //PUT : http://localhost:8080/STMWeb/rest/api/update/assignment/status/{assignmentId}
    return this.http.put('/STMWeb/rest/api/update/assignment/status/' + assignmentId
      , {}, this.getHeaderForRequest());
  }

  getDueDatedAssignments()
  {
    //GET : http://localhost:8080/STMWeb/rest/api/duedated/assignments
    return this.http.get<any[]>('/STMWeb/rest/api/duedated/assignments',
       this.getHeaderForRequest());
  }
  payment1(context: any) {
    const formData: FormData = new FormData();
    formData.set('CALLBACK_URL', context.CALLBACK_URL);
    formData.set('CHANNEL_ID', context.CHANNEL_ID);
    formData.set('CUST_ID', context.CUST_ID);
    formData.set('EMAIL', context.EMAIL);
    formData.set('INDUSTRY_TYPE_ID', context.INDUSTRY_TYPE_ID);
    formData.set('MID', context.MID);
    formData.set('MOBILE_NO', context.MOBILE_NO);
    formData.set('ORDER_ID', context.ORDER_ID);
    formData.set('TXN_AMOUNT', context.TXN_AMOUNT);
    formData.set('WEBSITE', context.WEBSITE);
    formData.set('CHECKSUMHASH', context.CHECKSUMHASH);
    const headers =  new Headers();
    headers.set('Content-Type', 'application/x-www-form-urlencoded');
    return this.http.post('https://securegw-stage.paytm.in/theia/processTransaction'
      , formData);
  }


}
