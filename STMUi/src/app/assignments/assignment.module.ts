import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { DragulaModule } from 'ng2-dragula';
import { AlertModule, BsDatepickerModule, ButtonsModule, TimepickerModule, ModalModule } from 'ngx-bootstrap';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';

import { CoreModule } from '@app/core';
import { SharedModule } from '@app/shared';
import { AssignmentRoutingModule } from '@app/assignments/assignment-routing.module';
import { AssignmentComponent } from '@app/assignments/assignment.component';
import { AssignmentStudentComponent } from '@app/assignments/assignment-student.component';
import { AssignmentScheduleComponent } from '@app/assignments/schedule-assignment.component';
import { CreateAssignmentComponent } from '@app/assignments/create-assignment.component';
import { AssignmentService } from '@app/assignments/assignment.service';
import { ToastyModule } from 'ng2-toasty';


@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    CoreModule,
    SharedModule,
    DragulaModule,
    ReactiveFormsModule,
    FormsModule,
    AssignmentRoutingModule,
    AlertModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ButtonsModule.forRoot(),
    TimepickerModule.forRoot(),
    ModalModule.forRoot(),
    ToastyModule.forRoot()
  ],
  declarations: [
    AssignmentComponent,
    AssignmentStudentComponent,
    AssignmentScheduleComponent,
    CreateAssignmentComponent
  ],
  providers: [AssignmentService,DatePipe]
})
export class AssignmentModule { }
