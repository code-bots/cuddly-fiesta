import { Component, OnInit, Input } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { keyBy, filter, clone, remove } from 'lodash';
import { AuthenticationService } from '../core/authentication/authentication.service';
import { UserInfo } from '../shared/dataModels/userInfo';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { NotificationService } from '@app/core/notification.service';
import { StudentService } from '@app/student/student.service';


@Component({
    selector: 'app-stu-dashboard',
    templateUrl: './stu-dashboard.component.html',
    styleUrls: ['./student.component.scss']
})
export class StudentDashboardComponent implements OnInit {

    userData : UserInfo;
    calendarOptions;
    events;
    studentResult : Array<any> = []
    @Input() data: string;
    @Input() rating: string;
    type : Array<String> = ['','Assignment','Seminar','Free Course', 'Consulting'];
    status : Array<String> = ['','Open','In-Progress','Complated'];
    assignmentDetails: Array<any> = [];
    seminars : Array<any> = []
    constructor(private router: Router,private authenticationService: AuthenticationService,
        private domSanitizer: DomSanitizer, private notificationService: NotificationService
        ,private studentService : StudentService) {
      }
      
    ngOnInit() {
        const savedCredentials = sessionStorage.getItem('credentials') || localStorage.getItem('credentials');
        if (savedCredentials) {
            this.authenticationService.getUserDetails(this.authenticationService.credentials.token)
            .subscribe(
              data => {
                this.userData = data;
              }
            );
          }
          console.log("studentId " + this.userData.student_id);
          this.getEnrollAssignmentsByStudent(this.userData.student_id);
          this.loadCalenderEvents();
          this.getStudentResult();
          this.getSeminarsDetails(2);
          console.log(this.type);
      }
     
    
      getEnrollAssignmentsByStudent(studentId : string)
      {
        this.studentService.getAssignmentOfStudent(this.userData.student_id).subscribe(
          response => {
            this.assignmentDetails = response;      
            response.forEach(aData =>
            {

            })
          }, error => {
            // error handling
          }
        );
        //throw new Error("Method not implemented.");
      }

      getStudentResult()
      {
          this.studentService.getResultOfStudent(this.userData.student_id).subscribe
          (
            data => 
            {
              this.studentResult = data;
              console.log("results :: " + this.studentResult);
            }
          )
      }

      loadCalenderEvents() {
        console.log("one");
        const __this = this;
          console.log("Enter in Student");
          this.studentService.getAssignmentOfStudent(this.userData.student_id).subscribe(
            data => {
              console.log("Enter in calender")
            this.events = this.getCalenderEvents(data);
            console.log("called");
            this.calendarOptions = {
              contentHeight: 400,
              selectable: true,
              fixedWeekCount : false,
              editable: false,
              eventLimit: true, 
              defaultView: 'month',
              defaultDate: new Date(),
              events: this.events
            };
            console.log("fine..");
            },
            err => console.log('error in loading events')
          );
      }
    
      getCalenderEvents(data: any): any {
                const events = [];
              
              data.forEach(d => {
                const schedule = d;
              events.push({
                  title: d.assignment_id.title,
                  start: this.getDateForEvent(schedule.assignment_id.due_date),
                // end: this.getDateForEvent(schedule.assignment_id.due_date),
                  color: '#ff5c56',
                  type: 'schedule'
                });
              });
    
   // console.log(event);
    return events;
      }
    

      saveFeedback(stuAssId : any)
      {
        const postData =
        {
            content : this.data,
            star : this.rating
        }
        console.log("data is : " + this.data);
        console.log("start is : " + this.rating);
        this.studentService.postFeeback(postData, stuAssId).subscribe(
      success => {
        this.notificationService.showNotification('Success', 'Feedback posted successfully', 'success');
      }
    );
      }
    
      getDateForEvent(date: string): string {
        const actualDate = date;
        console.log("actual date " + actualDate);
        return actualDate;
      }



      getSeminarsDetails(type : number)
      {
        this.studentService.getSeminarsDetails(type).subscribe(data=>
          {
            console.log(data);
            this.seminars = data;
          })
      }
}