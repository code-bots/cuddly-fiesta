import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Route, extract } from '@app/core';
import { StudentDashboardComponent } from '@app/student/stu-dashboard.component';


const routes: Routes = [
  Route.withShell([
    { path: 'stu-dashboard', component: StudentDashboardComponent, data: { title: extract('Dashboard') } }
  ])
];



@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class StudentRoutingModule { }
