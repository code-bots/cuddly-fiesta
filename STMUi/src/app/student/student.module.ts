import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { CoreModule } from '@app/core';
import { AlertModule, ButtonsModule, TimepickerModule, BsDatepickerModule, RatingModule, PopoverModule } from 'ngx-bootstrap';
import { AgmCoreModule } from '@agm/core';
import { CalendarModule } from 'ap-angular2-fullcalendar';
import { SharedModule } from '@app/shared';
import { StudentService } from '@app/student/student.service';
import { StudentDashboardComponent } from '@app/student/stu-dashboard.component';
import { StudentRoutingModule } from '@app/student/student.routing';



@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    CoreModule,
    SharedModule,
    FormsModule,
    CalendarModule.forRoot(),
    ReactiveFormsModule,
    StudentRoutingModule,
    PopoverModule.forRoot(),
    RatingModule.forRoot()
  ],
  declarations: [
   
    StudentDashboardComponent
  ],
  providers: [StudentService]
})
export class StudentModule { }
