import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, Headers, Response } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

const credentialsKey = 'credentials';
export interface Credentials {
    
    // Customize received credentials here
    username: string;
    token: string;
  }

@Injectable()
export class StudentService
{
  private _credentials: Credentials | null;

  constructor(private http: HttpClient) {
    const savedCredentials = sessionStorage.getItem(credentialsKey) || localStorage.getItem(credentialsKey);
    if (savedCredentials) {
      this._credentials = JSON.parse(savedCredentials);
    }
  }

    getHeaderForRequest() {
      const httpOptions = {
        headers: new HttpHeaders({ 'Authorization': 'bearer ' + this._credentials.token
      })};
      return httpOptions;
    }
   
      getAssignmentOfStudent(studentId: string)
      {       
        const headers =  new Headers();
        headers.append('Authorization', 'bearer ' + this._credentials.token);
        return this.http.get<any[]>('/STMWeb/rest/api/assignments/' + studentId
        , this. getHeaderForRequest());    
        //studentId
        //http://localhost:8080/STMWeb/rest/api/assignments/{studentId}
      }

      postFeeback(context : any,stuAssId : string)
      {    
        const headers =  new Headers();
        headers.append('Authorization', 'bearer ' + this._credentials.token);
        return this.http.post('/STMWeb/rest/api/feedback/' + stuAssId
        , context, this. getHeaderForRequest());    
        //http://localhost:8080/STMWeb/rest/api/feedback/{studentASsignmentId}
      }

      //GET : http://localhost:8080/STMWeb/rest/api/result/{student_ID}
      getResultOfStudent(studentId: string)
      {       
        const headers =  new Headers();
        headers.append('Authorization', 'bearer ' + this._credentials.token);
        return this.http.get<any[]>('/STMWeb/rest/api/result/' + studentId
        , this. getHeaderForRequest());    
      }

      getSeminarsDetails(type : number)
      {
        return this.http.get<any[]>('/STMWeb/rest/api/list/schedule?type=' + type
        , this. getHeaderForRequest());
      }


} 