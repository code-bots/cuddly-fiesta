import { Component, OnInit, ViewChild, TemplateRef, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { finalize } from 'rxjs/operators';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { environment } from '@env/environment';
import { Logger, I18nService, AuthenticationService } from '@app/core';
import { ProfileService } from '@app/profile/profile.service';
import { RegisterService } from '@app/login/register.service';
import { NotificationService } from '@app/core/notification.service';


@Component({
  selector: 'app-new-password',
  templateUrl: './new-password-component.html',
  styleUrls: ['./login.component.scss', '../animate.css']
})
export class NewPasswordComponent implements OnInit {
    passwordForm: FormGroup;
    error: string;
    @Input("userName") userName : string;
 
  constructor(private router: Router,
              private route: ActivatedRoute,
              private modalService: BsModalService,
              private formBuilder: FormBuilder,
              private i18nService: I18nService,
            private registerService : RegisterService,private notificationService: NotificationService) {
                
  }
 

  ngOnInit() { 
    console.log("username " + this.userName);
    this.createForm();
  }

  changePassword()
  {
      const postData =
      {
        userName : this.userName,
        newPassword : this.passwordForm.controls.new_pass.value
      }
      this.registerService.newPassword(postData).subscribe(data =>{
            console.log("password reset");
            this.notificationService.showNotification('Password Successfully Changes,please login with new password','','success');
            this.router.navigate(['/login'], { replaceUrl: true });
          },
          err=>{
              this.error = err;
            this.notificationService.showNotification('',err,'error');
          }
      )
  }
  
  private createForm() {
    this.passwordForm = this.formBuilder.group({
      new_pass: ['', Validators.required],
      re_pass: ['', Validators.required]
    });
  }


}
