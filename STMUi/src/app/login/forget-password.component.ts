import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { finalize } from 'rxjs/operators';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { environment } from '@env/environment';
import { Logger, I18nService, AuthenticationService } from '@app/core';
import { ProfileService } from '@app/profile/profile.service';
import { RegisterService } from '@app/login/register.service';
import { NotificationService } from '@app/core/notification.service';


@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password-component.html',
  styleUrls: ['./login.component.scss', '../animate.css']
})
export class ForgetPasswordComponent implements OnInit {

    forgetPasswordForm : FormGroup;
    @ViewChild('verifyOtp') modalContent: TemplateRef<any>;
    modalRef: BsModalRef;
    verifyForm: FormGroup;
    isShow : boolean;
    userOTP :string;
    username : string;
  constructor(private router: Router,
              private route: ActivatedRoute,
              private modalService: BsModalService,
              private formBuilder: FormBuilder,
              private i18nService: I18nService,
            private registerService : RegisterService,private notificationService: NotificationService) {
                
  }
  decline(): void {
    this.modalRef.hide();
  }

  ngOnInit() { 
    this.isShow = false;
      this.createForm();

  }

  
  private createForm() {
    this.forgetPasswordForm = this.formBuilder.group({
        username: ['', Validators.required]
    });
    
  }
  private createVerifyForm()
  {
    this.verifyForm = this.formBuilder.group({
      emailOtp: ['', Validators.required],
    });
  }
  getOTP()
  {
    console.log(this.forgetPasswordForm.controls.username.value);
    const postData =
    {
        userName : this.forgetPasswordForm.controls.username.value
    }
    
    this.registerService.forget(postData).subscribe(res =>
        {
            this.username =  this.forgetPasswordForm.controls.username.value;
            this.userOTP = res.message;
            this.createVerifyForm();
            this.modalRef = this.modalService.show(this.modalContent, {class: 'modal-sm'});
        },err => {
          const errorObj = err._body ? JSON.parse(err._body) : '';
          console.log(errorObj);
          if (errorObj && errorObj.errorCode && errorObj.errorCode === '0091') {
          this.notificationService.showNotification('Sending OTP Failed',
          ''
          + ' Sending OTP failed because' + errorObj.errorMessage, 'error');
          } 
      }
    );
  }

  confirm()
  {
    if(this.userOTP == this.verifyForm.controls.emailOtp.value)
    {
      this.modalRef.hide();
      this.isShow = true;
    }
    else{
      const msg = 'The number that you have entered doesn not  match your code. Please try again.';
      this.notificationService.showNotification('',msg,'warning');
    }

  }


}
