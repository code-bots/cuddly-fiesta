import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { finalize } from 'rxjs/operators';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { ViewChild } from '@angular/core';
import { environment } from '@env/environment';
import { Logger, I18nService, AuthenticationService } from '@app/core';
import { RegisterService } from '@app/login/register.service';
import { UserInfo } from '@app/shared/dataModels/userInfo';
import { TemplateRef } from '@angular/core';
import * as $ from 'jquery';
const log = new Logger('Login');

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss', '../animate.css']
})
export class LoginComponent implements OnInit {

  version: string = environment.version;
  error: string;
  loginForm: FormGroup;
  verifyForm: FormGroup;
  isLoading = false;
  loginTypeModel = '1';
  modalRef: BsModalRef;
  userData: UserInfo;
  @ViewChild('verifyOtp') modalContent: TemplateRef<any>;

  constructor(private router: Router,
              private modalService: BsModalService,
              private formBuilder: FormBuilder,
              private i18nService: I18nService,
              private authenticationService: AuthenticationService,
              private registerService: RegisterService) {
    this.createForm();
  }

  ngOnInit() { }

  login() {
    this.isLoading = true;
    const data = {
      username: this.loginForm.value.username,
      token: ''
    };
    console.log("1");
    this.authenticationService.login(this.loginForm.value)
      .pipe(finalize(() => {
        console.log("2");
        this.loginForm.markAsPristine();
        this.isLoading = false;
      }))
      .subscribe(credentials => {
        console.log("3");
        log.debug(`${credentials.username} successfully logged in`);
        console.log("4");
        const result = credentials as any;
        data.token = result.access_token;
        console.log("in");
        this.authenticationService.setCredentials(data, true);
        if (data.username !== 'admin@stmweb.com'){
        this.authenticationService.setAvailibility(true).subscribe();
        this.authenticationService.getUserDetails(result.access_token).subscribe(
          response => {
            this.userData = response;
            const isEnabled = this.userData.user_id.enabled;
            console.log("test " + this.userData.email);
            if (isEnabled) {
              console.log("enabled");
              this.routeAppropriate();
            } else {
              // otp
              this.modalRef = this.modalService.show(this.modalContent, {class: 'modal-sm'});
            }
            
          }, error => {

          }
        );
      } else{
        this.router.navigate(['tutorlist'], { replaceUrl: true });
      } 
    },
      error => {
        log.debug(`Login error: ${error}`);
        this.error = error;
      });
  }

  setLanguage(language: string) {
    this.i18nService.language = language;
  }

  get currentLanguage(): string {
    return this.i18nService.language;
  }

  get languages(): string[] {
    return this.i18nService.supportedLanguages;
  }

  private createForm() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
      remember: true
    });
    this.verifyForm = this.formBuilder.group({
      emailOtp: ['', Validators.required],
      smsOtp: ['', Validators.required]
    });
  }


  decline(): void {
    this.modalRef.hide();
  }
  confirm(): void {
    console.log("1 form value:: " + this.verifyForm.value);
    this.registerService.verifyOTP(this.verifyForm.value,
      this.userData.user_id.id).subscribe(
        data => {
          console.log("2");
          if (data['_body'] === 'Please try again,Entered values are incorrect')
          {
            $('#regenerate').html('<b>' + data['_body'] + '</b>');
          }
          else{
            $('#regenerate').html('');
            this.modalRef.hide();
            this.routeAppropriate();
          }
          
        },
        err => {
          console.log('Error in verification');
        }
      );
  }

  regenerateOtp()
  {
    console.log('Regenerate OTP');
    this.registerService.resendOTP(this.userData &&
      this.userData.user_id.roles[0].roleName === 'TUTOR' ? 'tutor' : 'student',
      this.userData.tutor_id ? this.userData.tutor_id : this.userData.student_id).subscribe(
      data => {
        console.log('Re-sent the OTP' + data);
      },
      err => {
        console.log('Error in re generating the OTP, Please contact your admin.');
      }
    );
  }

  forgetPassword()
  {
    console.log("called");
    this.router.navigate(['/forget_password'], { replaceUrl: true });
  }

  routeAppropriate() {
    if (this.userData && this.userData.user_id.roles[0].roleName === 'TUTOR') {
      this.router.navigate(['tut-dashboard'], { replaceUrl: true });
    } else if(this.userData && this.userData.user_id.roles[0].roleName === 'STUDENT') {
      this.router.navigate(['stu-dashboard'], { replaceUrl: true });
    }
    else{
      this.router.navigate(['tutorlist'], { replaceUrl: true });
    }
  }
  gotoRegister() {
    this.router.navigate(['/register'], { replaceUrl: true });
  }

}
