export class Student {
    registrationDetails: RegistrationDetails;
    username: string;
    password: string;
    token: string;
}

export class RegistrationDetails {
    first_name: string;
    middel_name: string;
    last_name: string;
    blood_group: string;
    food_preference: string;
    address: string;
    locality: string;
    city: string;
    state: string;
    pincode: string;
    mobile_number: number;
    email: string;
    dob: Date;
    emergency_contact: number;
    father_name : string;
    father_qualification: string;
    father_dob: Date;
    father_profession: string;
    mother_name:string;
    mother_qualification: string;
    mother_dob: Date;
    mother_profession: string;
    remind_pref: string;
    profile_pic: string;
}
