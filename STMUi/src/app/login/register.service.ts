import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Student } from './data.modal';

const credentialsKey = 'credentials';
export interface Credentials {
  // Customize received credentials here
  username: string;
  token: string;
}
@Injectable()
export class RegisterService {

  private _credentials: Credentials | null;

  constructor(private http: Http) {
      const savedCredentials = sessionStorage.getItem(credentialsKey) || localStorage.getItem(credentialsKey);
      if (savedCredentials) {
        this._credentials = JSON.parse(savedCredentials);
      }
    }

  private  getRequestHeaderWithOptions(headersInput: Headers) {
    let options: {};
    if (headersInput) {
      options = { headers: headersInput };
    } else {
      const headers = new Headers({
        'Content-Type': 'application/json',
        'Cache-control': 'no-cache',
        'Expires': '0',
        'Pragma': 'no-cache',
        'Authorization': 'bearer ' + this._credentials.token
    });
    options =  { headers: headers };
  }
  return options;
}

  register(context: Student | any, type: string) {
    return this.http.post('/STMWeb/rest/registration/' + type, context)
                .map((res: Response) => res.json());
  }

  forget(context: any) {
    return this.http.post('/STMWeb/rest/registration/forget', context)
                .map((res: Response) => res.json());
  }


  newPassword(context : any)
  {
    return this.http.put('/STMWeb/rest/registration/change/password', context)
    .map((res: Response) => res.json());
  }
   verifyOTP(context: any, userId: number) {
    const savedCredentials = sessionStorage.getItem(credentialsKey) || localStorage.getItem(credentialsKey);
    if (savedCredentials) {
      this._credentials = JSON.parse(savedCredentials);
    }
    const headers =  new Headers();
      headers.append('Authorization', 'bearer ' + this._credentials.token);
   return this.http.post('/STMWeb/rest/api/verifydetails/' + userId, context, { headers: headers });
}

resendOTP(type: string, userId: string) {
  const savedCredentials = sessionStorage.getItem(credentialsKey) || localStorage.getItem(credentialsKey);
      if (savedCredentials) {
        this._credentials = JSON.parse(savedCredentials);
     }
  const headers =  new Headers();
  headers.append('Authorization', 'bearer ' + this._credentials.token);
  return this.http.post('/STMWeb/rest/registration/resend/' + type + '/' + userId, {headers})
              .map((res: Response) => res.json());
}


///change/password

}
