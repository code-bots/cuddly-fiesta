import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { finalize } from 'rxjs/operators';

import { environment } from '@env/environment';
import { Logger, I18nService, AuthenticationService } from '@app/core';
import { Student, RegistrationDetails } from '@app/login/data.modal';
import { RegisterService } from '@app/login/register.service';
import { NotificationService } from '@app/core/notification.service';

const log = new Logger('Login');

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./login.component.scss']
})
export class RegisterComponent implements OnInit {

    version: string = environment.version;
    error: string;
    loginForm: FormGroup;
    studentRegistrationForm: FormGroup;
    tutorRegistrationForm: FormGroup;
    isLoading = false;
    registrationMode = 'student';
    student: Student;
    acceptTerm = false;
    public editEnabled = true;
    public profile_pic: string;

    constructor(private router: Router,
        private formBuilder: FormBuilder,
        private i18nService: I18nService,
        private authenticationService: AuthenticationService,
        private registerService: RegisterService,private notificationService: NotificationService) {
        this.createForm();
    }

    ngOnInit() { }

    private createForm() {
        this.studentRegistrationForm = this.formBuilder.group({
            first_name: ['', Validators.required],
            last_name: ['', Validators.required],
            blood_group: ['', Validators.required],
            food_preference: ['Vegetarian', Validators.required],
            address: '',
            locality:'',
            city:'',
            state:'',
            pincode:'',
            mobile_number: ['', Validators.required],
            email: ['', Validators.required],
            dob: ['', Validators.required],
            emergency_contact: ['', Validators.required],
            father_name:'',
            father_qualification: '',
            father_dob: '',
            father_profession: '',
            mother_name:'',
            mother_qualification: '',
            mother_dob: '',
            mother_profession: '',
            remind_pref: ['1', Validators.required],
            password: ['', Validators.required],
            re_password: ['', Validators.required]
        });
        this.tutorRegistrationForm = this.formBuilder.group({
            first_name: ['', Validators.required],
            last_name: ['', Validators.required],
            email: ['', Validators.required],
            address: '',
            locality:['', Validators.required],
            city:['', Validators.required],
            state:['', Validators.required],
            pincode:['', Validators.required],
            mobile_number: ['', Validators.required],
            dob: ['', Validators.required],
            qualification: '',
            password: ['', Validators.required],
            re_password: ['', Validators.required]
        });
    }

    private gotoRegister() {
        this.router.navigate(['/register'], { replaceUrl: true });
    }

    gotToLogin() {
        const studentToBeRegister = {
            registrationDetails: {
                first_name: this.studentRegistrationForm.value.first_name,
                last_name: this.studentRegistrationForm.value.last_name,
                blood_group: this.studentRegistrationForm.value.blood_group,
                address: this.studentRegistrationForm.value.address,
                locality:this.studentRegistrationForm.value.locality,
                city:this.studentRegistrationForm.value.city,
                state:this.studentRegistrationForm.value.state,
                pincode:this.studentRegistrationForm.value.pincode,
                mobile_number: this.studentRegistrationForm.value.mobile_number,
                email: this.studentRegistrationForm.value.email,
                dob: this.studentRegistrationForm.value.dob,
                emergency_contact: this.studentRegistrationForm.value.emergency_contact,
                father_name : this.studentRegistrationForm.value.father_name,
                father_qualification: this.studentRegistrationForm.value.father_qualification,
                father_dob: this.studentRegistrationForm.value.father_dob,
                father_profession: this.studentRegistrationForm.value.father_profession,
                mother_name:this.studentRegistrationForm.value.mother_name,
                mother_qualification: this.studentRegistrationForm.value.mother_qualification,
                mother_dob: this.studentRegistrationForm.value.mother_dob,
                mother_profession: this.studentRegistrationForm.value.mother_profession,
                remind_pref: this.studentRegistrationForm.value.remind_pref,
                food_preference: this.studentRegistrationForm.value.food_preference,
                middel_name: 'test',
                profile_pic: this.profile_pic
            },
            password: this.studentRegistrationForm.value.password
        } as Student;

        const tutorToBeRegister = {
            registrationDetails: {
                first_name: this.tutorRegistrationForm.value.first_name,
                last_name: this.tutorRegistrationForm.value.last_name,
                address: this.tutorRegistrationForm.value.address,
                locality : this.tutorRegistrationForm.value.locality,
                city : this.tutorRegistrationForm.value.city,
                state : this.tutorRegistrationForm.value.state,
                pincode : this.tutorRegistrationForm.value.pincode,
                mobile_number: this.tutorRegistrationForm.value.mobile_number,
                email: this.tutorRegistrationForm.value.email,
                dob: this.tutorRegistrationForm.value.dob,
                qualification: this.tutorRegistrationForm.value.qualification,
                profile_pic: this.profile_pic
            },
            password: this.tutorRegistrationForm.value.password
        };
        this.registerService.register(this.registrationMode === 'student' ? studentToBeRegister : tutorToBeRegister,
            this.registrationMode).subscribe(
                data => {
                    console.log(data);
                    this.router.navigate(['/login'], { replaceUrl: true });
                },err => {
                    const errorObj = err._body ? JSON.parse(err._body) : '';
                    if (errorObj && errorObj.errorCode && errorObj.errorCode === '0002') {
                    this.notificationService.showNotification('Registration failed',
                    this.registrationMode === 'student' ? 'Student ' : 'Tutor'
                    + ' registration failed to app. Reason of failure is ' + errorObj.errorMessage, 'error');
                    } else {
                        this.notificationService.showNotification('Registration failed',
                        this.registrationMode === 'student' ? 'Student ' : 'Tutor '
                        + ' registration failed to app.', 'error');
                    }
                }
            );
        // this.authenticationService.logout()
        //   .subscribe(() => this.router.navigate(['/login'], { replaceUrl: true }));
    }

    setProfilePicData($event: any) {
        //console.log("pro" + $event)
        this.profile_pic = $event;
    }

}
