import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { extract } from '@app/core';
import { LoginComponent } from './login.component';
import { RegisterComponent } from './register.component';
import { ChangePasswordComponent } from '@app/login/change-password.component';
import { ForgetPasswordComponent } from '@app/login/forget-password.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent, data: { title: extract('Login') } },
  { path: 'register', component: RegisterComponent, data: { title: extract('Register') } },
  { path: 'forget_password', component: ForgetPasswordComponent, data: { title: extract('Forget Password') } },
  { path: 'change_password/:type/:id', component: ChangePasswordComponent, data: { title: extract('Change password')}}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class LoginRoutingModule { }
