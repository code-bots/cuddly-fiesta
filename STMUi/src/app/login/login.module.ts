import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
import { RegisterComponent } from './register.component';
import { AlertModule, BsDatepickerModule, ButtonsModule, TimepickerModule } from 'ngx-bootstrap';
import { SharedModule } from '@app/shared';
import { RegisterService } from '@app/login/register.service';
import { HttpModule } from '@angular/http';
import { ChangePasswordComponent } from '@app/login/change-password.component';
import { ForgetPasswordComponent } from '@app/login/forget-password.component';
import { NewPasswordComponent } from '@app/login/new-password.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    ReactiveFormsModule,
    TranslateModule,
    NgbModule,
    LoginRoutingModule,
   
    ButtonsModule.forRoot(),
    BsDatepickerModule.forRoot(),
    AlertModule.forRoot(),
    TimepickerModule.forRoot()
  ],
  declarations: [
    LoginComponent,
    RegisterComponent,
    ChangePasswordComponent,
    ForgetPasswordComponent,
    NewPasswordComponent
  ],
  providers: [
    RegisterService
  ],
})
export class LoginModule { }
