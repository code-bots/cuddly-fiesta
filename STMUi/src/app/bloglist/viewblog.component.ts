import { Component, OnInit } from '@angular/core';
import { BlogService } from '@app/blog/blog.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BlogRoutingModule } from '@app/blog/blog.routing';
import { UserInfo } from '@app/shared/dataModels/userInfo';
import { AuthenticationService } from '@app/core/authentication/authentication.service';
import { DomSanitizer } from '@angular/platform-browser';
import { NotificationService } from '@app/core/notification.service';
import { filter, isEmpty, each, union } from 'lodash';
import * as $ from 'jquery';
import { TreeComponent } from 'angular-tree-component'; 

@Component({
  selector: 'app-viewblog',
  templateUrl: './viewblog.component.html'
})
export class ViewBlogComponent implements OnInit {
  userData: UserInfo;
  blog: any;
  commentForm: FormGroup;
  commentList: any;
  options = {
    displayField: 'description'
  };
  commentListChild: Array<any> = [];
  constructor(private blogService: BlogService,
    private authenticationService: AuthenticationService,
    private router: Router, private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private notificationService: NotificationService,
    private domSanitizer: DomSanitizer) { }



  ngOnInit() {
    this.route.params.subscribe(params => {
      this.authenticationService.getUserDetails(this.authenticationService.credentials.token)
    .subscribe(
      data => {
        this.userData = data;
        const blogId = params.id;
        this.loadBlog(blogId);
        this.loadComment(blogId);
        this.createComment();
      }
    );
    });
  
  }


  createComment() {
    this.commentForm = this.formBuilder.group(
      { 
        comment: ['', Validators.required]
      } 
    );
   }

  //auther_id: this.userData.tutor_id ? this.userData.tutor_id : this.userData.student_id,
  saveComment(blogId: string) {
    const postData = {
      description: this.commentForm.controls.comment.value,
      auther_id: this.userData.tutor_id ? this.userData.tutor_id : this.userData.student_id,
      comment_date: new Date().getTime()
    };

    this.blogService.saveBlogComment(postData, blogId).subscribe(data => {
      console.log('success');
      this.notificationService.showNotification('Comment Posted Successfully', '', 'success');
      this.loadComment(blogId);
    }, err => {
      console.log('Fail');
    });
  }


  saveReplay(cmtId: string, blogId: string) {
    const postData = {
      description: $('#' + cmtId).val(),
      auther_id:  this.userData.tutor_id ? this.userData.tutor_id : this.userData.student_id,
      comment_date: new Date().getTime(),
      parentcomment_id: cmtId
    };
    this.blogService.saveBlogComment(postData, blogId).subscribe(data => {
      this.notificationService.showNotification('Replied Successfully', '', 'success');
      this.loadComment(blogId);
    }, err => {
      console.log('Fail');
    });
  }

  loadBlog(blogId: string) {
    this.blogService.getBlogsById(blogId).subscribe(
      response => {
        this.blog = response;
      }, error => {
      }
    );
    //throw new Error("Method not implemented.");
  }

  goToBlogList() {
    this.router.navigate(['/bloglist'], { replaceUrl: true });
  }

  loadComment(blogId: string) {
    this.blogService.getComments(blogId).subscribe(response => {
      response.forEach(element => {
        if (element.parentcomment_id === null) {
          element.parentcomment_id = '0_';
        }
      });
      this.commentList = this.unflatten(response, undefined, undefined);
    }, error => {
    });
  }

  onUpdateData (treeComponent: TreeComponent, $event: any) {
    treeComponent.treeModel.expandAll();
  }

  unflatten(array: any, parent: any, tree: any) {
    tree = typeof this.commentList !== 'undefined' ? tree : [];
    parent = typeof parent !== 'undefined' ? parent : { id: '0_' };
    const children = filter( array, function(child: any) { return child.parentcomment_id === parent.id; });
    if (!isEmpty( children )) {
        if (parent.id === '0_' ) {
          tree = children;
        } else {
          parent['children'] = children;
        }
        each( children, function(child: any) {
          this.unflatten(array, child, tree);
        }.bind(this));
    }
    return tree;
} 
editCmt(cmtId: string, blogId: string) {
  const putData = {
      description: $('#' + cmtId).val(),
      comment_date: new Date().getTime()
    };
  this.blogService.editComment(putData, cmtId).subscribe(data => {
    this.notificationService.showNotification('Comment Edited Successfully', '', 'success');
    this.loadComment(blogId);
  }, err => {
    console.log('Fail');
  });
}

deleteCmt(cmtId: string, blogId: string) {
  this.blogService.deleteComment(cmtId).subscribe(data => {
    this.notificationService.showNotification('Comment deleted Successfully', '', 'success');
    this.loadComment(blogId);
  }, err => {
    console.log('Fail');
  });
}

}
