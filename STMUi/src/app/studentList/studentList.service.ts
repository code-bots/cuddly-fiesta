import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { Http, Headers, Response } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';

export interface Credentials {
  // Customize received credentials here
  username: string;
  token: string;
}

const credentialsKey = 'credentials';

@Injectable()
export class StudentListService {

  private _credentials: Credentials | null;

  constructor(private http: HttpClient) {
    const savedCredentials = sessionStorage.getItem(credentialsKey) || localStorage.getItem(credentialsKey);
    if (savedCredentials) {
      this._credentials = JSON.parse(savedCredentials);
    }
  }

  getHeaderForRequest() {
    const httpOptions = {
      headers: new HttpHeaders({ 'Authorization': 'bearer ' + this._credentials.token
    })};
    return httpOptions;
  }

  getStudentsWhoEnrollsAssignment(tutorID: string,page: number, pageSize: number, type: string, field: string) {
    // const headers =  new Headers();
    // headers.append('Authorization', 'bearer ' + this._credentials.token);
    return this.http.get<any>('/STMWeb/rest/api/studentlist/' + tutorID + '?page=' + page + '&size=' + pageSize +
    '&type=' + type + '&feild=' + field,
    this.getHeaderForRequest());

    //http://localhost:8080/STMWeb/rest/api/tutor/assignments/student/{tutor_id}
  }

  exportStudents(tutorId : string)
  {
    return this.http.get<any[]>('/STMWeb/rest/api/print/student/' + tutorId,
    this.getHeaderForRequest()); 
  }

  


}
