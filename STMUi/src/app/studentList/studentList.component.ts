import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { AuthenticationService } from '../core/authentication/authentication.service';
import { UserInfo } from '../shared/dataModels/userInfo';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { StudentListService } from '@app/studentList/studentList.service';
import { NotificationService } from '@app/core/notification.service';
import { ExcelService } from '@app/shared/excelService';
import { ChatService } from '@app/chat/chat.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { StudentService } from '@app/student/student.service';


@Component({
    selector: 'app-studentList',
    templateUrl: './studentList.component.html',
})
export class StudentListComponent implements OnInit {
  @ViewChild('verifyOtp') modalContent: TemplateRef<any>;
  @ViewChild('resultDetails') resultDetails: TemplateRef<any>;
  modalRef: BsModalRef;
  page = 1;
  pageSize = 5;
  status : Array<String> = ['','Open','In-Progress','Complated'];
  totalRecord = 0;
  searchTerm = '';
    userData : UserInfo;
    students: Array<any> = [];
  assignmentDetails: any[];
  studentResult: any[];
    constructor(private router: Router, private studentListService: StudentListService,private chatService : ChatService,
        private authenticationService: AuthenticationService,
        private studentService : StudentService, private modalService: BsModalService,private domSanitizer: DomSanitizer, private excelService : ExcelService,private notificationService: NotificationService) {
      }

    ngOnInit() {
        const savedCredentials = sessionStorage.getItem('credentials') || localStorage.getItem('credentials');
        if (savedCredentials) {
            this.authenticationService.getUserDetails(this.authenticationService.credentials.token)
            .subscribe(
              data => {
                this.userData = data;
                this.loadPatientsForDoctor();
              }
            );
          }





      }

      loadPatientsForDoctor()
      {
          this.studentListService.getStudentsWhoEnrollsAssignment(this.userData.tutor_id,this.page,this.pageSize,'asc','first_name')
          .subscribe(
          response => {
            this.students = response.listOfObjects;
            this.totalRecord = response.totalRecord;
          }, error => {
            // error handling
          }
        );
      }

      getAssignments(studentId : string)
      {
        this.studentService.getAssignmentOfStudent(studentId).subscribe(
          response => {
            this.assignmentDetails = response;      
            response.forEach(aData =>
            {

            })
          }, error => {
            // error handling
          }
        );
        this.modalRef = this.modalService.show(this.modalContent, {class: 'modal-lg'});
        console.log(studentId);
      }
      close()
      {
        this.modalRef.hide();
      }
      
      getResults(studentId : string)
      {
        this.studentService.getResultOfStudent(studentId).subscribe
          (
            data => 
            {
              this.studentResult = data;
              console.log("results :: " + this.studentResult);
            }
          )
        this.modalRef = this.modalService.show(this.resultDetails, {class: 'modal-lg'});
        console.log(studentId);
      }
      getPrint()
      {
        this.studentListService.exportStudents(this.userData.tutor_id).subscribe(data=>{
          console.log(data);
          this.excelService.exportAsExcelFile(data,"students");
        })
      }
}