import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { CoreModule } from '@app/core';
import { AlertModule, ButtonsModule, TimepickerModule, BsDatepickerModule, RatingModule, PaginationModule } from 'ngx-bootstrap';
import { AgmCoreModule } from '@agm/core';
import { SharedModule } from '@app/shared';
import { StudentListService } from '@app/studentList/studentList.service';
import { StudentListComponent } from '@app/studentList/studentList.component';
import { StudentListRoutingModule } from '@app/studentList/studentList.routing';
import { ToastyModule } from 'ng2-toasty';
import { NgPipesModule } from "ngx-pipes";


@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    NgPipesModule,
    CoreModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    StudentListRoutingModule,
    PaginationModule.forRoot(),
    ButtonsModule.forRoot(),
    RatingModule.forRoot(),
    ToastyModule.forRoot()
  ],
  declarations: [
   
    StudentListComponent
  ],
  providers: [StudentListService]
})
export class StudentListModule { }
