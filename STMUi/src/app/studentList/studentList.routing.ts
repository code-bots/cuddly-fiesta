import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Route, extract } from '@app/core';
import { StudentListComponent } from '@app/studentList/studentList.component';

const routes: Routes = [
  Route.withShell([
    { path: 'students', component: StudentListComponent, data: { title: extract('students') } }
  ])
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class StudentListRoutingModule { }
