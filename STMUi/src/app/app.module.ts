import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ServiceWorkerModule } from '@angular/service-worker';
import { TranslateModule } from '@ngx-translate/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastyModule } from 'ng2-toasty';

import { environment } from '@env/environment';
import { CoreModule } from '@app/core';
import { SharedModule } from '@app/shared';
import { HomeModule } from './home/home.module';
import { ProfileModule } from './profile/profile.module';
import { AssignmentModule } from './assignments/assignment.module';
import { ScheduleModule } from './schedule/schedule.module';
import { AboutModule } from './about/about.module';
import { LoginModule } from './login/login.module';
import { EmailModule } from './email/email.module';
import { TestResultsModule } from '@app/test-results/test-results.module';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ChatModule } from '@app/chat/chat.module';
import { ChatService } from '@app/chat/chat.service';
import { BlogComponent } from './blog/blog.component';
import { BlogModule } from '@app/blog/blog.module';
import { BloglistComponent } from './bloglist/bloglist.component';
import { TutorsModule } from '@app/tutors/tutor.module';
import { StudentListModule } from '@app/studentList/studentList.module';
import { TutorListModule } from '@app/tutorList/tutorList.module';
import { StudentModule } from '@app/student/student.module';
import { AuthInterceptor } from '@app/core/http/auth.interceptor';
import { Router } from '@angular/router';
import { NotificationService } from '@app/core/notification.service';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ServiceWorkerModule.register('/ngsw-worker.js', { enabled: environment.production }),
    FormsModule,
    HttpClientModule,
    TranslateModule.forRoot(),
    NgbModule.forRoot(),
    CoreModule,
    SharedModule,
    HomeModule,
    ChatModule,
    ProfileModule,
    AssignmentModule,
    ScheduleModule,
    AboutModule,
    LoginModule,
    EmailModule,
    TestResultsModule,
    BlogModule,
    StudentListModule,
    StudentModule,
    TutorListModule,
    TutorsModule,
    AppRoutingModule,
    ToastyModule.forRoot()
  ],
  declarations: [AppComponent],
  providers: [
    AuthInterceptor,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
      deps: [Router, NotificationService]
  }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
