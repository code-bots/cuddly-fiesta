import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable, empty } from 'rxjs';
import { throwError } from 'rxjs';
import 'rxjs/add/operator/catch';
import { of } from 'rxjs/observable/of';
import { environment } from '@env/environment';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { NotificationService } from '@app/core/notification.service';

/**
 * Prefixes all requests with `environment.serverUrl`.
 */
@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private router: Router, private notification: NotificationService) {}
  private handleAuthError(err: HttpErrorResponse): Observable<any> {
    // handle your auth error or rethrow
    if (err.status === 401 || err.status === 403) {
        // navigate /delete cookies or whatever
        console.log('err' + err.status)
        this.notification.showNotification('Warning',
        'Your authorization token expired, re login for using the app.', 'warning');
        sessionStorage.removeItem('credentials');
        localStorage.removeItem('credentials');
        this.router.navigateByUrl(`/login`);
        return empty();
    }
    console.log('err' + err.status)
    return Observable.throw(err);
}
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
   // request = request.clone({ url: environment.serverUrl + request.url });
   console.log('intercepted');
  return next.handle(request).catch(x => this.handleAuthError(x));
  }

}