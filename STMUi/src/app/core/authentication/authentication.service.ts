import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { Http, Response, Headers } from '@angular/http';
import { UserInfo } from '../../shared/dataModels/userInfo';
import { publishReplay, map, tap, refCount } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';

export interface Credentials {
  // Customize received credentials here
  username: string;
  token: string;
  
}

export interface LoginContext {
  username: string;
  password: string;
  remember?: boolean;
}

const credentialsKey = 'credentials';

/**
 * Provides a base for authentication workflow.
 * The Credentials interface as well as login/logout methods should be replaced with proper implementation.
 */
@Injectable()
export class AuthenticationService {

  static loggedInUser: string;
  static loggedInUserInfo: UserInfo;
  private _credentials: Credentials | null;
  _userDataCache: Observable<any>[] = [];

  constructor(private http: HttpClient) {
    const savedCredentials = sessionStorage.getItem(credentialsKey) || localStorage.getItem(credentialsKey);
    if (savedCredentials) {
      this._credentials = JSON.parse(savedCredentials);
    }
  }

  /**
   * Authenticates the user.
   * @param {LoginContext} context The login parameters.
   * @return {Observable<Credentials>} The user credentials.
   */
  login(context: LoginContext): Observable<Credentials> {
    AuthenticationService.loggedInUser = context.username;
    console.log("test");
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/X-www-form-urlencoded',
      'Authorization': 'Basic ' +
      btoa('testjwtclientid:XY7kmzoNzl100')
    })};
    return this.http.post<Credentials>('/STMWeb/oauth/token?grant_type=password&username='
      + context.username + '&password=' + context.password
      , null, httpOptions);
  }

  changePassword(data: any, type: string, id: string) {
    return this.http.put('/STMWeb/rest/api/' + type + '/changepassword/' + id,
      data, this.getRequestHeaderWithOptions(null));
  }

  setAvailibility(status: boolean) {
    return this.http.put('/STMWeb/rest/api/user/availability/' + status,
    {}, this.getRequestHeaderWithOptions(null));
  }

  getHeaderForRequest() {
    const headers = new Headers();
    headers.append('Authorization', 'bearer ' + this._credentials.token);
    return headers;
  }

  getUserDetails(token: string) {
    if (!this._userDataCache[token]) {
      this._userDataCache[token] = this.http
        .get('/STMWeb/rest/api/userInfo', this.getRequestHeaderWithOptions(null))
        .pipe(
       //   map((res: Response) => res.json()),
          tap(userData => console.log('User data fetched'), error => {
            if (error.status === 401) {
              console.log('UnAUthorized');
            }}),
          publishReplay(1),
          refCount(),
        );
    }
    return this._userDataCache[token];
  }


  /**
   * Logs out the user and clear credentials.
   * @return {Observable<boolean>} True if the user was logged out successfully.
   */
  logout(): Observable<boolean> {
    // Customize credentials invalidation here
    this.setCredentials();
    return of(true);
  }

  /**
   * Checks is the user is authenticated.
   * @return {boolean} True if the user is authenticated.
   */
  isAuthenticated(): boolean {
    return !!this.credentials;
  }

  /**
   * Gets the user credentials.
   * @return {Credentials} The user credentials or null if the user is not authenticated.
   */
  get credentials(): Credentials | null {
    return this._credentials;
  }

  /**
   * Sets the user credentials.
   * The credentials may be persisted across sessions by setting the `remember` parameter to true.
   * Otherwise, the credentials are only persisted for the current session.
   * @param {Credentials=} credentials The user credentials.
   * @param {boolean=} remember True to remember credentials across sessions.
   */
  public setCredentials(credentials?: Credentials, remember?: boolean) {
    this._credentials = credentials || null;

    if (credentials) {
      const storage = remember ? localStorage : sessionStorage;
      storage.setItem(credentialsKey, JSON.stringify(credentials));
    } else {
      sessionStorage.removeItem(credentialsKey);
      localStorage.removeItem(credentialsKey);
    }
  }

  getUserInfo(): UserInfo {
    return this._userDataCache[this._credentials.token];
  }

  private  getRequestHeaderWithOptions(headersInput: HttpHeaders) {
    let options: {};
    if (headersInput) {
      options = { headers: headersInput };
    } else {
      const headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Cache-control': 'no-cache',
        'Expires': '0',
        'Pragma': 'no-cache',
        'Authorization': 'bearer ' + this._credentials.token
    });
    options =  { headers: headers };
  }
  return options;
}
}
