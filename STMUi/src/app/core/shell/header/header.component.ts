import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthenticationService } from '../../authentication/authentication.service';
import { I18nService } from '../../i18n.service';
import { UserInfo } from '../../../shared/dataModels/userInfo';
import { DomSanitizer } from '@angular/platform-browser';
import { ADMIN } from '@app/shared/dataModels/constData';
import * as $ from 'jquery';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  menuHidden = true;
  userData: any;

  constructor(private router: Router,
              private authenticationService: AuthenticationService,
              private i18nService: I18nService,
              private domSanitizer: DomSanitizer) { }

  ngOnInit() {
    const savedCredentials = sessionStorage.getItem('credentials') || localStorage.getItem('credentials');
    if (savedCredentials) {
      const credentials = JSON.parse(savedCredentials);
      if (credentials.username === ADMIN) {
        console.log("login admin");
        this.userData = {
          first_name: 'Admin',
          last_name: '',
          email: ADMIN
        };
      } else {
        this.authenticationService.getUserDetails(this.authenticationService.credentials.token)
        .subscribe(
          data => {
            this.userData = data;
          });
      }
      setTimeout(function () {
        const _nav = $('.vertical-nav');
        $('.collapse-menu').click(function () {
          _nav.toggleClass('vertical-nav-sm');
          $('.dashboard-wrapper').toggleClass('dashboard-wrapper-lg');
          $('footer').toggleClass('footer-sm');
          $('i', this).toggleClass('fa-list');
          $('i', this).toggleClass('fa-th');
        });
  
        $('.toggle-menu').click(function () {
          _nav.toggleClass('vertical-nav-opened');
        });
      }, 1000);
  }
}
toggleMenu() {
  $('.vertical-nav').toggleClass('vertical-nav-opened');
}

  setLanguage(language: string) {
    this.i18nService.language = language;
  }

  logout() {
    const savedCredentials = sessionStorage.getItem('credentials') || localStorage.getItem('credentials');
    if (savedCredentials) {
      const credentials = JSON.parse(savedCredentials);
      if (credentials.username === ADMIN) {
      } else {
        this.authenticationService.setAvailibility(false).subscribe();
      }
      const removed = sessionStorage.removeItem('credentials') || localStorage.removeItem('credentials');
    }
    this.router.navigate(['/login'], { replaceUrl: false });
  }

  goTo(page: string, param: string) {
    console.log(page + '::' + param);
    if (param) {
      this.router.navigate(['/' + page, param], { replaceUrl: false });
    } else {
      this.router.navigate(['/' + page], { replaceUrl: false });
    }
  }

  goToChangePassword() {
    this.router.navigate(['/change_password', this.userData.tutor_id ? 'tutor' : 'student',
      this.userData.tutor_id ? this.userData.tutor_id : this.userData.student_id], { replaceUrl: false });
  }

  get currentLanguage(): string {
    return this.i18nService.language;
  }

  get languages(): string[] {
    return this.i18nService.supportedLanguages;
  }

  get userInfo(): UserInfo | null {
    this.userData = this.authenticationService.getUserInfo();
    return this.userData ? this.userData : null;
  }

  get isAdmin() {
    return this.userData && this.userData.email === ADMIN;
  }
}
