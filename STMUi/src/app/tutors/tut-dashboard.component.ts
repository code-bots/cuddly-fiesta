import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { AuthenticationService } from '../core/authentication/authentication.service';
import { UserInfo } from '../shared/dataModels/userInfo';
import { DomSanitizer } from '@angular/platform-browser';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { keyBy, filter, clone, remove } from 'lodash';
import { ProfileService } from '@app/profile/profile.service';
import { ScheduleService } from '@app/schedule/schedule.service';
import { NotificationService } from '@app/core/notification.service';
import { DatePipe } from '@angular/common';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { TemplateRef } from '@angular/core';
import { ViewChild } from '@angular/core';
import { TutorService } from '@app/tutors/tutor.service';
import { BlogService } from '@app/blog/blog.service';
import { AssignmentService } from '@app/assignments/assignment.service';
import { TutorListService } from '@app/tutorList/tutorList.service';
import { TestResultsService } from '@app/test-results/test-results.service';

@Component({
  selector: 'app-tut-dashboard',
  templateUrl: './tut-dashboard.component.html',
  styleUrls: ['./tutor.component.scss']
})
export class TutorDashBoardComponent implements OnInit {
  isLoading: boolean;
  userData: UserInfo;
  loading = false;
  type : Array<String> = ['','Assignment','Seminar','Free Course', 'Consulting'];
  status : Array<String> = ['','Open','In-Progress','Complated'];
  blogs = [];
  tutors =[];
  events;
  page = 1;
  pageSize = 500;
  totalRecord = 0;
  calendarOptions;
  assignmentDetails: Array<any> = [];
  enrollAssignments : Array<any> = [];
  questionSetList : Array<any> = [];
  results : any[];

  constructor(private authenticationService: AuthenticationService, private domSanitizer: DomSanitizer,
    private formBuilder: FormBuilder, private tutService: TutorService, private scheduleService: ScheduleService,
    private notificationService: NotificationService, private datePipe: DatePipe,
    private modalService: BsModalService,private blogService : BlogService,private assignmentService : AssignmentService
    ,private tutorListService :TutorListService,private examService : TestResultsService) {
  }


  ngOnInit() {

    console.log('test');
    this.authenticationService.getUserDetails(this.authenticationService.credentials.token)
      .subscribe(

        data => {
          this.userData = data;
          this.isLoading = true;
        
          this.loadBlogForTutor();
          this.loadStudentsWhoEnrolls();
          this.loadAssignmentForTutor();
          this.loadCalenderEvents();
          this.loadQuestionSet();
          this.loadTutors(1);
          this.loadResultsForTutor();
          console.log("all fine");
        }
      );
  }

  loadBlogForTutor() {
    this.blogService.getBlogsByTutor(this.userData.tutor_id).subscribe(
      data => {
        this.blogs = data;
      }
    );
  }

  loadResultsForTutor()
  {
    this.examService.getResultByTutor(this.userData.tutor_id).subscribe(data=>{
      this.results = data;
    })
  }
  loadAssignmentForTutor()
  {
    this.assignmentService.getAssignmentsForTutor(this.userData.tutor_id,'','').subscribe(
      data =>{
        this.assignmentDetails = data;
        console.log("Assignment for tutor Loaded");
      }
    );
  }

  loadQuestionSet()
  {
    this.tutService.getQuestionSetByTutor(this.userData.tutor_id).subscribe
    (
      data =>
      {
        this.questionSetList = data;
        console.log("questionSet loaded");
      }
    );
  }
  loadStudentsWhoEnrolls()
  {
    this.assignmentService.getStudentDetailsWhoEnrolls(this.userData.tutor_id).subscribe
    (
      data => {
          this.enrollAssignments = data;
          console.log("Student Data Loaded");
      }
    )
  }

  loadCalenderEvents() {
    console.log("one");
    const __this = this;
      console.log("Enter in Doctor");
      this.assignmentService.getAssignmentsForTutor(this.userData.tutor_id,'','').subscribe(
        data => {
          console.log("Enter in calender doctor")
        this.events = this.getCalenderEvents(data);
        console.log("called..");
        this.calendarOptions = {
          contentHeight: 400,
          selectable: true,
          defaultDate: new Date(),
      //    themeSystem: 'bootstrap4',
          editable: false,
          eventLimit: true, 
          defaultView: 'month',
          events: this.events
        };
        console.log("fine..");
        },
        err => console.log('error in loading events')
      );
    }
  

  getCalenderEvents(data: any): any {
    const events = [];
    if (this.userData.user_id.roles[0].roleName === 'TUTOR')
    {
      data.forEach(d => {
        const schedule = d;
      events.push({
        title: d.title,
          start: this.getDateForEvent(schedule.due_date),
        // end: this.getDateForEvent(schedule.assignment_id.due_date),
          color: '#ff5c56',
          type: 'schedule'
        });
      });
    }
    return events;
  }

  loadTutors(status : number) {
    this.tutorListService.getTutors(status,this.page, this.pageSize, 'asc', 'first_name').subscribe(
      response => {
        this.tutors = response.listOfObjects;
        this.totalRecord = response.totalRecord;
        console.log("tutor loaded");
      }, error => {
        // error handling
      }
    );
  }
  getDateForEvent(date: string): string {
    const actualDate = date;
    console.log("actual date " + actualDate);
    return actualDate;
  }

}
