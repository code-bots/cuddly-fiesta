import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Route, extract } from '@app/core';
import { TutorDashBoardComponent } from '@app/tutors/tut-dashboard.component';

const routes: Routes = [
  Route.withShell([
    { path: 'tut-dashboard', component: TutorDashBoardComponent, data: { title: extract('Dashboard') } }
  ])
];



@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class TutorRoutingModule { }
