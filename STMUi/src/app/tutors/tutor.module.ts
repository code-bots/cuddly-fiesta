import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { PaginationModule, ButtonsModule, ModalModule } from 'ngx-bootstrap';


import { CoreModule } from '@app/core';
import { SharedModule } from '@app/shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SelectModule } from 'ng2-select';
import { TutorDashBoardComponent } from '@app/tutors/tut-dashboard.component';
import { TutorService } from '@app/tutors/tutor.service';
import { TutorRoutingModule } from '@app/tutors/tutor-routing.module';
import { NgxEditorModule } from 'ngx-editor';
import { NgPipesModule } from 'ngx-pipes';
import { RatingModule } from 'ngx-bootstrap';
import { DatePipe } from '@angular/common';
import { ToastyModule } from 'ng2-toasty';
import { CalendarModule } from 'ap-angular2-fullcalendar';


@NgModule({
    imports: [
      CommonModule,
      TranslateModule,
      NgPipesModule,
      CoreModule,
      SharedModule,
      FormsModule,
      ReactiveFormsModule,
      SelectModule,
      CalendarModule.forRoot(),
      ModalModule.forRoot(),
      ButtonsModule.forRoot(),
      PaginationModule.forRoot(),
      RatingModule.forRoot(),
      ToastyModule.forRoot(),
      TutorRoutingModule
    ],
    declarations: [
        TutorDashBoardComponent
    ],
    providers: [
        TutorService,
        DatePipe
    ]
  })
  export class TutorsModule { }
