import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, Headers, Response } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

const credentialsKey = 'credentials';
export interface Credentials {
    
    // Customize received credentials here
    username: string;
    token: string;
  }

@Injectable()
export class TutorService
{
  private _credentials: Credentials | null;
  constructor(private http: HttpClient) {
    const savedCredentials = sessionStorage.getItem(credentialsKey) || localStorage.getItem(credentialsKey);
    if (savedCredentials) {
  this._credentials = JSON.parse(savedCredentials);
    }
  }

    getHeaderForRequest() {
      const httpOptions = {
        headers: new HttpHeaders({ 'Authorization': 'bearer ' + this._credentials.token
      })};
      return httpOptions;
    }

  getBlogByTutor(tutId: string) {
    // http://localhost:8080/STMWeb/rest/api/tutor/blog/{tutorId}
    return this.http.get<any[]>('/STMWeb/rest/api/tutor/blog/' + tutId
    ,this.getHeaderForRequest());
  }

  getAssignmentByTutor(tutId: string) {
    // http://localhost:8080/STMWeb/rest/api/tutor/{tutor_id}/assignments
    
 
    return this.http.get<any[]>('/STMWeb/rest/api/tutor/' + tutId +'/assignments'
    ,this.getHeaderForRequest());
  }

  
   //   GET : http://localhost:8080/STMWeb/rest/api/questionsetlist/{tutor_id}

  getQuestionSetByTutor(tutId : string)
  {
 
 
    return this.http.get<any[]>('/STMWeb/rest/api/questionsetlist/' + tutId
    ,this.getHeaderForRequest());
  }


    getStudentDetailsWhoEnrolls(tutId: string) {
    //http://localhost:8080/STMWeb/rest/api/tutor/assignments/student/{tutor_id}
    
 
    return this.http.get<any[]>('/STMWeb/rest/api/tutor/assignments/student' + tutId
    ,this.getHeaderForRequest());
  }


}