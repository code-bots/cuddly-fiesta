import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FileUploadModule } from 'ng2-file-upload';

import { LoaderComponent } from './loader/loader.component';
import { ProfilePictureComponent } from './profile-picture/profile-picture.component';
import { ExcelService } from '@app/shared/excelService';
@NgModule({
  imports: [
    CommonModule,
    FileUploadModule
  ],
  declarations: [
    LoaderComponent,
    ProfilePictureComponent
  ],
  exports: [
    LoaderComponent,
    ProfilePictureComponent
  ],
  providers:[
    ExcelService
  ]
})
export class SharedModule { }
