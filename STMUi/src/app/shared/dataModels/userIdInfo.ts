import { UserRoles } from './userRoles';
export class UserIdInfo {
    /** Common */
    accountNonExpired: boolean;
    accountNonLocked: boolean;
    authorities: string[];
    credentialsNonExpired: boolean;
    enabled: boolean;
    id: number;
    roles: UserRoles[];
    username: string;

    /** Used for tutor */
    weekdays: string;
}
