import {UserIdInfo} from './userIdInfo';

export class UserInfo {
    /** Common */
    aboutme: string;
    address: string;
    dob: string;
    email: string;
    first_name: string;
    last_name: string;
    locality: string;
    city:string;
    state:string;
    pincode:string;
    mobile_number: string;
    profile_pic: string;
    stmEmailId: string;
    user_id: UserIdInfo;

    /** Used for student */
    blood_group: string;
    emergency_contact: string;
    father_dob: string;
    father_name: string;
    father_profession: string;
    father_qualification: string;
    food_preference: string;
    mother_dob: string;
    mother_name: string;
    mother_profession: string;
    mother_qualification: string;
    remind_pref: string;
    student_id: string;

    /** Used for tutor */
    tutor_id: string;
    fees: number;
    inst_endTime: string;
    inst_startTime: string;
    institute: string;
    qualification: string;
}
