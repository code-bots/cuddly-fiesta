package org.privatechat.user.services;

import java.util.List;

import org.privatechat.user.DTOs.NotificationDTO;
import org.privatechat.user.DTOs.UserDTO;
import org.privatechat.user.exceptions.UserNotFoundException;
import org.privatechat.user.interfaces.IUserService;
import org.privatechat.user.mappers.UserMapper;
import org.privatechat.user.models.User;
import org.privatechat.user.repositories.UserRepository;
import org.privatechat.user.strategies.IUserRetrievalStrategy;
import org.privatechat.user.strategies.UserRetrievalByEmailStrategy;
import org.privatechat.user.strategies.UserRetrievalByIdStrategy;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

@Component
public class UserService implements IUserService {
  private UserRepository userRepository;

  @Autowired
  private SimpMessagingTemplate simpMessagingTemplate;
  
  @Autowired
  private BeanFactory beanFactory;

  @Autowired
  public UserService(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  private <T> User getUser(T userIdentifier, IUserRetrievalStrategy<T> strategy)
      throws UserNotFoundException {
    User user = strategy.getUser(userIdentifier);

    if (user == null) { throw new UserNotFoundException("User not found."); }

    return user;
  }

  public User getUser(long userId)
      throws BeansException, UserNotFoundException {
    return this.getUser(userId, beanFactory.getBean(UserRetrievalByIdStrategy.class));
  }

  public User getUser(String userEmail)
      throws BeansException, UserNotFoundException {
    return this.getUser(userEmail, beanFactory.getBean(UserRetrievalByEmailStrategy.class));
  }


  public boolean doesUserExist(String email) {
    User user = userRepository.findByUsername(email);

    return user != null;
  }


  public List<UserDTO> retrieveFriendsList(User user) {
    List<User> users = userRepository.findFriendsListFor(user.getUsername());

    return UserMapper.mapUsersToUserDTOs(users);
  }
  
  public UserDTO retrieveUserInfo(User user) {
    return new UserDTO(
      user.getId(),
      user.getUsername(),
      user.getUsername()
    );
  }

  // TODO: switch to a TINYINT field called "numOfConnections" to add/subtract
  // the total amount of user connections
  public void setIsPresent(User user, Boolean stat) {
    user.setIsPresent(stat);

    userRepository.save(user);
  }

  public Boolean isPresent(User user) {
    return user.getIsPresent(); 
  }

  public void notifyUser(User recipientUser, NotificationDTO notification) {
    if (this.isPresent(recipientUser)) {
      simpMessagingTemplate
        .convertAndSend("/topic/user.notification." + recipientUser.getId(), notification);
    } else {
      System.out.println("sending email notification to " + recipientUser.getUsername());
      // TODO: send email
    }
  }
}