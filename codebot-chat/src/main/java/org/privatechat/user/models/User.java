package org.privatechat.user.models;

import org.hibernate.validator.constraints.Email;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name="app_user", uniqueConstraints=@UniqueConstraint(columnNames={"username"}))
public class User {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long id;

  @NotNull(message="valid email required")
  @Email(message = "valid email required")
  private String username;

  public String getUsername() {
	return username;
}

public void setUsername(String username) {
	this.username = username;
}

@NotNull(message="valid password required")
  private String password;

//  @NotNull(message="valid name required")

  private Boolean isPresent;

  public User() {}

  public User(long id) {
    this.id = id;
  }

  public User(String email, String fullName, String password, String role) {
    this.username = email;
    this.password = password;
//    this.role = role;
  }


  public Boolean getIsPresent() {
    return this.isPresent;
  }

  public void setIsPresent(Boolean stat) {
    this.isPresent = stat;
  }


  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }
}